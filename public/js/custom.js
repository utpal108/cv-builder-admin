$('#flash-overlay-modal').modal();
$('div.alert').not('.alert-important').delay(3000).fadeOut(350);

function makeDeleteRequest(event, id) {
    event.preventDefault();
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
    }, function () {
        let form_id = $('#delete-form-'+id);
        $(form_id).submit();
    });
}