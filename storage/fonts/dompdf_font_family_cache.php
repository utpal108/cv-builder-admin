<?php return array (
  'sans-serif' => array(
    'normal' => $rootDir . '\lib\fonts\Helvetica',
    'bold' => $rootDir . '\lib\fonts\Helvetica-Bold',
    'italic' => $rootDir . '\lib\fonts\Helvetica-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $rootDir . '\lib\fonts\Times-Roman',
    'bold' => $rootDir . '\lib\fonts\Times-Bold',
    'italic' => $rootDir . '\lib\fonts\Times-Italic',
    'bold_italic' => $rootDir . '\lib\fonts\Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $rootDir . '\lib\fonts\Times-Roman',
    'bold' => $rootDir . '\lib\fonts\Times-Bold',
    'italic' => $rootDir . '\lib\fonts\Times-Italic',
    'bold_italic' => $rootDir . '\lib\fonts\Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $rootDir . '\lib\fonts\Courier',
    'bold' => $rootDir . '\lib\fonts\Courier-Bold',
    'italic' => $rootDir . '\lib\fonts\Courier-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $rootDir . '\lib\fonts\Helvetica',
    'bold' => $rootDir . '\lib\fonts\Helvetica-Bold',
    'italic' => $rootDir . '\lib\fonts\Helvetica-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $rootDir . '\lib\fonts\ZapfDingbats',
    'bold' => $rootDir . '\lib\fonts\ZapfDingbats',
    'italic' => $rootDir . '\lib\fonts\ZapfDingbats',
    'bold_italic' => $rootDir . '\lib\fonts\ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $rootDir . '\lib\fonts\Symbol',
    'bold' => $rootDir . '\lib\fonts\Symbol',
    'italic' => $rootDir . '\lib\fonts\Symbol',
    'bold_italic' => $rootDir . '\lib\fonts\Symbol',
  ),
  'serif' => array(
    'normal' => $rootDir . '\lib\fonts\Times-Roman',
    'bold' => $rootDir . '\lib\fonts\Times-Bold',
    'italic' => $rootDir . '\lib\fonts\Times-Italic',
    'bold_italic' => $rootDir . '\lib\fonts\Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $rootDir . '\lib\fonts\Courier',
    'bold' => $rootDir . '\lib\fonts\Courier-Bold',
    'italic' => $rootDir . '\lib\fonts\Courier-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $rootDir . '\lib\fonts\Courier',
    'bold' => $rootDir . '\lib\fonts\Courier-Bold',
    'italic' => $rootDir . '\lib\fonts\Courier-Oblique',
    'bold_italic' => $rootDir . '\lib\fonts\Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $rootDir . '\lib\fonts\DejaVuSans-Bold',
    'bold_italic' => $rootDir . '\lib\fonts\DejaVuSans-BoldOblique',
    'italic' => $rootDir . '\lib\fonts\DejaVuSans-Oblique',
    'normal' => $rootDir . '\lib\fonts\DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $rootDir . '\lib\fonts\DejaVuSansMono-Bold',
    'bold_italic' => $rootDir . '\lib\fonts\DejaVuSansMono-BoldOblique',
    'italic' => $rootDir . '\lib\fonts\DejaVuSansMono-Oblique',
    'normal' => $rootDir . '\lib\fonts\DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $rootDir . '\lib\fonts\DejaVuSerif-Bold',
    'bold_italic' => $rootDir . '\lib\fonts\DejaVuSerif-BoldItalic',
    'italic' => $rootDir . '\lib\fonts\DejaVuSerif-Italic',
    'normal' => $rootDir . '\lib\fonts\DejaVuSerif',
  ),
  'open sans' => array(
    'italic' => $fontDir . '\27fb03d2bf8c49880fbd15f3f96b7504',
    'normal' => $fontDir . '\a5a5970acc26f679ed9ad7632d8572df',
  ),
  'cormorant garamond' => array(
    'italic' => $fontDir . '\00082dc543ee264575b0765dbabfad41',
    'normal' => $fontDir . '\3113d0f853f13303095fdb31048541ce',
  ),
  'hind' => array(
    'normal' => $fontDir . '\261760f1d253dd0fc67e3e204fee81fa',
  ),
  'lora' => array(
    'italic' => $fontDir . '\2b0582d888f7469a714a5bc67edbe8c4',
    'normal' => $fontDir . '\fafe280ff05dab82a45641cad29c4797',
  ),
  'lusitana' => array(
    'normal' => $fontDir . '\ea2d12ce302fe2795787346a3ee9d63f',
  ),
  'merriweather' => array(
    'italic' => $fontDir . '\b8f3d56fe63d8752e225d90f709cd529',
    'normal' => $fontDir . '\6a1824cb2b26fd89f69b09165049ef2b',
  ),
  'merriweather sans' => array(
    'italic' => $fontDir . '\fc03acccb94ed042c921969d7645cce2',
    'normal' => $fontDir . '\46b19be9f27c085be15b61844bac75a4',
  ),
  'overpass' => array(
    'italic' => $fontDir . '\a7ecbac824479933d7d75fe6def76323',
    'normal' => $fontDir . '\f1e46e3ae5826381241baa61eac8d6d2',
  ),
  'quicksand' => array(
    'normal' => $fontDir . '\052ff5219b6af481a0b293012bd34a16',
  ),
  'raleway' => array(
    'italic' => $fontDir . '\bc83f8aad1e30e83d93d5aa461e403a5',
    'normal' => $fontDir . '\2ab197cf75098c7eed48e59f5cc9b2ff',
  ),
  'roboto' => array(
    'italic' => $fontDir . '\8195011e157d3a377682d918ecaef444',
    'normal' => $fontDir . '\cf99a9d56c411ce3fa8a2fc49c520e74',
  ),
  'rokkitt' => array(
    'normal' => $fontDir . '\41961e7a4ea9bcda2416df583d0a96ea',
  ),
  'ubuntu' => array(
    'italic' => $fontDir . '\15f3cc55691c1d88f3b084da1470eace',
    'normal' => $fontDir . '\406d7869351a86a99c4f79390fe5ac38',
  ),
) ?>