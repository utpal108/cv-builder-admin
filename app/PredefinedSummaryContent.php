<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PredefinedSummaryContent extends Model
{
    protected $fillable=['category','job_title', 'subcategory','summary'];
}
