<?php

function adminimage($url)
{
    if ($url && file_exists(public_path() . $url)) {
        return asset($url);
    } else {
        return asset('/admin_login/img/itclan.png');
    }
}

function getRandomNumber($length = 8)
{
    $characters = '0123456789';
    $string = '';

    for ($i = 0; $i < $length; $i++) {
        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    return $string;
}
