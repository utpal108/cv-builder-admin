<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PredefinedTeachingContent extends Model
{
    protected $fillable=['experience_title','institution','address'];
}
