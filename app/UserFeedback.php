<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFeedback extends Model
{
    protected $fillable=['document_id','session_id','full_name','rating','feedbacks'];
    protected $casts =[
        'feedbacks'=>'array'
    ];
}
