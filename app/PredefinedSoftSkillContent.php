<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PredefinedSoftSkillContent extends Model
{
    protected $fillable=['type','category', 'phrase'];
}
