<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PredefinedVolunteerContent extends Model
{
    protected $fillable=['type','category','phrase'];
}
