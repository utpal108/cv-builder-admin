<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PredefinedConferenceContent extends Model
{
    protected $fillable=['type', 'category', 'phrase'];
}
