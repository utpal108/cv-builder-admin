<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PredefinedProjectContent extends Model
{
    protected $fillable=['title','details'];
}
