<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PredefinedAwardContent extends Model
{
    protected $fillable=['title','institution'];
}
