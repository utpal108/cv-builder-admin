<?php

namespace App\Mail\Api;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMagicLink extends Mailable
{
    use Queueable, SerializesModels;
    public $user, $token;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($u, $t)
    {
        $this->user = $u;
        $this->token = $t;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('api.emails.magiclinkmail')
            ->subject($this->user->name . ' | This is your sign-in link to ' . config('app.name'));
    }
}
