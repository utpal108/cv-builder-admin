<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PredefinedAchievementContent extends Model
{
    protected $fillable=['type','category', 'phrase'];
}
