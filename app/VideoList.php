<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoList extends Model
{
    protected $fillable=['video_for','video_title', 'video_url', 'bg_image'];
}
