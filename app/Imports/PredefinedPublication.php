<?php

namespace App\Imports;

use App\PredefinedPublicationContent;
use Maatwebsite\Excel\Concerns\ToModel;

class PredefinedPublication implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!isset($row[0]) || !isset($row[1]) ) {
            return null;
        }

        return new PredefinedPublicationContent([
            'title'    => $row[0],
            'type'    => $row[1],
            'details'    => $row[2],
        ]);
    }
}
