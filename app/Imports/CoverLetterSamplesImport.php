<?php

namespace App\Imports;

use App\CoverLetterSample;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CoverLetterSamplesImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!isset($row[0]) || !isset($row[1]) ) {
            return null;
        }
        return new CoverLetterSample([
            'type'     => $row[0],
            'job_title'    => $row[1],
            'cover_title'    => $row[2],
            'cover_details'    => $row[3]
        ]);
    }

//    public function startRow(): int
//    {
//        return 2;
//    }
}
