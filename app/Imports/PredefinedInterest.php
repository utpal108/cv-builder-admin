<?php

namespace App\Imports;

use App\PredefinedInterestContent;
use Maatwebsite\Excel\Concerns\ToModel;

class PredefinedInterest implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!isset($row[0]) ) {
            return null;
        }

        return new PredefinedInterestContent([
            'title'    => $row[0],
        ]);
    }
}
