<?php

namespace App\Imports;

use App\PredefinedTechnicalSkillContent;
use Maatwebsite\Excel\Concerns\ToModel;

class PredefinedTechnicalSkill implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!isset($row[0]) || !isset($row[1]) || !isset($row[2])) {
            return null;
        }
        return new PredefinedTechnicalSkillContent([
            'type'    => $row[0],
            'category'    => $row[1],
            'phrase'    => $row[2],
        ]);
    }
}
