<?php

namespace App\Imports;

use App\PredefinedSummaryContent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithStartRow;

class PredefinedSummary implements ToModel, WithChunkReading, WithStartRow, ShouldQueue
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function model(array $row)
    {
        if (!isset($row[1])) {
            return null;
        }
        if (!isset($row[2]) && !isset($row[4])) {
            return null;
        }
        if(!isset($row[4])){
            $predefined_summary=PredefinedSummaryContent::where([['category',$row[1]],['job_title',$row[2]]])->first();
            if (!is_null($predefined_summary)){
                return null;
            }
        }
        return new PredefinedSummaryContent([
            'category'    => $row[1],
            'job_title'    => $row[2],
            'subcategory'    => $row[3],
            'summary'    => $row[4]
        ]);
    }

    public function startRow(): int
    {
        return 1;
    }

    public function batchSize(): int
    {
        return 500;
    }

    public function chunkSize(): int
    {
        return 500;
    }
}
