<?php

namespace App\Imports;

use App\PredefinedConferenceContent;
use Maatwebsite\Excel\Concerns\ToModel;

class PredefinedConference implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!isset($row[0]) || !isset($row[1]) || !isset($row[2])) {
            return null;
        }

        return new PredefinedConferenceContent([
            'type'    => $row[0],
            'category'    => $row[1],
            'phrase'    => $row[2],
        ]);
    }
}
