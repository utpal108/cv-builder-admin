<?php

namespace App\Imports;

use App\PredefinedWorkContent;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Illuminate\Contracts\Queue\ShouldQueue;

class PredefinedWork implements ToModel, WithChunkReading, WithStartRow, ShouldQueue
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!isset($row[1]) || !isset($row[3]) ) {
            return null;
        }

        return new PredefinedWorkContent([
            'category' => $row[1],
            'job_title' => $row[2],
            'responsibility' => $row[3],
        ]);
    }

    public function startRow(): int
    {
        return 1;
    }

    public function batchSize(): int
    {
        return 500;
    }

    public function chunkSize(): int
    {
        return 500;
    }
}
