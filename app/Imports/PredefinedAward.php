<?php

namespace App\Imports;

use App\PredefinedAwardContent;
use Maatwebsite\Excel\Concerns\ToModel;

class PredefinedAward implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!isset($row[0]) || !isset($row[1]) ) {
            return null;
        }

        return new PredefinedAwardContent([
            'title'    => $row[0],
            'institution'    => $row[1],
        ]);
    }
}
