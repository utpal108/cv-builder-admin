<?php

namespace App\Imports;

use App\PredefinedLanguageContent;
use Maatwebsite\Excel\Concerns\ToModel;

class PredefinedLanguage implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!isset($row[0]) ) {
            return null;
        }

        return new PredefinedLanguageContent([
            'language'    => $row[0],
        ]);
    }
}
