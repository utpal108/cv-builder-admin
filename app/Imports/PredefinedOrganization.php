<?php

namespace App\Imports;

use App\PredefinedOrganizationContent;
use Maatwebsite\Excel\Concerns\ToModel;

class PredefinedOrganization implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!isset($row[0]) || !isset($row[1]) ) {
            return null;
        }

        return new PredefinedOrganizationContent([
            'organization_name'    => $row[0],
            'organization_role'    => $row[1],
        ]);
    }
}
