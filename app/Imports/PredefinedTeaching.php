<?php

namespace App\Imports;

use App\PredefinedTeachingContent;
use Maatwebsite\Excel\Concerns\ToModel;

class PredefinedTeaching implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!isset($row[0]) || !isset($row[1]) || !isset($row[2])) {
            return null;
        }

        return new PredefinedTeachingContent([
            'experience_title'    => $row[0],
            'institution'    => $row[1],
            'address'    => $row[2],
        ]);
    }
}
