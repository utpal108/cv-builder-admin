<?php

namespace App\Imports;

use App\PredefinedCauseContent;
use Maatwebsite\Excel\Concerns\ToModel;

class PredefinedCause implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!isset($row[0])) {
            return null;
        }
        return new PredefinedCauseContent([
            'title'    => $row[0],
        ]);
    }
}
