<?php

namespace App\Imports;

use App\PredefinedProjectContent;
use Maatwebsite\Excel\Concerns\ToModel;

class PredefinedProject implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!isset($row[0]) || !isset($row[1]) ) {
            return null;
        }
        return new PredefinedProjectContent([
            'title'    => $row[0],
            'details'    => $row[1],
        ]);
    }
}
