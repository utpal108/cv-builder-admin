<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PredefinedLanguageContent extends Model
{
    protected $fillable=['language'];
}
