<?php

namespace App;

use App\Model\LanguageSets;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements MustVerifyEmail, JWTSubject
{
    use Notifiable, SoftDeletes, HasRoles;

    protected $fillable = [
        'first_name',
        'email',
        'password',
        'last_name',
        'birthday',
        'profile_image',
        'subscription_type',
        'expired_in',
        'user_type',
        'auth_type',
        'status',
        'verification_code',
        'username'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'name',
        'image'
    ];

    public function getNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function getImageAttribute()
    {
        if ($this->profile_image)
        {
            return Storage::url($this->profile_image);
        }else{
            return $this->profile_image;
        }
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function languagesets()
    {
        return $this->hasMany(LanguageSets::class, 'user_id', 'id');
    }

    public function userSubscriptions(){
        return $this->hasMany('App\UserSubscription');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\resetPasswordNotification($token));
    }

}
