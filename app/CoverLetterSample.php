<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoverLetterSample extends Model
{
    protected $fillable=['type','job_title','cover_title','cover_details'];
}
