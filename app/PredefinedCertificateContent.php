<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PredefinedCertificateContent extends Model
{
    protected $fillable=['type', 'category', 'phrase'];
}
