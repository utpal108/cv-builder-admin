<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PredefinedTechnicalSkillContent extends Model
{
    protected $fillable=['type','category', 'phrase'];
}
