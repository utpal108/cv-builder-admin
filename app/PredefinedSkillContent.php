<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PredefinedSkillContent extends Model
{
    protected $fillable=['type','category', 'phrase'];
}
