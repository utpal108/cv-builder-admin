<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserSubscriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'customer_id' => $this->customer_id,
            'subscription_id' => $this->subscription_id,
            'plan_id' => $this->plan_id,
            'subscription_start_date' => $this->subscription_start_date,
            'subscription_expire_date' => $this->subscription_expire_date,
            'subscription_status' => $this->subscription_status,
            'payment_gateway' => $this->payment_gateway,
            'plan_status' => $this->plan_status,
        ];
    }
}
