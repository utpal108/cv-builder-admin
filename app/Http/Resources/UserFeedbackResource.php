<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserFeedbackResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'document_id' => $this->document_id,
            'session_id' => $this->session_id,
            'full_name' => $this->full_name,
            'rating' => $this->rating,
            'feedbacks' => $this->feedbacks,
            'view_item' => true
        ];
    }
}
