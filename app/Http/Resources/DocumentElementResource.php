<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DocumentElementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'element_name' => $this->section_name,
            'element_shortcut' => $this->shortcut,
            'element_validations' => new DocumentElementValidationCollection($this->element_validations)
        ];
    }
}
