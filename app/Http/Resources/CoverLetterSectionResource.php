<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CoverLetterSectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'section_name' => $this->section_name,
            'section_shortcut' => $this->section_shortcut,
            'errors' => 0,
            'warnings' => 1,
            'section_elements' => new DocumentElementCollection($this->section_elements)
        ];
    }
}
