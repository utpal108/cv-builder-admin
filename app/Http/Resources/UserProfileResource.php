<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'birthday' => $this->birthday,
            'profile_image' => !is_null($this->profile_image) ? env('APP_URL') .'/storage/' .$this->profile_image : env('APP_URL') .'/images/default_profile.png',
            'is_password_set' =>!is_null($this->password) ? true : false,
            'get_special_offer' =>$this->get_special_offer,
            'default_language' =>$this->default_language,
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'subscription_expired_in' => $this->subscription_expired_in,
            'subscriptions' => new UserSubscriptionCollection($this->userSubscriptions)
        ];
    }
}
