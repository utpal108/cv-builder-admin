<?php

namespace App\Http\Resources;

use App\UserFeedback;
use Illuminate\Http\Resources\Json\JsonResource;

class ShareableLinkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'shareable_id' => $this->shareable_id,
            'hide_sensitive' => $this->hide_sensitive,
            'user_feedbacks' => new UserFeedbackCollection(UserFeedback::where('document_id',$this->document_id)->get())
        ];
    }
}
