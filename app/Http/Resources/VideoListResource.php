<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VideoListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'video_title' => $this->video_title,
            'video_url' => env('APP_URL') .'/storage/' .$this->video_url,
            'bg_image' => env('APP_URL') .'/storage/' .$this->bg_image,
        ];
    }
}
