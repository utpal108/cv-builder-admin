<?php

namespace App\Http\Resources;

use App\Model\Documents;

use Illuminate\Http\Resources\Json\JsonResource;

class LanguageSetsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'language_name' => $this->language_name,
            'total_cover' => Documents::where([['user_id',$this->user_id],['language_id',$this->id],['document_type','cover']])->get()->count(),
            'total_resume' => Documents::where([['user_id',$this->user_id],['language_id',$this->id],['document_type','resume']])->get()->count(),
        ];
    }
}
