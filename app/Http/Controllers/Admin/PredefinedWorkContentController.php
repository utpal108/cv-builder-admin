<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PredefinedWorkDataTable;
use App\Imports\PredefinedWork;
use App\PredefinedWorkContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PredefinedWorkContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PredefinedWorkDataTable $dataTable)
    {
        return $dataTable->render('admin.predefined_contents.work.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.predefined_contents.work.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ini_set('max_execution_time', 600);
        //        ini_set('memory_limit', '-1');
        $validator = Validator::make($request->all(), [
            'predefined_works' => 'required|file',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        PredefinedWorkContent::truncate();
//        Excel::import(new PredefinedWork)->queue(request()->file('predefined_works'));
        Excel::import(new PredefinedWork, request()->file('predefined_works'));
        flash('Predefined Works Added Successfully')->success();
        return redirect()->action('Admin\PredefinedWorkContentController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predefined_work=PredefinedWorkContent::findOrFail($id);
        return view('admin.predefined_contents.work.edit',compact('predefined_work'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'category' => 'required|string',
            'job_title' => 'required|string',
            'responsibility' => 'required|string',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $predefined_work=PredefinedWorkContent::findOrFail($id);
        $predefined_work->category=$request->category;
        $predefined_work->job_title=$request->job_title;
        $predefined_work->responsibility=$request->responsibility;
        $predefined_work->save();
        flash('Predefined Work updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PredefinedWorkContent::destroy($id);
        flash('Predefined Work deleted successfully');
        return redirect()->action('Admin\PredefinedWorkContentController@index');
    }
}
