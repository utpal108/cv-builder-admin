<?php

namespace App\Http\Controllers\Admin;

use App\CoverOptimizer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CoverOptimizerController extends Controller
{
    public function index(){
        $cover_optimizers=CoverOptimizer::all();
        return view('admin.cover_optimizer.index',compact('cover_optimizers'));
    }

    public function create(){
        abort(404);
    }

    public function edit($id){
        $cover_optimizer = CoverOptimizer::findOrFail($id);
        return view('admin.cover_optimizer.edit',compact('cover_optimizer'));
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|min:3|max:191',
            'details' => 'required|string',
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $cover_optimizer = CoverOptimizer::findOrFail($id);
        $cover_optimizer->title=$request->title;
        $cover_optimizer->details=$request->details;
        $cover_optimizer->save();
        flash('Cover Optimizer Updated successfully');
        return redirect()->action('Admin\CoverOptimizerController@index');
    }
}
