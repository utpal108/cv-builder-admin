<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PredefinedEducationDataTable;
use App\Imports\PredefinedEducation;
use App\PredefinedEducationContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PredefinedEducationContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PredefinedEducationDataTable $dataTable)
    {
        return $dataTable->render('admin.predefined_contents.education.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.predefined_contents.education.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'predefined_education' => 'required|file',
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        PredefinedEducationContent::truncate();
        Excel::import(new PredefinedEducation, request()->file('predefined_education'));
        flash('Predefined Study Program Added Successfully')->success();
        return redirect()->action('Admin\PredefinedEducationContentController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predefined_education=PredefinedEducationContent::findOrFail($id);
        return view('admin.predefined_contents.education.edit',compact('predefined_education'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string',
            'category' => 'required|string',
            'phrase' => 'required|string',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $predefined_education=PredefinedEducationContent::findOrFail($id);
        $predefined_education->type=$request->type;
        $predefined_education->category=$request->category;
        $predefined_education->phrase=$request->phrase;
        $predefined_education->save();
        flash('Predefined Study Program updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PredefinedEducationContent::destroy($id);
        flash('Predefined Study Program deleted successfully');
        return redirect()->action('Admin\PredefinedEducationContentController@index');
    }
}
