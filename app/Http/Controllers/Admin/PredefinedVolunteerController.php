<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PredefinedVolunteerDataTable;
use App\Imports\PredefinedVolunteer;
use App\PredefinedVolunteerContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PredefinedVolunteerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PredefinedVolunteerDataTable $dataTable)
    {
        return $dataTable->render('admin.predefined_contents.volunteer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.predefined_contents.volunteer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'predefined_volunteers' => 'required|file',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        PredefinedVolunteerContent::truncate();
        Excel::import(new PredefinedVolunteer, request()->file('predefined_volunteers'));
        flash('Predefined Volunteers Added Successfully')->success();
        return redirect()->action('Admin\PredefinedVolunteerController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predefined_volunteer=PredefinedVolunteerContent::findOrFail($id);
        return view('admin.predefined_contents.volunteer.edit',compact('predefined_volunteer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string',
            'category' => 'required|string',
            'phrase' => 'required|string',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $predefined_volunteer=PredefinedVolunteerContent::findOrFail($id);
        $predefined_volunteer->type=$request->type;
        $predefined_volunteer->category=$request->category;
        $predefined_volunteer->phrase=$request->phrase;
        $predefined_volunteer->save();
        flash('Predefined Volunteer updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PredefinedVolunteerContent::destroy($id);
        flash('Predefined Volunteer deleted successfully');
        return redirect()->action('Admin\PredefinedVolunteerController@index');
    }
}
