<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PredefinedPublicationDataTable;
use App\Imports\PredefinedPublication;
use App\PredefinedPublicationContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PredefinedPublicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PredefinedPublicationDataTable $dataTable)
    {
        return $dataTable->render('admin.predefined_contents.publication.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.predefined_contents.publication.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'predefined_publications' => 'required|file',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        PredefinedPublicationContent::truncate();
        Excel::import(new PredefinedPublication, request()->file('predefined_publications'));
        flash('Predefined Publications Added Successfully')->success();
        return redirect()->action('Admin\PredefinedPublicationController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predefined_publication=PredefinedPublicationContent::findOrFail($id);
        return view('admin.predefined_contents.publication.edit',compact('predefined_publication'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'type' => 'required|string',
            'details' => 'required|string',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $predefined_publication=PredefinedPublicationContent::findOrFail($id);
        $predefined_publication->title=$request->title;
        $predefined_publication->type=$request->type;
        $predefined_publication->details=$request->details;
        $predefined_publication->save();
        flash('Predefined Publication updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PredefinedPublicationContent::destroy($id);
        flash('Predefined Publication deleted successfully');
        return redirect()->action('Admin\PredefinedPublicationController@index');
    }
}
