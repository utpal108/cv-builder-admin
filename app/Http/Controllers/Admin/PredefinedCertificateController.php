<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PredefinedCertificateDataTable;
use App\Imports\PredefinedCertificate;
use App\PredefinedCertificateContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PredefinedCertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PredefinedCertificateDataTable $dataTable)
    {
        return $dataTable->render('admin.predefined_contents.certificate.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.predefined_contents.certificate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'predefined_certificates' => 'required|file',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        PredefinedCertificateContent::truncate();
        Excel::import(new PredefinedCertificate, request()->file('predefined_certificates'));
        flash('Predefined Certificates Added Successfully')->success();
        return redirect()->action('Admin\PredefinedCertificateController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predefined_certificate=PredefinedCertificateContent::findOrFail($id);
        return view('admin.predefined_contents.certificate.edit',compact('predefined_certificate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string',
            'category' => 'required|string',
            'phrase' => 'required|string',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $predefined_certificate=PredefinedCertificateContent::findOrFail($id);
        $predefined_certificate->type=$request->type;
        $predefined_certificate->category=$request->category;
        $predefined_certificate->phrase=$request->phrase;
        $predefined_certificate->save();
        flash('Predefined Certificate updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PredefinedCertificateContent::destroy($id);
        flash('Predefined Certificate deleted successfully');
        return redirect()->action('Admin\PredefinedCertificateController@index');
    }
}
