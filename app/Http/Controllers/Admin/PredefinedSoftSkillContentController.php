<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PredefinedSoftSkillDataTable;
use App\Imports\PredefinedSoftSkillImport;
use App\PredefinedSoftSkillContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PredefinedSoftSkillContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PredefinedSoftSkillDataTable $dataTable)
    {
        return $dataTable->render('admin.predefined_contents.soft_skill.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.predefined_contents.soft_skill.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'predefined_soft_skills' => 'required|file',
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        PredefinedSoftSkillContent::truncate();
        Excel::import(new PredefinedSoftSkillImport, request()->file('predefined_soft_skills'));
        flash('Predefined Soft Skills Added Successfully')->success();
        return redirect()->action('Admin\PredefinedSoftSkillContentController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predefined_soft_skill=PredefinedSoftSkillContent::findOrFail($id);
        return view('admin.predefined_contents.soft_skill.edit',compact('predefined_soft_skill'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'category' => 'required|string',
            'phrase' => 'required|string',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $predefined_soft_skill=PredefinedSoftSkillContent::findOrFail($id);
        $predefined_soft_skill->category=$request->category;
        $predefined_soft_skill->phrase=$request->phrase;
        $predefined_soft_skill->save();
        flash('Predefined Soft Skill updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PredefinedSoftSkillContent::destroy($id);
        flash('Predefined Soft Skill deleted successfully');
        return redirect()->action('Admin\PredefinedSoftSkillContentController@index');
    }
}
