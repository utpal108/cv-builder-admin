<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PredefinedSkillsDataTable;
use App\Imports\PredefinedSkillImport;
use App\PredefinedSkillContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PredefinedSkillContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PredefinedSkillsDataTable $dataTable)
    {
        return $dataTable->render('admin.predefined_contents.skill.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.predefined_contents.skill.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'predefined_skills' => 'required|file',
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        PredefinedSkillContent::truncate();
        Excel::import(new PredefinedSkillImport, request()->file('predefined_skills'));
        flash('Predefined Skills Added Successfully')->success();
        return redirect()->action('Admin\PredefinedSkillContentController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predefined_skill=PredefinedSkillContent::findOrFail($id);
        return view('admin.predefined_contents.skill.edit',compact('predefined_skill'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'category' => 'required|string',
            'phrase' => 'required|string',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $predefined_skill=PredefinedSkillContent::findOrFail($id);
        $predefined_skill->category=$request->category;
        $predefined_skill->phrase=$request->phrase;
        $predefined_skill->save();
        flash('Predefined Skill updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PredefinedSkillContent::destroy($id);
        flash('Predefined Skill deleted successfully');
        return redirect()->action('Admin\PredefinedSkillContentController@index');
    }
}
