<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PredefinedAchievementDataTable;
use App\Imports\PredefinedAchievement;
use App\PredefinedAchievementContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PredefinedAchievementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PredefinedAchievementDataTable $dataTable)
    {
        return $dataTable->render('admin.predefined_contents.achievement.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.predefined_contents.achievement.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'predefined_achievements' => 'required|file',
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        PredefinedAchievementContent::truncate();
        Excel::import(new PredefinedAchievement, request()->file('predefined_achievements'));
        flash('Predefined Achievements Added Successfully')->success();
        return redirect()->action('Admin\PredefinedAchievementController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predefined_achievement=PredefinedAchievementContent::findOrFail($id);
        return view('admin.predefined_contents.achievement.edit',compact('predefined_achievement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string',
            'category' => 'required|string',
            'phrase' => 'required|string',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $predefined_achievement=PredefinedAchievementContent::findOrFail($id);
        $predefined_achievement->type=$request->type;
        $predefined_achievement->category=$request->category;
        $predefined_achievement->phrase=$request->phrase;
        $predefined_achievement->save();
        flash('Predefined Achievement updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PredefinedAchievementContent::destroy($id);
        flash('Predefined Achievement deleted successfully');
        return redirect()->action('Admin\PredefinedAchievementController@index');
    }
}
