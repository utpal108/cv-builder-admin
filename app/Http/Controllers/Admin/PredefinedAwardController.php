<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PredefinedAwardDataTable;
use App\Imports\PredefinedAchievement;
use App\Imports\PredefinedAward;
use App\PredefinedAchievementContent;
use App\PredefinedAwardContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PredefinedAwardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PredefinedAwardDataTable $dataTable)
    {
        return $dataTable->render('admin.predefined_contents.award.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.predefined_contents.award.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'predefined_awards' => 'required|file',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        PredefinedAwardContent::truncate();
        Excel::import(new PredefinedAward, request()->file('predefined_awards'));
        flash('Predefined Award Added Successfully')->success();
        return redirect()->action('Admin\PredefinedAwardController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predefined_award=PredefinedAwardContent::findOrFail($id);
        return view('admin.predefined_contents.award.edit',compact('predefined_award'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'institution' => 'required|string',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $predefined_award=PredefinedAwardContent::findOrFail($id);
        $predefined_award->title=$request->title;
        $predefined_award->institution=$request->institution;
        $predefined_award->save();
        flash('Predefined Award updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PredefinedAwardContent::destroy($id);
        flash('Predefined Award deleted successfully');
        return redirect()->action('Admin\PredefinedAwardController@index');
    }
}
