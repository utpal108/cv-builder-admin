<?php

namespace App\Http\Controllers\Admin\Administrator;

use App\DataTables\Administrator\PermissionDatatable;
use App\Model\EmailGroup;
use App\Model\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PermissionDatatable $datatable)
    {
        return $datatable->render('admin.administrator.permission.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::where('parent_id', '')->pluck('name', 'id');
        return view('admin.administrator.permission.add', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:191'
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        Permission::create([
            'name' => $request->name,
            'parent_id' => $request->parent_id ?? '',
        ]);

        flash('Record Successfully Added')->success();
        return redirect()
            ->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = Permission::with('parent')->find($id);
        if ($permission) {
            return view('admin.administrator.permission.show', compact('permission'));
        } else {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permissions = Permission::where('parent_id', '')->pluck('name', 'id');
        $permission = Permission::findById($id);
        return view('admin.administrator.permission.edit', compact('permissions', 'permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:191'
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        Permission::where('id', $id)->update([
            'name' => $request->name,
            'parent_id' => $request->parent_id ?? '',
        ]);

        flash('Record Successfully Updated')->success();
        return redirect()
            ->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Permission::destroy($id);
        flash('Record Successfully deleted')->warning();
        return redirect()
            ->back();
    }
}
