<?php

namespace App\Http\Controllers\Admin\Administrator;

use App\Model\LanguageSets;
use App\Model\Roles;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\DataTables\Administrator\UserDataTable;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UserDataTable $dataTable)
    {
        return $dataTable->render('admin.administrator.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Roles::orderBy('name', 'asc')->pluck('name', 'id');
        return view('admin.administrator.user.add', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|min:3|max:191',
            'last_name' => 'required|string|min:3|max:191',
            'username' => 'required|string|min:3|max:191|unique:users',
            'email' => 'required|email|string|min:3|max:191|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'profile_image' => 'required',
            'user_type' => 'required',
            'role' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $allData = $request->all();
        $allData['password'] = bcrypt($request->password);
        if ($request->hasFile('profile_image')) {
            $path = $request->profile_image->store('images', 'public');
            $allData['profile_image'] = '/storage/' . $path;
        }

        $user = User::create($allData);
        LanguageSets::create(['language_name'=>'Default', 'user_id'=>$user->id]);
        $user->syncRoles($request->role);
        event(new Registered($user));


        flash('Record Successfully Added')->success();
        return redirect()
            ->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with('roles')
            ->find($id);
        return view('admin.administrator.user.show', compact('roles', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Roles::orderBy('name', 'asc')->pluck('name', 'id');
        $user = User::with('roles')
            ->find($id);
        return view('admin.administrator.user.edit', compact('roles', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|min:3|max:191',
            'last_name' => 'required|string|min:3|max:191',
            'username' => [
                'required',
                'string',
                Rule::unique('users')->ignore($id)
            ],
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($id)
            ],
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back()->withInput();
        }

        $allData = $request->except(['_token', '_method', 'password', 'password_confirmation', 'role']);
        if ($request->password) {
            $validator = Validator::make($request->all(), [
                'password' => 'required|string|min:8|confirmed',
            ]);

            if ($validator->fails()) {
                flash($validator->errors()->first())->error();
                return redirect()
                    ->back()->withInput();
            }
            $allData['password'] = bcrypt($request->password);
        }

        if ($request->hasFile('profile_image')) {
            $path = $request->profile_image->store('images', 'public');
            $allData['profile_image'] = '/storage/' . $path;
        }

        User::where('id', $id)->update($allData);
        $user = User::find($id);
        if (auth()->user()->user_type == 'admin') {
            $user->syncRoles($request->role);
        }

        flash('Record Successfully Updated')->success();
        return redirect()
            ->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        Storage::disk('public')->delete(str_replace('/storage/', '', $user->profile_image));
        User::destroy($id);
        flash('Record Successfully Updated')->success();
        return redirect()
            ->back();
    }
}
