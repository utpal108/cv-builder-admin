<?php

namespace App\Http\Controllers\Admin\Administrator;

use App\DataTables\Administrator\RolesDataTable;
use App\Model\Permission;
use App\Model\Roles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(RolesDataTable $dataTable)
    {
        return $dataTable->render('admin.administrator.role.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::where('parent_id', '')
            ->with('children')
            ->get();

        return view('admin.administrator.role.add', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:191',
            'permissions' => 'required',
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        $role = Roles::create([
            'name' => $request->name,
        ]);
        $role->syncPermissions($request->permissions);
        flash('Record Successfully Added')->success();
        return redirect()
            ->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Roles::with('permissions')->find($id);
        return view('admin.administrator.role.show', compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Roles::with('permissions')->find($id);
        $permissions = Permission::where('parent_id', '')
            ->with('children')
            ->get();
        return view('admin.administrator.role.edit', compact('role', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:191',
            'permissions' => 'required',
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        $role = Roles::findById($id);
        $role->name = $request->name;
        $role->save();

        $role->syncPermissions($request->permissions);

        flash('Record Successfully Updated')->success();
        return redirect()
            ->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Role::destroy($id);
        flash('Record Successfully Edited')->success();
        return redirect()
            ->back();
    }
}
