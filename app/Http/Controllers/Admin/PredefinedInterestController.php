<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PredefinedInterestDataTable;
use App\Imports\PredefinedInterest;
use App\PredefinedInterestContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PredefinedInterestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PredefinedInterestDataTable $dataTable)
    {
        return $dataTable->render('admin.predefined_contents.interest.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.predefined_contents.interest.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'predefined_interests' => 'required|file',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        PredefinedInterestContent::truncate();
        Excel::import(new PredefinedInterest, request()->file('predefined_interests'));
        flash('Predefined Interests Added Successfully')->success();
        return redirect()->action('Admin\PredefinedInterestController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predefined_interest=PredefinedInterestContent::findOrFail($id);
        return view('admin.predefined_contents.interest.edit',compact('predefined_interest'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $predefined_interest=PredefinedInterestContent::findOrFail($id);
        $predefined_interest->title=$request->title;
        $predefined_interest->save();
        flash('Predefined Interest updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PredefinedInterestContent::destroy($id);
        flash('Predefined Interest deleted successfully');
        return redirect()->action('Admin\PredefinedInterestController@index');
    }
}
