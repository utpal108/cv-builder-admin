<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PredefinedTechnicalSkillDataTable;
use App\Imports\PredefinedTechnicalSkill;
use App\PredefinedTechnicalSkillContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PredefinedTechnicalSkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PredefinedTechnicalSkillDataTable $dataTable)
    {
        return $dataTable->render('admin.predefined_contents.technical_skill.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.predefined_contents.technical_skill.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'predefined_technical_skills' => 'required|file',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        PredefinedTechnicalSkillContent::truncate();
        Excel::import(new PredefinedTechnicalSkill, request()->file('predefined_technical_skills'));
        flash('Predefined Technical Skill Added Successfully')->success();
        return redirect()->action('Admin\PredefinedTechnicalSkillController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predefined_technical_skill=PredefinedTechnicalSkillContent::findOrFail($id);
        return view('admin.predefined_contents.technical_skill.edit',compact('predefined_technical_skill'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'category' => 'required|string',
            'phrase' => 'required|string',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $predefined_technical_skill=PredefinedTechnicalSkillContent::findOrFail($id);
        $predefined_technical_skill->category=$request->category;
        $predefined_technical_skill->phrase=$request->phrase;
        $predefined_technical_skill->save();
        flash('Predefined Technical Skill updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PredefinedTechnicalSkillContent::destroy($id);
        flash('Predefined Technical Skill deleted successfully');
        return redirect()->action('Admin\PredefinedTechnicalSkillController@index');
    }
}
