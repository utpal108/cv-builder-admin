<?php

namespace App\Http\Controllers\Admin;

use App\Tip;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TipsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tips=Tip::all();
        return view('admin.tips.index',compact('tips'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tips.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Tip::create($request->all());
        flash('Tips created successfully');
        return redirect()->action('Admin\TipsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tip=Tip::findOrFail($id);
        return view('admin.tips.edit',compact('tip'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tip=Tip::findOrFail($id);
        $tip->tips_for = $request->tips_for;
        $tip->tips_content = $request->tips_content;
        $tip->save();
        flash('Tips updated successfully');
        return  redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tip::destroy($id);
        flash('Tips deleted successfully');
        return redirect()->action('Admin\TipsController@index');
    }
}
