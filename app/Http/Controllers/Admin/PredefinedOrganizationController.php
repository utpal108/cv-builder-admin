<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PredefinedOrganizationDataTable;
use App\Imports\PredefinedOrganization;
use App\PredefinedOrganizationContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PredefinedOrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PredefinedOrganizationDataTable $dataTable)
    {
        return $dataTable->render('admin.predefined_contents.organization.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.predefined_contents.organization.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'predefined_organizations' => 'required|file',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        PredefinedOrganizationContent::truncate();
        Excel::import(new PredefinedOrganization, request()->file('predefined_organizations'));
        flash('Predefined Organizations Added Successfully')->success();
        return redirect()->action('Admin\PredefinedOrganizationController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predefined_organization=PredefinedOrganizationContent::findOrFail($id);
        return view('admin.predefined_contents.organization.edit',compact('predefined_organization'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'organization_name' => 'required|string',
            'organization_role' => 'required|string',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $predefined_organization=PredefinedOrganizationContent::findOrFail($id);
        $predefined_organization->organization_name=$request->organization_name;
        $predefined_organization->organization_role=$request->organization_role;
        $predefined_organization->save();
        flash('Predefined Organization updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PredefinedOrganizationContent::destroy($id);
        flash('Predefined Organization deleted successfully');
        return redirect()->action('Admin\PredefinedOrganizationController@index');
    }
}
