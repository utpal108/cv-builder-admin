<?php

namespace App\Http\Controllers\Admin;

use App\CoverLetterSample;
use App\DataTables\CoverSamplesDataTable;
use App\Imports\CoverLetterSamplesImport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class CoverSampleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CoverSamplesDataTable $dataTable)
    {
        return $dataTable->render('admin.cover_samples.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cover_samples.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cover_samples' => 'required|file',
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        CoverLetterSample::truncate();
        Excel::import(new CoverLetterSamplesImport, request()->file('cover_samples'));
        flash('Cover Samples Added Successfully')->success();
        return redirect()->action('Admin\CoverSampleController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cover_sample=CoverLetterSample::findOrFail($id);
        return view('admin.cover_samples.edit',compact('cover_sample'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string',
            'job_title' => 'required|string',
            'cover_title' => 'required|string',
            'cover_details' => 'required|string'
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $cover_sample=CoverLetterSample::findOrFail($id);
        $cover_sample->type=$request->type;
        $cover_sample->job_title=$request->job_title;
        $cover_sample->cover_title=$request->cover_title;
        $cover_sample->cover_details=$request->cover_details;
        $cover_sample->save();
        flash('Cover sample updated successfully');
        return redirect()->action('Admin\CoverSampleController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CoverLetterSample::destroy($id);
        flash('Cover sample deleted successfully');
        return redirect()->action('Admin\CoverSampleController@index');
    }
}
