<?php

namespace App\Http\Controllers\Admin;

use App\Model\Documents;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DocumentsController extends Controller
{
    public function downloadDocument($cover_letter_id){
        $pdf = PDF::loadView('pdf.test.create')->setPaper('a4');
        $pdf->setOption('header-html', view('pdf.test.header'));
        $pdf->setOption('margin-top', 25);
        return $pdf->download('test_pdf.pdf');

    }
}
