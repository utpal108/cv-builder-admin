<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PredefinedConferenceDataTable;
use App\Imports\PredefinedConference;
use App\PredefinedConferenceContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PredefinedConferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PredefinedConferenceDataTable $dataTable)
    {
        return $dataTable->render('admin.predefined_contents.conference.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.predefined_contents.conference.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'predefined_conferences' => 'required|file',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        PredefinedConferenceContent::truncate();
        Excel::import(new PredefinedConference, request()->file('predefined_conferences'));
        flash('Predefined Conferences Added Successfully')->success();
        return redirect()->action('Admin\PredefinedConferenceController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predefined_conference=PredefinedConferenceContent::findOrFail($id);
        return view('admin.predefined_contents.conference.edit',compact('predefined_conference'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|string',
            'category' => 'required|string',
            'phrase' => 'required|string',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $predefined_conference=PredefinedConferenceContent::findOrFail($id);
        $predefined_conference->type=$request->type;
        $predefined_conference->category=$request->category;
        $predefined_conference->phrase=$request->phrase;
        $predefined_conference->save();
        flash('Predefined Conference updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PredefinedConferenceContent::destroy($id);
        flash('Predefined Conference deleted successfully');
        return redirect()->action('Admin\PredefinedConferenceController@index');
    }
}
