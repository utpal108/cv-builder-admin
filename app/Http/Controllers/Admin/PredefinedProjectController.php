<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PredefinedProjectDataTable;
use App\Imports\PredefinedProject;
use App\PredefinedProjectContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PredefinedProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PredefinedProjectDataTable $dataTable)
    {
        return $dataTable->render('admin.predefined_contents.project.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.predefined_contents.project.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'predefined_projects' => 'required|file',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        PredefinedProjectContent::truncate();
        Excel::import(new PredefinedProject, request()->file('predefined_projects'));
        flash('Predefined Projects Added Successfully')->success();
        return redirect()->action('Admin\PredefinedProjectController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predefined_project=PredefinedProjectContent::findOrFail($id);
        return view('admin.predefined_contents.project.edit',compact('predefined_project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'details' => 'required|string',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $predefined_project=PredefinedProjectContent::findOrFail($id);
        $predefined_project->title=$request->title;
        $predefined_project->details=$request->details;
        $predefined_project->save();
        flash('Predefined Project updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PredefinedProjectContent::destroy($id);
        flash('Predefined Project deleted successfully');
        return redirect()->action('Admin\PredefinedProjectController@index');
    }
}
