<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PredefinedSummaryDataTable;
use App\Imports\PredefinedSummary;
use App\PredefinedSummaryContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PredefinedSummaryContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PredefinedSummaryDataTable $dataTable)
    {
        return $dataTable->render('admin.predefined_contents.summary.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.predefined_contents.summary.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'predefined_summaries' => 'required|file',
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        PredefinedSummaryContent::truncate();
        Excel::import(new PredefinedSummary, request()->file('predefined_summaries'));
        flash('Predefined Summary Added Successfully')->success();
        return redirect()->action('Admin\PredefinedSummaryContentController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predefined_summary=PredefinedSummaryContent::findOrFail($id);
        return view('admin.predefined_contents.summary.edit',compact('predefined_summary'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'category' => 'required|string',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $predefined_summary=PredefinedSummaryContent::findOrFail($id);
        $predefined_summary->category=$request->category;
        $predefined_summary->job_title=$request->job_title;
        $predefined_summary->subcategory=$request->subcategory;
        $predefined_summary->summary=$request->summary;
        $predefined_summary->save();
        flash('Predefined Summary updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PredefinedSummaryContent::destroy($id);
        flash('Predefined Summary deleted successfully');
        return redirect()->action('Admin\PredefinedSummaryContentController@index');
    }
}
