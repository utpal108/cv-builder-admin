<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PredefinedTeachingDataTable;
use App\Imports\PredefinedTeaching;
use App\PredefinedTeachingContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PredefinedTeachingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PredefinedTeachingDataTable $dataTable)
    {
        return $dataTable->render('admin.predefined_contents.teaching.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.predefined_contents.teaching.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'predefined_teachings' => 'required|file',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        PredefinedTeachingContent::truncate();
        Excel::import(new PredefinedTeaching, request()->file('predefined_teachings'));
        flash('Predefined Teaching Added Successfully')->success();
        return redirect()->action('Admin\PredefinedTeachingController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predefined_teaching=PredefinedTeachingContent::findOrFail($id);
        return view('admin.predefined_contents.teaching.edit',compact('predefined_teaching'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'experience_title' => 'required|string',
            'institution' => 'required|string',
            'address' => 'required|string',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $predefined_teaching=PredefinedTeachingContent::findOrFail($id);
        $predefined_teaching->experience_title=$request->experience_title;
        $predefined_teaching->institution=$request->institution;
        $predefined_teaching->address=$request->address;
        $predefined_teaching->save();
        flash('Predefined Teaching updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PredefinedTeachingContent::destroy($id);
        flash('Predefined Teaching deleted successfully');
        return redirect()->action('Admin\PredefinedTeachingController@index');
    }
}
