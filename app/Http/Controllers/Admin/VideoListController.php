<?php

namespace App\Http\Controllers\Admin;

use App\VideoList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class VideoListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $video_lists=VideoList::all();
        return view('admin.video_list.index',compact('video_lists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.video_list.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'video_for' => 'required|string|min:3|max:191',
            'video_title' => 'string|max:191',
            'tips_video' => 'required',
            'bg_image' => 'required',
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $allData=$request->all();
        if ($request->hasFile('tips_video')){
            $allData['video_url'] = $request->tips_video->store('videos');
        }
        if ($request->hasFile('bg_image')){
            $allData['bg_image'] = $request->bg_image->store('images');
        }
        VideoList::create($allData);
        flash('New video added successfully');
        return redirect()->action('Admin\VideoListController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video_list=VideoList::findOrFail($id);
        return view('admin.video_list.edit',compact('video_list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'video_for' => 'required|string|min:3|max:191',
            'video_title' => 'required|string|max:191',
        ]);

        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        $video_list=VideoList::findOrFail($id);

        if ($request->hasFile('tips_video')){
            Storage::delete($video_list->video_url);
            $video_list->video_url = $request->tips_video->store('videos');
        }

        if ($request->hasFile('bg_image')){
            Storage::delete($video_list->bg_image);
            $video_list->bg_image = $request->bg_image->store('images');
        }

        $video_list->video_for=$request->video_for;
        $video_list->video_title=$request->video_title;
        $video_list->save();
        flash('Video list updated successfully');
        return redirect()->action('Admin\VideoListController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video_list=VideoList::findOrFail($id);
        Storage::delete($video_list->video_url);
        Storage::delete($video_list->bg_image);
        VideoList::destroy($id);
        flash('Video deleted successfully');
        return redirect()->action('Admin\VideoListController@index');
    }
}
