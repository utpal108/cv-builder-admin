<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PredefinedLanguageDataTable;
use App\Imports\PredefinedInterest;
use App\Imports\PredefinedLanguage;
use App\PredefinedInterestContent;
use App\PredefinedLanguageContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PredefinedLanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PredefinedLanguageDataTable $dataTable)
    {
        return $dataTable->render('admin.predefined_contents.language.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.predefined_contents.language.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'predefined_languages' => 'required|file',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        PredefinedInterestContent::truncate();
        Excel::import(new PredefinedLanguage, request()->file('predefined_languages'));
        flash('Predefined Languages Added Successfully')->success();
        return redirect()->action('Admin\PredefinedLanguageController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predefined_language=PredefinedLanguageContent::findOrFail($id);
        return view('admin.predefined_contents.language.edit',compact('predefined_language'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'language' => 'required|string',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $predefined_language=PredefinedLanguageContent::findOrFail($id);
        $predefined_language->language=$request->language;
        $predefined_language->save();
        flash('Predefined Language updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PredefinedLanguageContent::destroy($id);
        flash('Predefined Language deleted successfully');
        return redirect()->action('Admin\PredefinedLanguageController@index');
    }
}
