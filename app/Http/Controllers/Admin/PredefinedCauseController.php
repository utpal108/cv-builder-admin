<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PredefinedCauseDataTable;
use App\Imports\PredefinedCause;
use App\PredefinedCauseContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class PredefinedCauseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PredefinedCauseDataTable $dataTable)
    {
        return $dataTable->render('admin.predefined_contents.cause.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.predefined_contents.cause.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'predefined_causes' => 'required|file',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }
        PredefinedCauseContent::truncate();
        Excel::import(new PredefinedCause, request()->file('predefined_causes'));
        flash('Predefined Causes Added Successfully')->success();
        return redirect()->action('Admin\PredefinedCauseController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $predefined_cause=PredefinedCauseContent::findOrFail($id);
        return view('admin.predefined_contents.cause.edit',compact('predefined_cause'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
        ]);
        if ($validator->fails()) {
            flash($validator->errors()->first())->error();
            return redirect()
                ->back();
        }

        $predefined_cause=PredefinedCauseContent::findOrFail($id);
        $predefined_cause->title=$request->title;
        $predefined_cause->save();
        flash('Predefined Cause updated successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PredefinedCauseContent::destroy($id);
        flash('Predefined Cause deleted successfully');
        return redirect()->action('Admin\PredefinedCauseController@index');
    }
}
