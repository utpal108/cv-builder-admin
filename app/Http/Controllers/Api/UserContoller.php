<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\UserProfileCollection;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserContoller extends Controller
{
    public function index(Request $request){
        $validator = Validator::make($request->all(), [
            'subscription_type' => [
                'required',
                Rule::in(['all','basic','premium']),
            ],
        ]);

        if ($validator->fails()) {
            return \response()
                ->json([
                    'errors' => $validator->errors()->first()
                ], 200);
        }
        if ($request->subscription_type==='premium'){
            $users=User::where([['user_type','<>','admin'],['status','active'],['subscription_expired_in','>=',date('d-m-Y')]])->paginate(5);
        }
        elseif ($request->subscription_type==='basic'){
            $users=User::where([['user_type','<>','admin'],['status','active']])
                ->where(function ($query) {
                    $query->where('subscription_expired_in', '<', date('d-m-Y'))
                        ->orWhere('subscription_expired_in', null)
                        ->orWhere('subscription_expired_in', '');
                })->paginate(5);
        }
        else{
            $users=User::where([['user_type','<>','admin'],['status','active']])->paginate(5);
        }

        return new UserProfileCollection($users);
    }
}
