<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\VideoListCollection;
use App\VideoList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VideoListController extends Controller
{
    public function get_resume_videolist(){
        return new VideoListCollection(VideoList::where('video_for','resume')->orWhere('video_for','both')->get());
    }

    public function get_coverletter_videolist(){
        return new VideoListCollection(VideoList::where('video_for','cover')->orWhere('video_for','both')->get());
    }
}
