<?php

namespace App\Http\Controllers\Api;

use App\CoverLetterSample;
use App\Http\Resources\CoverPredefinedContentCollection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CoverPredefinedController extends Controller
{
    public function get_predefined_contents(Request $request){
        $validator = Validator::make($request->all(), [
            'cover_type'=>'required|string',
            'job_title'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $predefined_contents=CoverLetterSample::where([['type',$request->cover_type],['job_title', 'like', $request->job_title.'%']])->get();
        return new CoverPredefinedContentCollection($predefined_contents);
    }
    public function special_case_cover_letters(){
        $predefined_contents=CoverLetterSample::where('type','special case')->distinct()->get('job_title');
        return response()->json($predefined_contents);
    }
    public function post_interview_cover_letters(){
        $predefined_contents=CoverLetterSample::where('type','post interview')->distinct()->get('job_title');
        return response()->json($predefined_contents);
    }
}
