<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CoverLetterSectionCollection;
use App\Http\Resources\CoverLetterSectionResource;
use App\Model\Documents;
use App\Model\DocumentSection;
use App\Model\LanguageSets;
use Couchbase\Document;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
//use Barryvdh\DomPDF\Facade as PDF;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;

class DocumentsController extends Controller
{
    public function addDocument(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'document_type'=>'required|string',
            'document_title'=>'required',
            'document_layout'=>'required',
            'template_data'=>'required',
            'language_id'=>'required|numeric',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $user_id = auth('api')->user()->id;

        $language = LanguageSets::where([['id', $request->language_id],['user_id', $user_id]])->first();
        if (is_null($language)){
            return response()
                ->json([
                    'errors'=>'Invalid language'
                ]);
        }

        $document_id = md5(uniqid('ic_documents_', true));
        $template_data=$request->template_data;
        $template_data['id']=$document_id;

        $template=Documents::create([
            'document_id'=>$document_id,
            'document_type'=>$request->document_type,
            'document_title'=>$request->document_title,
            'document_layout'=>$request->document_layout,
            'template_data'=>$template_data,
            'user_id'=>$user_id,
            'language_id'=>$request->language_id,
        ]);


        return response()
            ->json([
                'message'=>__('Document Created Successfully'),
                'document_id'=>$document_id
            ]);
    }

    public function getAllDocuments(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'document_type'=>'required|string',
            'language_id'=>'required|numeric',
        ]);
        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $auth_user=auth('api')->user();
        $user_id = $auth_user->id;
        if ($auth_user->document_order_by == 'date'){
            $documents = Documents::where([['user_id', $user_id],['document_type', $request->document_type],['language_id', $request->language_id]])->orderBy('created_at','desc')->get();
        }
        else{
            $documents = Documents::where([['user_id', $user_id],['document_type', $request->document_type],['language_id', $request->language_id]])->orderBy('document_title')->get();
        }

        return response()
            ->json($documents, 200);
    }

    public function updateDocument(Request $request)
    {
        $document = Documents::where('user_id', auth('api')->user()->id)->where('document_id', $request->id)->firstOrFail();
        $document->document_title=$request->document_data['letter_info']['name'];
        $document->document_layout=$request->document_data['template_data']['templates']['layout'];
        $document->template_data=$request->document_data;
        if (isset($request->share_modified)){
            $document->share_modified=$request->share_modified;
        }
        $document->save();
//        Documents::where('user_id', auth('api')->user()->id)
//            ->where('document_id', $request->id)
//            ->update([
//                'document_title'=>$request->document_data['letter_info']['name'],
//                'document_layout'=>$request->document_data['template_data']['templates']['layout'],
//                'template_data'=>json_encode($request->document_data),
//                'share_modified'=>false,
//            ]);
        return response()
            ->json([
                'message'=>'done'
            ]);
    }

    public function getDocumentById(Request $request)
    {
        $document = Documents::where('user_id', auth('api')->user()->id)
            ->where('document_id', $request->id)
            ->first();

        return response()
            ->json([
                'document'=>$document->template_data,
                'template'=>$document->document_layout,
                'share_modified'=>$document->share_modified,
                'shareable_link'=>$document->shareable_link
            ]);
    }

    public function downloadDocument($cover_letter_id){
        $document=Documents::where('document_id',$cover_letter_id)->firstOrFail();
        if ($document->document_type === 'cover'){
            $pdf = PDF::loadView('api.pdf.cover_letter.create',compact('document'))->setPaper('a4');
            return $pdf->download('cover_letter.pdf');
        }
        else{
            $layout_type = $document->template_data['template_data']['sections']['layout_type'];
            $layout_orders=['left'=>[], 'right'=>[]];

            if ($layout_type == 'predefined'){
                $layout_orders=$document->template_data['template_data']['sections']['predefined_layout_order'];
            }
            else {
                  $layout = $document->template_data['template_data']['sections']['layout'];
                  if ($layout == true){
                      $layout_orders['left'] = $document->template_data['template_data']['sections']['single_layout_order'];
                  }
                  else {
                      $layout_orders= $document->template_data['template_data']['sections']['double_layout_order'];
                  }
            }
            $pdf = PDF::loadView('api.pdf.resume.create',compact('document','layout_orders'))->setPaper('a4');
            return $pdf->download('resume.pdf');
        }
    }

    public function duplicateDocument(Request $request){
        $validator = Validator::make($request->all(), [
            'document_id'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        $duplicate_document=Documents::where('document_id',$request->document_id)->firstOrFail();
        $document_id = md5(uniqid('ic_documents_', true));

        Documents::create([
            'document_id'=>$document_id,
            'document_type'=>$duplicate_document->document_type,
            'document_layout'=>$duplicate_document->document_layout,
            'document_title'=>$duplicate_document->document_title,
            'template_data'=>$duplicate_document->template_data,
            'user_id'=>auth('api')->user()->id,
            'language_id'=>$duplicate_document->language_id,
        ]);

        return response()->json(['status'=>'success']);
    }

    public function deleteDocument(Request $request){
        $validator = Validator::make($request->all(), [
            'document_id'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        $document=Documents::where('document_id',$request->document_id)->firstOrFail();
        if ($document->user_id == auth('api')->user()->id){
            Documents::destroy($document->id);
            return response()->json(['status'=>'success']);
        }
        else{
            return response()->json(['status'=>'Unauthorized'],403);
        }


    }

    public function updateDocumentOrder(Request $request){
        $validator = Validator::make($request->all(), [
            'document_order'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        $user=auth('api')->user();
        $user->document_order_by=$request->document_order;
        $user->save();
        return response()->json(['status'=>'success']);
    }

    public function getCoverLetterSections(){
//        $sections = DocumentSection::with('section_elements.element_validations')->get();
        return new CoverLetterSectionCollection(DocumentSection::where('document_type','cover_letter')->get());
//        $sections=[
//            'title'=>[
//                'id'=>1,
//                'name'=>'Title',
//                'errors'=>1,
//                'warnings'=>2,
//                'error_messages'=>[],
//                'warning_messages'=>[],
//            ],
//            'contact'=>[
//                'id'=>1,
//                'name'=>'Contact',
//                'errors'=>3,
//                'warnings'=>0,
//                'messages'=>[],
//            ],
//            'social_media'=>[
//                'id'=>1,
//                'name'=>'Social Media',
//                'errors'=>2,
//                'warnings'=>0,
//                'messages'=>[],
//            ],
//            'details'=>[
//                'id'=>1,
//                'name'=>'Details',
//                'errors'=>0,
//                'warnings'=>1,
//                'messages'=>[],
//            ],
//            'footer'=>[
//                'id'=>1,
//                'name'=>'Footer',
//                'errors'=>0,
//                'warnings'=>2,
//                'messages'=>[],
//            ]
//        ];

//        return response()->json($sections);
    }

    public function addResume(){
        $document_layout='template01';

        $template_data=[
          'layout_type'=>'predefined/custom',
          'section_layout'=>'single/double',
          'predefined_layout'=>'template01',
          'predefined_layout_order'=>[
             'left'=>['language','skill','education'],
             'right'=>['awards','teaching'],
          ],
          'single_layout_order'=>['language','skill','education'],
          'double_layout_order'=>[
              'left'=>['language','skill','education'],
              'right'=>['awards','teaching'],
          ],
          'section_data'=>[
              'skill'=>[
                  'month_from'=>'01',
                  'year_from'=>'2019',
                  'month_to'=>'02',
                  'year_to'=>'2020'
              ],
              'awards'=>[
                  'month_from'=>'01',
                  'year_from'=>'2019',
                  'month_to'=>'02',
                  'year_to'=>'2020'
              ],
          ]
        ];
    }
}
