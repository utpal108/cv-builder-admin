<?php

namespace App\Http\Controllers\Api;

use App\Model\ShareableLink;
use App\UserFeedback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UserFeedbackController extends Controller
{
    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'session_id'=>'required|exists:user_feedback,session_id',
//            'feedbacks'=>'required',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $feedback=UserFeedback::where('session_id', $request->session_id)->firstOrFail();
        $feedback->feedbacks = $request->feedbacks;
        if (isset($request->rating)){
            $feedback->rating = $request->rating;
        }
        $feedback->save();
        return response()->json(['status'=>'success']);
    }

    public function delete(Request $request){
        $validator = Validator::make($request->all(), [
            'feedback_id'=>'required|exists:user_feedback,id',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        UserFeedback::destroy($request->feedback_id);
        return response()->json(['status'=>'success']);
    }

    public function addSecrectKey(Request $request){
        $validator = Validator::make($request->all(), [
            'full_name'=>'required',
            'rating'=>'required',
//            'feedbacks'=>'required',
            'sharable_id'=>'required|exists:shareable_links,shareable_id',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $shareable_link=ShareableLink::where('shareable_id',$request->sharable_id)->firstOrFail();
        $secret_id=Str::random(15).date('ymdims');
        UserFeedback::create(['full_name'=>$request->full_name, 'session_id'=>$secret_id, 'document_id'=>$shareable_link->document_id, 'rating'=>$request->rating, 'feedbacks'=>$request->feedbacks]);
        return response()->json(['status'=>'success','session_id'=>$secret_id]);
    }
}
