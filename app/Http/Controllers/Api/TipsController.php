<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\TipsCollection;
use App\Tip;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TipsController extends Controller
{
    public function get_resume_tips(){
        return new TipsCollection(Tip::where('tips_for','resume')->orWhere('tips_for','both')->get());
    }

    public function get_coverletter_tips(){
        return new TipsCollection(Tip::where('tips_for','cover')->orWhere('tips_for','both')->get());
    }
}
