<?php

namespace App\Http\Controllers\Api;

use App\Model\LanguageSets;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\Api\SendVerificationCode;
use Carbon\Carbon;
use Socialite;
use App\Mail\Api\SendMagicLink;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class RegisterController extends Controller
{
    use SendsPasswordResetEmails;

    public function registerUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|min:3|max:191',
            'last_name' => 'required|string|min:3|max:191',
            'email' => 'required|string|email',
        ], [
            'first_name.required' => 'Given name is required',
            'first_name.min' => 'Given name minimum 3 character',
            'last_name.required' => 'Family name is required',
            'last_name.min' => 'Family name minimum 3 character',
            'email.required' => 'Email address required',
            'email.email' => 'Please input a valid email',
        ]);

        if ($validator->fails()) {
            return \response()
                ->json([
                    'errors' => $validator->errors()->first()
                ], 200);
        }
        $random_number = \getRandomNumber(6);

        $user=User::withTrashed()->where('email',$request->email)->first();
        if (!is_null($user)){
            if (!is_null($user->deleted_at)){
                $user->first_name=$request->first_name;
                $user->last_name=$request->last_name;
                $user->password=null;
                $user->verification_code=$random_number;
                $user->user_type='user';
                $user->auth_type='web';
                $user->status='pending';
                $user->deleted_at=null;
                $user->save();
            }
            else{
                return \response()
                    ->json([
                        'errors' => 'Email already exist'
                    ], 200);
            }
        }
        else{
            $user = User::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'verification_code' => $random_number,
                'user_type' => 'user',
                'auth_type' => 'web',
                'status' => 'pending',
            ]);
            LanguageSets::create(['language_name'=>'Default', 'user_id'=>$user->id]);
        }

        Mail::to($user)->send(new SendVerificationCode($user));

        return \response()
            ->json([
                'message' => __('Please check your mail for confirmation code')
            ], 200);
    }

    public function resendVerificationCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
        ], [
            'email.required' => 'Email address required',
            'email.email' => 'Please input a valid email',
        ]);

        if ($validator->fails()) {
            return \response()
                ->json([
                    'errors' => $validator->errors()->first()
                ], 200);
        }
        $random_number = \getRandomNumber(6);
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return \response()
                ->json([
                    'errors' => __('Sorry user not found please register first')
                ], 200);
        }
        User::where('email', $request->email)->update([
            'verification_code' => $random_number,
        ]);
        $user = User::where('email', $request->email)->first();
        Mail::to($user)->send(new SendVerificationCode($user));

        return \response()
            ->json([
                'message' => __('Please check your mail for confirmation code')
            ], 200);
    }

    public function confirmAccount(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|numeric',
            'email' => 'required|string|email',
        ], [
            'code.required' => 'Code is required',
            'code.numeric' => 'Code must be numeric',
            'email.required' => 'Email address required',
            'email.email' => 'Please input a valid email',
        ]);

        if ($validator->fails()) {
            return \response()
                ->json([
                    'errors' => $validator->errors()->first()
                ], 200);
        }

        $user = User::where('email', $request->email)
            ->where('verification_code', $request->code)
            ->first();

        if ($user) {
            $user->status = 'active';
            $user->email_verified_at = Carbon::now();
            $user->verification_code = '';
            $user->save();
            if (!$token = auth()->guard('api')->login($user)) {
                return response()->json(['error' => __('Incorrect email or password')], 401);
            }

            return response()->json([
                'message' => __('login successfully'),
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth()->guard('api')->factory()->getTTL() * 60
            ], 200);
        } else {
            return \response()
                ->json([
                    'errors' => __('Please try a valid verification code')
                ], 200);
        }
    }

    public function socialLogin($provider)
    {
        $social_logins = ['google', 'facebook', 'linkedin'];
        if (in_array($provider, $social_logins)){
            return Socialite::driver($provider)->stateless()->redirect();
        }
        else{
            return redirect('https://cvbility.com');
        }
    }

    public function socialCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();
        $find_user = User::withTrashed()->where('email',$user->getEmail())->first();
        $first_name= '';
        $last_name= '';
        $fileContents = file_get_contents($user->getAvatar());
        $profile_image='ic-profile-image/' .$user->getId().'.'.'png';
        Storage::put($profile_image, $fileContents);

        if ($provider === 'google'){
            $first_name = $user->user['given_name'];
            $last_name = $user->user['family_name'];
        }
        elseif ($provider === 'facebook'){
            $first_name = $user->user['name'];
        }
        elseif ($provider === 'linkedin'){
            $first_name = $user->first_name;
            $last_name = $user->last_name;
        }

        if (!is_null($find_user)){
            $user=$find_user;
            if (!is_null($find_user->deleted_at) || ($find_user->status==='pending')){
                $find_user->first_name=$first_name;
                $find_user->last_name=$last_name;
                $find_user->profile_image=$profile_image;
                $find_user->password=null;
                $find_user->verification_code='';
                $find_user->email_verified_at=Carbon::now();
                $find_user->user_type='user';
                $find_user->auth_type='social';
                $find_user->status='active';
                $find_user->deleted_at=null;
                $find_user->save();
            }
        }
        else{
            $user = User::create([
                'first_name' => $first_name,
                'last_name' => $last_name,
                'profile_image' => $profile_image,
                'email' => $user->getEmail(),
                'verification_code' => '',
                'email_verified_at' => Carbon::now(),
                'user_type' => 'user',
                'auth_type' => 'social',
                'status' => 'active'
            ]);
            LanguageSets::create(['language_name'=>'Default', 'user_id'=>$user->id]);
        }

        if (!$token = auth()->guard('api')->login($user)) {
            return response()->json(['error' => __('Incorrect email or password')], 401);
        }

        return \redirect()
            ->to(\config('settings.web_url').'magic/login/' . $token);
    }

    public function sendMagicLink(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
        ], [
            'email.required' => 'Email address required',
            'email.email' => 'Please input a valid email',
        ]);

        if ($validator->fails()) {
            return \response()
                ->json([
                    'errors' => $validator->errors()->first()
                ], 200);
        }

        $user = User::where('email', $request->email)
            ->first();

        if ($user) {
            if (!$token = auth()->guard('api')->login($user)) {
                return response()->json(['error' => 'Incorrect email'], 401);
            }
            $response=Mail::to($user)->send(new SendMagicLink($user, $token));
            return \response()
                ->json([
                    'message' => __('one time login link has been send to your mail')
                ], 200);
        } else {
            return \response()
                ->json([
                    'errors' => __('Please enter valid email address')
                ], 200);
        }
    }

    public function sendPasswordRestMail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
        ], [
            'email.required' => 'Email address required',
            'email.email' => 'Please input a valid email',
        ]);

        if ($validator->fails()) {
            return \response()
                ->json([
                    'errors' => $validator->errors()->first()
                ], 200);
        }

        $response = $this->broker()->sendResetLink(
            $this->credentials($request)
        );

        if ($response == 'passwords.sent') {
            return \response()
                ->json([
                    'message' => __('Please check your mail')
                ], 200);
        } else {
            return \response()
                ->json([
                    'errors' => __('Sorry mail address not found')
                ], 200);
        }
    }
}
