<?php

namespace App\Http\Controllers\Api;

use App\CoverOptimizer;
use App\Http\Resources\CoverOptimizerCollection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CoverOptimizerController extends Controller
{
    public function get_revisions(){
        $allData=[];
        $revision_optimizers=CoverOptimizer::where('type','revision')->get();
        foreach ($revision_optimizers as $revision_optimizer){
            $allData[$revision_optimizer->section_name][$revision_optimizer->element_name]['title']=$revision_optimizer->title;
            $allData[$revision_optimizer->section_name][$revision_optimizer->element_name]['details']=$revision_optimizer->details;
        }
        return response()->json($allData);
    }

    public function get_suggestions(){
        $allData=[];
        $suggestion_optimizers=CoverOptimizer::where('type','suggestion')->get();
        foreach ($suggestion_optimizers as $suggestion_optimizer){
            $allData[$suggestion_optimizer->section_name][$suggestion_optimizer->element_name]['title']=$suggestion_optimizer->title;
            $allData[$suggestion_optimizer->section_name][$suggestion_optimizer->element_name]['details']=$suggestion_optimizer->details;
        }
        return response()->json($allData);
    }
}
