<?php

namespace App\Http\Controllers\Api;

use App\PredefinedSectionTips;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PredefinedTipsController extends Controller
{
    public function search(Request $request){
        $validator = Validator::make($request->all(), [
            'section_name'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $predefined_tip=PredefinedSectionTips::where('section_name',$request->section_name)->firstOrFail();
        return response()->json(['id'=>$predefined_tip->id, 'tips'=>$predefined_tip->tips, 'video_url'=>$predefined_tip->video_url]);
    }
}
