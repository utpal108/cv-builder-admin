<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\UserProfileResource;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
        Resource::withoutWrapping();
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:191',
            'password' => 'required|min:8|string'
        ]);

        if ($validator->fails()) {
            return \response()
                ->json([
                    'message' => $validator->errors()->first()
                ], 200);
        }

        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
            'status' => 'active',
        ];

        if (!$token = auth()->guard('api')->attempt($credentials)) {
            return response()->json(['error' => __('Incorrect email or password')], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function user_info()
    {
        return new UserProfileResource(auth('api')->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->guard('api')->logout();

        return response()->json(['message' => __('Successfully logged out')]);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->guard('api')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->guard('api')->factory()->getTTL() * 60
        ]);
    }

    public function getExperienceLevel(){
        $user = auth('api')->user();
        return response()->json(['experience_level'=>$user->experience_level]);
    }

    public function updateExperienceLevel(Request $request){
        $validator = Validator::make($request->all(), [
            'experience_level'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
            ]);
        }

        $user = auth('api')->user();
        $user->experience_level=$request->experience_level;
        $user->save();
        return response()->json(['status'=>'success']);
    }
}
