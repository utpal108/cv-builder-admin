<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AccountController extends Controller
{
    use SendsPasswordResetEmails;

    public function uploadProfileImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'profile_image'=>'required'
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'status'=>'failed',
                    'errors'=>$validator->errors()->first()
                ]);
        }

        $user = auth('api')->user();

        if (preg_match('/^data:image\/(\w+);base64,/', $request->profile_image)) {
            $data = substr($request->profile_image, strpos($request->profile_image, ',') + 1);
            $data = base64_decode($data);
            $new_profile_image = 'ic-profile-image/' .Str::random(50).'.'.'png';
            Storage::delete($user->profile_image);
            Storage::put($new_profile_image, $data);
            $user->profile_image=$new_profile_image;
            $user->save();
            return response()
                ->json([
                    'status'=>'success',
                    'profile_image' => env('APP_URL') .'/storage/' .$user->profile_image
                ], 200);
        }
        else{
            return response()
                ->json(['status'=>'failed'], 403);
        }
    }

    public function updateBasicProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|min:3|max:191',
            'last_name' => 'required|string|min:3|max:191',
            'birthday' => 'required|date',
        ], [
            'first_name.required' => 'First name is required',
            'first_name.min' => 'First name minimum 3 character',
            'last_name.required' => 'Last name is required',
            'last_name.min' => 'Last name minimum 3 character',
            'birthday.required' => 'Date of birth is required',
            'birthday.date' => 'Invalid Birthday date',
        ]);

        if ($validator->fails()) {
            return \response()
                ->json([
                    'errors' => $validator->errors()->first()
                ], 200);
        }

        $user = User::find(auth('api')->user()->id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->birthday = $request->birthday;
        $user->save();

        return response()
            ->json([
                'message'=>__('Profile Updated Successfully')
            ], 200);
    }

    public function passwordChangeRequest(Request $request)
    {
        $response = $this->broker()->sendResetLink([
            'email'=>auth('api')->user()->email
        ]);

        if ($response == 'passwords.sent') {
            return \response()
                ->json([
                    'message' => __('Please check your mail')
                ], 200);
        } else {
            return \response()
                ->json([
                    'errors' => __('Sorry mail address not found')
                ], 200);
        }
    }

    public function changeMail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'=>'required|email|unique:users',
            'password'=>'required|string|min:8',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        $credentials = [
            'email' => auth('api')->user()->email,
            'password' => $request->password,
            'status' => 'active',
            'auth_type' => 'web',
        ];

        if (auth()->guard('api')->attempt($credentials))
        {
            $user = User::find(auth('api')->user()->id);
            $user->email = $request->email;
            $user->save();

            return response()
                ->json([
                    'message'=>__('Email address successfully changed')
                ], 200);

        }else{
            return response()
                ->json([
                    'errors'=>__('Password incorrect')
                ], 200);
        }
    }

    public function deleteAccountPermanently(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password'=>'required|string|min:8',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        $credentials = [
            'email' => auth('api')->user()->email,
            'password' => $request->password,
            'status' => 'active',
        ];

        if (auth()->guard('api')->attempt($credentials))
        {
            User::destroy(auth('api')->user()->id);

            return response()
                ->json([
                    'message'=>__('Account Successfully deleted')
                ], 200);

        }else{
            return response()
                ->json([
                    'errors'=>__('Password incorrect')
                ], 200);
        }
    }

    public function updateSpecialOfferAccess(Request $request){
        $validator = Validator::make($request->all(), [
            'get_special_offer'=>'required|boolean',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $user=auth('api')->user();
        $user->get_special_offer= $request->get_special_offer;
        $user->save();

        return response()->json(['status'=>'success']);
    }

    public function updateDefaultLanguage(Request $request){
        $validator = Validator::make($request->all(), [
            'default_language'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $user=auth('api')->user();
        $user->default_language= $request->default_language;
        $user->save();

        return response()->json(['status'=>'success']);
    }
}
