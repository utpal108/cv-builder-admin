<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ShareableLinkCollection;
use App\Http\Resources\ShareableLinkResource;
use App\Model\Documents;
use App\Model\ShareableLink;
use App\UserFeedback;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ShareableLinkController extends Controller
{
    public function getShareableData(Request $request){
        $validator = Validator::make($request->all(), [
            'shareable_id'=>'required|exists:shareable_links,shareable_id',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        $shareable_link=ShareableLink::where('shareable_id',$request->shareable_id)->first();
        if (is_null($shareable_link)){
            return response()->json(['data'=>null]);
        }
        else{
            $user_info=[
                'first_name'=>$shareable_link->user_info->first_name,
                'last_name'=>$shareable_link->user_info->last_name,
                'profile_image'=>Storage::url($shareable_link->user_info->profile_image),
                'date_of_birth'=>$shareable_link->user_info->birthday
            ];
            $template_data=$shareable_link->template_data;
            if ($shareable_link->hide_sensitive){
                $template_data['letter_info']['user_info']=[
                    "first_name"=> 'Confidential',
                    "last_name"=> 'Confidential',
                    'profile_image'=>Storage::url($shareable_link->user_info->profile_image),
                    "date_of_birth"=> 'Confidential',
                    "email"=> 'Confidential',
                    "phone_no"=> 'Confidential',
                    "country"=> 'Confidential',
                    "city"=> 'Confidential',
                    "address"=> 'Confidential',
                    "website"=> 'Confidential',
                    "facebook"=> 'Confidential',
                    "linkedin"=> 'Confidential',
                    "twitter"=> 'Confidential',
                    "quora"=> 'Confidential',
                    "skype"=> 'Confidential',
                    "github"=> 'Confidential',
                    "stack_overflow"=> 'Confidential',
                    "medium"=> 'Confidential',
                    "instagram"=> 'Confidential',
                ];
            }
            return response()->json(['data'=>$template_data, 'user_info'=>$user_info]);
        }
    }

    public function getShareableLink(Request $request){
        $validator = Validator::make($request->all(), [
            'document_id'=>'required|exists:documents,document_id',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        $document=Documents::where('document_id',$request->document_id)->firstOrFail();
        UserFeedback::where([['created_at','<', Carbon::now()->subDays(30)],['document_id',$document->id]])->get();
        $shareable_link=ShareableLink::where('document_id',$document->id)->first();
        if (is_null($shareable_link)){
            return response()->json(['data'=>[]]);
        }
        else{
            return new ShareableLinkResource($shareable_link);
        }
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'document_id'=>'required|exists:documents,document_id',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $document=Documents::where('document_id',$request->document_id)->firstOrFail();
        $shareable_id=Str::random(15).date('ymdims');
        ShareableLink::updateOrCreate(
            ['document_id' => $document->id],
            ['user_id'=>$document->user_id,'document_type'=>$document->document_type,'shareable_id'=>$shareable_id, 'template_data'=>$document->template_data, 'hide_sensitive'=>true]);
        return response()->json(['status'=>'success', 'shareable_id'=>$shareable_id]);
    }

    public function publishChange(Request $request){
        $validator = Validator::make($request->all(), [
            'document_id'=>'required|exists:documents,document_id',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $document=Documents::where('document_id',$request->document_id)->firstOrFail();
        $document->share_modified=true;
        $document->save();
        $shareable_link=ShareableLink::where('document_id',$document->id)->firstOrFail();
        $shareable_link->template_data=$document->template_data;
        $shareable_link->save();
        return response()->json(['status'=>'success']);
    }

    public function changeHideSensitiveOption(Request $request){
        $validator = Validator::make($request->all(), [
            'document_id'=>'required',
            'hide_sensitive_option'=>'required',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $document=Documents::where('document_id',$request->document_id)->firstOrFail();
        $shareable_link=ShareableLink::where('document_id',$document->id)->firstOrFail();
        $shareable_link->hide_sensitive=$request->hide_sensitive_option;
        $shareable_link->save();
        return response()->json(['status'=>'success']);
    }

    public function delete(Request $request){
        $validator = Validator::make($request->all(), [
            'document_id'=>'required',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        $document=Documents::where('document_id',$request->document_id)->firstOrFail();
        ShareableLink::where('document_id',$document->id)->delete();
        return response()->json(['status'=>'Success']);
    }
}
