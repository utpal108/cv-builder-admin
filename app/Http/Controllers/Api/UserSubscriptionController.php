<?php

namespace App\Http\Controllers\Api;

use App\UserSubscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DateTime;

class UserSubscriptionController extends Controller
{
    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|string|min:3|max:191',
            'subscription_id' => 'required|string|min:3|max:191',
            'subscription_start_date' => 'required|string|min:3|max:50',
            'subscription_expire_date' => 'required|string|min:3|max:50',
            'subscription_status' => 'required|string|min:1|max:50',
            'plan_id' => 'required|string|min:1|max:100',
            'plan_status' => 'required|string|min:1|max:50',
            'payment_gateway' => 'required|string|min:1|max:50',
        ]);

        if ($validator->fails()) {
            return \response()
                ->json([
                    'errors' => $validator->errors()->first()
                ], 200);
        }

        UserSubscription::updateOrCreate(
            ['user_id'=>auth('api')->user()->id, 'customer_id' => $request->customer_id, 'plan_id' => $request->plan_id],
            ['subscription_id' => $request->subscription_id, 'subscription_start_date' => $request->subscription_start_date,'subscription_expire_date' => $request->subscription_expire_date, 'subscription_status' => $request->subscription_status, 'plan_status' => $request->plan_status, 'payment_gateway' => $request->payment_gateway]
        );
        $user=auth('api')->user();
        if(is_null($user->subscription_expired_in) || ($user->subscription_expired_in <= date("m/d/Y h:i:s"))){
            $user->subscription_expired_in = $request->subscription_expire_date;
        }
        else{
            $current_subs_date=new DateTime($user->subscription_expired_in);
            $date1 = new DateTime($request->subscription_start_date);
            $date2 = $date1->diff(new DateTime($request->subscription_expire_date));
            date_add($current_subs_date,date_interval_create_from_date_string($date2->days ." days"));
            $user->subscription_expired_in = date_format($current_subs_date,"m/d/Y h:i:s");
        }
        $user->save();

        return response()->json(['status'=>'success']);
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|string|min:3|max:191',
            'subscription_id' => 'required|string|min:3|max:191',
            'subscription_expire_date' => 'required|string|min:3|max:50',
            'subscription_status' => 'required|string|min:1|max:50',
            'plan_id' => 'required|string|min:1|max:100',
            'plan_status' => 'required|string|min:1|max:50',
        ]);

        if ($validator->fails()) {
            return \response()
                ->json([
                    'errors' => $validator->errors()->first()
                ], 200);
        }

        $user_subscription = UserSubscription::where(['user_id'=>auth('api')->user()->id, 'customer_id'=>$request->customer_id, 'subscription_id'=>$request->subscription_id, 'plan_id'=>$request->plan_id])->first();
        if (is_null($user_subscription)){
            return response()->json(['errors'=>'package not found']);
        }
        $user_subscription->subscription_expire_date=$request->subscription_expire_date;
        $user_subscription->subscription_status=$request->subscription_status;
        $user_subscription->plan_status=$request->plan_status;
        $user_subscription->save();

        return response()->json(['status'=>'success']);
    }

    public function delete(Request $request){
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|string|min:3|max:191',
            'subscription_id' => 'required|string|min:3|max:191',
            'subscription_expire_date' => 'required|string|min:3|max:50',
            'subscription_status' => 'required|string|min:1|max:50',
            'plan_id' => 'required|string|min:1|max:100',
            'plan_status' => 'required|string|min:1|max:50',
        ]);

        if ($validator->fails()) {
            return \response()
                ->json([
                    'errors' => $validator->errors()->first()
                ], 200);
        }
        $user_subscription=UserSubscription::where(['user_id'=>auth('api')->user()->id, 'customer_id'=>$request->customer_id, 'subscription_id'=>$request->subscription_id, 'plan_id'=>$request->plan_id])->first();
        if (is_null($user_subscription)){
            return response()->json(['errors'=>'package not found']);
        }

        $user_subscription->delete();

        return response()->json(['status'=>'success']);
    }
}
