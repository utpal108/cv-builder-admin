<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\PredefinedAchievementCollection;
use App\Http\Resources\PredefinedAwardCollection;
use App\Http\Resources\PredefinedCauseCollection;
use App\Http\Resources\PredefinedCertificateCollection;
use App\Http\Resources\PredefinedConferenceCollection;
use App\Http\Resources\PredefinedEducationCollection;
use App\Http\Resources\PredefinedInterestCollection;
use App\Http\Resources\PredefinedLanguageCollection;
use App\Http\Resources\PredefinedOrganizationCollection;
use App\Http\Resources\PredefinedProjectCollection;
use App\Http\Resources\PredefinedPublicationCollection;
use App\Http\Resources\PredefinedSkillCollection;
use App\Http\Resources\PredefinedSoftSkillCollection;
use App\Http\Resources\PredefinedTeachingCollection;
use App\Http\Resources\PredefinedTechnicalSkillCollection;
use App\Http\Resources\PredefinedVolunteerCollection;
use App\Http\Resources\PredefinedWorkCollection;
use App\PredefinedAchievementContent;
use App\PredefinedAwardContent;
use App\PredefinedCauseContent;
use App\PredefinedCertificateContent;
use App\PredefinedConferenceContent;
use App\PredefinedEducationContent;
use App\PredefinedInterestContent;
use App\PredefinedLanguageContent;
use App\PredefinedOrganizationContent;
use App\PredefinedProjectContent;
use App\PredefinedPublicationContent;
use App\PredefinedSkillContent;
use App\PredefinedSoftSkillContent;
use App\PredefinedSummaryContent;
use App\PredefinedTeachingContent;
use App\PredefinedTechnicalSkillContent;
use App\PredefinedVolunteerContent;
use App\PredefinedWorkContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PredefinedContentController extends Controller
{
    public function get_predefined_skills(Request $request){
        $validator = Validator::make($request->all(), [
            'skill_category'=>'required',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        if ($request->skill_category=='*'){
            $predefined_skills=PredefinedSkillContent::limit(100)->get();
        }
        else{
            $predefined_skills=PredefinedSkillContent::where('category', $request->skill_category)->get();
        }
        return new PredefinedSkillCollection($predefined_skills);
    }

    public function get_predefined_skill_categories(Request $request){
        $validator = Validator::make($request->all(), [
            'skill_category'=>'required',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        if ($request->skill_category=='*'){
            $skill_categories=PredefinedSkillContent::groupBy('category')->select('category')->get()->pluck('category')->all();
        }
        else{
            $skill_categories=PredefinedSkillContent::where('category', 'like', $request->skill_category.'%')->groupBy('category')->select('category')->get()->pluck('category')->all();
        }

        return response()->json($skill_categories);
    }

    public function get_predefined_soft_skills(Request $request){
        $validator = Validator::make($request->all(), [
            'skill_category'=>'required',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        if ($request->skill_category=='*'){
            $predefined_skills=PredefinedSoftSkillContent::limit(100)->get();
        }
        else{
            $predefined_skills=PredefinedSoftSkillContent::where('category', $request->skill_category)->get();
        }
        return new PredefinedSoftSkillCollection($predefined_skills);
    }

    public function get_predefined_soft_skill_categories(Request $request){
        $validator = Validator::make($request->all(), [
            'skill_category'=>'required',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        if ($request->skill_category=='*'){
            $skill_categories=PredefinedSoftSkillContent::groupBy('category')->select('category')->get()->pluck('category')->all();
        }
        else{
            $skill_categories=PredefinedSoftSkillContent::where('category', 'like', $request->skill_category.'%')->groupBy('category')->select('category')->get()->pluck('category')->all();
        }

        return response()->json($skill_categories);
    }

    public function get_predefined_educations(Request $request){
        $validator = Validator::make($request->all(), [
            'search_item'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        if ($request->search_item=='*'){
            $study_programs=PredefinedEducationContent::all();
        }
        else{
            $study_programs=PredefinedEducationContent::where('phrase', 'like', $request->search_item.'%')->get();
        }
        return new PredefinedEducationCollection($study_programs);
    }

    public function get_predefined_achievements(Request $request){
        $validator = Validator::make($request->all(), [
            'search_item'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        if ($request->search_item=='*'){
            $achievements=PredefinedAchievementContent::where('category', 'like', '%General%')->get();
        }
        else{
            $achievements=PredefinedAchievementContent::where('category', 'like', $request->search_item.'%')->get();
        }
        return new PredefinedAchievementCollection($achievements);
    }

    public function get_predefined_awards(Request $request){
        $validator = Validator::make($request->all(), [
            'award_title'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $awards=PredefinedAwardContent::where('title', 'like', $request->award_title.'%')->get();
        return new PredefinedAwardCollection($awards);
    }

    public function get_predefined_causes(Request $request){
        $validator = Validator::make($request->all(), [
            'cause_title'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $causes=PredefinedCauseContent::where('title', 'like', $request->cause_title.'%')->get();
        return new PredefinedCauseCollection($causes);
    }

    public function get_predefined_certificates(Request $request){
        $validator = Validator::make($request->all(), [
            'search_item'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        if ($request->search_item=='*'){
            $certificates=PredefinedCertificateContent::all();
        }
        else{
            $certificates=PredefinedCertificateContent::where('phrase', 'like', $request->search_item.'%')->get();
        }
        return new PredefinedCertificateCollection($certificates);
    }

    public function get_predefined_conferences(Request $request){
        $validator = Validator::make($request->all(), [
            'search_item'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        if ($request->search_item=='*'){
            $conferences=PredefinedConferenceContent::all();
        }
        else{
            $conferences=PredefinedConferenceContent::where('phrase', 'like', $request->search_item.'%')->get();
        }
        return new PredefinedConferenceCollection($conferences);
    }

    public function get_predefined_interests(Request $request){
        $validator = Validator::make($request->all(), [
            'interest_title'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $interests=PredefinedInterestContent::where('title', 'like', $request->interest_title.'%')->get();
        return new PredefinedInterestCollection($interests);
    }

    public function get_predefined_languages(Request $request){
        $validator = Validator::make($request->all(), [
            'language'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $languages=PredefinedLanguageContent::where('language', 'like', $request->language.'%')->get();
        return new PredefinedLanguageCollection($languages);
    }

    public function get_predefined_organizations(Request $request){
        $validator = Validator::make($request->all(), [
            'organization_name'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $organizations=PredefinedOrganizationContent::where('organization_name', 'like', $request->organization_name.'%')->get();
        return new PredefinedOrganizationCollection($organizations);
    }

    public function get_predefined_projects(Request $request){
        $validator = Validator::make($request->all(), [
            'project_title'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $achievements=PredefinedProjectContent::where('title', 'like', $request->project_title.'%')->get();
        return new PredefinedProjectCollection($achievements);
    }

    public function get_predefined_publications(Request $request){
        $validator = Validator::make($request->all(), [
            'publication_title'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $publications=PredefinedPublicationContent::where('title', 'like', $request->publication_title.'%')->get();
        return new PredefinedPublicationCollection($publications);
    }

    public function get_predefined_teaching(Request $request){
        $validator = Validator::make($request->all(), [
            'experience_title'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $teaching=PredefinedTeachingContent::where('experience_title', 'like', $request->experience_title.'%')->get();
        return new PredefinedTeachingCollection($teaching);
    }

    public function get_predefined_technical_skills(Request $request){
        $validator = Validator::make($request->all(), [
            'skill_category'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        if ($request->skill_category=='*'){
            $technical_skills=PredefinedTechnicalSkillContent::limit(100)->get();
        }
        else{
            $technical_skills=PredefinedTechnicalSkillContent::where('category', $request->skill_category)->get();
        }
        return new PredefinedTechnicalSkillCollection($technical_skills);

    }

    public function get_predefined_technical_skill_categories(Request $request){
        $validator = Validator::make($request->all(), [
            'skill_category'=>'required',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        if ($request->skill_category=='*'){
            $skill_categories=PredefinedSkillContent::groupBy('category')->select('category')->get()->pluck('category')->all();
        }
        else{
            $skill_categories=PredefinedSkillContent::where('category', 'like', $request->skill_category.'%')->groupBy('category')->select('category')->get()->pluck('category')->all();
        }

        return response()->json($skill_categories);
    }

    public function get_predefined_volunteers(Request $request){
        $validator = Validator::make($request->all(), [
            'search_item'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        if ($request->search_item=='*'){
            $volunteers=PredefinedVolunteerContent::all();
        }
        else{
            $volunteers=PredefinedVolunteerContent::where('phrase', 'like', $request->search_item.'%')->get();
        }

        return new PredefinedVolunteerCollection($volunteers);
    }

    public function get_predefined_works(Request $request){
        $validator = Validator::make($request->all(), [
            'job_title'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        if ($request->job_title=='*'){
            $works=PredefinedWorkContent::where('category', 'General')->get();
        }
        else{
            $works=PredefinedWorkContent::where('job_title', $request->job_title)->get();
            if ($works->count()<=0){
                $works=PredefinedWorkContent::where('category', 'General')->get();
            }
        }
        return new PredefinedWorkCollection($works);
    }

    public function job_title_suggestions(Request $request){
        $validator = Validator::make($request->all(), [
            'job_title'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        $job_titles=PredefinedWorkContent::where('job_title', 'like', $request->job_title.'%')->groupBy('job_title')->select('job_title')->get()->pluck('job_title')->all();
        return response()->json($job_titles);
    }

    public function get_relevant_job_titles(Request $request){
        $validator = Validator::make($request->all(), [
            'job_title'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        $job_title=PredefinedWorkContent::where('job_title', $request->job_title)->firstOrFail();
        $job_titles=PredefinedWorkContent::where([['category', $job_title->category],['job_title','<>',$request->job_title]])->groupBy('job_title')->select('job_title')->limit(16)->get()->pluck('job_title')->all();
        return response()->json($job_titles);
    }

    public function get_category_from_job_titles(Request $request){
        $validator = Validator::make($request->all(), [
            'job_title'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $category_name='';
        $job_title=PredefinedWorkContent::where('job_title', $request->job_title)->first();
        if (!is_null($job_title)){
            $category_name=$job_title->category;
        }
        return response()->json(['category_name'=>$category_name]);
    }

    public function get_predefined_summary_categories(Request $request){
        $validator = Validator::make($request->all(), [
            'summary_category'=>'required',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        if ($request->summary_category=='*'){
            $summary_categories=PredefinedSummaryContent::groupBy('category')->select('category')->get()->pluck('category')->all();
        }
        else{
            $summary_categories=PredefinedSummaryContent::where('category', 'like', $request->summary_category.'%')->groupBy('category')->select('category')->get()->pluck('category')->all();
        }
        return response()->json($summary_categories);
    }

    public function get_predefined_summary_job_title(){
        $summary_job_titles=PredefinedSummaryContent::where([['job_title','<>',null],['subcategory','<>','General']])->groupBy('job_title')->select('job_title')->get()->pluck('job_title')->all();
        return response()->json($summary_job_titles);
    }

    public function summary_job_title_suggestions(Request $request){
        $validator = Validator::make($request->all(), [
            'job_title'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }

        $job_titles=PredefinedSummaryContent::where([['job_title', 'like', $request->job_title.'%'],['subcategory',null]])->groupBy('job_title')->select('job_title')->get()->pluck('job_title')->all();
        return response()->json($job_titles);
    }

    public function get_predefined_summary_by_job_title(Request $request){
        $validator = Validator::make($request->all(), [
            'job_title'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $summaries=PredefinedSummaryContent::where([['job_title',$request->job_title],['summary','<>',null]])->select('category','summary')->get();
        if ($summaries->count()>0){
            $selected_category=$summaries[0]['category'];
            $predefined_content=$summaries->pluck('summary')->all();
        }
        else{
            $summaries=PredefinedSummaryContent::where('job_title',$request->job_title)->select('category')->get();
            if ($summaries->count()>0){
                $selected_category='General';
                $predefined_content=PredefinedSummaryContent::where([['category',$summaries[0]['category']],['subcategory','general']])->select('summary')->get()->pluck('summary')->all();
            }
            else{
                $selected_category='General';
                $predefined_content=PredefinedSummaryContent::where([['category','general'],['subcategory','general']])->get()->pluck('summary')->all();
            }
        }
        return response()->json(['selected_category'=>$selected_category, 'predefined_content'=>$predefined_content]);
    }

    public function get_predefined_summary_by_category(Request $request){
        $validator = Validator::make($request->all(), [
            'category'=>'required|string',
        ]);

        if ($validator->fails())
        {
            return response()
                ->json([
                    'errors'=>$validator->errors()->first()
                ]);
        }
        $summaries=PredefinedSummaryContent::where([['category',$request->category],['subcategory','general']])->select('summary')->get()->pluck('summary')->all();
        return response()->json($summaries);
    }
}
