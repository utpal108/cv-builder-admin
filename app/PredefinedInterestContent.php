<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PredefinedInterestContent extends Model
{
    protected $fillable=['title'];
}
