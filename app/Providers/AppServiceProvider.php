<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Model\Setting;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        config([
            'settings' => Setting::all([
                'setting_key', 'setting_value'
            ])->keyBy('setting_key')
                ->transform(function ($setting) {
                    return $setting->setting_value;
                })
                ->toArray()
        ]);
    }
}
