<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PredefinedEducationContent extends Model
{
    protected $fillable=['type', 'category', 'phrase'];
}
