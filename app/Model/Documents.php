<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Documents extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'document_id',
        'document_type',
        'document_layout',
        'document_title',
        'user_id',
        'language_id',
        'template_data'
    ];

    protected $casts =[
        'template_data'=>'array'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo(LanguageSets::class, 'language_id', 'id');
    }

    public function shareable_link(){
        return $this->hasOne('App\Model\ShareableLink','document_id');
    }
}
