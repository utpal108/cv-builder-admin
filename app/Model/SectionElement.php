<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SectionElement extends Model
{
    protected $fillable=['section_id','element_name','shortcut'];

    public function element_validations(){
        return $this->hasMany('App\Model\ElementValidation', 'element_id');
    }
}
