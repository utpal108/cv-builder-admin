<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DocumentSection extends Model
{
    protected $fillable=['document_type','section_name','section_shortcut'];

    public function section_elements(){
        return $this->hasMany('App\Model\SectionElement','section_id');
    }
}
