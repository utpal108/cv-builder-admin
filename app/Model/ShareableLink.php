<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ShareableLink extends Model
{
    protected $fillable=['user_id','document_id','document_type','shareable_id','template_data','hide_sensitive'];

    protected $casts =[
        'template_data'=>'array'
    ];

    public function user_info(){
        return $this->belongsTo('App\User','user_id');
    }
}
