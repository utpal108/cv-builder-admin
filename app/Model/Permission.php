<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Permission extends \Spatie\Permission\Models\Permission
{
    public function children() {
        return $this->hasMany(Permission::class,'parent_id');
    }
    public function parent() {
        return $this->belongsTo(Permission::class,'parent_id');
    }
}
