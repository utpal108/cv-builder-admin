<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PredefinedOrganizationContent extends Model
{
    protected $fillable=['organization_name','organization_role'];
}
