<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PredefinedPublicationContent extends Model
{
    protected $fillable=['title','type','details'];
}
