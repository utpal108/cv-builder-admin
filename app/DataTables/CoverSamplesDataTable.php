<?php

namespace App\DataTables;

use App\CoverLetterSample;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;

class CoverSamplesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
//    public function dataTable($query)
//    {
//        return datatables()
//            ->eloquent($query)
//            ->addColumn('action', 'coversamples.action');
//    }

    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function ($cover_sample) {
                $buttons = '';
                $buttons .= '<a class="btn btn-primary btn-circle waves-effect waves-circle waves-float" href="' . route('cover-samples.edit', ['id' => $cover_sample->id]) . '" title="Edit"><i class="material-icons">edit</i></a>';
                $buttons .= '<form action="' . route('cover-samples.destroy', ['id' => $cover_sample->id]) . '"  id="delete-form-' . $cover_sample->id . '" method="post" style="display: inline-block">
                <input type="hidden" name="_token" value="' . csrf_token() . '">
                <input type="hidden" name="_method" value="DELETE">
                <button class="btn bg-red btn-circle waves-effect waves-circle waves-float m-l-5" onclick="return makeDeleteRequest(event, ' . $cover_sample->id . ')"  type="submit" title="Delete"><i class="material-icons">delete</i></form>
                ';
                return $buttons;
            })->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\CoverLetterSample $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(CoverLetterSample $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
//    public function html()
//    {
//        return $this->builder()
//                    ->setTableId('coversamples-table')
//                    ->columns($this->getColumns())
//                    ->minifiedAjax()
//                    ->dom('Bfrtip')
//                    ->orderBy(1)
//                    ->buttons(
//                        Button::make('create'),
//                        Button::make('export'),
//                        Button::make('print'),
//                        Button::make('reset'),
//                        Button::make('reload')
//                    );
//    }

    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => 'auto', 'printable' => false, 'title' => 'ACTION'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
//    protected function getColumns()
//    {
//        return [
//            Column::computed('action')
//                  ->exportable(false)
//                  ->printable(false)
//                  ->width(60)
//                  ->addClass('text-center'),
//            Column::make('id'),
//            Column::make('add your columns'),
//            Column::make('created_at'),
//            Column::make('updated_at'),
//        ];
//    }

    protected function getColumns()
    {
        return [
            [
                'defaultContent' => '',
                'data'           => 'DT_RowIndex',
                'name'           => 'DT_RowIndex',
                'title'          => 'SL#',
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ],
            [
                'title' => 'Cover Title',
                'name' => 'cover_title',
                'data' => 'cover_title'
            ],

        ];
    }
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'CoverSamples_' . date('YmdHis');
    }
}
