<?php

namespace App\DataTables;

use App\PredefinedWorkContent;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Html\Editor\Editor;

class PredefinedWorkDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function ($predefined_works) {
                $buttons = '';
                $buttons .= '<a class="btn btn-primary btn-circle waves-effect waves-circle waves-float" href="' . route('predefined-works.edit', ['id' => $predefined_works->id]) . '" title="Edit"><i class="material-icons">edit</i></a>';
                $buttons .= '<form action="' . route('predefined-works.destroy', ['id' => $predefined_works->id]) . '"  id="delete-form-' . $predefined_works->id . '" method="post" style="display: inline-block">
                <input type="hidden" name="_token" value="' . csrf_token() . '">
                <input type="hidden" name="_method" value="DELETE">
                <button class="btn bg-red btn-circle waves-effect waves-circle waves-float m-l-5" onclick="return makeDeleteRequest(event, ' . $predefined_works->id . ')"  type="submit" title="Delete"><i class="material-icons">delete</i></form>
                ';
                return $buttons;
            })->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\PredefinedWorkContent $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PredefinedWorkContent $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => 'auto', 'printable' => false, 'title' => 'ACTION'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'defaultContent' => '',
                'data'           => 'DT_RowIndex',
                'name'           => 'DT_RowIndex',
                'title'          => 'SL#',
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ],
            [
                'title' => 'Job Title',
                'name' => 'job_title',
                'data' => 'job_title'
            ],
            [
                'title' => 'Responsibility',
                'name' => 'responsibility',
                'data' => 'responsibility'
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PredefinedWork_' . date('YmdHis');
    }
}
