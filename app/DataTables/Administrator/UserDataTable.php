<?php

namespace App\DataTables\Administrator;


use App\User;
use Yajra\DataTables\Services\DataTable;
use PDF;

class UserDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function ($user) {
                $buttons = '';

                $buttons .= '<a class="btn btn-primary btn-circle waves-effect waves-circle waves-float" href="' . route('users.edit', ['id' => $user->id]) . '" title="Edit"><i class="material-icons">edit</i></a>';
                $buttons .= '<a class="btn btn-info btn-circle waves-effect waves-circle waves-float m-l-5" href="' . route('users.show', ['id' => $user->id]) . '" title="Show"><i class="material-icons">remove_red_eye</i></a>';
                $buttons .= '<form action="' . route('users.destroy', ['id' => $user->id]) . '"  id="delete-form-' . $user->id . '" method="post" style="display: inline-block">
<input type="hidden" name="_token" value="' . csrf_token() . '">
<input type="hidden" name="_method" value="DELETE">
<button class="btn bg-red btn-circle waves-effect waves-circle waves-float m-l-5" onclick="return makeDeleteRequest(event, ' . $user->id . ')"  type="submit" title="Delete"><i class="material-icons">delete</i></form>
';
                return $buttons;
            })->editColumn('roles', function ($user) {
                return count($user->roles) ? $user->roles[0]->name : '';
            })->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Administration\PermissionDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery()->with('roles')->where('user_type', 'admin')->orderBy('id', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => 'auto', 'printable' => false, 'title' => 'ACTION'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'defaultContent' => '',
                'data'           => 'DT_RowIndex',
                'name'           => 'DT_RowIndex',
                'title'          => 'SL#',
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ],
            [
                'title' => 'FIRST NAME',
                'name' => 'first_name',
                'data' => 'first_name'
            ],
            [
                'title' => 'LAST NAME',
                'name' => 'last_name',
                'data' => 'last_name'
            ],
            [
                'title' => 'EMAIL',
                'name' => 'email',
                'data' => 'email',
            ],
            [
                'title' => 'ROLE',
                'name' => 'roles',
                'data' => 'roles',
                'orderable'      => false,
                'searchable'     => false,
            ],
            [
                'title' => 'USER TYPE',
                'name' => 'user_type',
                'data' => 'user_type',
            ],
            [
                'title' => 'STATUS',
                'name' => 'status',
                'data' => 'status',
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'USERS_' . date('YmdHis');
    }

    public function pdf()
    {
        $excel = app('excel');
        $data = $this->getDataForExport();

        $pdf = PDF::loadView('vendor.datatables.print', [
            'data' => $data
        ]);
        return $pdf->download($this->getFilename() . '.pdf');
    }
}
