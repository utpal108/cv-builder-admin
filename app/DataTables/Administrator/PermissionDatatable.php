<?php

namespace App\DataTables\Administrator;

use App\Model\Permission;
use Yajra\DataTables\Services\DataTable;
use PDF;

class PermissionDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', function ($permission) {
                $buttons = '';

                $buttons .= '<a class="btn btn-primary btn-circle waves-effect waves-circle waves-float" href="' . route('permissions.edit', ['id' => $permission->id]) . '" title="Edit"><i class="material-icons">edit</i></a>';
                $buttons .= '<a class="btn btn-info btn-circle waves-effect waves-circle waves-float m-l-5" href="' . route('permissions.show', ['id' => $permission->id]) . '" title="Show"><i class="material-icons">remove_red_eye</i></a>';
                $buttons .= '<form action="' . route('permissions.destroy', ['id' => $permission->id]) . '"  id="delete-form-' . $permission->id . '" method="post" style="display: inline-block">
<input type="hidden" name="_token" value="' . csrf_token() . '">
<input type="hidden" name="_method" value="DELETE">
<button class="btn bg-red btn-circle waves-effect waves-circle waves-float m-l-5" onclick="return makeDeleteRequest(event, ' . $permission->id . ')"  type="submit" title="Delete"><i class="material-icons">delete</i></form>
';
                return $buttons;
            })->editColumn('parent', function ($data) {
                return $data->parent ? $data->parent->name : '';
            })
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Administration\PermissionDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Permission $model)
    {
        return $model->newQuery()->with('parent')->orderBy('id', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => 'auto', 'printable' => false, 'title' => 'ACTION'])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            [
                'defaultContent' => '',
                'data'           => 'DT_RowIndex',
                'name'           => 'DT_RowIndex',
                'title'          => 'SL#',
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => '',
            ],
            [
                'title' => 'NAME',
                'name' => 'name',
                'data' => 'name'
            ],
            [
                'title' => 'PARENT NAME',
                'name' => 'parent',
                'data' => 'parent',
                'orderable'      => false,
                'searchable'     => false,
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Permission_' . date('YmdHis');
    }

    public function pdf()
    {
        $excel = app('excel');
        $data = $this->getDataForExport();

        $pdf = PDF::loadView('vendor.datatables.print', [
            'data' => $data
        ]);
        return $pdf->download($this->getFilename() . '.pdf');
    }
}
