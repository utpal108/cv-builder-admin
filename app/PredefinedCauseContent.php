<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PredefinedCauseContent extends Model
{
    protected $fillable=['title'];
}
