<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PredefinedWorkContent extends Model
{
    protected $fillable=['category','job_title','responsibility'];
}
