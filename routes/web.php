<?php

use Illuminate\Support\Facades\Input;

Route::get('/', function () {
    return redirect()
        ->to('/login');
});
Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

Route::group(['prefix' => 'admin', 'middleware' => ['verified', 'auth', 'admin'], 'namespace' => 'Admin'], function () {
    Route::resources([
        'users' => 'Administrator\UserController',
        'roles' => 'Administrator\RoleController',
        'permissions' => 'Administrator\PermissionController',
        'tips' => 'TipsController',
        'video-lists' => 'VideoListController',
        'cover-optimizers' => 'CoverOptimizerController',
        'cover-samples' => 'CoverSampleController',
        'predefined-skills' => 'PredefinedSkillContentController',
        'predefined-soft-skills' => 'PredefinedSoftSkillContentController',
        'predefined-study-programs' => 'PredefinedEducationContentController',
        'predefined-achievements' => 'PredefinedAchievementController',
        'predefined-awards' => 'PredefinedAwardController',
        'predefined-causes' => 'PredefinedCauseController',
        'predefined-certificates' => 'PredefinedCertificateController',
        'predefined-conferences' => 'PredefinedConferenceController',
        'predefined-interests' => 'PredefinedInterestController',
        'predefined-languages' => 'PredefinedLanguageController',
        'predefined-organizations' => 'PredefinedOrganizationController',
        'predefined-projects' => 'PredefinedProjectController',
        'predefined-publications' => 'PredefinedPublicationController',
        'predefined-teachings' => 'PredefinedTeachingController',
        'predefined-technical-skills' => 'PredefinedTechnicalSkillController',
        'predefined-volunteers' => 'PredefinedVolunteerController',
        'predefined-works' => 'PredefinedWorkContentController',
        'predefined-tips' => 'PredefinedTipsController',
        'predefined-summary' => 'PredefinedSummaryContentController',
    ]);
});

Route::get('pdf-header', function(){
    return view('api.pdf.cover_letter.partials.header');
})->name('pdf.header');

Route::get('download-document/{cover_letter_id}', 'Admin\DocumentsController@downloadDocument');
