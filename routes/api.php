<?php
Route::post('v1/auth/login', 'Api\AuthController@login');

Route::group([
    'middleware' => ['cors', 'jwt.verify'],
    'prefix' => 'v1/auth',
    'namespace' => 'Api'
], function ($router) {
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('get-all-user-info', 'UserContoller@index');
    Route::post('get-user-info', 'AuthController@user_info');
    Route::get('get-user-experience-level', 'AuthController@getExperienceLevel');
    Route::post('update-user-experience-level', 'AuthController@updateExperienceLevel');
    Route::post('upload-profile-image', 'AccountController@uploadProfileImage');
});

Route::group([
    'prefix' => 'v1/auth',
    'middleware' => ['cors'],
    'namespace' => 'Api'
], function ($router) {
    Route::post('register', 'RegisterController@registerUser');
    Route::post('resend-verification-code', 'RegisterController@resendVerificationCode');
    Route::post('account-confirmation', 'RegisterController@confirmAccount');
    Route::get('/social-login/{provider}', 'RegisterController@socialLogin');
    Route::get('callback/{provider}', 'RegisterController@socialCallback');
    Route::post('magic-link', 'RegisterController@sendMagicLink');
    Route::post('password-reset', 'RegisterController@sendPasswordRestMail');
    Route::post('reset-password', 'UserPasswordResetController@callResetPassword');
});


Route::group([
    'middleware' => ['cors', 'jwt.verify'],
    'prefix' => 'v1',
    'namespace' => 'Api'
], function ($router) {
//    Route::post('upload-profile-image', 'AccountController@uploadProfileImage');
    Route::post('update-basic-profile', 'AccountController@updateBasicProfile');
    Route::post('request-password-change', 'AccountController@passwordChangeRequest');
    Route::post('change-mail', 'AccountController@changeMail');
    Route::post('account-deletion', 'AccountController@deleteAccountPermanently');
    Route::post('update-special-offer-access', 'AccountController@updateSpecialOfferAccess');
    Route::post('update-default-language', 'AccountController@updateDefaultLanguage');
    Route::post('add-documents', 'DocumentsController@addDocument');
    Route::post('get-documents', 'DocumentsController@getAllDocuments');
    Route::post('update-document', 'DocumentsController@updateDocument');
    Route::post('update-document-order-by', 'DocumentsController@updateDocumentOrder');
    Route::post('get-document', 'DocumentsController@getDocumentById');
    Route::post('duplicate-document', 'DocumentsController@duplicateDocument');
    Route::post('delete-document', 'DocumentsController@deleteDocument');
    Route::post('get-shareable-link', 'ShareableLinkController@getShareableLink');
    Route::post('create-shareable-link', 'ShareableLinkController@store');
    Route::post('publish-shareable-link', 'ShareableLinkController@publishChange');
    Route::post('change-hide-sensitive-option', 'ShareableLinkController@changeHideSensitiveOption');
    Route::post('delete-shareable-link', 'ShareableLinkController@delete');
    Route::post('delete-user-feedback', 'UserFeedbackController@delete');
//    Route::post('update-user-feedback', 'UserFeedbackController@update');
    Route::get('get-cover-letter-sections', 'DocumentsController@getCoverLetterSections');
    Route::get('get-cover-letter-videolists', 'VideoListController@get_coverletter_videolist');
    Route::get('get-cover-letter-tips', 'TipsController@get_coverletter_tips');
    Route::get('get-resume-videolists', 'VideoListController@get_resume_videolist');
    Route::get('get-resume-tips', 'TipsController@get_resume_tips');
    Route::get('get-cover-revision-optimizers', 'CoverOptimizerController@get_revisions');
    Route::get('get-cover-suggestion-optimizers', 'CoverOptimizerController@get_suggestions');
    Route::post('get-cover-predefined-contents', 'CoverPredefinedController@get_predefined_contents');
    Route::get('get-special-case-cover-letters', 'CoverPredefinedController@special_case_cover_letters');
    Route::get('get-post-interview-cover-letters', 'CoverPredefinedController@post_interview_cover_letters');
    Route::post('get-predefined-skills', 'PredefinedContentController@get_predefined_skills');
    Route::post('get-predefined-skill-categories', 'PredefinedContentController@get_predefined_skill_categories');
    Route::post('get-predefined-soft-skills', 'PredefinedContentController@get_predefined_soft_skills');
    Route::post('get-predefined-soft-skill-categories', 'PredefinedContentController@get_predefined_soft_skill_categories');
    Route::post('get-predefined-educations', 'PredefinedContentController@get_predefined_educations');
    Route::post('get-predefined-achievements', 'PredefinedContentController@get_predefined_achievements');
    Route::post('get-predefined-awards', 'PredefinedContentController@get_predefined_awards');
    Route::post('get-predefined-causes', 'PredefinedContentController@get_predefined_causes');
    Route::post('get-predefined-certificates', 'PredefinedContentController@get_predefined_certificates');
    Route::post('get-predefined-conferences', 'PredefinedContentController@get_predefined_conferences');
    Route::post('get-predefined-interests', 'PredefinedContentController@get_predefined_interests');
    Route::post('get-predefined-languages', 'PredefinedContentController@get_predefined_languages');
    Route::post('get-predefined-organizations', 'PredefinedContentController@get_predefined_organizations');
    Route::post('get-predefined-projects', 'PredefinedContentController@get_predefined_projects');
    Route::post('get-predefined-publications', 'PredefinedContentController@get_predefined_publications');
    Route::post('get-predefined-teaching', 'PredefinedContentController@get_predefined_teaching');
    Route::post('get-predefined-technical-skills', 'PredefinedContentController@get_predefined_technical_skills');
    Route::post('get-predefined-technical-skill-categories', 'PredefinedContentController@get_predefined_technical_skill_categories');
    Route::post('get-predefined-volunteers', 'PredefinedContentController@get_predefined_volunteers');
    Route::post('get-predefined-works', 'PredefinedContentController@get_predefined_works');
    Route::post('get-predefined-tips', 'PredefinedTipsController@search');
    Route::post('get-job-title-suggestions', 'PredefinedContentController@job_title_suggestions');
    Route::post('get-relevant-job-titles', 'PredefinedContentController@get_relevant_job_titles');
    Route::post('get-category-from-job-titles', 'PredefinedContentController@get_category_from_job_titles');
    Route::post('get-predefined-summary-categories', 'PredefinedContentController@get_predefined_summary_categories');
    Route::get('get-predefined-summary-job-title', 'PredefinedContentController@get_predefined_summary_job_title');
    Route::post('get-predefined-summary-by-job_title', 'PredefinedContentController@get_predefined_summary_by_job_title');
    Route::post('get-predefined-summary-by-category', 'PredefinedContentController@get_predefined_summary_by_category');
    Route::post('get-summary-job-title-suggestions', 'PredefinedContentController@summary_job_title_suggestions');

    //resources controller
    Route::resources([
        'language-sets'=>'LanguageSetsController'
    ]);

    Route::post('add-new-subscription', 'UserSubscriptionController@create');
    Route::post('update-subscription', 'UserSubscriptionController@update');
    Route::post('delete-subscription', 'UserSubscriptionController@delete');

    Route::group(['middleware'=>['isAdmin']],function (){
        Route::post('add-user-subscription', 'SubscriptionController@create');
        Route::post('update-user-subscription', 'SubscriptionController@update');
        Route::post('delete-user-subscription', 'SubscriptionController@delete');
    });
});

Route::group([
    'middleware' => ['cors',],
    'prefix' => 'v1',
    'namespace' => 'Api'
], function ($router) {
    Route::get('download-document/{cover_letter_id}', 'DocumentsController@downloadDocument');
    Route::post('get-shareable-data', 'ShareableLinkController@getShareableData');
    Route::post('add-secret-key', 'UserFeedbackController@addSecrectKey');
    Route::post('update-user-feedback', 'UserFeedbackController@update');

});

Route::options('{any}', ['middleware' => ['cors'], function () { return response(['status' => 'success']); }])->where('any', '.*');
