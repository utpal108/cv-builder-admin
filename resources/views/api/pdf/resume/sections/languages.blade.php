<style>
    .ic-resume-language-label-field{
        color: {{ $document->template_data['template_data']['themes']['color'] ?? '#909090' }}
    }
</style>
<div id="ic-resume-language" class="ic-resume-single-slot {{ ($document->template_data['template_data']['sections']['section_data']['languages']['show_infographics']) ? 'infographics-show' : '' }}" data-style="{{ $document->template_data['template_data']['sections']['section_data']['languages']['progress_bar_type'] }}">
    <div class="ic-resume-language-header ic-resume-single-slot-header ic-resume-menu-slot-main">
        <div class="ic-resume-language-header-holder ic-resume-single-slot-header-holder">
            <div class="ic-resume-language-header-title ic-resume-single-slot-edit-text ic-resume-single-slot-header-title">Languages</div>
        </div>
    </div>
    <div class="ic-resume-legend-row">
        <div class="ic-resume-progress-bar-legend">
            <div class="ic-resume-legend-holder">
                <div class="ic-resume-legend">
                    <div class="ic-resume-line-dec"></div>
                    <div class="ic-resume-line-dec"></div>
                    <div class="ic-resume-line-dec"></div>
                    <div class="ic-resume-line-dec"></div>
                </div>
            </div>
        </div>
        <div class="ic-resume-progress-bar-legend">
            <div class="ic-resume-legend-holder">
                <div class="ic-resume-legend">
                    <div class="ic-resume-line-dec"></div>
                    <div class="ic-resume-line-dec"></div>
                    <div class="ic-resume-line-dec"></div>
                    <div class="ic-resume-line-dec"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="ic-resume-language-groups ic-resume-single-slot-groups">
        @foreach($document->template_data['template_data']['sections']['section_data']['languages']['data'] as $sl=>$single_language)
            <div class="ic-resume-language-group ic-resume-single-slot-group ic-resume-single-slot-content-text-main ic-resume-menu-slot-main">
                <div class="ic-resume-language-rate-holder" data-rate="{{ $single_language['ratting'] }}">
                    <div class="ic-resume-language-rate-shape">
                        <div class="ic-resume-language-rate-inner"></div>
                    </div>
                    <div class="ic-resume-language-piecart-shape">
                        <div class="ic-resume-language-piecart-line-1"></div>
                        <div class="ic-resume-language-piecart-line-2"></div>
                        <div class="ic-resume-language-piecart-line-3"></div>
                        <div class="ic-resume-language-piecart-line-4"></div>
                        <div class="ic-resume-language-piecart-line-5"></div>
                        <div class="ic-resume-language-piecart-bottom"></div>
                    </div>
                    <div class="ic-resume-language-rate">
                        <ul class="ic-resume-language-rates-inner">
                            <li class="ic-resume-language-rate-item active" data-rate="1"><span>1/5</span></li>
                            <li class="ic-resume-language-rate-item" data-rate="2"><span>2/5</span></li>
                            <li class="ic-resume-language-rate-item" data-rate="3"><span>3/5</span></li>
                            <li class="ic-resume-language-rate-item" data-rate="4"><span>4/5</span></li>
                            <li class="ic-resume-language-rate-item" data-rate="5"><span>5/5</span></li>
                        </ul>
                    </div>
                </div>
                @php
                    $right_rotate=0;
                    $left_rotate=0;
                    if ($single_language['slider_progress']>50){
                        $right_rotate=180;
                        $left_rotate=($single_language['slider_progress']-50)*3.6;
                    }
                    else{
                        $right_rotate=$single_language['slider_progress']*3.6;
                    }
                @endphp
                <div class="ic-resume-round-slider" >
                    <div class="ic-resume-round-line-1">
                        <div class="ic-resume-round-line-2">
                            <div class="ic-resume-round-hanlde" style="-webkit-transform: rotate({{ $single_language['slider_progress']*3.6 }}deg);transform: rotate({{ $single_language['slider_progress']*3.6 }}deg);">
                                <div class="ic-resume-round-arrow"></div>
                            </div>
                            <div class="ic-resume-round-slider-inner">
                                <div class="progress-value">{{ $single_language['slider_progress'] }}</div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <span class="progress-left">
                            <span class="progress-bar" style="-webkit-transform: rotate({{ $left_rotate }}deg);transform: rotate({{ $left_rotate }}deg); {{ ($left_rotate==0) ? 'background:transparent;' : '' }}"></span>
                        </span>
                        <span class="progress-right">
                            <span class="progress-bar" style="-webkit-transform: rotate({{ $right_rotate }}deg);transform: rotate({{ $right_rotate }}deg);"></span>
                        </span>
                    </div>
                </div>
                <div class="ic-resume-single-slot-group-header">
                    <div class="ic-resume-language-content ic-resume-single-slot-content-text">
                        <div class="ic-resume-language-titlex ic-resume-single-slot-titlex ic-resume-single-slot-edit-text">
                            <div class="ic-resume-language-titlex-text ic-resume-single-slot-titlex-text">
                                {{ $single_language['language_title'] ?? 'Language' }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ic-resume-language-label-holder">
                    <div class="ic-resume-language-label-field">{{ $document->template_data['template_data']['sections']['section_data']['languages']['proficiency_ratting'][$single_language['ratting']] ?? 0 }}</div>
                </div>
                <div class="ic-resume-rating">
                    <ul class="ic-resume-rating-stars ic-resume-editor-star">
                        <li class="star {{ ($single_language['ratting']>= 1) ? 'selected' : '' }}"  data-value="1">
                            <div class="star-main">
                                <svg version="1.1" class="ic-resume-start-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 482.829 482.829" style="enable-background:new 0 0 482.829 482.829;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path d="M8.538,201.486c39.093,35.828,80.379,68.967,125.332,97.159c-14.82,50.318-30.976,100.194-48.287,149.715
                                                  c-3.171,9.074,2.963,15.34,10.296,16.747c3.671,2.233,8.34,2.498,13.114-0.909c43.445-31.026,88.065-60.352,132.004-90.678
                                                  c44.43,32.646,87.856,66.618,134.387,96.289c10.308,6.581,20.017-2.316,20.626-11.781c0.513-2.062,0.6-4.347-0.025-6.865
                                                  c-12.288-49.982-26.726-99.224-48.905-145.677c47.728-30.499,86.34-72.39,130.864-107.191c8.353-6.525,4.905-15.965-2.194-20.467
                                                  c-2.254-2.892-5.737-4.883-10.526-4.883H315.296c-20.043-50.403-40.497-100.633-56.782-152.418
                                                  c-4.845-15.419-27.048-11.077-27.683,2.453c-23.354,49.835-44.788,100.399-57.262,154.094
                                                  c-53.349-0.338-106.676-2.564-160.03-2.725C-2.674,174.301-4.307,196.771,8.538,201.486z M180.466,205.21
                                                  c1.236,0.005,2.371-0.147,3.435-0.386c6.21,0.898,12.637-1.919,14.3-9.912c9.569-45.907,26.052-89.261,45.011-131.809
                                                  c14.922,42.533,31.964,84.279,48.573,126.201c0.051,0.134,0.133,0.233,0.193,0.363c1.011,6.012,5.454,11.336,13.335,11.336
                                                  h125.649c-34.729,30.168-67.604,62.518-107.476,86.079c-2.955,1.747-4.829,4.042-5.84,6.546
                                                  c-2.498,3.844-3.271,8.841-0.594,14.051c19.393,37.795,32.575,77.855,43.574,118.662c-37.399-25.726-73.301-53.532-109.954-80.344
                                                  c-1.046-0.762-2.079-1.3-3.11-1.727c-3.781-2.641-8.752-3.22-13.858,0.325c-35.914,24.923-72.346,49.084-108.285,73.955
                                                  c13.363-39.694,25.989-79.617,37.714-119.83c1.872-6.423-0.645-11.466-4.801-14.421c-0.95-2.671-2.796-5.149-5.855-7.038
                                                  c-35.635-21.963-68.736-47.162-100.283-74.408C94.953,203.614,137.702,205.079,180.466,205.21z"/>
                                                </g>
                                            </g>
                                            </svg>
                                <svg class="ic-resume-start-2" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.481 19.481" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 19.481 19.481">
                                    <g>
                                        <path d="m10.201,.758l2.478,5.865 6.344,.545c0.44,0.038 0.619,0.587 0.285,0.876l-4.812,4.169 1.442,6.202c0.1,0.431-0.367,0.77-0.745,0.541l-5.452-3.288-5.452,3.288c-0.379,0.228-0.845-0.111-0.745-0.541l1.442-6.202-4.813-4.17c-0.334-0.289-0.156-0.838 0.285-0.876l6.344-.545 2.478-5.864c0.172-0.408 0.749-0.408 0.921,0z"/>
                                    </g>
                                </svg>
                                <div class="inner-star"></div>
                            </div>
                        </li>
                        <li class="star {{ ($single_language['ratting']>= 2) ? 'selected' : '' }}"  data-value="2">
                            <div class="star-main">
                                <svg version="1.1" class="ic-resume-start-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 482.829 482.829" style="enable-background:new 0 0 482.829 482.829;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path d="M8.538,201.486c39.093,35.828,80.379,68.967,125.332,97.159c-14.82,50.318-30.976,100.194-48.287,149.715
                                                  c-3.171,9.074,2.963,15.34,10.296,16.747c3.671,2.233,8.34,2.498,13.114-0.909c43.445-31.026,88.065-60.352,132.004-90.678
                                                  c44.43,32.646,87.856,66.618,134.387,96.289c10.308,6.581,20.017-2.316,20.626-11.781c0.513-2.062,0.6-4.347-0.025-6.865
                                                  c-12.288-49.982-26.726-99.224-48.905-145.677c47.728-30.499,86.34-72.39,130.864-107.191c8.353-6.525,4.905-15.965-2.194-20.467
                                                  c-2.254-2.892-5.737-4.883-10.526-4.883H315.296c-20.043-50.403-40.497-100.633-56.782-152.418
                                                  c-4.845-15.419-27.048-11.077-27.683,2.453c-23.354,49.835-44.788,100.399-57.262,154.094
                                                  c-53.349-0.338-106.676-2.564-160.03-2.725C-2.674,174.301-4.307,196.771,8.538,201.486z M180.466,205.21
                                                  c1.236,0.005,2.371-0.147,3.435-0.386c6.21,0.898,12.637-1.919,14.3-9.912c9.569-45.907,26.052-89.261,45.011-131.809
                                                  c14.922,42.533,31.964,84.279,48.573,126.201c0.051,0.134,0.133,0.233,0.193,0.363c1.011,6.012,5.454,11.336,13.335,11.336
                                                  h125.649c-34.729,30.168-67.604,62.518-107.476,86.079c-2.955,1.747-4.829,4.042-5.84,6.546
                                                  c-2.498,3.844-3.271,8.841-0.594,14.051c19.393,37.795,32.575,77.855,43.574,118.662c-37.399-25.726-73.301-53.532-109.954-80.344
                                                  c-1.046-0.762-2.079-1.3-3.11-1.727c-3.781-2.641-8.752-3.22-13.858,0.325c-35.914,24.923-72.346,49.084-108.285,73.955
                                                  c13.363-39.694,25.989-79.617,37.714-119.83c1.872-6.423-0.645-11.466-4.801-14.421c-0.95-2.671-2.796-5.149-5.855-7.038
                                                  c-35.635-21.963-68.736-47.162-100.283-74.408C94.953,203.614,137.702,205.079,180.466,205.21z"/>
                                                </g>
                                            </g>
                                            </svg>
                                <svg class="ic-resume-start-2" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.481 19.481" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 19.481 19.481">
                                    <g>
                                        <path d="m10.201,.758l2.478,5.865 6.344,.545c0.44,0.038 0.619,0.587 0.285,0.876l-4.812,4.169 1.442,6.202c0.1,0.431-0.367,0.77-0.745,0.541l-5.452-3.288-5.452,3.288c-0.379,0.228-0.845-0.111-0.745-0.541l1.442-6.202-4.813-4.17c-0.334-0.289-0.156-0.838 0.285-0.876l6.344-.545 2.478-5.864c0.172-0.408 0.749-0.408 0.921,0z"/>
                                    </g>
                                </svg>
                                <div class="inner-star"></div>
                            </div>
                        </li>
                        <li class="star {{ ($single_language['ratting']>= 3) ? 'selected' : '' }}"  data-value="3">
                            <div class="star-main">
                                <svg version="1.1" class="ic-resume-start-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 482.829 482.829" style="enable-background:new 0 0 482.829 482.829;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path d="M8.538,201.486c39.093,35.828,80.379,68.967,125.332,97.159c-14.82,50.318-30.976,100.194-48.287,149.715
                                                  c-3.171,9.074,2.963,15.34,10.296,16.747c3.671,2.233,8.34,2.498,13.114-0.909c43.445-31.026,88.065-60.352,132.004-90.678
                                                  c44.43,32.646,87.856,66.618,134.387,96.289c10.308,6.581,20.017-2.316,20.626-11.781c0.513-2.062,0.6-4.347-0.025-6.865
                                                  c-12.288-49.982-26.726-99.224-48.905-145.677c47.728-30.499,86.34-72.39,130.864-107.191c8.353-6.525,4.905-15.965-2.194-20.467
                                                  c-2.254-2.892-5.737-4.883-10.526-4.883H315.296c-20.043-50.403-40.497-100.633-56.782-152.418
                                                  c-4.845-15.419-27.048-11.077-27.683,2.453c-23.354,49.835-44.788,100.399-57.262,154.094
                                                  c-53.349-0.338-106.676-2.564-160.03-2.725C-2.674,174.301-4.307,196.771,8.538,201.486z M180.466,205.21
                                                  c1.236,0.005,2.371-0.147,3.435-0.386c6.21,0.898,12.637-1.919,14.3-9.912c9.569-45.907,26.052-89.261,45.011-131.809
                                                  c14.922,42.533,31.964,84.279,48.573,126.201c0.051,0.134,0.133,0.233,0.193,0.363c1.011,6.012,5.454,11.336,13.335,11.336
                                                  h125.649c-34.729,30.168-67.604,62.518-107.476,86.079c-2.955,1.747-4.829,4.042-5.84,6.546
                                                  c-2.498,3.844-3.271,8.841-0.594,14.051c19.393,37.795,32.575,77.855,43.574,118.662c-37.399-25.726-73.301-53.532-109.954-80.344
                                                  c-1.046-0.762-2.079-1.3-3.11-1.727c-3.781-2.641-8.752-3.22-13.858,0.325c-35.914,24.923-72.346,49.084-108.285,73.955
                                                  c13.363-39.694,25.989-79.617,37.714-119.83c1.872-6.423-0.645-11.466-4.801-14.421c-0.95-2.671-2.796-5.149-5.855-7.038
                                                  c-35.635-21.963-68.736-47.162-100.283-74.408C94.953,203.614,137.702,205.079,180.466,205.21z"/>
                                                </g>
                                            </g>
                                            </svg>
                                <svg class="ic-resume-start-2" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.481 19.481" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 19.481 19.481">
                                    <g>
                                        <path d="m10.201,.758l2.478,5.865 6.344,.545c0.44,0.038 0.619,0.587 0.285,0.876l-4.812,4.169 1.442,6.202c0.1,0.431-0.367,0.77-0.745,0.541l-5.452-3.288-5.452,3.288c-0.379,0.228-0.845-0.111-0.745-0.541l1.442-6.202-4.813-4.17c-0.334-0.289-0.156-0.838 0.285-0.876l6.344-.545 2.478-5.864c0.172-0.408 0.749-0.408 0.921,0z"/>
                                    </g>
                                </svg>
                                <div class="inner-star"></div>
                            </div>
                        </li>
                        <li class="star {{ ($single_language['ratting']>= 4) ? 'selected' : '' }}"  data-value="4">
                            <div class="star-main">
                                <svg version="1.1" class="ic-resume-start-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 482.829 482.829" style="enable-background:new 0 0 482.829 482.829;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path d="M8.538,201.486c39.093,35.828,80.379,68.967,125.332,97.159c-14.82,50.318-30.976,100.194-48.287,149.715
                                                  c-3.171,9.074,2.963,15.34,10.296,16.747c3.671,2.233,8.34,2.498,13.114-0.909c43.445-31.026,88.065-60.352,132.004-90.678
                                                  c44.43,32.646,87.856,66.618,134.387,96.289c10.308,6.581,20.017-2.316,20.626-11.781c0.513-2.062,0.6-4.347-0.025-6.865
                                                  c-12.288-49.982-26.726-99.224-48.905-145.677c47.728-30.499,86.34-72.39,130.864-107.191c8.353-6.525,4.905-15.965-2.194-20.467
                                                  c-2.254-2.892-5.737-4.883-10.526-4.883H315.296c-20.043-50.403-40.497-100.633-56.782-152.418
                                                  c-4.845-15.419-27.048-11.077-27.683,2.453c-23.354,49.835-44.788,100.399-57.262,154.094
                                                  c-53.349-0.338-106.676-2.564-160.03-2.725C-2.674,174.301-4.307,196.771,8.538,201.486z M180.466,205.21
                                                  c1.236,0.005,2.371-0.147,3.435-0.386c6.21,0.898,12.637-1.919,14.3-9.912c9.569-45.907,26.052-89.261,45.011-131.809
                                                  c14.922,42.533,31.964,84.279,48.573,126.201c0.051,0.134,0.133,0.233,0.193,0.363c1.011,6.012,5.454,11.336,13.335,11.336
                                                  h125.649c-34.729,30.168-67.604,62.518-107.476,86.079c-2.955,1.747-4.829,4.042-5.84,6.546
                                                  c-2.498,3.844-3.271,8.841-0.594,14.051c19.393,37.795,32.575,77.855,43.574,118.662c-37.399-25.726-73.301-53.532-109.954-80.344
                                                  c-1.046-0.762-2.079-1.3-3.11-1.727c-3.781-2.641-8.752-3.22-13.858,0.325c-35.914,24.923-72.346,49.084-108.285,73.955
                                                  c13.363-39.694,25.989-79.617,37.714-119.83c1.872-6.423-0.645-11.466-4.801-14.421c-0.95-2.671-2.796-5.149-5.855-7.038
                                                  c-35.635-21.963-68.736-47.162-100.283-74.408C94.953,203.614,137.702,205.079,180.466,205.21z"/>
                                                </g>
                                            </g>
                                            </svg>
                                <svg class="ic-resume-start-2" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.481 19.481" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 19.481 19.481">
                                    <g>
                                        <path d="m10.201,.758l2.478,5.865 6.344,.545c0.44,0.038 0.619,0.587 0.285,0.876l-4.812,4.169 1.442,6.202c0.1,0.431-0.367,0.77-0.745,0.541l-5.452-3.288-5.452,3.288c-0.379,0.228-0.845-0.111-0.745-0.541l1.442-6.202-4.813-4.17c-0.334-0.289-0.156-0.838 0.285-0.876l6.344-.545 2.478-5.864c0.172-0.408 0.749-0.408 0.921,0z"/>
                                    </g>
                                </svg>
                                <div class="inner-star"></div>
                            </div>
                        </li>
                        <li class="star {{ ($single_language['ratting']>= 5) ? 'selected' : '' }}"  data-value="5">
                            <div class="star-main">
                                <svg version="1.1" class="ic-resume-start-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 482.829 482.829" style="enable-background:new 0 0 482.829 482.829;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path d="M8.538,201.486c39.093,35.828,80.379,68.967,125.332,97.159c-14.82,50.318-30.976,100.194-48.287,149.715
                                                  c-3.171,9.074,2.963,15.34,10.296,16.747c3.671,2.233,8.34,2.498,13.114-0.909c43.445-31.026,88.065-60.352,132.004-90.678
                                                  c44.43,32.646,87.856,66.618,134.387,96.289c10.308,6.581,20.017-2.316,20.626-11.781c0.513-2.062,0.6-4.347-0.025-6.865
                                                  c-12.288-49.982-26.726-99.224-48.905-145.677c47.728-30.499,86.34-72.39,130.864-107.191c8.353-6.525,4.905-15.965-2.194-20.467
                                                  c-2.254-2.892-5.737-4.883-10.526-4.883H315.296c-20.043-50.403-40.497-100.633-56.782-152.418
                                                  c-4.845-15.419-27.048-11.077-27.683,2.453c-23.354,49.835-44.788,100.399-57.262,154.094
                                                  c-53.349-0.338-106.676-2.564-160.03-2.725C-2.674,174.301-4.307,196.771,8.538,201.486z M180.466,205.21
                                                  c1.236,0.005,2.371-0.147,3.435-0.386c6.21,0.898,12.637-1.919,14.3-9.912c9.569-45.907,26.052-89.261,45.011-131.809
                                                  c14.922,42.533,31.964,84.279,48.573,126.201c0.051,0.134,0.133,0.233,0.193,0.363c1.011,6.012,5.454,11.336,13.335,11.336
                                                  h125.649c-34.729,30.168-67.604,62.518-107.476,86.079c-2.955,1.747-4.829,4.042-5.84,6.546
                                                  c-2.498,3.844-3.271,8.841-0.594,14.051c19.393,37.795,32.575,77.855,43.574,118.662c-37.399-25.726-73.301-53.532-109.954-80.344
                                                  c-1.046-0.762-2.079-1.3-3.11-1.727c-3.781-2.641-8.752-3.22-13.858,0.325c-35.914,24.923-72.346,49.084-108.285,73.955
                                                  c13.363-39.694,25.989-79.617,37.714-119.83c1.872-6.423-0.645-11.466-4.801-14.421c-0.95-2.671-2.796-5.149-5.855-7.038
                                                  c-35.635-21.963-68.736-47.162-100.283-74.408C94.953,203.614,137.702,205.079,180.466,205.21z"/>
                                                </g>
                                            </g>
                                            </svg>
                                <svg class="ic-resume-start-2" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.481 19.481" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 19.481 19.481">
                                    <g>
                                        <path d="m10.201,.758l2.478,5.865 6.344,.545c0.44,0.038 0.619,0.587 0.285,0.876l-4.812,4.169 1.442,6.202c0.1,0.431-0.367,0.77-0.745,0.541l-5.452-3.288-5.452,3.288c-0.379,0.228-0.845-0.111-0.745-0.541l1.442-6.202-4.813-4.17c-0.334-0.289-0.156-0.838 0.285-0.876l6.344-.545 2.478-5.864c0.172-0.408 0.749-0.408 0.921,0z"/>
                                    </g>
                                </svg>
                                <div class="inner-star"></div>
                            </div>
                        </li>
                    </ul>
                    <div class="ic-resume-rating-slider-main">
                        <div class="ic-resume-rating-slider ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
                            <div class="ui-slider-range ui-corner-all ui-widget-header ui-slider-range-min" style="width: {{ $single_language['slider_progress'] }}%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
