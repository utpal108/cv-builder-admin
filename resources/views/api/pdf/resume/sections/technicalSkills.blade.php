<div id="ic-resume-tech-skill" class="ic-resume-single-slot {{ (!$document->template_data['template_data']['sections']['layout'] || $document->template_data['template_data']['sections']['section_data']['technicalSkills']['single_slot']) ? 'one-col-active' : 'two-col-active' }}">
    <div class="ic-resume-tech-skill-header ic-resume-single-slot-header ic-resume-menu-slot-main">
        <div class="ic-resume-tech-skill-header-holder ic-resume-single-slot-header-holder">
            <div class="ic-resume-tech-skill-header-title ic-resume-single-slot-edit-text ic-resume-single-slot-header-title">{{ $document->template_data['template_data']['sections']['section_data']['technicalSkills']['title'] ?? 'Technical Skills' }}</div>
        </div>
    </div>
    <div class="ic-resume-tech-skill-groups ic-resume-single-slot-groups">
        @foreach($document->template_data['template_data']['sections']['section_data']['technicalSkills']['data'] as $sl=>$single_skill)
            <div class="ic-resume-tech-skill-group ic-resume-single-slot-group ic-resume-single-slot-content-text-main ic-resume-menu-slot-main" id="ic-resume-tech-skill-group-{{ $sl }}">
                <div class="ic-resume-tech-skill-content ic-resume-single-slot-content-text">
                    <div class="ic-resume-tech-skill-content-inner">
                        <div class="ic-resume-tech-skill-group-name ic-resume-single-slot-group-name">
                            <div class="ic-resume-tech-skill-group-name-text ic-resume-single-slot-group-name-text">
                                <span class="ic-resume-single-slot-edit-text">{{ $single_skill['group_name'] ?? 'Group Name' }}</span>
                            </div>
                        </div>
                        <div class="ic-resume-tech-skill-skill ic-resume-single-slot-skill">
                            <div class="ic-resume-tech-skill-skill-text ic-resume-single-slot-skill-text ic-resume-single-slot-edit-text">{{ $single_skill['skills_list'] ?? 'List of Technical Skills' }}</div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
