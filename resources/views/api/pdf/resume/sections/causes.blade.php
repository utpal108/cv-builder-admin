
<div id="ic-resume-cause" class="ic-resume-single-slot {{ ($document->template_data['template_data']['sections']['section_data']['causes']['show_icon']) ? 'icon-show' : '' }}">
    <div class="ic-resume-cause-header ic-resume-single-slot-header ic-resume-menu-slot-main">
        <div class="ic-resume-cause-header-holder ic-resume-single-slot-header-holder">
            <div class="ic-resume-cause-header-title ic-resume-single-slot-edit-text ic-resume-single-slot-header-title">{{ $document->template_data['template_data']['sections']['section_data']['causes']['title'] ?? 'Causes' }}</div>
        </div>
    </div>
    <div class="ic-resume-cause-groups ic-resume-single-slot-groups">
        @foreach($document->template_data['template_data']['sections']['section_data']['causes']['data'] as $index=>$single_cause)
            <div class="ic-resume-cause-group ic-resume-single-slot-group ic-resume-single-slot-content-text-main ic-resume-menu-slot-main" id="ic-resume-cause-group-{{ $index }}">
                <div class="ic-resume-cause-icon ic-resume-single-slot-icon">
                    @include('api.pdf.resume.option_icons.forground.' .str_replace('.','_',$single_cause['icon']))
                </div>
                <div class="ic-resume-cause-content ic-resume-single-slot-content-text">
                    <div class="ic-resume-cause-icontitle ic-resume-single-slot-icontitle ic-resume-single-slot-edit-text">
                        <div class="ic-resume-cause-icontitle-text ic-resume-single-slot-icontitle-text">
                            {{ $single_cause['cause'] ?? 'Cause' }}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
