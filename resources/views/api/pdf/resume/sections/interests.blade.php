<div id="ic-resume-interest" class="ic-resume-single-slot {{ ($document->template_data['template_data']['sections']['section_data']['interests']['show_icon']) ? 'icon-show' : '' }}">
    <div class="ic-resume-interest-header ic-resume-single-slot-header ic-resume-menu-slot-main">
        <div class="ic-resume-interest-header-holder ic-resume-single-slot-header-holder">
            <div class="ic-resume-interest-header-title ic-resume-single-slot-edit-text ic-resume-single-slot-header-title">{{ $document->template_data['template_data']['sections']['section_data']['interests']['title'] ?? 'Interests' }}</div>
        </div>
    </div>
    <div class="ic-resume-interest-groups ic-resume-single-slot-groups">
        @foreach($document->template_data['template_data']['sections']['section_data']['interests']['data'] as $sl=>$single_interest)
            <div class="ic-resume-interest-group ic-resume-single-slot-group ic-resume-single-slot-content-text-main ic-resume-menu-slot-main" id="ic-resume-interest-group-{{ $sl }}">
                <div class="ic-resume-interest-icon ic-resume-single-slot-icon">
                    @include('api.pdf.resume.option_icons.forground.' .str_replace('.','_',$single_interest['icon']))
                </div>
                <div class="ic-resume-interest-content ic-resume-single-slot-content-text">
                    <div class="ic-resume-interest-icontitle ic-resume-single-slot-icontitle ic-resume-single-slot-edit-text">
                        <div class="ic-resume-interest-icontitle-text ic-resume-single-slot-icontitle-text">
                            {{ $single_interest['interest'] ?? 'Interest' }}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
