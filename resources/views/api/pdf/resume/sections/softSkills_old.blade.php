<style>
    #ic-resume-soft-skill.highlight-active .ic-resume-skill-group.ic-resume-single-slot-group .ic-resume-single-slot-group-header, #ic-resume-skill.highlight-active .ic-resume-skill-group.ic-resume-single-slot-group .ic-resume-single-slot-group-header{
        background: {{ $document->template_data['template_data']['themes']['color'] ?? '#0063D4' }};
    }
</style>
<div id="ic-resume-soft-skill" class="ic-resume-single-slot {{ ($document->template_data['template_data']['sections']['section_data']['softSkills']['show_infographics']) ? 'infographics-show' : '' }} {{ ($document->template_data['template_data']['sections']['section_data']['softSkills']['is_highlighted']) ? 'highlight-active' : '' }}" data-style="{{ $document->template_data['template_data']['sections']['section_data']['softSkills']['progress_bar_type'] }}">
    <div class="ic-resume-skill-header ic-resume-single-slot-header ic-resume-menu-slot-main">
        <div class="ic-resume-skill-header-holder ic-resume-single-slot-header-holder">
            <div class="ic-resume-skill-header-title ic-resume-single-slot-edit-text ic-resume-single-slot-header-title">{{ $document->template_data['template_data']['sections']['section_data']['softSkills']['title'] ?? 'Soft Skills' }}</div>
        </div>
    </div>
    <div class="ic-resume-legend-row">
        <div class="ic-resume-progress-bar-legend">
            <div class="ic-resume-legend-holder">
                <div class="ic-resume-legend">
                    <div class="ic-resume-line-dec"></div>
                    <div class="ic-resume-line-dec"></div>
                    <div class="ic-resume-line-dec"></div>
                    <div class="ic-resume-line-dec"></div>
                </div>
            </div>
        </div>
        <div class="ic-resume-progress-bar-legend">
            <div class="ic-resume-legend-holder">
                <div class="ic-resume-legend">
                    <div class="ic-resume-line-dec"></div>
                    <div class="ic-resume-line-dec"></div>
                    <div class="ic-resume-line-dec"></div>
                    <div class="ic-resume-line-dec"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="ic-resume-skill-groups ic-resume-single-slot-groups">
        @foreach($document->template_data['template_data']['sections']['section_data']['softSkills']['data'] as $sl=>$single_skill)
            <div class="ic-resume-skill-group ic-resume-single-slot-group ic-resume-single-slot-content-text-main ic-resume-menu-slot-main" id="ic-resume-skill-group-{{ $sl }}">
                <div class="ic-resume-single-slot-group-header">
                    <div class="ic-resume-skill-content ic-resume-single-slot-content-text">
                        <div class="ic-resume-skill-titlex ic-resume-single-slot-titlex ic-resume-single-slot-edit-text">
                            <div class="ic-resume-skill-titlex-text ic-resume-single-slot-titlex-text">
                                {{ $single_skill['skill_title'] ?? 'Soft Skill' }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ic-resume-rating">
                    <ul class="ic-resume-rating-stars ic-resume-editor-star">
                        <li class="star {{ ($single_skill['star_progress'] >= 1) ? 'selected' : '' }}" data-value="1">
                            <div class="star-main">
                                <div class="inner-star"></div>
                            </div>
                        </li>
                        <li class="star {{ ($single_skill['star_progress'] >= 2) ? 'selected' : '' }}" data-value="2">
                            <div class="star-main">
                                <div class="inner-star"></div>
                            </div>
                        </li>
                        <li class="star {{ ($single_skill['star_progress'] >= 3) ? 'selected' : '' }}" data-value="3">
                            <div class="star-main">
                                <div class="inner-star"></div>
                            </div>
                        </li>
                        <li class="star {{ ($single_skill['star_progress'] >= 4) ? 'selected' : '' }}" data-value="4">
                            <div class="star-main">
                                <div class="inner-star"></div>
                            </div>
                        </li>
                        <li class="star {{ ($single_skill['star_progress'] >= 5) ? 'selected' : '' }}" data-value="5">
                            <div class="star-main">
                                <div class="inner-star"></div>
                            </div>
                        </li>
                    </ul>
                    <div class="ic-resume-rating-slider-main">
                        <div class="ic-resume-rating-slider ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
                            <div class="ui-slider-range ui-corner-all ui-widget-header ui-slider-range-min" style="width: {{ $single_skill['slider_progress'] }}%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
