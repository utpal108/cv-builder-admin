<div id="ic-resume-organization" class="ic-resume-single-slot {{ (!$document->template_data['template_data']['sections']['layout'] || $document->template_data['template_data']['sections']['section_data']['organizations']['single_slot']) ? 'one-col-active' : 'two-col-active' }}">
    <div class="ic-resume-organization-header ic-resume-single-slot-header ic-resume-menu-slot-main">
        <div class="ic-resume-organization-header-holder ic-resume-single-slot-header-holder">
            <div class="ic-resume-organization-header-title ic-resume-single-slot-edit-text ic-resume-single-slot-header-title">{{ $document->template_data['template_data']['sections']['section_data']['organizations']['title'] ?? 'Organizations' }}</div>
        </div>
    </div>
    <div class="ic-resume-organization-groups ic-resume-single-slot-groups">
        @foreach($document->template_data['template_data']['sections']['section_data']['organizations']['data'] as $sl=>$single_language)
            <div class="ic-resume-organization-group ic-resume-single-slot-group ic-resume-single-slot-content-text-main ic-resume-menu-slot-main" id="ic-resume-organization-group-{{ $sl }}">
                <div class="ic-resume-organization-content ic-resume-single-slot-content-text">
                    <div class="ic-resume-organization-name ic-resume-single-slot-name">
                        <div class="ic-resume-organization-name-text ic-resume-single-slot-name-text">
                            <span class="ic-resume-single-slot-edit-text">{{ $single_language['name'] ?? 'Organization Name' }}
                                @if(!is_null($single_language['starting_year']))
                                    (
                                    @if(!is_null($single_language['starting_month']))
                                        {{ $single_language['starting_month'] }}/
                                    @endif
                                    <span>{{ $single_language['starting_year'] }}</span>
                                    @if(!is_null($single_language['is_present']))
                                        - Present )
                                    @elseif(!is_null($single_language['end_year']) && !is_null($single_language['end_month'])))
                                    - {{ $single_language['end_month']}}/ {{ $single_language['end_year'] }}
                                    @elseif(!is_null($single_language['end_year']) && is_null($single_language['end_month'])))
                                    - <span>{{ $single_language['end_year'] }}</span> )
                                    @endif
                                @endif
                            </span>
                            @if($single_language['hyperlink'] !== '' &&  $single_language['hyperlink'] !== null)
                                <div class="ic-resume-hyperlink-main">
                                    <a class="ic-resume-hyperlink-icon" href="{{ $single_language['hyperlink'] }}">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 150 150" style="enable-background:new 0 0 150 150;" xml:space="preserve"><g><g><path class="x-945d87f0-d548-11e9-97fe-1b28d2ba3ad0-q1xb_q-hyperlink" d="M112.8,85.2h-4.9c-0.7,0-1.3,0.3-2,0.7c-0.3,0.3-0.7,1-0.7,2v25.3c0,3.6-1.3,6.6-3.6,8.9c-2.6,2.6-5.6,3.6-8.9,3.6h-66c-3.6,0-6.6-1.3-8.9-3.6c-2.6-2.6-3.6-5.6-3.6-8.9v-66c0-3.6,1.3-6.6,3.6-8.9c2.6-2.6,5.6-3.6,8.9-3.6h55.8c0.7,0,1.3-0.3,2-0.7c0.3-0.3,0.7-1,0.7-2v-4.9c0-0.7-0.3-1.3-0.7-2c-0.3-0.3-1-0.7-2-0.7H26.8c-6.2,0-11.5,2.3-16.1,6.6c-4.6,4.6-6.6,9.8-6.6,16.1v66c0,6.2,2.3,11.5,6.6,16.1c4.6,4.6,9.8,6.6,16.1,6.6h66c6.2,0,11.5-2.3,16.1-6.6c4.6-4.6,6.6-9.8,6.6-16.1V87.8c0-0.7-0.3-1.3-0.7-2C114.2,85.5,113.5,85.2,112.8,85.2L112.8,85.2z"></path><path class="x-945d87f0-d548-11e9-97fe-1b28d2ba3ad0-q1xb_q-hyperlink" d="M144.4,15.9c-1-1-2.3-1.6-3.6-1.6H100c-1.3,0-2.6,0.7-3.6,1.6c-1,1-1.6,2.3-1.6,3.6c0,1.3,0.7,2.6,1.6,3.6l13.8,13.8L59,88.5c-0.7,0.7-0.7,1-0.7,2c0,0.7,0.3,1.3,0.7,2l8.9,8.9c0.7,0.7,1,0.7,2,0.7c0.7,0,1.3-0.3,2-0.7l51.5-51.5l13.8,13.8c1,1,2.3,1.6,3.6,1.6c1.3,0,2.6-0.7,3.6-1.6s1.6-2.3,1.6-3.6V19.5C145.7,17.9,145.3,16.9,144.4,15.9L144.4,15.9z"></path></g></g></svg>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                    @if(!is_null($single_language['description']) && ($single_language['description'] !== ''))
                        <div class="ic-resume-organization-desc ic-resume-single-slot-desc">
                            <div class="ic-resume-organization-desc-text ic-resume-single-slot-desc-text ic-resume-single-slot-edit-text">{{ $single_language['description'] }}</div>
                        </div>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
</div>
