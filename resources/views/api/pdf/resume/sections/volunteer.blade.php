<div id="ic-resume-volunteer" class="ic-resume-single-slot ic-resume-list-style {{ (!$document->template_data['template_data']['sections']['layout'] || $document->template_data['template_data']['sections']['section_data']['volunteers']['single_slot']) ? 'one-col-active' : 'two-col-active' }}">
    <div class="ic-resume-volunteer-header ic-resume-single-slot-header ic-resume-menu-slot-main ">
        <div class="ic-resume-volunteer-header-holder ic-resume-single-slot-header-holder">
            <div class="ic-resume-volunteer-header-title ic-resume-single-slot-edit-text ic-resume-single-slot-header-title">{{ $document->template_data['template_data']['sections']['section_data']['volunteers']['title'] ?? 'Volunteer Experience' }}</div>
        </div>
    </div>
    <div class="ic-resume-volunteer-groups ic-resume-single-slot-groups">
        @foreach($document->template_data['template_data']['sections']['section_data']['volunteers']['data'] as $index=>$single_volunteer)
            <div class="ic-resume-volunteer-group ic-resume-single-slot-group ic-resume-single-slot-content-text-main ic-resume-menu-slot-main" id="ic-resume-volunteer-group-{{ $index }}">
                <div class="ic-resume-volunteer-content ic-resume-single-slot-content-text">
                    <div class="ic-resume-volunteer-role ic-resume-single-slot-role">
                        <div class="ic-resume-volunteer-title-text ic-resume-single-slot-title-text ic-resume-single-slot-edit-text">{{ $single_volunteer['title'] ?? 'Title/Position' }}</div>
                    </div>
                    <div class="ic-resume-volunteer-place ic-resume-single-slot-place">
                        <div class="ic-resume-volunteer-place-text ic-resume-single-slot-place-text">
                            <span class="ic-resume-single-slot-edit-text">{{ $single_volunteer['company'] ?? 'Organization' }}</span>
                            @if($single_volunteer['hyperlink'] !== '' && $single_volunteer['hyperlink'] !== null)
                                <div class="ic-resume-hyperlink-main">
                                    <a href="{{ $single_volunteer['hyperlink'] }}" class="ic-resume-hyperlink-icon">
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 150 150" style="enable-background:new 0 0 150 150;" xml:space="preserve"><g><g><path class="x-945d87f0-d548-11e9-97fe-1b28d2ba3ad0-q1xb_q-hyperlink" d="M112.8,85.2h-4.9c-0.7,0-1.3,0.3-2,0.7c-0.3,0.3-0.7,1-0.7,2v25.3c0,3.6-1.3,6.6-3.6,8.9c-2.6,2.6-5.6,3.6-8.9,3.6h-66c-3.6,0-6.6-1.3-8.9-3.6c-2.6-2.6-3.6-5.6-3.6-8.9v-66c0-3.6,1.3-6.6,3.6-8.9c2.6-2.6,5.6-3.6,8.9-3.6h55.8c0.7,0,1.3-0.3,2-0.7c0.3-0.3,0.7-1,0.7-2v-4.9c0-0.7-0.3-1.3-0.7-2c-0.3-0.3-1-0.7-2-0.7H26.8c-6.2,0-11.5,2.3-16.1,6.6c-4.6,4.6-6.6,9.8-6.6,16.1v66c0,6.2,2.3,11.5,6.6,16.1c4.6,4.6,9.8,6.6,16.1,6.6h66c6.2,0,11.5-2.3,16.1-6.6c4.6-4.6,6.6-9.8,6.6-16.1V87.8c0-0.7-0.3-1.3-0.7-2C114.2,85.5,113.5,85.2,112.8,85.2L112.8,85.2z"></path><path class="x-945d87f0-d548-11e9-97fe-1b28d2ba3ad0-q1xb_q-hyperlink" d="M144.4,15.9c-1-1-2.3-1.6-3.6-1.6H100c-1.3,0-2.6,0.7-3.6,1.6c-1,1-1.6,2.3-1.6,3.6c0,1.3,0.7,2.6,1.6,3.6l13.8,13.8L59,88.5c-0.7,0.7-0.7,1-0.7,2c0,0.7,0.3,1.3,0.7,2l8.9,8.9c0.7,0.7,1,0.7,2,0.7c0.7,0,1.3-0.3,2-0.7l51.5-51.5l13.8,13.8c1,1,2.3,1.6,3.6,1.6c1.3,0,2.6-0.7,3.6-1.6s1.6-2.3,1.6-3.6V19.5C145.7,17.9,145.3,16.9,144.4,15.9L144.4,15.9z"></path></g></g></svg>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                    @if($single_volunteer['starting_year'])
                    <div class="ic-resume-volunteer-period ic-resume-single-slot-period">
                        <div class="ic-resume-volunteer-period-class ic-resume-single-slot-period-class ic-resume-single-slot-edit-text">
                            @if(!is_null($single_volunteer['starting_year']))
                                @if(!is_null($single_volunteer['starting_month']))
                                    {{ $single_volunteer['starting_month'] }}/
                                @endif
                                <span>{{ $single_volunteer['starting_year'] }}</span>
                                @if(!is_null($single_volunteer['is_present']))
                                    - Present
                                @elseif(!is_null($single_volunteer['end_year']) && !is_null($single_volunteer['end_month'])))
                                - {{ $single_volunteer['end_month']}}/ {{ $single_volunteer['end_year'] }}
                                @elseif(!is_null($single_volunteer['end_year']) && is_null($single_volunteer['end_month'])))
                                - <span>{{ $single_volunteer['end_year'] }}</span>
                                @endif
                            @endif
                        </div>
                    </div>
                    @endif
                    @if($single_volunteer['city'] !== `` && $single_volunteer['city'] !== null && $single_volunteer['starting_year'])
                    <div class="ic-resume-volunteer-location ic-resume-single-slot-location">
                        <div class="ic-resume-volunteer-location-text ic-resume-single-slot-location-text ic-resume-single-slot-edit-text">{{ $single_volunteer['city'] }}</div>
                    </div>
                    @endif
                    @if($single_volunteer['company_description'] !== `` && $single_volunteer['company_description'] !== null)
                    <div class="ic-resume-volunteer-desc ic-resume-single-slot-desc">
                        <div class="ic-resume-volunteer-desc-text ic-resume-single-slot-desc-text ic-resume-single-slot-edit-text">{{ $single_volunteer['company_description'] }}</div>
                    </div>
                    @endif
                    <div class="ic-resume-volunteer-task-list ic-resume-single-slot-list">
                        @if($single_volunteer['achievements_title'] !== `` && $single_volunteer['achievements_title']  !== null)
                            <div class="ic-resume-volunteer-task-header ic-resume-single-slot-list-header">
                                <div class="ic-resume-volunteer-task-after ic-resume-single-slot-list-after">
                                    <div class="ic-resume-volunteer-task-text ic-resume-single-slot-list-header-text ic-resume-single-slot-edit-text">{{ $single_volunteer['achievements_title'] }}</div>
                                </div>
                            </div>
                        @endif
                        @if((count($single_volunteer['accomplishments_list']) >1) && ($single_volunteer['accomplishments_list'][0] !== ``) && ($single_volunteer['accomplishments_list'][0] !== null))
                            <div class="ic-resume-volunteer-task-wrapper ic-resume-single-slot-list-wrapper">
                                @foreach($single_volunteer['accomplishments_list'] as $single_accomplishment_list)
                                    @if($single_accomplishment_list['element_title'] !== '' &&  !is_null($single_accomplishment_list['element_title']))
                                        <div id="ic-resume-volunteer-task-list-0" class="ic-resume-volunteer-task-list-item ic-resume-single-slot-list-item">
                                            <div class="ic-resume-volunteer-list-item-bullet ic-resume-single-slot-list-item-bullet">
                                            </div>
                                            <div class="ic-resume-volunteer-task-sub-content ic-resume-single-slot-list-sub-content">
                                                <div class="ic-resume-volunteer-task-list-text ic-resume-single-slot-list-text">
                                                    <span class="ic-resume-single-slot-edit-text">{{ $single_accomplishment_list['element_title'] }}</span>
                                                    @if($single_accomplishment_list['element_url']!== '' &&  $single_accomplishment_list['element_url'] !== null)
                                                        <div class="ic-resume-hyperlink-main">
                                                            <a href="{{ $single_accomplishment_list['element_url'] }}" class="ic-resume-hyperlink-icon">
                                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 150 150" style="enable-background:new 0 0 150 150;" xml:space="preserve"><g><g><path class="x-945d87f0-d548-11e9-97fe-1b28d2ba3ad0-q1xb_q-hyperlink" d="M112.8,85.2h-4.9c-0.7,0-1.3,0.3-2,0.7c-0.3,0.3-0.7,1-0.7,2v25.3c0,3.6-1.3,6.6-3.6,8.9c-2.6,2.6-5.6,3.6-8.9,3.6h-66c-3.6,0-6.6-1.3-8.9-3.6c-2.6-2.6-3.6-5.6-3.6-8.9v-66c0-3.6,1.3-6.6,3.6-8.9c2.6-2.6,5.6-3.6,8.9-3.6h55.8c0.7,0,1.3-0.3,2-0.7c0.3-0.3,0.7-1,0.7-2v-4.9c0-0.7-0.3-1.3-0.7-2c-0.3-0.3-1-0.7-2-0.7H26.8c-6.2,0-11.5,2.3-16.1,6.6c-4.6,4.6-6.6,9.8-6.6,16.1v66c0,6.2,2.3,11.5,6.6,16.1c4.6,4.6,9.8,6.6,16.1,6.6h66c6.2,0,11.5-2.3,16.1-6.6c4.6-4.6,6.6-9.8,6.6-16.1V87.8c0-0.7-0.3-1.3-0.7-2C114.2,85.5,113.5,85.2,112.8,85.2L112.8,85.2z"></path><path class="x-945d87f0-d548-11e9-97fe-1b28d2ba3ad0-q1xb_q-hyperlink" d="M144.4,15.9c-1-1-2.3-1.6-3.6-1.6H100c-1.3,0-2.6,0.7-3.6,1.6c-1,1-1.6,2.3-1.6,3.6c0,1.3,0.7,2.6,1.6,3.6l13.8,13.8L59,88.5c-0.7,0.7-0.7,1-0.7,2c0,0.7,0.3,1.3,0.7,2l8.9,8.9c0.7,0.7,1,0.7,2,0.7c0.7,0,1.3-0.3,2-0.7l51.5-51.5l13.8,13.8c1,1,2.3,1.6,3.6,1.6c1.3,0,2.6-0.7,3.6-1.6s1.6-2.3,1.6-3.6V19.5C145.7,17.9,145.3,16.9,144.4,15.9L144.4,15.9z"></path></g></g></svg>
                                                            </a>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endif
                    </div>
                    @if((($single_volunteer['contact_person'] !== '' && $single_volunteer['contact_person'] !== null) || ($single_volunteer['contact_info'] !== '' && $single_volunteer['contact_info'] !== null)))
                        <div class="ic-resume-volunteer-contact-holder ic-resume-single-slot-contact-holder ic-resume-single-slot-edit-text">
                            <div class="ic-resume-volunteer-contact-label ic-resume-single-slot-contact-label ic-resume-volunteer-after">
                                <div class="ic-resume-volunteer-contact-text ic-resume-single-slot-contact-text">{{ $single_volunteer['contact_title'] ?? 'Contact' }} :</div>
                            </div>
                            <div class="ic-resume-volunteer-contact-info ic-resume-single-slot-contact-info">
                                @if(($single_volunteer['contact_person'] !== '' && $single_volunteer['contact_person'] !== null))
                                    <div class="ic-resume-volunteer-contact-person-text ic-resume-single-slot-contact-person-text">{{ $single_volunteer['contact_person'] }}</div>
                                @endif
                                @if((($single_volunteer['contact_person'] !== '' && $single_volunteer['contact_person'] !== null) && ($single_volunteer['contact_info'] !== '' && $single_volunteer['contact_info'] !== null)))
                                    <div class="ic-resume-volunteer-contact-separator ic-resume-single-slot-contact-separator">–</div>
                                @endif
                                @if(($single_volunteer['contact_info'] !== '' && $single_volunteer['contact_info'] !== null))
                                    <div class="ic-resume-volunteer-contact-info-text ic-resume-single-slot-contact-info-text">{{ $single_volunteer['contact_info'] }}</div>
                                @endif
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        @endforeach
    </div>
</div>
