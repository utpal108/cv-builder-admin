<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
     y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><style
        type="text/css">    .sc0 {
            fill: {{ $document->template_data['template_data']['themes']['color'] ?? '#479099' }}
        }</style>
    <g>
        <path class="sc0"
              d="M5.1,0c-0.5,0-0.8,0.5-0.8,1c0,0,0,0.5,0.2,1.3C4.6,2.7,5,3,5.4,3c0,0,0.1,0,0.1,0C6,2.9,6.3,2.5,6.3,2 C6.1,1.3,6.1,0.8,6.1,0.8C6,0.3,5.6-0.1,5.1,0z"/>
        <path class="sc0"
              d="M30,26.5c-0.2,0.4,0,1,0.4,1.2c0.7,0.4,1.4,0.8,2.1,1.3c0.2,0.1,0.3,0.2,0.5,0.2c0.3,0,0.5-0.1,0.7-0.4 c0.3-0.4,0.2-0.9-0.2-1.2c-0.7-0.5-1.5-1-2.3-1.4C30.7,25.9,30.2,26.1,30,26.5z"/>
        <path class="sc0"
              d="M8.2,9.3C8,8.9,7.5,8.7,7.1,8.8C6.6,9,6.4,9.5,6.6,9.9c0.3,0.8,0.6,1.7,1,2.4c0.1,0.3,0.5,0.5,0.8,0.5 c0.1,0,0.2,0,0.4-0.1c0.4-0.2,0.6-0.7,0.4-1.2C8.8,10.9,8.5,10.1,8.2,9.3z"/>
        <path class="sc0"
              d="M18.5,24.2c0.3,0,0.6-0.2,0.8-0.5c0.2-0.4,0-1-0.4-1.2c-0.7-0.4-1.4-0.8-2.1-1.3c-0.4-0.3-0.9-0.2-1.2,0.2 c-0.3,0.4-0.2,0.9,0.2,1.2c0.7,0.5,1.5,1,2.3,1.4C18.2,24.2,18.3,24.2,18.5,24.2z"/>
        <path class="sc0"
              d="M43,45.6c0.1,0.4,0.5,0.6,0.8,0.6c0.1,0,0.2,0,0.2,0c0.5-0.1,0.7-0.6,0.6-1.1c-0.3-0.8-0.5-1.7-0.8-2.5 c-0.2-0.5-0.7-0.7-1.1-0.5c-0.5,0.2-0.7,0.7-0.5,1.1C42.5,44,42.8,44.8,43,45.6z"/>
        <path class="sc0"
              d="M10.3,13.9c-0.2-0.4-0.8-0.6-1.2-0.3c-0.4,0.2-0.6,0.8-0.3,1.2c0.4,0.8,0.9,1.6,1.4,2.3 c0.2,0.2,0.4,0.4,0.7,0.4c0.2,0,0.3,0,0.5-0.2c0.4-0.3,0.5-0.8,0.2-1.2C11.1,15.4,10.7,14.7,10.3,13.9z"/>
        <path class="sc0"
              d="M13.6,21.1c0.2,0.2,0.4,0.2,0.6,0.2c0.2,0,0.5-0.1,0.6-0.3c0.3-0.4,0.3-0.9-0.1-1.2c-0.6-0.5-1.2-1.1-1.7-1.8 c-0.3-0.4-0.9-0.4-1.2-0.1c-0.4,0.3-0.4,0.9-0.1,1.2C12.4,19.8,13,20.5,13.6,21.1z"/>
        <path class="sc0"
              d="M35.6,29.2c-0.4-0.3-0.9-0.3-1.2,0.1s-0.3,0.9,0.1,1.2c0.6,0.5,1.2,1.1,1.7,1.8c0.2,0.2,0.4,0.3,0.7,0.3 c0.2,0,0.4-0.1,0.6-0.2c0.4-0.3,0.4-0.9,0.1-1.2C36.9,30.4,36.2,29.8,35.6,29.2z"/>
        <path class="sc0"
              d="M39,36.3c0.2,0.3,0.5,0.4,0.8,0.4c0.2,0,0.3,0,0.4-0.1c0.4-0.2,0.6-0.8,0.3-1.2c-0.5-0.8-0.9-1.5-1.4-2.2 c-0.3-0.4-0.8-0.5-1.2-0.2c-0.4,0.3-0.5,0.8-0.2,1.2C38.1,34.9,38.6,35.6,39,36.3z"/>
        <path class="sc0"
              d="M41.3,40.8c0.1,0.3,0.5,0.5,0.8,0.5c0.1,0,0.2,0,0.3-0.1c0.4-0.2,0.7-0.7,0.5-1.1c-0.3-0.8-0.7-1.6-1.1-2.4 c-0.2-0.4-0.7-0.6-1.2-0.4c-0.4,0.2-0.6,0.7-0.4,1.2C40.6,39.3,41,40,41.3,40.8z"/>
        <path class="sc0"
              d="M6.8,4.5C6.6,4,6.2,3.7,5.7,3.8C5.2,4,4.9,4.4,5.1,4.9c0.2,0.9,0.4,1.7,0.7,2.5c0.1,0.4,0.5,0.6,0.8,0.6 c0.1,0,0.2,0,0.2,0c0.5-0.1,0.7-0.6,0.6-1.1C7.2,6.1,6.9,5.3,6.8,4.5z"/>
        <path class="sc0"
              d="M43.9,49.3c0.1,0.4,0.4,0.7,0.9,0.7c0.1,0,0.1,0,0.2,0c0.5-0.1,0.8-0.5,0.7-1c0,0-0.1-0.5-0.3-1.3 c-0.1-0.5-0.6-0.8-1-0.6c-0.5,0.1-0.8,0.6-0.6,1C43.8,48.8,43.9,49.3,43.9,49.3z"/>
        <path class="sc0"
              d="M44.9,0c-0.5-0.1-0.9,0.3-1,0.7c0,0-0.1,0.6-0.3,1.4H9.9C9.4,2.2,9,2.6,9,3.1s0.4,0.9,0.9,0.9h33.3 c-0.3,1.1-0.7,2.5-1.3,3.9H11.8c-0.5,0-0.9,0.4-0.9,0.9c0,0.5,0.4,0.9,0.9,0.9h29.4c-0.6,1.3-1.3,2.6-2.1,3.9c0,0,0,0,0,0H16 c-0.5,0-0.9,0.4-0.9,0.9s0.4,0.9,0.9,0.9h21.9c-1,1.4-2.2,2.8-3.6,4c-0.1,0-0.1,0-0.2,0H21c-0.5,0-0.9,0.4-0.9,0.9 c0,0.5,0.4,0.9,0.9,0.9h11.2c-2.1,1.5-4.5,2.7-7.4,3.4c-0.2,0.1-0.5,0.1-0.7,0.2c-0.1-0.2-0.3-0.4-0.6-0.4 c-0.8-0.2-1.6-0.4-2.4-0.6c-0.5-0.2-1,0.1-1.1,0.6c-0.2,0.5,0.1,1,0.6,1.1c0.3,0.1,0.6,0.2,0.9,0.3C7,31.2,4.4,48.8,4.4,49 c-0.1,0.5,0.3,0.9,0.7,1c0,0,0.1,0,0.1,0c0.4,0,0.8-0.3,0.9-0.8c0,0,0.1-0.5,0.2-1.2H40c0.5,0,0.9-0.4,0.9-0.9s-0.4-0.9-0.9-0.9 H6.8c0.3-1.1,0.7-2.5,1.3-3.9h30c0.5,0,0.9-0.4,0.9-0.9s-0.4-0.9-0.9-0.9H8.8c0.6-1.3,1.3-2.6,2.1-3.9h22.9c0.5,0,0.9-0.4,0.9-0.9 c0-0.5-0.4-0.9-0.9-0.9H12.2c1-1.4,2.2-2.7,3.6-3.9c0,0,0,0,0,0h13.1c0.5,0,0.9-0.4,0.9-0.9s-0.4-0.9-0.9-0.9H18.1 c2-1.4,4.4-2.5,7.1-3.1c0.1,0,0.1,0,0.2-0.1c0.1,0.1,0.2,0.2,0.4,0.2c0.8,0.1,1.6,0.3,2.4,0.6c0.1,0,0.2,0,0.3,0 c0.4,0,0.7-0.2,0.8-0.6c0.1-0.5-0.1-0.9-0.6-1.1c-0.1,0-0.2,0-0.2-0.1C43,19.2,45.6,1.2,45.6,1C45.7,0.5,45.4,0.1,44.9,0z"/>
    </g></svg>
