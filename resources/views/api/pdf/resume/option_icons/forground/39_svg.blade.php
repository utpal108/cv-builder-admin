<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
     y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><style
        type="text/css">    .sc0 {
            fill: {{ $document->template_data['template_data']['themes']['color'] ?? '#479099' }}
        }</style>
    <g>
        <path class="sc0"
              d="M49.8,46.9c0.3-0.5,0.3-1,0-1.5l-9-17.1c-0.3-0.5-0.8-0.8-1.3-0.8c0,0,0,0,0,0c-0.6,0-1.1,0.3-1.3,0.9L34,36.9 l-7.9-22c-0.2-0.6-0.8-1-1.4-1c0,0,0,0,0,0c-0.6,0-1.2,0.4-1.4,1l-6.1,16L14,24.7c-0.3-0.5-0.8-0.8-1.3-0.8c0,0,0,0,0,0 c-0.6,0-1.1,0.3-1.3,0.8L0.2,45.4c-0.3,0.5-0.2,1,0,1.5c0.3,0.5,0.8,0.7,1.3,0.7h47C49,47.7,49.5,47.4,49.8,46.9z M39.5,29.1 l2.7,5.2l-1.1,0.6L39,33.6L37.1,34L39.5,29.1z M24.7,15.3l2.6,7.2l-1.8-0.8l-2.1,1.5L22.2,22L24.7,15.3z M12.6,25.4l2.4,4.5l-0.7,1 l-1.3-1l-1.4,1l-1.4-0.9L12.6,25.4z M1.5,46.1l8-14.9l2.2,1.4l1.3-0.9l1.7,1.2l1.1-1.6l0.7,1.3l-5.1,13.5L1.5,46.1z M13,46.2 l8.6-22.6l1.7,1.7l2.4-1.7l2.2,0.9c0,0,0.1,0,0.1,0l5.1,14.1c0,0.1,0.1,0.1,0.1,0.2l-3.6,7.5L13,46.2z M36.3,35.7l2.4-0.5l2.4,1.3 l1.7-0.9l5.6,10.6l-17.3,0L36.3,35.7z"/>
        <g>
            <path class="sc0"
                  d="M22.8,6.8c-0.5-0.8-1.4-1.4-2.6-1.6c-0.6-0.8-2.2-2.8-4.5-2.9c-1.7-0.1-3.3,0.8-4.8,2.6c-1,0-3.4,0-4.3,1.5 	c-0.7,1.1-0.4,2.7,0.8,4.7C7.7,11.7,8.3,12,9,12h12c0.8,0,1.5-0.5,1.8-1.2C23.2,9.7,23.6,8.1,22.8,6.8z M21.3,10.3 	c-0.1,0.1-0.2,0.2-0.4,0.2H9c-0.1,0-0.3-0.1-0.3-0.2C7.7,8.9,7.4,7.8,7.8,7.3c0.4-0.7,1.7-0.8,2.6-0.8c0.3,0,0.5,0,0.7,0l0.4,0 	l0.3-0.3c1.3-1.6,2.5-2.4,3.8-2.4c1.1,0.1,2.1,0.8,2.7,1.4c-1.2,0.3-2.5,1.1-2.9,3c-0.1,0.4,0.2,0.8,0.6,0.9c0.1,0,0.1,0,0.2,0 	c0.3,0,0.7-0.2,0.7-0.6c0.4-2,2.3-2,2.8-1.9l0.1,0c1,0.1,1.6,0.4,1.9,0.9C22,8.4,21.6,9.7,21.3,10.3z"/>
        </g>
        <g>
            <path class="sc0"
                  d="M45.2,16.4c-0.4-0.6-1-1-1.9-1.2c-0.4-0.6-1.6-1.9-3.2-2c-1.2-0.1-2.3,0.5-3.4,1.8c-0.8,0-2.3,0.1-3,1.2 	c-0.5,0.8-0.3,2,0.6,3.4c0.3,0.4,0.8,0.7,1.3,0.7h8.1c0.6,0,1.2-0.4,1.4-1C45.5,18.5,45.8,17.3,45.2,16.4z M43.8,18.8h-8.1l0,0 	c-0.5-0.8-0.7-1.5-0.6-1.8c0.2-0.3,0.9-0.5,1.6-0.5c0.2,0,0.3,0,0.4,0l0.4,0l0.3-0.3c0.8-1,1.6-1.5,2.4-1.5c0.5,0,1,0.3,1.4,0.6 	c-0.7,0.3-1.5,0.9-1.7,2.1c-0.1,0.4,0.2,0.8,0.6,0.9c0.1,0,0.1,0,0.2,0c0.3,0,0.7-0.2,0.7-0.6c0.3-1.2,1.4-1.2,1.6-1.1l0,0 	c0.6,0.1,0.9,0.2,1.1,0.5C44.1,17.5,44.1,18.1,43.8,18.8z"/>
        </g>
    </g></svg>
