
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
     y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><style
        type="text/css">    .sc0 {
            fill: {{ $document->template_data['template_data']['themes']['color'] ?? '#479099' }}
        }</style>
    <path class="sc0"
          d="M46.1,32.9c-2-0.5-4.5-0.5-7,0c-5.7,1-9.5,4-9,6.8c0.4,2.2,3.3,3.5,7.2,3.5c1.1,0,2.4-0.1,3.7-0.3	c5.2-0.9,8.9-3.5,9.1-6.1c0,0,0,0,0-0.1v-0.1c0-0.1,0-0.1,0-0.2V2.7c0-0.5-0.2-0.9-0.6-1.2C49,1.1,48.6,1,48.1,1.1L19.7,6.5	c-0.8,0.1-1.3,0.8-1.3,1.6v31.5c-0.6-0.4-1.3-0.7-2.2-0.9c-2-0.5-4.5-0.5-7,0c-5.7,1-9.5,4-9,6.8c0.4,2.2,3.3,3.5,7.2,3.5	c1.1,0,2.4-0.1,3.7-0.3c5.2-0.9,8.9-3.5,9.1-6.1c0,0,0,0,0-0.1v-0.1c0-0.1,0-0.1,0-0.2l0-29l28.4-5.7v26.3	C47.8,33.5,47,33.2,46.1,32.9z M40.6,41.2c-4.9,0.9-8.7-0.3-9-1.8c-0.3-1.6,2.8-4,7.7-4.9c2.3-0.4,4.6-0.4,6.3,0	c1.5,0.4,2.5,1,2.6,1.8c0,0.1,0,0.1,0,0.2v0.1C48.2,38.2,45.2,40.4,40.6,41.2z M10.6,47c-4.9,0.9-8.7-0.3-9-1.8	c-0.3-1.6,2.8-4,7.7-4.9c2.3-0.4,4.6-0.4,6.3,0c1.5,0.4,2.5,1,2.6,1.8c0,0.1,0,0.1,0,0.2v0.1C18.2,44,15.2,46.2,10.6,47z M20,11.6	l0-3.4l28.4-5.5v3.2L20,11.6z"/></svg>
