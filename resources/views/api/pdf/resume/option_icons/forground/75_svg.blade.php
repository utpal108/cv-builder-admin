<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
     y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><style
        type="text/css">    .sc0 {
            fill: {{ $document->template_data['template_data']['themes']['color'] ?? '#479099' }}
        }</style>
    <path class="sc0"
          d="M25,50c13.8,0,25-11.2,25-25S38.8,0,25,0S0,11.2,0,25S11.2,50,25,50z M48.5,25c0,6-2.2,11.4-5.9,15.6	c-12.3-16-2.6-28.4-0.1-31.2C46.2,13.5,48.5,19,48.5,25z M25,1.5c6.4,0,12.2,2.6,16.5,6.8c-2.6,2.9-13.1,16.5,0,33.4	c-4.2,4.2-10.1,6.8-16.5,6.8c-6.3,0-12.1-2.5-16.3-6.6c13.7-17.3,2.4-31.2,0-33.8C12.9,4,18.7,1.5,25,1.5z M7.6,9.2	c2.3,2.4,12.8,15.1-0.1,31.6C3.8,36.6,1.5,31.1,1.5,25C1.5,18.9,3.8,13.4,7.6,9.2z"/></svg>
