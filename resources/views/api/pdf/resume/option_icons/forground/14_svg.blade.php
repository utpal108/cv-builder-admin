<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
     y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><style
        type="text/css">    .sc0 {
            fill: {{ $document->template_data['template_data']['themes']['color'] ?? '#479099' }}
        }</style>
    <g>
        <path class="sc0" d="M19.6,32.5V7.8L6.7,31.1L19.6,32.5z M18.1,30.8l-9-1l9-16.2V30.8z"/>
        <path class="sc0"
              d="M49.3,38.9h-5.9l5.3-6.6L21.8,34v-1.3l21.8-1.9L20.3,4v25.4v3.4v1.3L5.1,35.1l0.8,3.9H0.7 c-0.4,0-0.7,0.3-0.7,0.7s0.3,0.7,0.7,0.7h5.5h35.9h7.2c0.4,0,0.7-0.3,0.7-0.7S49.7,38.9,49.3,38.9z M21.8,8l18.8,21.6l-18.8,1.6 v-1.8V8z M7.4,38.9l-0.5-2.5l38.5-2.4l-4,4.9H7.4z"/>
        <path class="sc0"
              d="M23.9,41.7H8.7c-0.4,0-0.7,0.3-0.7,0.7s0.3,0.7,0.7,0.7h15.3c0.4,0,0.7-0.3,0.7-0.7S24.4,41.7,23.9,41.7z"/>
        <path class="sc0"
              d="M38.3,43.2c0.4,0,0.7-0.3,0.7-0.7s-0.3-0.7-0.7-0.7H28.1c-0.4,0-0.7,0.3-0.7,0.7s0.3,0.7,0.7,0.7H38.3z"/>
        <path class="sc0"
              d="M16,45.2c0,0.4,0.3,0.7,0.7,0.7h14.7c0.4,0,0.7-0.3,0.7-0.7s-0.3-0.7-0.7-0.7H16.7C16.3,44.5,16,44.8,16,45.2z "/>
    </g></svg>
