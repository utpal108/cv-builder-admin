<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
     y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><style
        type="text/css">    .sc0 {
            fill: {{ $document->template_data['template_data']['themes']['color'] ?? '#479099' }}
        }</style>
    <g>
        <path class="sc0"
              d="M46.6,25.7h-5v-7.1h-2.3v-1.6h4.7c0,1.1,0.9,2,2,2h0.6c1.1,0,2-0.9,2-2v-4.9c0-1.1-0.9-2-2-2h-0.6 c-1.1,0-2,0.9-2,1.9h-4.7V2.5H10.8v9.3H6.7c-0.1-1.1-0.9-1.9-2-1.9H4c-1.1,0-2,0.9-2,2v4.9c0,1.1,0.9,2,2,2h0.6c1.1,0,2-0.9,2-2 h4.1v1.6H8.5v7.1h-5c-1.9,0-3.4,1.5-3.4,3.4v15c0,1.9,1.5,3.4,3.4,3.4h43.2c1.9,0,3.4-1.5,3.4-3.4v-15C50,27.2,48.5,25.7,46.6,25.7 z M6.7,15.4v-2.1h37.2v2.1H6.7z M45.4,11.9c0-0.3,0.2-0.5,0.5-0.5h0.6c0.3,0,0.5,0.2,0.5,0.5v4.9c0,0.3-0.2,0.5-0.5,0.5h-0.6 c-0.3,0-0.5-0.2-0.5-0.5V11.9z M12.3,4h25.4v7.8H12.3V4z M5.2,16.8c0,0.3-0.2,0.5-0.5,0.5H4c-0.3,0-0.5-0.2-0.5-0.5v-4.9 c0-0.3,0.2-0.5,0.5-0.5h0.6c0.3,0,0.5,0.2,0.5,0.5V16.8z M12.3,16.9h25.4v1.6h-1.9l-0.2,0.5c0,0-1.4,3.8-10.6,3.8 c-9.2,0-10.6-3.8-10.6-3.8l-0.2-0.5h-1.9V16.9z M10,20h3.3c0.7,1.3,3.3,4.4,11.8,4.4s11.1-3.1,11.8-4.4H40v5.6H10V20z M48.5,44.1 c0,1.1-0.9,1.9-1.9,1.9H3.4c-1.1,0-1.9-0.9-1.9-1.9v-15c0-1.1,0.9-1.9,1.9-1.9h43.2c1.1,0,1.9,0.9,1.9,1.9V44.1z"/>
        <path class="sc0"
              d="M15.6,31.3h4c0.4,0,0.7-0.3,0.7-0.7c0-0.4-0.3-0.7-0.7-0.7h-4c-0.4,0-0.7,0.3-0.7,0.7 C14.8,31,15.2,31.3,15.6,31.3z"/>
        <path class="sc0"
              d="M23,31.3h4c0.4,0,0.7-0.3,0.7-0.7c0-0.4-0.3-0.7-0.7-0.7h-4c-0.4,0-0.7,0.3-0.7,0.7C22.2,31,22.6,31.3,23,31.3 z"/>
        <path class="sc0"
              d="M30,31.3h4c0.4,0,0.7-0.3,0.7-0.7c0-0.4-0.3-0.7-0.7-0.7h-4c-0.4,0-0.7,0.3-0.7,0.7C29.2,31,29.6,31.3,30,31.3 z"/>
        <path class="sc0"
              d="M11.9,33.7c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7h4c0.4,0,0.7-0.3,0.7-0.7c0-0.4-0.3-0.7-0.7-0.7H11.9 z"/>
        <path class="sc0"
              d="M19.3,39c0.4,0,0.7-0.3,0.7-0.7c0-0.4-0.3-0.7-0.7-0.7h-4c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7H19.3z "/>
        <path class="sc0"
              d="M8.4,31.3h4c0.4,0,0.7-0.3,0.7-0.7c0-0.4-0.3-0.7-0.7-0.7h-4c-0.4,0-0.7,0.3-0.7,0.7C7.6,31,8,31.3,8.4,31.3z"/>
        <path class="sc0"
              d="M11.8,37.5h-4c-0.4,0-0.7,0.3-0.7,0.7C7,38.7,7.4,39,7.8,39h4c0.4,0,0.7-0.3,0.7-0.7 C12.5,37.8,12.2,37.5,11.8,37.5z"/>
        <path class="sc0"
              d="M23.2,39h4c0.4,0,0.7-0.3,0.7-0.7c0-0.4-0.3-0.7-0.7-0.7h-4c-0.4,0-0.7,0.3-0.7,0.7C22.5,38.7,22.8,39,23.2,39 z"/>
        <path class="sc0"
              d="M35.3,37.5h-4c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7h4c0.4,0,0.7-0.3,0.7-0.7 C36,37.8,35.7,37.5,35.3,37.5z"/>
        <path class="sc0"
              d="M37.2,31.3h4c0.4,0,0.7-0.3,0.7-0.7c0-0.4-0.3-0.7-0.7-0.7h-4c-0.4,0-0.7,0.3-0.7,0.7 C36.4,31,36.8,31.3,37.2,31.3z"/>
        <path class="sc0"
              d="M42.2,39c0.4,0,0.7-0.3,0.7-0.7c0-0.4-0.3-0.7-0.7-0.7h-4c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7H42.2z "/>
        <path class="sc0"
              d="M24,34.5c0-0.4-0.3-0.7-0.7-0.7h-4c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7h4C23.7,35.2,24,34.9,24,34.5 z"/>
        <path class="sc0"
              d="M31.3,34.5c0-0.4-0.3-0.7-0.7-0.7h-4c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7h4 C31,35.2,31.3,34.9,31.3,34.5z"/>
        <path class="sc0"
              d="M33.9,33.7c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7h4c0.4,0,0.7-0.3,0.7-0.7c0-0.4-0.3-0.7-0.7-0.7H33.9 z"/>
        <path class="sc0"
              d="M42.4,41.1h-3.9c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7h3.9c0.4,0,0.7-0.3,0.7-0.7 C43.1,41.5,42.8,41.1,42.4,41.1z"/>
        <path class="sc0"
              d="M11.6,41.1H7.6c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7h3.9c0.4,0,0.7-0.3,0.7-0.7 C12.3,41.5,12,41.1,11.6,41.1z"/>
        <path class="sc0"
              d="M34.4,41.1H15.6c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7h18.9c0.4,0,0.7-0.3,0.7-0.7 C35.2,41.5,34.8,41.1,34.4,41.1z"/>
        <path class="sc0"
              d="M15.3,7.3h19.4c0.4,0,0.7-0.3,0.7-0.7c0-0.4-0.3-0.7-0.7-0.7H15.3c-0.4,0-0.7,0.3-0.7,0.7 C14.6,6.9,14.9,7.3,15.3,7.3z"/>
        <path class="sc0"
              d="M34.7,8.8H15.3c-0.4,0-0.7,0.3-0.7,0.7s0.3,0.7,0.7,0.7h19.4c0.4,0,0.7-0.3,0.7-0.7S35.1,8.8,34.7,8.8z"/>
    </g></svg>
