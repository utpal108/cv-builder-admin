<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
     y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><style
        type="text/css">    .sc0 {
            fill: {{ $document->template_data['template_data']['themes']['color'] ?? '#479099' }}
        }</style>
    <g>
        <path class="sc0"
              d="M49.1,1.6L48.9,1l-0.6-0.1C48,0.8,38-1.7,30.4,2.1l-0.2,0.1c-0.3,0.3-5.1,4.1-10.2,10 c-1.1,0.2-11.2,2.4-17,8.3l-1.2,1.3l1.7,0.4c0.2,0,3.9,0.9,6.5,5.4c-0.1,0.3-0.3,0.7-0.4,1l-2.8,4.3l2.2,2.2c0,0,0,0,0,0s0,0,0,0 l2.8,2.8c0,0,0,0.1,0.1,0.1c0,0,0.1,0.1,0.1,0.1l2.8,2.8c0,0,0,0,0,0c0,0,0,0,0,0l2.2,2.2l4.3-2.8c0.4-0.1,0.7-0.3,1-0.5 c4.3,2.6,5.2,6.3,5.2,6.4l0.4,1.8l1.3-1.3c5.5-5.5,7.8-14.8,8.2-16.8c6-5.2,9.9-10,10.2-10.3l0.1-0.2C51.7,12,49.2,2,49.1,1.6z  M5.6,20.9c3.9-3.3,9.3-5.2,12.5-6.1c-2.5,3.2-5,6.8-7,10.7C9.2,22.9,7,21.5,5.6,20.9z M10.7,30.5l0.8,0.8L9.7,33l-0.4-0.4 L10.7,30.5z M14.4,34.2L12.7,36l-1.6-1.6l1.7-1.7L14.4,34.2z M14,37.3l1.7-1.7l1.5,1.5l-1.7,1.7L14,37.3z M16.9,40.2l1.7-1.7 l0.8,0.8l-2.1,1.3L16.9,40.2z M29,44.3c-0.6-1.4-2-3.5-4.5-5.4c3.8-1.9,7.4-4.3,10.5-6.8C34.1,35.3,32.2,40.5,29,44.3z M21.2,38.3 l-7.7-7.7c0,0,0,0,0,0s0,0,0,0l-1.9-1.9C16.8,16.8,28,6.7,30.8,4.3l14.9,14.9C43.3,22,33.2,33.1,21.2,38.3z M46.7,17.4L32.5,3.3 C38.3,1,45.3,2.2,47.3,2.6C47.8,4.7,49,11.7,46.7,17.4z"/>
        <path class="sc0"
              d="M32.5,12.1c-1.4,0-2.8,0.6-3.8,1.6c-2.1,2.1-2.1,5.5,0,7.5c1,1,2.3,1.6,3.8,1.6s2.8-0.6,3.8-1.6 c2.1-2.1,2.1-5.5,0-7.5C35.3,12.7,33.9,12.1,32.5,12.1z M34.9,19.9c-1.3,1.3-3.5,1.3-4.8,0c-1.3-1.3-1.3-3.5,0-4.8 c0.6-0.6,1.5-1,2.4-1c0.9,0,1.8,0.4,2.4,1C36.2,16.4,36.2,18.5,34.9,19.9z"/>
        <path class="sc0"
              d="M11.1,38.9c-0.4-0.4-1-0.4-1.4,0l-9.5,9.5c-0.4,0.4-0.4,1,0,1.4C0.5,49.9,0.7,50,1,50s0.5-0.1,0.7-0.3l9.5-9.5 C11.5,39.9,11.5,39.3,11.1,38.9z"/>
        <path class="sc0"
              d="M1,43c0.2,0,0.5-0.1,0.7-0.3l5.9-5.9c0.4-0.4,0.4-1,0-1.4s-1-0.4-1.4,0l-5.9,5.9c-0.4,0.4-0.4,1,0,1.4 C0.5,42.9,0.8,43,1,43z"/>
        <path class="sc0"
              d="M13.3,42.4l-5.9,5.9c-0.4,0.4-0.4,1,0,1.4C7.5,49.9,7.8,50,8,50s0.5-0.1,0.7-0.3l5.9-5.9c0.4-0.4,0.4-1,0-1.4 C14.3,42,13.6,42,13.3,42.4z"/>
    </g></svg>
