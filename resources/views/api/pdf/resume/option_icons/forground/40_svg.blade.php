<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
     y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><style
        type="text/css">    .sc0 {
            fill: {{ $document->template_data['template_data']['themes']['color'] ?? '#479099' }}
        }</style>
    <g>
        <path class="sc0"
              d="M37.8,9c0.3,0,0.6,0,1-0.1c2.1-0.4,3.4-2.1,3.4-4.5c0-1.3-0.4-2.4-1.2-3.2C39.7,0,37.9,0,37.9,0H27.2l0,0 l-15,0c-0.1,0-1.9,0-3.1,1.2C8.3,2,7.9,3,7.9,4.3c0,2.4,1.3,4.1,3.4,4.5c0.3,0.1,0.6,0.1,1,0.1c0.9,0,1.7-0.2,2.3-0.7v34.4 c-2.3,0.5-3.9,2.5-3.9,4.9V50h28.9v-2.4c0-2.4-1.7-4.4-3.9-4.9V8.3C36.1,8.7,36.9,9,37.8,9z M38,47.6v1H12v-1c0-2,1.6-3.6,3.6-3.6 h18.9C36.4,44,38,45.6,38,47.6z M15.9,42.6V7.8h18.2v34.8H15.9z M35.1,4.2c-0.6,0.6-0.8,1.3-0.7,2.2H15.6c0.1-0.8-0.1-1.6-0.7-2.2 c-0.7-0.7-1.8-0.9-2.9-0.4c-0.4,0.2-0.5,0.6-0.4,0.9c0.2,0.4,0.6,0.5,0.9,0.4c0.5-0.2,1-0.2,1.3,0.1c0.3,0.4,0.4,1,0.1,1.6 c-0.2,0.5-1.3,0.9-2.5,0.7c-0.7-0.1-2.2-0.7-2.2-3.1c0-0.9,0.3-1.6,0.7-2.1c0.8-0.8,2.1-0.8,2.1-0.8h17c0,0,0,0,0,0l8.7,0 c0,0,1.3,0,2.1,0.8c0.5,0.5,0.8,1.2,0.8,2.1c0,2.4-1.6,3-2.2,3.1c-1.2,0.2-2.2-0.2-2.5-0.7c-0.3-0.6-0.2-1.2,0.1-1.6 c0.3-0.3,0.8-0.3,1.3-0.1c0.4,0.2,0.8,0,0.9-0.4c0.2-0.4,0-0.8-0.4-0.9C36.9,3.3,35.8,3.5,35.1,4.2z"/>
        <path class="sc0"
              d="M19.8,12.6c-0.4,0-0.7,0.3-0.7,0.7v23.4c0,0.4,0.3,0.7,0.7,0.7s0.7-0.3,0.7-0.7V13.3 C20.5,12.9,20.2,12.6,19.8,12.6z"/>
        <path class="sc0"
              d="M25.1,10.8c-0.4,0-0.7,0.3-0.7,0.7v26.9c0,0.4,0.3,0.7,0.7,0.7s0.7-0.3,0.7-0.7V11.5 C25.8,11.2,25.5,10.8,25.1,10.8z"/>
        <path class="sc0"
              d="M30.4,12.6c-0.4,0-0.7,0.3-0.7,0.7v23.4c0,0.4,0.3,0.7,0.7,0.7c0.4,0,0.7-0.3,0.7-0.7V13.3 C31.1,12.9,30.7,12.6,30.4,12.6z"/>
    </g></svg>
