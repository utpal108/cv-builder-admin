<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
     y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><style
        type="text/css">    .sc0 {
            fill: {{ $document->template_data['template_data']['themes']['color'] ?? '#479099' }}
        }</style>
    <path class="sc0"
          d="M25,0L0.2,14.1l9.6,5.5L0.2,25l6.4,3.7c0.1,0.1,0.2,0.2,0.3,0.2l2.8,1.6L0.2,36l24.4,14l25.2-14.3l-9.7-5.4	l9.7-5.5l-9.7-5.4l9.7-5.5L25,0z M25,1.7l21.8,12.2L24.6,26.4L3.2,14.1L25,1.7z M8.7,28.2l8.1-4.6l4.3,2.5L13,30.7L8.7,28.2z M11.2,20.5l4,2.3l-8.1,4.6l-4-2.3L11.2,20.5z M46.8,35.7L24.6,48.3L3.2,36l8.1-4.6l13.4,7.7l14-7.9L46.8,35.7z M24.6,37.4l-4.3-2.5	l22.1-12.5l4.4,2.5L24.6,37.4z M40.9,21.5L18.8,34l-4.3-2.5l8.1-4.6l2,1.2l14-7.9L40.9,21.5z"/></svg>
