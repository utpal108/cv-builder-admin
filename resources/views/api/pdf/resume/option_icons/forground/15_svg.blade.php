<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
     y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><style
        type="text/css">    .sc0 {
            fill: {{ $document->template_data['template_data']['themes']['color'] ?? '#479099' }}
        }</style>
    <path class="sc0"
          d="M42.3,1.5c0.4,0,0.7-0.3,0.7-0.7S42.7,0,42.3,0H11.3c0,0,0,0,0,0v0C8.9,0,7,2,7,4.4v41.3h0C7,48,8.9,50,11.3,50	v0h30.1c0.9,0,1.6-0.7,1.6-1.6V7.2h-0.7c-1.6,0-2.9-1.3-2.9-2.9S40.7,1.5,42.3,1.5z M8.5,45.6v-38c0.8,0.7,1.7,1.1,2.8,1.1v39.8	C9.7,48.5,8.5,47.2,8.5,45.6z M41.5,48.4c0,0.1-0.1,0.1-0.1,0.1H12.8V8.7h3.8v20.1l5.1-4.9l5.4,4.8v-20h14.5V48.4z M18,8.7h7.5v16.7	l-4-3.5L18,25.3V8.7z M11.3,7.2c-1.6,0-2.9-1.3-2.9-2.9s1.3-2.9,2.9-2.9v0c0,0,0,0,0,0H39c-0.7,0.8-1.1,1.8-1.1,2.9	c0,1.1,0.4,2.1,1.1,2.9H11.3z"/></svg>
