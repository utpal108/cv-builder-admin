<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
     y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><style
        type="text/css">    .sc0 {
            fill: {{ $document->template_data['template_data']['themes']['color'] ?? '#479099' }}
        }</style>
    <g>
        <path class="sc0"
              d="M45.3,24.4v-4.2h-4.1v-4.4h-4.6v-2.9h4.6V6.7h-6.1v4.6h-4.4v4.4H19.2v-4.4h-4.4V6.7H8.8v6.1h4.6v2.9H8.8v4.4 H4.7v4.2H0V39h6.2V25.8H9V39h4.4v4.2h10v-5.7h-8.5v-2.8h20.2v2.8h-8.5v5.7h10V39H41V25.8h2.9V39H50V24.4H45.3z M36.6,8.2h3.1v3.1 h-3.1V8.2z M10.3,11.3V8.2h3.1v3.1H10.3z M4.7,37.6H1.5V25.8h3.2V37.6z M21.9,39v2.8h-7V39H21.9z M35.1,41.8h-7V39h7V41.8z  M39.5,24.4v13.2h-2.9v-4.3H13.4v4.3h-2.9V24.4H6.2v-2.7h4.1v-4.4h4.6v-4.4h2.9v4.4h14.6v-4.4h2.9v4.4h4.6v4.4h4.1v2.7H39.5z  M48.5,37.6h-3.2V25.8h3.2V37.6z"/>
        <path class="sc0" d="M13.1,26.3h6.5v-6.5h-6.5V26.3z M14.6,21.3H18v3.5h-3.5V21.3z"/>
        <path class="sc0" d="M30.6,26.3H37v-6.5h-6.5V26.3z M32,21.3h3.5v3.5H32V21.3z"/>
    </g></svg>
