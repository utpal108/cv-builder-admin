<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
     y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><style
        type="text/css">    .sc0 {
            fill: {{ $document->template_data['template_data']['themes']['color'] ?? '#479099' }}
        }</style>
    <g>
        <path class="sc0"
              d="M48.1,5.2V2.4c0-0.8-0.7-1.5-1.5-1.5h-3.8c-0.8,0-1.5,0.7-1.5,1.5v2.7h-2V2.4c0-0.8-0.7-1.5-1.5-1.5H34 c-0.8,0-1.5,0.7-1.5,1.5v2.7h-2.1v10.6v1.5v9.1v1.5V37v1.5v10.6H50V38.5V37v-9.1v-1.5v-9.1v-1.5V5.2H48.1z M42.8,2.4h3.8v2.7h-3.8 V2.4z M34,2.4h3.8v2.7H34V2.4z M48.5,47.6H31.9v-9.1h16.6V47.6z M48.5,37H31.9v-9.1h16.6V37z M48.5,26.4H31.9v-9.1h16.6V26.4z  M31.9,15.8V6.7h0.6h6.8h2h6.8h0.4v9.1H31.9z"/>
        <path class="sc0"
              d="M25.8,13c0-0.8-0.7-1.5-1.5-1.5h-3.8c-0.8,0-1.5,0.7-1.5,1.5v2.7h-2V13c0-0.8-0.7-1.5-1.5-1.5h-3.8 c-0.8,0-1.5,0.7-1.5,1.5v2.7h-2V13c0-0.8-0.7-1.5-1.5-1.5H2.9c-0.8,0-1.5,0.7-1.5,1.5v2.7H0v10.6v1.5V37v1.5v10.6h27.1V38.5V37 v-9.1v-1.5V15.8h-1.4V13z M20.5,13h3.8v2.7h-3.8V13z M11.7,13h3.8v2.7h-3.8V13z M2.9,13h3.8v2.7H2.9V13z M25.6,47.6H1.5v-9.1h24.1 V47.6z M25.6,37H1.5v-9.1h24.1V37z M1.5,26.4v-9.1h6.7h2H17h2h6.7v9.1H1.5z"/>
    </g></svg>
