<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
     y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><style
        type="text/css">    .st0 {
            fill: {{ $document->template_data['template_data']['themes']['color'] ?? '#479099' }}
        }</style>
    <path class="st0"
          d="M49.3,36.3h-2.1L35.4,19.4c-0.1-0.2-0.2-0.4-0.4-0.5c-0.4-0.2-0.8-0.1-1,0.3l-4.1,5.6l-8.4-12	c-0.1-0.2-0.2-0.4-0.5-0.5c-0.4-0.2-0.8-0.1-1.1,0.3L3,36.3H0.7C0.3,36.3,0,36.6,0,37c0,0.4,0.3,0.7,0.7,0.7h2.4c0.1,0,0.1,0,0.2,0	c0,0,0.1,0,0.1,0h45.8c0.4,0,0.7-0.3,0.7-0.7C50,36.6,49.7,36.3,49.3,36.3z M35,21.4l10.4,14.8h-7.5l-5-7.2L35,21.4z M33,22.9	l-1.3,4.6l-1-1.4L33,22.9z M21,14.9l15.1,21.4H15.3L21,14.9z M19.1,16.2l-5.4,20.1H4.8L19.1,16.2z"/></svg>
