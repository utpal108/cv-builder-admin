<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
     y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve"><style
        type="text/css">    .sc0 {
            fill: {{ $document->template_data['template_data']['themes']['color'] ?? '#479099' }}
        }</style>
    <path class="sc0"
          d="M47,6H3C1.3,6,0,7.3,0,9V41c0,1.6,1.3,3,3,3h44c1.6,0,3-1.3,3-3V9C50,7.3,48.7,6,47,6z M3,7.5h44	c0.8,0,1.5,0.7,1.5,1.5v4.6h-47V9C1.5,8.1,2.2,7.5,3,7.5z M8.7,15h3v16.1h-3V15z M13.2,32.7V15h4.1v17.6h2.2v9.9h-8.5v-9.9H13.2z M21.7,15v16.1h-3V15H21.7z M21,32.7h2.2V15h4.1v17.6h2.2v9.9H21V32.7z M31.7,15v16.1h-3V15H31.7z M31,32.7h2.2V15h4.1v17.6h2.2v9.9	H31V32.7z M38.8,31.2V15h3v16.1H38.8z M1.5,41V15h5.7v17.6h2.2v9.9H3C2.2,42.5,1.5,41.9,1.5,41z M47,42.5h-6v-9.9h2.2V15h5.2v26	C48.5,41.9,47.8,42.5,47,42.5z"/></svg>
