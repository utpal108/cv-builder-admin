<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cvbility</title>

    <!-- <link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond:400,400i,500,500i,600,700|Hind:400,500,600,700|Lora:400,400i,700|Lusitana:400,700|Merriweather+Sans:400,400i,700|Merriweather:400,400i,700|Overpass:400,400i,600,700|Quicksand:400,500,600,700|Raleway:400,400i,500,500i,600,700|Roboto:400,400i,500,700|Rokkitt:400,500,600,700|Ubuntu:400,400i,500,500i,700&display=swap" rel="stylesheet"> -->
    <style type="text/css">
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Cormorant_Garamond/CormorantGaramond-Bold.ttf);
            font-family: 'CormorantGaramond-Bold';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Cormorant_Garamond/CormorantGaramond-Italic.ttf);
            font-family: 'CormorantGaramond-Italic';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Cormorant_Garamond/CormorantGaramond-Regular.ttf);
            font-family: 'CormorantGaramond-Regular';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Hind/Hind-Regular.ttf);
            font-family: 'Hind-Regular';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Hind/Hind-Bold.ttf);
            font-family: 'Hind-Bold';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Lusitana/Lusitana-Regular.ttf);
            font-family: 'Lusitana-Regular';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Lusitana/Lusitana-Bold.ttf);
            font-family: 'Lusitana-Bold';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Lora/Lora-Regular.ttf);
            font-family: 'Lora-Regular';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Lora/Lora-Bold.ttf);
            font-family: 'Lora-Bold';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Lora/Lora-Italic.ttf);
            font-family: 'Lora-Italic';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Merriweather/Merriweather-Regular.ttf);
            font-family: 'Merriweather-Regular';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Merriweather/Merriweather-Bold.ttf);
            font-family: 'Merriweather-Bold';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Merriweather/Merriweather-Italic.ttf);
            font-family: 'Merriweather-Italic';
        }


        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Merriweather_Sans/MerriweatherSans-Regular.ttf);
            font-family: 'MerriweatherSans-Regular';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Merriweather_Sans/MerriweatherSans-Bold.ttf);
            font-family: 'MerriweatherSans-Bold';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Merriweather_Sans/MerriweatherSans-Italic.ttf);
            font-family: 'MerriweatherSans-Italic';
        }


        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Overpass/Overpass-Regular.ttf);
            font-family: 'Overpass-Regular';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Overpass/Overpass-Bold.ttf);
            font-family: 'Overpass-Bold';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Overpass/Overpass-Italic.ttf);
            font-family: 'Overpass-Italic';
        }


        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Raleway/Raleway-Regular.ttf);
            font-family: 'Raleway-Regular';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Raleway/Raleway-Bold.ttf);
            font-family: 'Raleway-Bold';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Raleway/Raleway-Italic.ttf);
            font-family: 'Raleway-Italic';
        }


        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Ubuntu/Ubuntu-Regular.ttf);
            font-family: 'Ubuntu-Regular';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Ubuntu/Ubuntu-Bold.ttf);
            font-family: 'Ubuntu-Bold';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Ubuntu/Ubuntu-Italic.ttf);
            font-family: 'Ubuntu-Italic';
        }


        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Roboto/Roboto-Regular.ttf);
            font-family: 'Roboto-Regular';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Roboto/Roboto-Bold.ttf);
            font-family: 'Roboto-Bold';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Roboto/Roboto-Italic.ttf);
            font-family: 'Roboto-Italic';
        }


        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Rokkitt/static/Rokkitt-Bold.ttf);
            font-family: 'Rokkitt-Bold';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Rokkitt/static/Rokkitt-Regular.ttf);
            font-family: 'Rokkitt-Regular';
        }


        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Quicksand/static/Quicksand-Bold.ttf);
            font-family: 'Quicksand-Bold';
        }
        @font-face {
            src: url(@php echo $_SERVER['DOCUMENT_ROOT'] @endphp/fonts/Quicksand/static/Quicksand-Regular.ttf);
            font-family: 'Quicksand-Regular';
        }

        /*================= Coverletter Fonts ===============================*/
        .ic-coverletter-page-wrapper[d-font="ubuntu"]{
            font-family: 'Ubuntu-Regular', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="raleway"]{
            font-family: 'Raleway-Regular', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="roboto"]{
            font-family: 'Roboto-Regular', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="quicksand"]{
            font-family: 'Quicksand-Regular', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="overpass"]{
            font-family: 'Overpass-Regular', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="merriweather-sans"]{
            font-family: 'MerriweatherSans-Regular', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="hind"]{
            font-family: 'Hind-Regular', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="lora"]{
            font-family: 'Lora-Regular', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="cormorant"]{
            font-family: 'CormorantGaramond-Regular', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="rokkitt"]{
            font-family: 'Rokkitt-Regular', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="merriweather"]{
            font-family: 'Merriweather-Regular', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="lusitana"]{
            font-family: 'Lusitana-Regular', sans-serif;
        }

        .ic-coverletter-page-wrapper[d-font="ubuntu"] .ic-resume-single-slot-group-name-text,
        .ic-coverletter-page-wrapper[d-font="ubuntu"] .ic-resume-single-slot-title-text,
        .ic-coverletter-page-wrapper[d-font="ubuntu"] .ic-resume-publication-name-text.ic-resume-single-slot-name-text,
        .ic-coverletter-page-wrapper[d-font="ubuntu"] .ic-resume-single-slot-header-title{
            font-family: 'Ubuntu-Bold', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="raleway"] .ic-resume-single-slot-group-name-text,
        .ic-coverletter-page-wrapper[d-font="raleway"] .ic-resume-single-slot-title-text,
        .ic-coverletter-page-wrapper[d-font="raleway"] .ic-resume-publication-name-text.ic-resume-single-slot-name-text,
        .ic-coverletter-page-wrapper[d-font="raleway"] .ic-resume-single-slot-header-title{
            font-family: 'Raleway-Bold', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="roboto"] .ic-resume-single-slot-group-name-text,
        .ic-coverletter-page-wrapper[d-font="roboto"] .ic-resume-single-slot-title-text,
        .ic-coverletter-page-wrapper[d-font="roboto"] .ic-resume-publication-name-text.ic-resume-single-slot-name-text,
        .ic-coverletter-page-wrapper[d-font="roboto"] .ic-resume-single-slot-header-title{
            font-family: 'Roboto-Bold', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="quicksand"] .ic-resume-single-slot-group-name-text,
        .ic-coverletter-page-wrapper[d-font="quicksand"] .ic-resume-single-slot-title-text,
        .ic-coverletter-page-wrapper[d-font="quicksand"] .ic-resume-publication-name-text.ic-resume-single-slot-name-text,
        .ic-coverletter-page-wrapper[d-font="quicksand"] .ic-resume-single-slot-header-title{
            font-family: 'Quicksand-Bold', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="overpass"] .ic-resume-single-slot-group-name-text,
        .ic-coverletter-page-wrapper[d-font="overpass"] .ic-resume-single-slot-title-text,
        .ic-coverletter-page-wrapper[d-font="overpass"] .ic-resume-publication-name-text.ic-resume-single-slot-name-text,
        .ic-coverletter-page-wrapper[d-font="overpass"] .ic-resume-single-slot-header-title{
            font-family: 'Overpass-Bold', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="merriweather-sans"] .ic-resume-single-slot-group-name-text,
        .ic-coverletter-page-wrapper[d-font="merriweather-sans"] .ic-resume-single-slot-title-text,
        .ic-coverletter-page-wrapper[d-font="merriweather-sans"] .ic-resume-publication-name-text.ic-resume-single-slot-name-text,
        .ic-coverletter-page-wrapper[d-font="merriweather-sans"] .ic-resume-single-slot-header-title{
            font-family: 'MerriweatherSans-Bold', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="hind"] .ic-resume-single-slot-group-name-text,
        .ic-coverletter-page-wrapper[d-font="hind"] .ic-resume-single-slot-title-text,
        .ic-coverletter-page-wrapper[d-font="hind"] .ic-resume-publication-name-text.ic-resume-single-slot-name-text,
        .ic-coverletter-page-wrapper[d-font="hind"] .ic-resume-single-slot-header-title{
            font-family: 'Hind-Bold', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="lora"] .ic-resume-single-slot-group-name-text,
        .ic-coverletter-page-wrapper[d-font="lora"] .ic-resume-single-slot-title-text,
        .ic-coverletter-page-wrapper[d-font="lora"] .ic-resume-publication-name-text.ic-resume-single-slot-name-text,
        .ic-coverletter-page-wrapper[d-font="lora"] .ic-resume-single-slot-header-title{
            font-family: 'Lora-Bold', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="cormorant"] .ic-resume-single-slot-group-name-text,
        .ic-coverletter-page-wrapper[d-font="cormorant"] .ic-resume-single-slot-title-text,
        .ic-coverletter-page-wrapper[d-font="cormorant"] .ic-resume-publication-name-text.ic-resume-single-slot-name-text,
        .ic-coverletter-page-wrapper[d-font="cormorant"] .ic-resume-single-slot-header-title{
            font-family: 'CormorantGaramond-Bold', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="rokkitt"] .ic-resume-single-slot-group-name-text,
        .ic-coverletter-page-wrapper[d-font="rokkitt"] .ic-resume-single-slot-title-text,
        .ic-coverletter-page-wrapper[d-font="rokkitt"] .ic-resume-publication-name-text.ic-resume-single-slot-name-text,
        .ic-coverletter-page-wrapper[d-font="rokkitt"] .ic-resume-single-slot-header-title{
            font-family: 'Rokkitt-Bold', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="merriweather"] .ic-resume-single-slot-group-name-text,
        .ic-coverletter-page-wrapper[d-font="merriweather"] .ic-resume-single-slot-title-text,
        .ic-coverletter-page-wrapper[d-font="merriweather"] .ic-resume-publication-name-text.ic-resume-single-slot-name-text,
        .ic-coverletter-page-wrapper[d-font="merriweather"] .ic-resume-single-slot-header-title{
            font-family: 'Merriweather-Bold', sans-serif;
        }
        .ic-coverletter-page-wrapper[d-font="lusitana"] .ic-resume-single-slot-group-name-text,
        .ic-coverletter-page-wrapper[d-font="lusitana"] .ic-resume-single-slot-title-text,
        .ic-coverletter-page-wrapper[d-font="lusitana"] .ic-resume-publication-name-text.ic-resume-single-slot-name-text,
        .ic-coverletter-page-wrapper[d-font="lusitana"] .ic-resume-single-slot-header-title{
            font-family: 'Lusitana-Bold', sans-serif;
        }
    </style>

    <style type="text/css">
        html, body, div, span, applet, object, iframe,
        h1, h2, h3, h4, h5, h6, p, blockquote, pre, abbr, acronym, address, big, cite, code,
        del, dfn, em, img, ins, kbd, q, s, samp,
        small, strike, strong, sub, sup, tt, var,
        b, u, i, center,
        dl, dt, dd, ol, ul, li,
        fieldset, form, label, legend,
        table, caption, tbody, tfoot, thead, tr, th, td,
        article, aside, canvas, details, embed,
        figure, figcaption, footer, header, hgroup,
        menu, nav, output, ruby, section, summary,
        time, mark, audio, video {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            vertical-align: baseline;
        }

        html {
            line-height: 1;
        }

        ol, ul {
            list-style: none;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
        }

        caption, th, td {
            text-align: left;
            font-weight: normal;
            vertical-align: middle;
        }
        a img {
            border: none;
        }

        article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary {
            display: block;
        }

        body {
            font-size: 16px;margin: 0;color: #202020;background-color: #F5F5F5;
        }
        body a {
            text-decoration: none;
            color: inherit;
        }
        body a:hover {
            color: inherit;
        }
        *, ::after, ::before {
            box-sizing: border-box;
        }
        body .container {
            min-width: 500px;
            margin: 0 auto;
            padding: 0 20px;
        }
        body .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }
        body .left {
            float: left;
        }
        body .right {
            float: right;
        }
        body .helper {
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }
        body .no-break {
            page-break-inside: avoid;
        }




        .ic-color-item.color-1{
            background-color: #3C96FD
        }
        .ic-color-item.color-2{
            background-color: #531EFF
        }
        .ic-color-item.color-3{
            background-color: #981EFF
        }
        .ic-color-item.color-4{
            background-color: #FFB81E
        }
        .ic-color-item.color-5{
            background-color: #FF9D1E
        }
        .ic-color-item.color-6{
            background-color: #336196
        }
        .ic-color-item.color-7{
            background-color: #EC1EFF
        }
        .ic-color-item.color-8{
            background-color: #7C99BA
        }
        .ic-color-item.color-9{
            background-color: #575757
        }
        .ic-color-item.color-10{
            background-color: #174172
        }
        .ic-color-item.color-11{
            background-color: #67FFB8
        }
        .ic-color-item.color-12{
            background-color: #7BAB95
        }
        .ic-color-item.color-13{
            background-color: #89AED9
        }
        .ic-color-item.color-14{
            background-color: #5DD3FF
        }
        .ic-color-item.color-15{
            background-color: #8D8D8D
        }
        .ic-color-item.color-16{
            background-color: #FF3E8B
        }
        .ic-color-item.color-17{
            background-color: #2AAE70
        }
        .ic-color-item.color-18{
            background-color: #0067DD
        }
        .ic-color-item.color-19{
            background-color: #02A5E1
        }
        .ic-color-item.color-20{
            background-color: #11355F
        }
        .ic-color-item.color-21{
            background-color: #193042
        }
        .ic-color-item.color-22{
            background-color: #7D203F
        }
        .ic-color-item.color-23{
            background-color: #458966
        }
        .ic-color-item.color-24{
            background-color: #374458
        }
        .ic-color-item.color-25{
            background-color: #8E8E8E
        }
        .ic-color-item.color-26{
            background-color: #000000
        }
        .ic-color-item.color-27{
            background-color: #697084
        }
        .ic-color-item.color-28{
            background-color: #C6A067
        }
        .ic-color-item.color-29{
            background-color: #CC5018
        }
        .ic-color-item.color-30{
            background-color: #D11E2F
        }
        .ic-color-item.color-31{
            background-color: #9E2B6F
        }
        .ic-color-item.color-32{
            background-color: #4A633D
        }
        .ic-color-item.color-33{
            background-color: #7CA02B
        }
        .ic-color-item.color-34{
            background-color: #1886D1
        }
        .ic-color-item.color-35{
            background-color: #512B8E
        }
        .ic-color-item.color-36{
            background-color: #2AAD71
        }
        .ic-color-item.color-37{
            background-color: #6B2929
        }
        .ic-color-item.color-38{
            background-color: #B57E43
        }
        .ic-color-item.color-39{
            background-color: #C61436
        }
        .ic-color-item.color-40{
            background-color: #0A5650
        }
        .ic-secondary-color.color-1{
            background-color: #001729;
        }
        .ic-secondary-color.color-2{
            background-color: #E5C749;
        }
        .ic-secondary-color.color-3{
            background-color: #C6A067;
        }
        .ic-secondary-color.color-4{
            background-color: #9E2B6F;
        }
        .ic-secondary-color.color-5{
            background-color: #193042;
        }
        .ic-secondary-color.color-6{
            background-color: #697084;
        }
        .ic-secondary-color.color-7{
            background-color: #E26D49;
        }
        .ic-secondary-color.color-8{
            background-color: #F04E41;
        }
        .ic-secondary-color.color-9{
            background-color: #2DBCAD;
        }
        .ic-secondary-color.color-10{
            background-color: #384458;
        }
        .ic-secondary-color.color-11{
            background-color: #F04E41;
        }
        .ic-secondary-color.color-12{
            background-color: #4F90CD;
        }
        .ic-secondary-color.color-13{
            background-color: #8E8E8E;
        }
        .ic-secondary-color.color-14{
            background-color: #EC1EFF;
        }
        .ic-secondary-color.color-15{
            background-color: #67FFB8;
        }
        .ic-secondary-color.color-16{
            background-color: #531EFF;
        }
        .ic-secondary-color.color-17{
            background-color: #FF9D1E;
        }
        .ic-secondary-color.color-18{
            background-color: #FFB81E;
        }
        .ic-secondary-color.color-19{
            background-color: #E26D49;
        }
        .ic-secondary-color.color-20{
            background-color: #5291CB;
        }
        .ic-secondary-color.color-21{
            background-color: #C61436;
        }
        .ic-secondary-color.color-22{
            background-color: #0A5650;
        }
        .ic-secondary-color.color-23{
            background-color: #9E2B6F;
        }
        .ic-secondary-color.color-24{
            background-color: #D11E2F;
        }
        .ic-secondary-color.color-25{
            background-color: #C6A067;
        }
        .ic-secondary-color.color-26{
            background-color: #1E87FF;
        }
        .ic-secondary-color.color-27{
            background-color: #001729;
        }
        .ic-secondary-color.color-28{
            background-color: #512B8E;
        }
        .ic-secondary-color.color-29{
            background-color: #458966;
        }
        .ic-secondary-color.color-30{
            background-color: #193042;
        }
        .ic-secondary-color.color-31{
            background-color: #282923;
        }
        .ic-secondary-color.color-32{
            background-color: #512B8E;
        }
        .ic-secondary-color.color-33{
            background-color: #FF3B08;
        }
        .ic-secondary-color.color-34{
            background-color: #9E0000;
        }
        .ic-secondary-color.color-35{
            background-color: #575757;
        }
        .ic-secondary-color.color-36{
            background-color: #F5C14C;
        }
        .ic-secondary-color.color-37{
            background-color: #458966;
        }
        .ic-secondary-color.color-38{
            background-color: #6E4598;
        }
        .ic-secondary-color.color-39{
            background-color: #C6A067;
        }
        .ic-secondary-color.color-40{
            background-color: #697084;
        }

        /*==============================================================
      ============================== coverletter-main-pert ==========
      ===============================================================*/

        .ic-coverletter-scrollbar-part {
            width: 100%;
            height: 100%;
            z-index: 99;
            box-sizing: border-box;
            position: relative;
        }
        .ic-coverletter-page-wrapper {
            margin-left: auto;
            margin-right: auto;
            z-index: 99;
        }
        .ic-coverletter-page-inner{
            background-color: #fff;
        }
        .ic-coverletter-page-wrapper {
            width: 1190px!important;
            min-height: 1684px!important;
        }
        .ic-coverletter-page-inner{
            width: 1190px!important;
            min-height: 1684px!important;
        }
        .ic-coverletter-us-format-page.ic-coverletter-page-wrapper {
            width: 1224px!important;
            min-height: 1584px!important;
        }
        .ic-coverletter-us-format-page .ic-coverletter-page-inner{
            width: 1224px!important;
            min-height: 1584px!important;
        }
        .ic-coverletter-box{
            height: auto;
            position: relative;
            padding-top: 40px;
        }
        .ic-coverletter-page-inner:first-child .ic-coverletter-box{
            padding-top: 0px;
        }
        .ic-coverletter-box-top{
            padding-top: 75px;
            padding-bottom: 30px;
        }
        .ic-coverletter-top-header-left {
            width: 50%;
            float: left;
        }
        .ic-coverletter-header-left {
            float: left;
            max-width: 100%;
            margin-bottom: 20px;
        }

        .ic-coverletter-box-name-holder{
            padding-right: 26px;
            padding-bottom: 30px;
            padding-left: 90px;
            position: relative;
        }

        .ic-coverletter-header-left:nth-child(2){
            width:100%;
        }
        div#ic-coverletter-full-name {
            word-wrap: break-word;
            font-size: 46px;
            min-height: 46px;
            line-height: 48px;
            color: #202020;
            margin-left: -1px;
        }
        #ic-coverletter-title{
            margin-top: 4px;
            position: relative;
        }
        .ic-coverletter-title-text{
            color:#202020;
            font-size:24px;
            min-height: 24px;
            line-height: 26px;
            padding-bottom: 2px;
            display: inline-block;
            word-wrap: break-word;
        }

        .ic-coverletter-contact-item {
            margin-top: 18px;
            font-size: 16px;
            line-height: 20px;
            text-align: right;
        }
        .ic-coverletter-contact-item a{
            color:#202020;
            margin-right: 10px;
        }
        .ic-coverletter-contact-item svg polygon,
        .ic-coverletter-contact-item svg path{
            fill:{{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-coverletter-contact-item svg{
            width:18px;
            height: 18px;
        }


        /*====================================== Resume Template CSS ====================================*/
        .ic-resume-template-editor .ic-coverletter-box-top{
            padding-top: 44px;
            padding-bottom: 0;
        }
        .ic-resume-top-header{
            padding-left: 50px;
            padding-right: 50px;
            margin-bottom: 24px;
            display: inline-block;
            width: 100%;
        }
        .ic-resume-scrollbar-part.one-column-layout .ic-resume-top-header{
            margin-bottom: 34px;
        }
        .ic-resume-top-header-inner {
            height: auto;
            display: inline-block;
            width: 100%;
            min-height: 160px;
            position: relative;
        }
        #template-professional .ic-resume-top-header-inner{
            min-height: 180px;
        }
        .ic-resume-photo{
            float: left;
            position: relative;
        }
        .ic-resume-photo .ic-resume-photo-img{
            max-width: 180px!important;
            max-height: 180px!important;
            min-width: 160px!important;
            min-height: 160px!important;
        }
        .ic-resume-photo .ic-resume-photo-img img{
            width:100%;
        }
        .ic-resume-box-name-holder{
            position: relative;
            width: 76.5%;
            height: 100%;
            float: left;
            height: auto !important;
            margin-left: 34px;
        }
        .ic-resume-box-name-holder div#ic-coverletter-full-name {
            font-size: 46px;
            min-height: 46px;
            line-height: 48px;
        }
        .ic-resume-box-name-holder .ic-coverletter-title-text {
            font-size: 24px;
            min-height: 24px;
            line-height: 26px;
            width: auto;
            font-kerning: normal;
            text-rendering: optimizeLegibility;
            speak: none;
            -webkit-font-smoothing: subpixel-antialiased;
            -moz-osx-font-smoothing: grayscale;
            font-variant: normal;
            word-wrap: break-word;
            -webkit-word-break: normal;
            word-break: normal;
            white-space: normal;
        }
        .ic-coverletter-title-sumury{
            margin-top: 8px;
        }
        .ic-coverletter-title-sumury-text {
            font-size: 18px;
            min-height: 18px;
            line-height: 22px;
            padding-bottom: 2px;
        }
        .ic-resume-box-name-holder #ic-cv-title {
            padding-bottom: 10px;
            position: relative;
        }
        #template-functional .ic-resume-box-name-holder #ic-cv-title:after {
            content: "";
            position: absolute;
            bottom: 5px;
            left: 0;
            height: 2px;
            width: 270px;
            background: #fff;
        }
        .ic-resume-box-name-holder #ic-coverletter-title {
            margin-top: 6px;
        }

        .ic-resume-photo-img{
            border-radius: 100%;
            overflow:hidden;
            border: 1px solid #d3d3d3;
            transition: all 0.6s ease-in-out;
        }
        .ic-resume-photo-img[data-photo-style="ic-photo-style-1"]{
            border-radius: 100%;
        }
        .ic-resume-photo-img[data-photo-style="ic-photo-style-2"]{
            border-bottom-right-radius: 20px;
            border-bottom-left-radius: 50%;
            border-top-left-radius: 50%;
            border-top-right-radius: 50%;
        }
        .ic-resume-photo-img[data-photo-style="ic-photo-style-3"]{
            border-radius: 20px;
        }
        .ic-resume-photo-img[data-photo-style="ic-photo-style-4"]{
            border-radius: 0px;
        }
        .ic-resume-box-wrapper{
            overflow:hidden;
            width: 100%;
        }
        .ic-resume-box-slot1{
            width:50% !important;
            float: left;
        }
        .ic-resume-box-slot2{
            width:50%!important;
            float: right;
        }

        .ic-resume-scrollbar-part.one-column-layout .ic-resume-box-slot2{
            display: none;
        }
        .ic-resume-scrollbar-part .ic-resume-box-slot1 .ic-coverletter-contact-items {
            margin: 4px 30px 36px 50px;
            /*width: 240px;*/
        }
        .ic-resume-scrollbar-part.one-column-layout .ic-coverletter-contact-items {
            margin: 0;
            width: 100%;
            display: inline-block;
        }
        .ic-resume-scrollbar-part .ic-resume-box-slot1 .ic-coverletter-contact-item {
            text-align: left;
        }
        .ic-resume-scrollbar-part.one-column-layout .ic-coverletter-contact-item {
            -webkit-flex-direction: inherit;
            -moz-flex-direction: inherit;
            -ms-flex-direction: inherit;
            -o-flex-direction: inherit;
            flex-direction: inherit;
            width: 50%;
            float: left;
            margin-top: 18px;
        }
        .ic-resume-scrollbar-part.one-column-layout .ic-coverletter-contact-item a {
            margin-right: 0px;
            margin-left: 10px;
        }
        .ic-resume-scrollbar-part .ic-coverletter-contact-item svg {
            width: 20px;
            height: 20px;
            order: 1;
        }
        .ic-resume-scrollbar-part .ic-coverletter-contact-item a {
            order: 2;
            font-size: 16px;
        }
        .resume-template #template-functional .ic-resume-box-slot2{
            padding-right: 54px;
        }
        .ic-resume-single-slot{
            margin-bottom: 32px;
        }
        .ic-resume-single-slot-header {
            padding-bottom: 2px;
            margin-bottom: 12px;
        }
        .ic-resume-box-slot2 .ic-resume-single-slot-header {
            padding-left: 36px;
            padding-right: 50px;
        }
        .ic-resume-box-slot1 .ic-resume-single-slot-header {
            padding-left: 50px;
            padding-right: 36px;
        }
        .one-column-layout .ic-resume-box-slot1 .ic-resume-single-slot-header {
            padding-left: 50px;
            padding-right: 50px;
        }

        .ic-resume-single-slot-header-holder {
            padding-bottom: 2px;
        }
        .ic-resume-single-slot-header-title {
            font-size: 28px;
            line-height: 30px;
            min-height: 28px;
            font-weight: 700;
            color: #1E87FF;
            padding-bottom: 2px;
            text-transform: uppercase;
        }
        .ic-resume-single-slot-group{
            margin-bottom: 24px;
        }
        .ic-resume-scrollbar-part.one-column-layout .ic-resume-single-slot-group{
            margin-left: 0px;
        }
        .ic-resume-scrollbar-part.one-column-layout .ic-resume-single-slot.two-col-active .ic-resume-single-slot-group:nth-of-type(2n){
            margin-left: 24px;
        }
        .ic-resume-single-slot-group:last-child{
            margin-bottom: 0 !important;
        }
        .ic-resume-work-place-link svg{
            height: 24px;
            width: 24px;
        }
        .ic-resume-work-place-link svg path{
            fill:#909090;
        }
        .ic-resume-work-role{
            margin-bottom: 3px;
        }
        .ic-resume-single-slot-title-text {
            font-size: 24px;
            min-height: 24px;
            line-height: 26px;
            color: #202020;
            font-weight: 700;
            margin-bottom: 2px;
        }
        .ic-resume-single-slot-place-text{
            font-size: 24px;
            min-height: 24px;
            line-height: 26px;
            color: #202020;
            font-weight: 400;
            margin-bottom: 4px;
        }
        .ic-resume-single-slot-period,
        .ic-resume-single-slot-place{
            margin-bottom: 8px;
        }

        .ic-resume-single-slot-period-edit>div{
            float: left;
        }
        .ic-resume-single-slot-period-edit{
            overflow:hidden;
            width: 100%;
        }
        .ic-resume-single-slot-period-start-month, .ic-resume-single-slot-period-start-year,
        .ic-resume-single-slot-period-end-month, .ic-resume-single-slot-period-end-year{
            font-size: 16px!important;
            min-height: 16px;
            line-height: 18px!important;
            width: 54px;
            min-width: 54px!important;
            padding:4px 0;
            text-align: center;
            /*color: #1E87FF !important;*/
            border:none;
            border-bottom:1px dashed rgba(150,150,150,.7);
            outline: none;
        }
        .ic-resume-single-slot-period-class-present{
            font-size: 16px!important;
            min-height: 16px;
            line-height: 20px!important;
            width: 100px;
            min-width: 100px!important;
            padding:4px 0;
            /*color: #1E87FF !important;*/
            border:none;
            border-bottom:1px solid rgba(150,150,150,.7);
            outline: none;
            font-style: italic;
        }
        .ic-resume-single-slot-period-start-month:focus, .ic-resume-single-slot-period-start-year:focus,
        .ic-resume-single-slot-period-end-month:focus, .ic-resume-single-slot-period-end-year:focus, .ic-resume-single-slot-period-class-present:focus{
            border-bottom-color: #1E87FF;
        }
        .ic-resume-single-slot-edit-period-start>*,
        .ic-resume-single-slot-edit-period-end>*{
            float: left;
        }
        .ic-resume-single-slot-period-text-extra {
            font-size: 22px!important;
            min-height: 22px;
            line-height: 20px!important;
            padding:4px;
        }
        .ic-resume-single-slot-period-separator{
            padding: 4px 8px;
            font-size: 20px!important;
            min-height: 22px;
            line-height: 20px!important;
        }
        .ic-resume-single-slot-period-btn {
            width: 16px;
            height: 16px;
            top: 5px;
            border: 1px solid #202020;
            border-radius: 3px;
            float: left;
            margin-right: 10px;
            position: relative;
            z-index: 999;
            overflow: hidden!important;
            box-sizing: border-box;
            display: block;
            -webkit-transition: .3s ease-out;
            -moz-transition: .3s ease-out;
            transition: .3s ease-out;
        }
        .ic-resume-single-slot-period-btn-inner {
            position: absolute;
            height: 100%;
            width: 100%;
            top: 0;
            left: 0;
            border-radius: 5px;
            border-width: 2px;
            border-style: solid;
            border-color: #fff;
            background-color: #1E87FF;
            z-index: 9;
            box-sizing: border-box;
            display: block;
            -webkit-transition: .3s ease-out;
            -moz-transition: .3s ease-out;
            transition: .3s ease-out;
            -webkit-transform: scale(0);
            -moz-transform: scale(0);
            transform: scale(0);
        }
        .ic-resume-single-slot-btn-period-present {
            padding-top: 2px;
            padding-left: 10px;
        }
        .ic-resume-single-slot-period-edit.present-active .ic-resume-single-slot-period-btn-inner{
            -webkit-transform: scale(1);
            -moz-transform: scale(1);
            transform: scale(1);
        }
        .ic-resume-single-slot-period-edit.present-active .ic-resume-single-slot-period-btn{
            border: 1px solid #1E87FF;
        }
        .ic-resume-single-slot-period-edit .ic-resume-single-slot-present-show,
        .ic-resume-single-slot-period-edit.present-active .ic-resume-single-slot-present-hide{
            display: none;
        }
        .ic-resume-single-slot-period-edit.present-active .ic-resume-single-slot-present-show{
            display: block;
        }
        .ic-resume-single-slot-location {
            overflow: hidden;
            margin-bottom: 4px;
        }

        .ic-resume-single-slot-location-text {
            width: auto !important;
            text-align: left;
            float: left;
            font-size: 16px!important;
            min-height: 16px;
            line-height: 18px!important;
            color:#1E87FF;
        }

        .ic-resume-single-slot-desc-text{
            font-size: 16px!important;
            min-height: 16px;
            line-height: 18px!important;
        }
        .ic-resume-single-slot-desc {
            margin-bottom: 8px;
        }
        .ic-resume-single-slot-list-header{
            margin-bottom: 8px;
            overflow: hidden;
        }
        .ic-resume-single-slot-list-header-text{
            font-size: 16px!important;
            min-height: 16px;
            line-height: 18px!important;
            color:#1E87FF;
            font-style: italic;
            width: auto !important;
            text-align: left;
            float: left;
        }
        .ic-resume-single-slot-list-item-bullet svg{
            width:8px;
            height:8px;
        }
        .ic-resume-single-slot-list-item-bullet svg path{
            fill:{{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-resume-single-slot-list-item{
            margin-bottom: 8px;
            position: relative;
        }
        .ic-resume-single-slot-list-item:last-child{
            margin-bottom: 0;
        }
        .ic-resume-single-slot-list-item-bullet {
            position: absolute;
            top:12px;
            height: 2px;
            width: 8px;
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-resume-single-slot-list-sub-content{
            padding-left: 20px;
        }
        .ic-resume-single-slot-list-text{
            font-size: 16px!important;
            min-height: 16px;
            line-height: 22px!important;
        }
        .ic-resume-single-slot-list-text.editable-div {
            border-bottom-style: dashed !important;
        }
        .ic-resume-single-slot-list{
            margin-bottom: 14px;
        }
        .ic-resume-single-slot-contact-label{
            vertical-align: top;
            padding-right: 10px;
            font-style: italic;
            color: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-resume-single-slot-contact-holder .ic-resume-single-slot-contact-info, .ic-resume-single-slot-contact-holder .ic-resume-single-slot-contact-label {
            display: table-cell;
            vertical-align: middle;
        }
        .ic-resume-single-slot-contact-info-text,
        .ic-resume-single-slot-contact-person-text{
            font-size: 16px!important;
            min-height: 16px;
            line-height: 21px!important;
            width: auto !important;
            font-style: italic;
            float: left;
        }
        .ic-resume-single-slot-contact-person-text.editable-div ,
        .ic-resume-single-slot-contact-info-text.editable-div {
            border-bottom-style: dashed !important;
        }
        .ic-resume-single-slot-content-text-main.edit-active .ic-resume-single-slot-content-text-edit{
            display: block;
        }
        .ic-resume-single-slot-content-text-main .ic-resume-single-slot-content-text-edit,
        .ic-resume-single-slot-content-text-main.edit-active .ic-resume-single-slot-content-text{
            display: none;
        }
        .ic-resume-single-slot-content-text{
            height: auto;
            display: inline-block;
            width: 100%;
        }
        .ic-resume-single-slot-content-text .ic-resume-single-slot-period{
            width: 50%;
            float: left;
        }

        .ic-resume-single-slot-content-text .ic-resume-single-slot-location{
            width: 50%;
            float: right;
            text-align: right;
            margin-bottom: 8px;
        }
        .ic-resume-single-slot-content-text .ic-resume-single-slot-location-text {
            text-align: right;
            float: right;
            font-style: italic;
        }
        .ic-resume-single-slot-period-class{
            font-size: 16px!important;
            min-height: 18px;
            line-height: 18px!important;
            font-style: italic;
            color:#1E87FF;
        }
        .ic-resume-single-slot-content-text .ic-resume-single-slot-desc {
            width: 100%;
            float: left;
        }
        .ic-resume-single-slot-content-text .ic-resume-single-slot-desc-text {
            font-style: italic;
            color: #909090;
        }
        .ic-resume-single-slot-content-text .ic-resume-single-slot-list {
            float: left;
            width: 100%;
        }
        .ic-resume-single-slot-content-text .ic-resume-single-slot-contact-holder {
            float: left;
            width: 100%;
        }
        .ic-resume-single-slot-content-text .ic-resume-single-slot-list-text {
            min-height: 22px;
            line-height: 22px!important;
        }
        .ic-resume-single-slot-content-text .ic-resume-single-slot-list-item-bullet{
            top:10px;
        }
        .ic-resume-single-slot-content-text .ic-resume-single-slot-contact-info>div{
            float: left;
        }
        .ic-resume-single-slot-content-text .ic-resume-single-slot-contact-info-text, .ic-resume-single-slot-content-text .ic-resume-single-slot-contact-person-text {
            line-height: 27px!important;
        }
        .ic-resume-single-slot-content-text .ic-resume-single-slot-contact-separator{
            padding: 0 6px;
            line-height: 27px!important;
        }
        .ic-resume-menu-slot-main{
            position: relative;
        }
        .ic-resume-hyperlink-main{
            position: relative;
            display: inline-block;
        }
        .ic-resume-hyperlink-icon{
            position: absolute;
            right: -23px;
            top:0px;
            opacity: 0;
            visibility: hidden;
            transition: all 0.2s ease-in-out;
        }
        .ic-resume-hyperlink-main.icon-show .ic-resume-hyperlink-icon{
            right: -33px;
            opacity: 1;
            visibility: visible;
        }
        .ic-resume-hyperlink-icon svg{
            height: 24px;
            width: 24px;
        }
        .ic-resume-hyperlink-icon svg path{
            fill:{{ $document->template_data['template_data']['themes']['color'] ?? '#505050' }};
            transition: all 0.2s ease-in-out;
        }
        .ic-resume-hyperlink-icon:hover svg path{
            fill:#828282;
        }
        .ic-resume-single-slot-content-text .ic-resume-hyperlink-icon{
            cursor: pointer;
            position: static;
            opacity: 1;
            visibility: visible;
        }
        .ic-resume-single-slot-content-text .ic-resume-hyperlink-icon svg{
            width: 16px;
            height: 16px;
            margin-left: 5px;
            margin-top: -5px;
        }
        .ic-resume-single-slot-content-text .ic-resume-submenu-hyperlink{
            right: auto;
            left:0;
        }
        .ic-resume-single-slot-content-text .ic-resume-hyperlink-icon svg path{
            fill:{{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-resume-single-slot-name-text {
            font-size: 20px!important;
            min-height: 20px;
            line-height: 22px!important;
            margin-bottom: 4px;
        }
        .ic-resume-slot-settings-main {
            position: absolute;
            bottom: 40px;
            left: 0;
            width:350px;
            box-shadow: 0px 1px 10px rgba(14, 14, 14, .3);
            background: #fff;
            border-radius: 4px;
            padding: 10px 15px 15px;
            box-sizing: border-box;
            opacity: 0;
            visibility: hidden;
            height: 0;
        }
        .ic-resume-slot-settings-main.settings-active{
            bottom: 50px;
            opacity: 1;
            visibility: visible;
            height: auto;
        }
        .ic-resume-slot-settings-label{
            margin-bottom: 8px;
        }
        .ic-resume-slot-settings-label i{
            font-size: 16px;
            font-style: italic;
        }
        .ic-resume-slot-settings-main p{
            margin-bottom: 15px;
        }
        .ic-resume-slot-settings-inner{
            overflow: hidden;
        }
        .ic-resume-slot-settings-main .ic-resume-slot-settings-one-col{
            padding-right: 25px;
        }
        .ic-resume-slot-settings-main .ic-resume-slot-settings-two-col{
            padding-left: 15px;
        }
        .ic-resume-slot-settings-main .ic-resume-slot-settings-two-col,
        .ic-resume-slot-settings-main .ic-resume-slot-settings-one-col {
            width: 50%;
            margin-bottom: 20px;
            box-sizing: border-box;
            display: table;
            float: left;
        }
        .ic-resume-slot-settings-main .ic-resume-slot-settings-col-holder {
            width: 100%;
            height: 30px;
            display: table-cell;
            box-sizing: border-box;
            position: relative;
        }
        .ic-resume-slot-settings-main .ic-resume-slot-settings-col-symbol{
            border: 2px dashed #1E87FF;
            box-sizing: border-box;
            height: 100%;
            width: 100%;
        }
        .ic-resume-slot-settings-main .ic-resume-slot-settings-two-col .ic-resume-slot-settings-col-symbol {
            width: 45%;
            float: left;
        }
        .ic-resume-slot-settings-main .ic-resume-slot-settings-two-col .ic-resume-slot-settings-col-symbol:first-of-type {
            margin-right: 10%;
        }
        .ic-resume-single-slot .ic-resume-slot-settings-two-col .ic-resume-slot-settings-col-holder:after,
        .ic-resume-single-slot .ic-resume-slot-settings-one-col .ic-resume-slot-settings-col-holder:after {
            content: "";
            position: absolute;
            bottom: -12px;
            left: 50%;
            -webkit-transform: translateX(-50%) scaleX(0);
            -moz-transform: translateX(-50%) scaleX(0);
            transform: translateX(-50%) scaleX(0);
            height: 2px;
            width: 75px;
            background-color: #1E87FF;
            -webkit-transition: all 0.3s ease-in-out;
            -moz-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
        }
        .ic-resume-single-slot.two-col-active .ic-resume-slot-settings-two-col .ic-resume-slot-settings-col-holder:after,
        .ic-resume-single-slot .ic-resume-slot-settings-two-col .ic-resume-slot-settings-col-holder:hover:after,
        .ic-resume-single-slot.one-col-active .ic-resume-slot-settings-one-col .ic-resume-slot-settings-col-holder:after,
        .ic-resume-single-slot .ic-resume-slot-settings-one-col .ic-resume-slot-settings-col-holder:hover:after{
            -webkit-transform: translateX(-50%) scaleX(1);
            -moz-transform: translateX(-50%) scaleX(1);
            transform: translateX(-50%) scaleX(1);
        }
        .ic-resume-single-slot .ic-resume-single-slot-groups {
            width: 100%;
            display: inline-block;
        }
        .ic-resume-single-slot.two-col-active .ic-resume-single-slot-group{
            width: calc(100%/2 - 24px);
            float: left;
        }
        .ic-resume-single-slot.two-col-active .ic-resume-single-slot-group:nth-of-type(2n+1) {
            margin-left: 0!important;
            margin-right: 24px!important;
            clear: both;
        }
        .ic-resume-slot-settings-special-options {
            display: inline-block;
            width: 100%;
            margin-top: 10px;
        }
        .ic-resume-slot-settings-list-per-row{
            width: 50%;
            box-sizing: border-box;
            display: table;
            float: left;
            margin-bottom: 20px;
        }
        .ic-resume-slot-settings-list-per-row:nth-of-type(2n+1) {
            padding-right: 25px;
        }
        .ic-resume-slot-settings-list-per-row:nth-of-type(2n+2) {
            padding-left: 15px;
        }
        .ic-resume-slot-settings-list-holder {
            width: 100%;
            height: 30px;
            display: table-cell;
            box-sizing: border-box;
            position: relative;
        }
        .ic-resume-slot-settings-list-symbol{
            border: 1px dashed #1E87FF;
            box-sizing: border-box;
            height: 100%;
            width: 100%;
            display: table;
        }
        .ic-resume-slot-settings-list-inner {
            display: table-cell;
            height: 100%;
            position: relative;
        }
        .ic-resume-slot-settings-list-inner>div {
            height: 2px;
            width: calc(100% - 14px);
            left: 7px;
            background-color: #1E87FF;
            position: absolute;
        }
        .ic-resume-slot-settings-list-inner>div:nth-of-type(1) {
            top: 33.33%;
        }
        .ic-resume-slot-settings-list-inner>div:nth-of-type(2) {
            top: 66.66%;
        }
        .ic-resume-slot-settings-list-per-row .ic-resume-slot-settings-list-holder:after{
            content: "";
            position: absolute;
            bottom: -10px;
            left: 50%;
            -webkit-transform: translateX(-50%) scaleX(0);
            -moz-transform: translateX(-50%) scaleX(0);
            transform: translateX(-50%) scaleX(0);
            height: 2px;
            width: 50px;
            background-color: #1E87FF;
            -webkit-transition: all 0.3s ease-in-out;
            -moz-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
        }
        .ic-resume-slot-settings-list-per-row .ic-resume-slot-settings-list-holder:hover:after,
        .ic-resume-slot-settings-list-per-row.active .ic-resume-slot-settings-list-holder:after{
            -webkit-transform: translateX(-50%) scaleX(1);
            -moz-transform: translateX(-50%) scaleX(1);
            transform: translateX(-50%) scaleX(1);
        }
        .ic-resume-single-slot-list-wrapper{
            display: inline-block;
            width: 100%;
        }
        .ic-resume-single-slot[data-course-list="course-list-2"] .ic-resume-single-slot-list-item:nth-of-type(2n+1) {
            padding-right: 18px;
        }
        .ic-resume-single-slot[data-course-list="course-list-2"] .ic-resume-single-slot-list-item:nth-of-type(2n+2) {
            padding-left: 18px;
        }
        .ic-resume-single-slot[data-course-list="course-list-2"] .ic-resume-single-slot-list-item {
            width: 50%;
            float: left!important;
        }
        .ic-resume-single-slot[data-course-list="course-list-3"] .ic-resume-single-slot-list-item:nth-of-type(3n+1) {
            padding-right: 32px;
        }
        .ic-resume-single-slot[data-course-list="course-list-3"] .ic-resume-single-slot-list-item:nth-of-type(3n+3) {
            padding-left: 32px;
        }
        .ic-resume-single-slot[data-course-list="course-list-3"] .ic-resume-single-slot-list-item:nth-of-type(3n+2) {
            padding-right: 16px;
            padding-left: 16px;
        }
        .ic-resume-single-slot[data-course-list="course-list-3"] .ic-resume-single-slot-list-item {
            width: 33.33%;
            float: left!important;
        }
        .ic-resume-single-slot-group-name {
            padding-right: 30px;
            width: 180px;
            float: left;
            display: table-cell;
            vertical-align: top;
        }
        .ic-resume-tech-skill-skill.ic-resume-single-slot-skill {
            float: none;
            display: table-cell;
        }
        .ic-resume-single-slot-group-name-text[contenteditable=true]:empty:before {
            color: #202020 !important;
            font-weight: 700;
        }
        .ic-resume-single-slot-group-name-text {
            font-size: 18px;
            min-height: 18px;
            line-height: 22px;
            color: #202020;
            font-weight: 700;
        }
        .ic-resume-single-slot-skill-text {
            font-size: 18px!important;
            min-height: 18px;
            line-height: 22px!important;
        }
        .ic-resume-tech-skill-group.ic-resume-single-slot-group {
            margin-bottom: 12px;
        }
        .ic-resume-single-slot-author,
        .ic-resume-single-slot-type {
            display: inline-block;
            width: 100%;
        }
        .ic-resume-single-slot-author-text,
        .ic-resume-single-slot-type-text {
            font-size: 16px!important;
            min-height: 16px;
            line-height: 18px!important;
            color:#909090;
            width:initial!important;
            font-style: italic;
        }
        .ic-resume-single-slot-author-text.editable-div[contenteditable=true]:empty:before,
        .ic-resume-single-slot-type-text.editable-div[contenteditable=true]:empty:before {
            color: #909090;
        }
        .ic-resume-publication-name-text.ic-resume-single-slot-name-text{
            font-weight: 700;
            margin-bottom: 10px;
        }
        .ic-resume-publication-name-text.editable-div[contenteditable=true]:empty:before {
            font-weight: 700;
        }
        .ic-resume-single-slot-content-text-edit .ic-resume-single-slot-extra-text{
            border-bottom-style: dashed!important;
        }
        .ic-resume-single-slot-extra-text,
        .ic-resume-single-slot-author-list-text {
            font-size: 18px!important;
            min-height: 18px;
            line-height: 22px!important;
            margin-bottom: 10px;
        }
        .ic-resume-single-slot-date-text {
            font-size: 18px!important;
            min-height: 18px;
            line-height: 22px!important;
            width:initial!important;
            font-style: italic;
        }
        .ic-resume-publication-desc-text.ic-resume-single-slot-desc-text {
            color:#202020;
            font-style: normal;
        }
        .ic-resume-single-slot-content-text-edit .ic-resume-hyperlink-main{
            display: block;
        }
        .ic-resume-interest-group.ic-resume-single-slot-group,
        .ic-resume-cause-group.ic-resume-single-slot-group{
            width:auto;
            margin-right: 10px;
            margin-bottom: 8px;
            float: left;
            margin-left: 0 !important;
            max-width: 100%;
            min-height: 44px;
            transition: padding 0.2s ease-in-out;
        }
        .ic-resume-single-slot-icontitle {
            border: 2px solid #b2b2b2;
            padding: 6px 14px;
            border-radius: 6px;
            float: left;
            transition: all 0.2s ease-in-out;
        }

        .ic-resume-interest-icontitle-text,
        .ic-resume-cause-icontitle-text{
            font-size: 18px!important;
            min-height: 18px;
            line-height: 22px!important;
            padding-top: 0 !important;
        }

        .ic-resume-single-slot-icon{
            height: 30px;
            width: 30px;
            position: absolute;
            left: 0;
            top: -4px;
            background-size: 30px 30px;
            background-position: 0 0;
            opacity: 0;
            visibility: hidden;
            transition: all 0.2s ease-in-out;
            transition-delay: .1s;
        }
        .ic-resume-single-slot.icon-show .ic-resume-single-slot-icon{
            opacity: 1;
            visibility: visible;
        }
        .ic-resume-single-slot.icon-show .ic-resume-interest-group.ic-resume-single-slot-group,
        .ic-resume-single-slot.icon-show .ic-resume-cause-group.ic-resume-single-slot-group{
            padding-left: 40px;
        }
        .ic-resume-single-slot.icon-show .ic-resume-single-slot-icontitle {
            border: none;
            padding: 0px;
            border-radius: 0px;
            float: left;
        }
        .ic-resume-single-slot.icon-show .ic-resume-single-slot-content-text{
            margin-top: 2px
        }
        .ic-resume-single-slot.icon-show .ic-resume-interest-group.ic-resume-single-slot-group:nth-of-type(2n+1) ,
        .ic-resume-single-slot.icon-show .ic-resume-cause-group.ic-resume-single-slot-group:nth-of-type(2n+1) {
            padding-right: 18px;
            clear: both;
        }
        .ic-resume-single-slot.icon-show .ic-resume-interest-group.ic-resume-single-slot-group,
        .ic-resume-single-slot.icon-show .ic-resume-cause-group.ic-resume-single-slot-group{
            width: 50%;
            min-height: 36px;
            margin-bottom: 12px;
            margin-right: 0;
        }
        #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(2),
        .ic-resume-single-slot.icon-show .ic-resume-interest-group.ic-resume-single-slot-group:nth-last-of-type(2),
        .ic-resume-single-slot.icon-show .ic-resume-cause-group.ic-resume-single-slot-group:nth-last-of-type(2){
            margin-bottom: 0 !important;
        }
        .one-column-layout #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(3),
        .one-column-layout #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(4){
            margin-bottom: 0 !important;
        }
        .one-column-layout #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(3),
        .one-column-layout #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(4){
            margin-bottom: 12px !important;
        }
        .one-column-layout .ic-resume-single-slot.icon-show .ic-resume-interest-group.ic-resume-single-slot-group:nth-last-of-type(2),
        .one-column-layout .ic-resume-single-slot.icon-show .ic-resume-cause-group.ic-resume-single-slot-group:nth-last-of-type(2),
        .one-column-layout .ic-resume-single-slot.icon-show .ic-resume-interest-group.ic-resume-single-slot-group:nth-last-of-type(3),
        .one-column-layout .ic-resume-single-slot.icon-show .ic-resume-cause-group.ic-resume-single-slot-group:nth-last-of-type(3),
        .one-column-layout .ic-resume-single-slot.icon-show .ic-resume-interest-group.ic-resume-single-slot-group:nth-last-of-type(4),
        .one-column-layout .ic-resume-single-slot.icon-show .ic-resume-cause-group.ic-resume-single-slot-group:nth-last-of-type(4){
            margin-bottom: 0 !important;
        }

        .ic-resume-single-slot-titlex {
            font-size: 18px!important;
            min-height: 18px;
            line-height: 22px!important;
            overflow: hidden;
        }
        .ic-resume-single-slot-titlex-text{
            float: left;
            width: auto !important;
        }
        #ic-resume-soft-skill.highlight-active .ic-resume-single-slot-titlex-text,
        #ic-resume-skill.highlight-active .ic-resume-single-slot-titlex-text{
            float: none;
        }
        #ic-resume-soft-skill.highlight-active .ic-resume-single-slot-titlex,
        #ic-resume-skill.highlight-active .ic-resume-single-slot-titlex{
            overflow: visible;
        }
        #ic-resume-soft-skill.highlight-active .ic-resume-skill-group.ic-resume-single-slot-group ,
        #ic-resume-skill.highlight-active .ic-resume-skill-group.ic-resume-single-slot-group {
            width: auto;
            margin-right: 10px;
            margin-bottom: 8px;
            float: left;
            margin-left: 0;
            max-width: 100%;
            transition: padding 0.2s ease-in-out;
        }
        #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group,
        #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group,
        #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group{
            width: 100%;
            margin-right: 0px;
            margin-bottom: 24px !important;
            float: left;
            margin-left: 0;
            max-width: 100%;
            transition: padding 0.2s ease-in-out;
        }

        #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:last-child,
        #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:last-child,
        #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:last-child{
            margin-bottom: 0 !important;
        }
        .one-column-layout #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(2),
        .one-column-layout #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(1),
        .one-column-layout #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-last-of-type(2),
        .one-column-layout #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-last-of-type(2){
            margin-bottom: 0 !important;
        }
        .ic-resume-skill-group.ic-resume-single-slot-group:nth-last-of-type(1),
        .ic-resume-skill-group.ic-resume-single-slot-group:nth-last-of-type(2){
            margin-bottom: 0 !important;
        }
        .one-column-layout .ic-resume-skill-group.ic-resume-single-slot-group:nth-last-of-type(1),
        .one-column-layout .ic-resume-skill-group.ic-resume-single-slot-group:nth-last-of-type(2),
        .one-column-layout .ic-resume-skill-group.ic-resume-single-slot-group:nth-last-of-type(3),
        .one-column-layout .ic-resume-skill-group.ic-resume-single-slot-group:nth-last-of-type(4){
            margin-bottom: 0 !important;
        }
        #ic-resume-soft-skill.highlight-active.infographics-show .ic-resume-single-slot-titlex ,
        #ic-resume-skill.highlight-active.infographics-show .ic-resume-single-slot-titlex {
            color: #202020;
        }
        .ic-resume-skill-group.ic-resume-single-slot-group {
            width: 50%;
            margin-right: 0px;
            margin-bottom: 14px;
            float: left;
            margin-left: 0 !important;
            max-width: 100%;
            transition: padding 0.2s ease-in-out;
        }
        .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1) {
            padding-right: 18px;
        }
        .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+2) {
            padding-left: 18px;
        }
        #ic-resume-soft-skill.highlight-active .ic-resume-skill-group.ic-resume-single-slot-group ,
        #ic-resume-skill.highlight-active .ic-resume-skill-group.ic-resume-single-slot-group {
            padding: 0;
            min-height: 44px;
        }
        #ic-resume-soft-skill.highlight-active.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group ,
        #ic-resume-skill.highlight-active.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group {
            min-height: auto;
        }
        #ic-resume-soft-skill.highlight-active .ic-resume-skill-group.ic-resume-single-slot-group .ic-resume-single-slot-group-header,
        #ic-resume-skill.highlight-active .ic-resume-skill-group.ic-resume-single-slot-group .ic-resume-single-slot-group-header{
            background: #1E87FF;
            padding: 5.5px 14px;
            border-radius: 4px;
            line-height: 22px;
        }
        #ic-resume-soft-skill.highlight-active.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group .ic-resume-single-slot-group-header,
        #ic-resume-skill.highlight-active.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group .ic-resume-single-slot-group-header{
            background: transparent;
            padding: 0;
            border-radius: 0;
        }
        #ic-resume-soft-skill.highlight-active .ic-resume-single-slot-titlex ,
        #ic-resume-skill.highlight-active .ic-resume-single-slot-titlex {
            color:#fff;
        }
        .one-column-layout .ic-resume-box-slot1 .ic-resume-skill-groups,
        .one-column-layout .ic-resume-box-slot1 .ic-resume-language-groups{
            padding-right: 50px;
            padding-left: 50px;
        }
        .ic-resume-box-slot1 .ic-resume-skill-groups,
        .ic-resume-box-slot1 .ic-resume-language-groups{
            padding-right: 36px;
            padding-left: 50px;
        }
        .ic-resume-box-slot2 .ic-resume-skill-groups,
        .ic-resume-box-slot2 .ic-resume-language-groups{
            padding-right: 50px;
            padding-left: 36px;
        }

        #ic-resume-soft-skill.highlight-active .editable-div[contenteditable=true]:empty:before ,
        #ic-resume-skill.highlight-active .editable-div[contenteditable=true]:empty:before {
            color: #fff;
        }
        #ic-resume-soft-skill.highlight-active .ic-resume-single-slot-content-text-edit .editable-div ,
        #ic-resume-skill.highlight-active .ic-resume-single-slot-content-text-edit .editable-div {
            padding: 4px 0;
            border-bottom: 1px solid rgba(255,255,255,.7);
        }
        .ic-resume-rating {
            display: none;
        }
        #ic-resume-language.infographics-show .ic-resume-rating ,
        #ic-resume-soft-skill.infographics-show .ic-resume-rating ,
        #ic-resume-skill.infographics-show .ic-resume-rating {
            display: inline-block;
        }
        #ic-resume-soft-skill.infographics-show .editable-div[contenteditable=true]:empty:before ,
        #ic-resume-skill.infographics-show .editable-div[contenteditable=true]:empty:before {
            color: #202020;
        }
        #ic-resume-soft-skill.infographics-show .ic-resume-single-slot-content-text-edit .editable-div ,
        #ic-resume-skill.infographics-show .ic-resume-single-slot-content-text-edit .editable-div {
            border-bottom: 1px solid rgba(150,150,150,.7);
        }

        ul.ic-resume-rating-stars {
            list-style: none;
            margin:0;
            padding:0;
            overflow:hidden;
        }
        ul.ic-resume-rating-stars li.star{
            float: left;
        }

        ul.ic-resume-rating-stars li.star .star-main {
            border-radius: 100%;
        }
        ul.ic-resume-rating-stars .inner-star {
            border: 2px solid {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            border-radius: 100%;
            transition: all 0.1s ease-in-out;
        }
        ul.ic-resume-rating-stars.hover li.star.selected .inner-star{
            background: transparent;
        }
        ul.ic-resume-rating-stars.hover li.star.selected.hover .inner-star,
        ul.ic-resume-rating-stars li.star.selected .inner-star,
        ul.ic-resume-rating-stars li.star.hover .inner-star{
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-resume-single-slot[data-style=infographic-style-2] ul.ic-resume-rating-stars.hover li.star.selected .inner-star,
        .ic-resume-single-slot[data-style=infographic-style-2] ul.ic-resume-rating-stars li.star .inner-star{
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            opacity: .5;
        }
        .ic-resume-single-slot[data-style=infographic-style-2] ul.ic-resume-rating-stars.hover li.star.selected.hover .inner-star,
        .ic-resume-single-slot[data-style=infographic-style-2] ul.ic-resume-rating-stars li.star.selected .inner-star,
        .ic-resume-single-slot[data-style=infographic-style-2] ul.ic-resume-rating-stars li.star.hover .inner-star{
            opacity: 1;
        }
        .ic-resume-single-slot[data-style=infographic-style-3] ul.ic-resume-rating-stars.hover li.star.selected .inner-star,
        .ic-resume-single-slot[data-style=infographic-style-3] ul.ic-resume-rating-stars li.star .inner-star{
            background: #202020;
            border: 4px solid #fff;
        }
        .ic-resume-single-slot[data-style=infographic-style-3] ul.ic-resume-rating-stars.hover li.star.selected.hover .inner-star,
        .ic-resume-single-slot[data-style=infographic-style-3] ul.ic-resume-rating-stars li.star.selected .inner-star,
        .ic-resume-single-slot[data-style=infographic-style-3] ul.ic-resume-rating-stars li.star.hover .inner-star{
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            border: 2px solid {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars li.star .star-main{
            border: 2px solid {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars.hover li.star.selected .inner-star,
        .ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars li.star .inner-star{
            display: none;
        }
        .ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars.hover li.star.selected.hover .inner-star,
        .ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars li.star.selected .inner-star,
        .ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars li.star.hover .inner-star{
            background: #7f7f7f;
            border: 4px solid #fff;
            width: 16px;
            height: 16px;
            display: block;
        }
        .ic-resume-single-slot[data-style=infographic-style-5] ul.ic-resume-rating-stars li.star .star-main{
            border-radius: 4px;
        }
        .ic-resume-single-slot[data-style=infographic-style-5] ul.ic-resume-rating-stars.hover li.star.selected .inner-star,
        .ic-resume-single-slot[data-style=infographic-style-5] ul.ic-resume-rating-stars li.star .inner-star{
            border-radius: 4px;
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            opacity: .5;
        }
        .ic-resume-single-slot[data-style=infographic-style-5] ul.ic-resume-rating-stars.hover li.star.selected.hover .inner-star,
        .ic-resume-single-slot[data-style=infographic-style-5] ul.ic-resume-rating-stars li.star.selected .inner-star,
        .ic-resume-single-slot[data-style=infographic-style-5] ul.ic-resume-rating-stars li.star.hover .inner-star{
            opacity: 1;
        }

        .ic-resume-single-slot[data-style=infographic-style-1] .ic-resume-rating-slider-main,.ic-resume-single-slot[data-style=infographic-style-2] .ic-resume-rating-slider-main,
        .ic-resume-single-slot[data-style=infographic-style-3] .ic-resume-rating-slider-main,.ic-resume-single-slot[data-style=infographic-style-4] .ic-resume-rating-slider-main,
        .ic-resume-single-slot[data-style=infographic-style-5] .ic-resume-rating-slider-main{
            display: none;
        }
        .ic-resume-single-slot[data-style=infographic-style-6] .ic-resume-rating-stars,.ic-resume-single-slot[data-style=infographic-style-7] .ic-resume-rating-stars,.ic-resume-single-slot[data-style=infographic-style-8] .ic-resume-rating-stars,
        .ic-resume-single-slot[data-style=infographic-style-9] .ic-resume-rating-stars,.ic-resume-single-slot[data-style=infographic-style-10] .ic-resume-rating-stars,.ic-resume-single-slot[data-style=infographic-style-11] .ic-resume-rating-stars,
        .ic-resume-single-slot[data-style=infographic-style-12] .ic-resume-rating-stars{
            display: none;
        }
        .ic-resume-rating-slider{
            height: 16px;
            width: 100%;
            border: 2px solid {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            position: relative;
            width: 156px;
            overflow:hidden;
        }
        .ui-slider-range {
            position: absolute;
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            left: 0;
            height: 100%;
        }
        .vue-slider {
            width: 156px !important;
            height: 16px !important;
            padding: 0 !important;
        }
        .vue-slider-rail{
            height: 16px !important;
            border: 2px solid #1E87FF;
            position: relative;
            width: 156px !important;
            overflow:hidden;
        }
        .vue-slider-process{
            position: absolute;
            background: #1E87FF;
            left: 0;
            height: 100%;
        }
        .ic-resume-single-slot.infographics-show .ic-resume-legend-row{
            overflow:hidden;
            display: block;
        }
        .ic-resume-single-slot[data-style=infographic-style-1] .ic-resume-legend-row,.ic-resume-single-slot[data-style=infographic-style-2] .ic-resume-legend-row,
        .ic-resume-single-slot[data-style=infographic-style-3] .ic-resume-legend-row,.ic-resume-single-slot[data-style=infographic-style-4] .ic-resume-legend-row,
        .ic-resume-single-slot[data-style=infographic-style-5] .ic-resume-legend-row{
            display: none;
        }
        .ic-resume-legend-row{
            display: none;
        }

        .ic-resume-single-slot .ic-resume-line-dec{
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            height: 100%;
            width: 2px;
            position: absolute;
        }
        .ic-resume-single-slot .ic-resume-line-dec:nth-child(1) {
            left: 0!important;
        }
        .ic-resume-single-slot .ic-resume-line-dec:nth-child(2) {
            left: 33.33%!important;
        }
        .ic-resume-single-slot .ic-resume-line-dec:nth-child(3) {
            right: 33.33%!important;
        }
        .ic-resume-single-slot .ic-resume-line-dec:nth-child(4) {
            right: 0!important;
        }
        .ic-resume-single-slot[data-style=infographic-style-6] .ic-resume-rating-slider{
            border:none;
        }
        .ic-resume-single-slot[data-style=infographic-style-6] .ui-slider-range {
            background: transparent;
            border: 2px solid {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-resume-single-slot[data-style=infographic-style-7] .ic-resume-rating-slider{
            border:none;
        }
        .ic-resume-single-slot[data-style=infographic-style-7] .ui-slider-range {
            height: 2px;
            top: 50%;
        }
        .ic-resume-single-slot[data-style=infographic-style-9] .ic-resume-rating-slider{
            border-radius:8px;
        }
        .ic-resume-single-slot[data-style=infographic-style-10] .ic-resume-rating-slider {
            border: none;
        }
        .ic-resume-single-slot[data-style=infographic-style-10] .ui-slider-range {
            z-index: 1;
        }
        .ic-resume-single-slot[data-style=infographic-style-10] .ic-resume-rating-slider:after,
        .ic-resume-single-slot[data-style=infographic-style-12] .ic-resume-rating-slider:after {
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            position: absolute;
            content: "";
            opacity: .5;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
        }
        .ic-resume-single-slot[data-style=infographic-style-11] .ic-resume-rating-slider {
            border: none;
        }
        .ic-resume-single-slot[data-style=infographic-style-12] .ic-resume-rating-slider {
            border: none;
            border-radius:8px;
        }
        .ic-resume-single-slot[data-style=infographic-style-12] .ui-slider-range {
            z-index: 1;
        }
        #ic-resume-language .ic-resume-infographics-slot-slider{
            height: 0;
            opacity: 0;
            visibility: hidden;
        }
        #ic-resume-language.infographics-show .ic-resume-slot-settings-main.settings-active .ic-resume-infographics-slot-slider{
            height: 100%;
            opacity: 1;
            visibility: visible;
        }
        .ic-resume-single-slot.infographics-show .ic-resume-lang-levels{
            display: none;
        }
        .ic-resume-infographic-item.lang-rate {
            height: 60px;
        }
        .ic-resume-infographic-item.lang-rate .ic-resume-infographic-item-inner {
            width: 40px;
        }
        .ic-resume-infographic-item.lang-rate[data-infographic-style="infographic-style-14"] .ic-resume-infographic-item-icon {
            background-position: -40px 0;
        }
        .ic-resume-infographic-item.lang-rate .ic-resume-infographic-item-inner:after{
            width:25px;
        }
        .ic-resume-lang-levels {
            padding: 16px 0 5px;
        }
        .ic-resume-lang-label-proficiency{
            margin-bottom: 5px;
        }
        .ic-resume-lang-levels ul{
            padding:0;
            margin:0;
            margin-top: 5px;
            list-style:none;
        }
        .ic-resume-lang-levels ul li{
            margin-bottom: 10px;
            overflow:hidden;
        }
        .ic-resume-lang-levels ul li:last-child{
            margin-bottom: 0px;
            overflow:hidden;
        }
        .ic-resume-lang-levels ul li span{
            opacity: .6;
            float: left;
            margin-top: 6px;
            margin-right: 12px;
            font-size:14px;
        }
        .ic-resume-lang-levels ul li input{
            border:none;
            border-bottom: 1px solid #c5c5c5;
            padding:3px 0;
            width: calc(100% - 44px);
            float: right;
            outline: none;
            font-size:14px;
        }
        #ic-resume-language.infographics-show .ic-resume-language-label-holder{
            display: none;
        }
        .ic-resume-language-label-holder{
            font-size: 16px;
            min-height: 16px;
            line-height: 18px;
            color:{{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            font-style: italic;
            margin-top: -1px;
            position: relative;
        }
        .ic-resume-language-range{
            width: max-content;
            position: absolute;
            top: -18px;
            left: 0;
            box-shadow: 0px 1px 10px rgba(14, 14, 14, .2);
            background: #fff;
            border-radius: 4px;
            opacity: 0;
            visibility: hidden;
            transition: all 0.2s ease-in-out;
            z-index: 9999;
        }
        .ic-resume-language-label-holder.active .ic-resume-language-range {
            top: -32px;
            opacity: 1;
            visibility: visible;
        }
        .ic-resume-language-range-item-btn-settings svg{
            height: 18px;
            width: 18px;
            margin-top: -3px;
        }
        ul.ic-resume-language-range-inner {
            padding: 0 10px;
            margin: 0;
            list-style: none;
            display: -webkit-flex;
            display: -moz-flex;
            display: -ms-flex;
            display: -o-flex;
            display: flex;
        }
        ul.ic-resume-language-range-inner li {
            padding: 8px 6px 6px 6px;
            font-style: initial;
            color: #202020;
        }
        ul.ic-resume-language-range-inner li:hover,
        ul.ic-resume-language-range-inner li.active{
            color:{{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        #ic-resume-language[data-style="infographic-style-13"] .ic-resume-rating,
        #ic-resume-language[data-style="infographic-style-14"] .ic-resume-rating{
            display: none;
        }
        #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group,
        #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group{
            width:20%;
            margin-bottom: 24px;
            padding: 0 4px !important;
        }
        #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-single-slot-group-header,
        #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-single-slot-group-header{
            padding-right: 0;
            text-align: center;
        }
        #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-titlex.ic-resume-single-slot-titlex.ic-resume-single-slot-edit-text,
        #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-titlex.ic-resume-single-slot-titlex.ic-resume-single-slot-edit-text {
            text-align: center;
        }
        #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-titlex-text.ic-resume-single-slot-titlex-text,
        #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-titlex-text.ic-resume-single-slot-titlex-text {
            float: none;
            width: 100% !important;
        }
        #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1){
            padding-right: 0;
            clear: none;
        }
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(5n+1),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(5n+1){
            clear: both;
        }
        .two-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(3n+1),
        .two-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(3n+1){
            clear: both;
        }
        #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+2),
        #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+2){
            padding-left: 0;
        }
        #ic-resume-language[data-style="infographic-style-13"] .ic-resume-language-rate-shape {
            height: 80px;
            width: 80px;
            background: #d1d1d1;
            margin: 0 auto;
            position: relative;
        }
        #ic-resume-language[data-style="infographic-style-14"] .ic-resume-language-rate-shape {
            height: 80px;
            width: 80px;
            margin: 0 auto;
            border:2px solid #202020;
            border-radius: 100%;
            position: relative;
        }
        .ic-resume-language-rate-holder {
            margin-bottom: 8px;
            display: none;
        }
        #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-rate-holder ,
        #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-rate-holder {
            display: block;
        }
        #ic-resume-language[data-style="infographic-style-13"] .ic-resume-language-rate-inner {
            display: block;
            position: absolute;
            bottom: 0;
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            height: 80%;
            width: 100%;
        }
        #ic-resume-language[data-style="infographic-style-14"] .ic-resume-language-rate-inner {
            display: block;
            position: absolute;
            bottom: 0;
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            height: 100%;
            width: 100%;
            border-radius: 100%;
            border:20px solid #fff;
        }
        #ic-resume-language[data-style="infographic-style-13"] .ic-resume-language-rate-holder[data-rate="1"] .ic-resume-language-rate-inner {
            height: 20%;
        }
        #ic-resume-language[data-style="infographic-style-13"] .ic-resume-language-rate-holder[data-rate="2"] .ic-resume-language-rate-inner {
            height: 40%;
        }
        #ic-resume-language[data-style="infographic-style-13"] .ic-resume-language-rate-holder[data-rate="3"] .ic-resume-language-rate-inner {
            height: 58%;
        }
        #ic-resume-language[data-style="infographic-style-13"] .ic-resume-language-rate-holder[data-rate="4"] .ic-resume-language-rate-inner {
            height: 80%;
        }
        #ic-resume-language[data-style="infographic-style-13"] .ic-resume-language-rate-holder[data-rate="5"] .ic-resume-language-rate-inner {
            height: 100%;
        }
        #ic-resume-language[data-style="infographic-style-14"] .ic-resume-language-rate-holder[data-rate="1"] .ic-resume-language-rate-inner {
            border:20px solid #fff;
        }
        #ic-resume-language[data-style="infographic-style-14"] .ic-resume-language-rate-holder[data-rate="2"] .ic-resume-language-rate-inner {
            border:14px solid #fff;
        }
        #ic-resume-language[data-style="infographic-style-14"] .ic-resume-language-rate-holder[data-rate="3"] .ic-resume-language-rate-inner {
            border:12px solid #fff;
        }
        #ic-resume-language[data-style="infographic-style-14"] .ic-resume-language-rate-holder[data-rate="4"] .ic-resume-language-rate-inner {
            border:6px solid #fff;
        }
        #ic-resume-language[data-style="infographic-style-14"] .ic-resume-language-rate-holder[data-rate="5"] .ic-resume-language-rate-inner {
            border:2px solid #fff;
        }
        .ic-resume-language-rate{
            width: max-content;
            position: absolute;
            top: 10px;
            left: 0;
            box-shadow: 0px 1px 10px rgba(14, 14, 14, .2);
            background: #fff;
            border-radius: 4px;
            opacity: 0;
            visibility: hidden;
            transition: all 0.2s ease-in-out;
            z-index: 9999;
        }
        .ic-resume-language-rate-holder.active .ic-resume-language-rate {
            top: 0px;
            opacity: 1;
            visibility: visible;
        }
        ul.ic-resume-language-rates-inner {
            padding: 0 10px;
            margin: 0;
            list-style: none;
            display: -webkit-flex;
            display: -moz-flex;
            display: -ms-flex;
            display: -o-flex;
            display: flex;
        }
        ul.ic-resume-language-rates-inner li {
            padding: 8px 6px 6px 6px;
            font-style: initial;
            color: #202020;
        }
        ul.ic-resume-language-rates-inner li:hover,
        ul.ic-resume-language-rates-inner li.active{
            color:{{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-resume-single-slot[data-style=infographic-style-6] .vue-slider-rail{
            border:none;
        }
        .ic-resume-single-slot[data-style=infographic-style-6] .vue-slider-process {
            background: transparent;
            border: 2px solid {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-resume-single-slot[data-style=infographic-style-7] .vue-slider-rail{
            border:none;
        }
        .ic-resume-single-slot[data-style=infographic-style-7] .vue-slider-process {
            height: 2px !important;
            top: 50% !important;
        }
        .ic-resume-single-slot[data-style=infographic-style-9] .vue-slider-rail{
            border-radius:8px;
        }
        .ic-resume-single-slot[data-style=infographic-style-10] .vue-slider-rail {
            border: none;
            background: rgba(30,135,255,.5);
        }
        .ic-resume-single-slot[data-style=infographic-style-11] .vue-slider-rail {
            border: none;
        }
        .ic-resume-single-slot[data-style=infographic-style-12] .vue-slider-rail {
            border: none;
            border-radius:8px;
            background: rgba(30,135,255,.5);
        }

        /*====================================slot-one========================*/
        .ic-resume-single-slot-titlex-text,
        .ic-resume-single-slot-title-text,
        .ic-resume-single-slot-place-text,
        .ic-resume-single-slot-location-text,
        .ic-resume-single-slot-desc-text,
        .ic-resume-single-slot-list-header-text,
        .ic-resume-single-slot-list-text,
        .ic-resume-single-slot-name-text,
        .ic-resume-single-slot-contact-person-text,
        .ic-resume-single-slot-contact-info-text,
        .ic-resume-single-slot-skill-text,
        .ic-resume-single-slot-group-name-text,
        .ic-resume-single-slot-type-text,
        .ic-resume-single-slot-author-text,
        .ic-resume-single-slot-extra-text,
        .ic-resume-single-slot-author-list-text,
        .ic-resume-single-slot-date-text,
        .ic-resume-single-slot-icontitle{
            font-kerning: normal;
            text-rendering: optimizeLegibility;
            speak: none;
            -webkit-font-smoothing: subpixel-antialiased;
            -moz-osx-font-smoothing: grayscale;
            font-variant: normal;
            word-wrap: break-word;
            -webkit-word-break: normal;
            word-break: normal;
            white-space: normal;
            max-width: 100%;
        }

        .resume-template #template-functional .ic-resume-box-slot1 .ic-resume-single-slot-header{
            padding-left: 0;
        }
        .resume-template #template-functional .ic-resume-box-slot1 .ic-resume-single-slot-group{
            margin-left: 0;
        }
        .resume-template #template-functional .ic-resume-box-slot1 .ic-resume-single-slot-header .ic-resume-menu-slot{
            left:0;
        }

        .ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot-place-text {
            font-size: 22px;
            display: inherit !important;
        }

        .ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot-group-name {
            padding-right: 0px;
            width: 100%;
            margin-bottom: 4px;
            float: none;
            display: block;
        }
        .ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-tech-skill-skill.ic-resume-single-slot-skill{
            display: block;
        }




        .ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-progress-bar-legend {
            width: 100%;
            float: none;
        }
        .ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot .ic-resume-progress-bar-legend:nth-of-type(2n+1) {
            padding-left: 0;
            padding-right: 0px;
        }
        .ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot .ic-resume-progress-bar-legend:nth-of-type(2n+2) {
            display: none;
        }

        .ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+2) {
            padding-left: 0px;
        }
        .ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-soft-skill.highlight-active .ic-resume-skill-group.ic-resume-single-slot-group,
        .ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-skill.highlight-active .ic-resume-skill-group.ic-resume-single-slot-group {
            width: auto !important;
        }
        .ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-soft-skill.highlight-active.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group,
        .ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-skill.highlight-active.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group {
            width: 100% !important;
        }
        .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #ic-resume-skill.highlight-active.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #ic-resume-soft-skill.highlight-active.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        .two-column-layout #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1){
            clear: both;
        }
        #ic-resume-soft-skill.highlight-active .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #ic-resume-skill.highlight-active .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1){
            clear: none;
        }
        .one-column-layout #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(4n+1){
            clear: both;
        }
        .ic-resume-box-slot1 #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group{
            width: 25%;
            padding-right: 36px;
        }
        .ic-resume-scrollbar-part.two-column-layout #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group{
            width: 50%;
            margin-bottom: 24px;
        }
        #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1) {
            padding-right: 18px;
        }
        #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+2) {
            padding-left: 18px;
            padding-right: 0;
        }
        .ic-resume-box-slot1 #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group{
            width: 50%;
            padding-right: 0px;
        }
        .ic-resume-box-slot1 #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(4n) {
            padding-right: 0;
        }

        .ic-resume-box-slot1 #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group,
        .ic-resume-box-slot1 #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group {
            width: 20%;
            margin-bottom: 24px;
            padding: 0 4px !important;
        }


        .ic-resume-box-slot1 .ic-resume-interest-groups.ic-resume-single-slot-groups, .ic-resume-box-slot1 .ic-resume-cause-groups.ic-resume-single-slot-groups{
            margin-left: 0;
        }

        .ic-coverletter-page-inner {
            position: relative;
            margin-bottom: 130px;
            padding-bottom: 102px;
        }
        .ic-coverletter-page-inner:last-child{
            margin-bottom: 0;
        }
        .ic-editor-footer {
            padding: 30px 50px;
            display: block;
            vertical-align: middle;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            width: 100%;
            bottom: 0;
            position: absolute;
        }
        .ic-editor-footer .ic-editor-page-no{
            float: right;
            display: inline-block;
        }
        .ic-editor-footer .ic-editor-page-no .ic-editor-page-size{
            font-size: 18px!important;
            min-height: 18px;
            line-height: 22px!important;
            font-style: italic;
            color:#7c7c7c;
        }
        .ic-editor-footer .ic-editor-page-no .ic-editor-page-size {
            -webkit-touch-callout: none; /* iOS Safari */
            -webkit-user-select: none; /* Safari */
            -khtml-user-select: none; /* Konqueror HTML */
            -moz-user-select: none; /* Old versions of Firefox */
            -ms-user-select: none; /* Internet Explorer/Edge */
            user-select: none; /* Non-prefixed version, currently
                                        supported by Chrome, Opera and Firefox */
        }
        .ic-bg-skin-top {
            top: 0!important;
            background-position: left top!important;
        }
        .ic-bg-skin-bottom {
            bottom: 0!important;
            background-position: left bottom!important;
        }
        .ic-bg-skin-bottom, .ic-bg-skin-top {
            position: absolute!important;
            width: 100%;
            background-size: cover!important;
        }
        .ic-bg-skin-bottom svg,
        .ic-bg-skin-top svg{
            position: absolute;
        }
        .ic-bg-skin-top svg{
            top: 0;
        }
        .ic-bg-skin-bottom svg{
            bottom: 0;
        }

        .ic-bg-skin-bottom,
        .ic-bg-skin-top {
            height: 842px!important;
        }
        .ic-coverletter-us-format-page.ic-coverletter-page-wrapper .ic-bg-skin-top,
        .ic-coverletter-us-format-page.ic-coverletter-page-wrapper .ic-bg-skin-bottom{
            height: 792px!important;
        }
        .ic-coverletter-bottom-svg{
            position: absolute;
            bottom: 0;
        }


        #ic-layout-control-menu {
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            box-sizing: border-box;
            display: none;
            height: 26px;
            left: 0;
            right: 0;
            padding-left: 154px;
            position: fixed;
            z-index: 99999;
            background-color: #1E87FF;
            transition: padding .4s ease-out;
            width: 100%!important;
        }
        .ic-coverletter-side-active #ic-layout-control-menu{
            padding-left: 520px;
        }
        .layout-control-active #ic-layout-control-menu{
            display: block;
        }
        .ic-coverletter-contact-item:first-child{
            margin-top: 0;
        }
        .ic-coverletter-title-sumury-text[contenteditable=true]:empty:focus:before {
            color: #969696 !important;
        }


        .ic-resume-progress-bar-legend {
            width: 50%;
            float: left;
        }
        .one-column-layout .ic-resume-box-slot1 .ic-resume-legend-row{
            padding-left: 50px;
            padding-right: 50px;
        }
        .ic-resume-box-slot1 .ic-resume-legend-row{
            padding-left: 50px;
            padding-right: 36px;
        }
        .ic-resume-box-slot2 .ic-resume-legend-row{
            padding-left: 36px;
            padding-right: 50px;
        }
        .ic-resume-single-slot .ic-resume-legend-holder {
            margin-top: 4px;
            margin-bottom: 12px;
            overflow: hidden;
        }
        .ic-resume-single-slot .ic-resume-legend {
            float: right;
            width: 156px;
            height: 8px;
            position: relative;
        }

        .ic-resume-box-slot2 .ic-resume-single-slot .ic-resume-single-slot-groups{
            padding-left: 36px;
            padding-right: 50px;
        }
        .ic-resume-box-slot1 .ic-resume-single-slot .ic-resume-single-slot-groups{
            padding-left: 50px;
            padding-right: 36px;
        }
        .one-column-layout .ic-resume-box-slot1 .ic-resume-single-slot .ic-resume-single-slot-groups{
            padding-left: 50px;
            padding-right: 50px;
        }
        #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group .ic-resume-single-slot-group-header,
        #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group .ic-resume-single-slot-group-header{
            padding-right: 50% !important;
        }
        #ic-resume-skill.infographics-show .ic-resume-rating,
        #ic-resume-soft-skill.infographics-show .ic-resume-rating {
            position: absolute;
            width: 50%;
            top:0;
            right: 0;
        }
        .one-column-layout #ic-resume-skill.infographics-show .ic-resume-rating,
        .one-column-layout #ic-resume-soft-skill.infographics-show .ic-resume-rating {
            width: calc(50% - 18px);
        }
        ul.ic-resume-rating-stars li.star,
        ul.ic-resume-rating-stars li.star{
            padding-right: 20px;
        }
        ul.ic-resume-rating-stars li.star .star-main,
        ul.ic-resume-rating-stars .inner-star {
            height: 24px;
            width: 24px;
        }
        .ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars.hover li.star.selected.hover .inner-star,
        .ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars li.star.selected .inner-star,
        .ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars li.star.hover .inner-star{
            width:20px;
            height: 20px;
        }
        #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1) .ic-resume-rating,
        #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1) .ic-resume-rating{
            right: 0;
        }
        .ic-resume-scrollbar-part.one-column-layout #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1) .ic-resume-rating,
        .ic-resume-scrollbar-part.one-column-layout #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1) .ic-resume-rating{
            right: 36px;
        }
        .vue-slider-rail,
        .ic-resume-single-slot .ic-resume-legend {
            float: right;
            width: 50%!important;
        }
        #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group,
        #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group{
            width:100%;
            min-height: 28px !important;
        }
        .ic-resume-scrollbar-part.one-column-layout #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group,
        .ic-resume-scrollbar-part.one-column-layout #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group{
            width: 50%;
        }
        .ic-resume-scrollbar-part.one-column-layout #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+2),
        .ic-resume-scrollbar-part.one-column-layout #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+2){
            padding-left: 36px;
        }
        .ic-resume-scrollbar-part.one-column-layout #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        .ic-resume-scrollbar-part.one-column-layout #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1){
            padding-right: 36px;
        }

        #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+2),
        #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+2){
            padding-left: 0;
        }
        .one-column-layout .ic-resume-progress-bar-legend:nth-of-type(2n+1) .ic-resume-legend-holder{
            padding-right: 36px;
        }
        .one-column-layout .ic-resume-progress-bar-legend:nth-of-type(2n+2) .ic-resume-legend-holder{
            padding-left: 36px;
        }
        .two-column-layout .ic-resume-single-slot .ic-resume-progress-bar-legend{
            width: 100%;
            float: none;
        }
        .two-column-layout .ic-resume-single-slot .ic-resume-progress-bar-legend:nth-of-type(2n+2) {
            display: none;
        }
        #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1){
            padding-right: 0;
        }
        .ic-resume-rating-slider{
            width: 100%;
        }
        #ic-resume-soft-skill.infographics-show .ic-resume-single-slot-content-text-edit .editable-div,
        #ic-resume-skill.infographics-show .ic-resume-single-slot-content-text-edit .editable-div{
            padding: 0 !important;
        }
        .ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group,
        .ic-resume-box-slot2 #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group{
            width: 100%;
            float: left;
            margin-bottom: 0;
            display: inline-block;
        }
        #ic-resume-language.infographics-show .ic-resume-single-slot-group-header{
            width: 160px;
            padding-right: 26px;
            float: left;
        }
        #ic-resume-language.infographics-show ul.ic-resume-rating-stars li.star .star-main,
        #ic-resume-language.infographics-show ul.ic-resume-rating-stars .inner-star {
            height: 20px;
            width: 20px;
        }
        #ic-resume-language.ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars.hover li.star.selected.hover .inner-star,
        #ic-resume-language.ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars li.star.selected .inner-star,
        #ic-resume-language.ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars li.star.hover .inner-star{
            width:16px;
            height: 16px;
        }
        #ic-resume-language.infographics-show .ic-resume-rating .ic-resume-rating-stars{
            margin-top: 4px;
        }
        #ic-resume-language.infographics-show .ic-resume-rating {
            position: static;
            width: 172px;
            float: right;
        }
        #ic-resume-language.infographics-show ul.ic-resume-rating-stars li.star{
            padding-right: 18px;
        }
        #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group{
            margin-bottom: 12px !important;
        }
        #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group:last-child{
            margin-bottom: 0px !important;
        }
        #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group,
        #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group {
            width: calc(100%/3) !important;
        }
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group,
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group {
            width: calc(100%/5) !important;
        }
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-single-slot-group-header,
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-single-slot-group-header{
            width: 100%;
        }
        .one-column-layout .ic-resume-box-slot1 .ic-resume-language-edit-content,
        .one-column-layout .ic-resume-box-slot1 .ic-resume-language-content,
        #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-single-slot-group-header,
        #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-single-slot-group-header{
            padding-right: 0;
        }
        .two-column-layout .ic-resume-single-slot.icon-show .ic-resume-interest-group,
        .two-column-layout .ic-resume-single-slot.icon-show .ic-resume-cause-group{
            width:50%;
        }
        .one-column-layout .ic-resume-single-slot.icon-show .ic-resume-interest-group,
        .one-column-layout .ic-resume-single-slot.icon-show .ic-resume-cause-group{
            width:25%;
        }

        .one-column-layout .ic-resume-single-slot.icon-show .ic-resume-interest-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        .one-column-layout .ic-resume-single-slot.icon-show .ic-resume-cause-group.ic-resume-single-slot-group:nth-of-type(2n+1){
            clear: none;
        }
        .one-column-layout .ic-resume-single-slot.icon-show .ic-resume-interest-group.ic-resume-single-slot-group:nth-of-type(2n+2),
        .one-column-layout .ic-resume-single-slot.icon-show .ic-resume-cause-group.ic-resume-single-slot-group:nth-of-type(2n+2){
            padding-right: 18px;
        }
        .one-column-layout .ic-resume-single-slot.icon-show .ic-resume-interest-group.ic-resume-single-slot-group:nth-of-type(4n+1),
        .one-column-layout .ic-resume-single-slot.icon-show .ic-resume-cause-group.ic-resume-single-slot-group:nth-of-type(4n+1){
            clear: both;
        }
        .one-column-layout .ic-resume-skill-group.ic-resume-single-slot-group{
            width: 25%;
        }
        .one-column-layout .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+2){
            padding-left: 0;
            padding-right: 18px;
        }
        .one-column-layout .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(4n){
            padding-right: 0px;
        }
        .one-column-layout .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1){
            clear: none;
        }
        .one-column-layout .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(4n+1){
            clear: both;
        }
        ul.ic-resume-rating-stars li.star:last-child{
            padding-right: 0px !important;
        }
        .one-column-layout #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(4n+1),
        .one-column-layout #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(4n+2){
            padding-right: 36px !important;
            padding-left: 0 !important;
        }
        .one-column-layout #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(4n+3),
        .one-column-layout #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(4n+4){
            padding-left: 36px !important;
            padding-right: 0 !important;
        }
        .one-column-layout #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(4n+3),
        .one-column-layout #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(4n+4),
        .one-column-layout #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(4n+1),
        .one-column-layout #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(4n+2) {
            padding-right: 0px !important;
            padding-left: 0px !important;
        }
        .one-column-layout #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1) {
            padding-right: 36px !important;
            padding-left: 0px !important;
        }
        .one-column-layout #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+2) {
            padding-right: 0px !important;
            padding-left: 36px !important;
        }

        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+2),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+2),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1) {
            padding-right: 0px !important;
            padding-left: 0px !important;
        }
        #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group,
        #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group {
            margin-bottom: 24px !important;
        }
        .two-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(1),
        .two-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(2),
        .two-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(3),
        .two-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(1),
        .two-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(2),
        .two-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(3) {
            margin-bottom: 0 !important;
        }
        .two-column-layout #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1) {
            clear: both;
        }
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(1),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(2),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(3),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(4),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(5),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(1),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(2),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(3),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(4),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(5){
            margin-bottom: 0px !important;
        }
        #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1){
            padding-right: 0;
        }
        #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+2){
            padding-left: 0;
        }

        .resume-template .ic-coverletter-contact-items-inner {
            width: 100%;
            display: inline-block;
        }
        .resume-template .ic-coverletter-contact-items {
            margin: 0;
            width: 100%;
            display: inline-block;
            padding-right: 50px;
            padding-left: 50px;
            padding-top: 20px;
            padding-bottom: 20px;
            border-top: 2px solid #000;
            border-bottom: 2px solid #000;
        }
        .resume-template .ic-coverletter-contact-item {
            text-align: left;
            width: 50%;
            float: left;
        }
        .resume-template .ic-coverletter-contact-item:nth-of-type(2n+1){
            padding-right: 36px;
        }
        .resume-template .ic-coverletter-contact-item:nth-of-type(2n+2){
            padding-left: 36px;
        }
        .resume-template .ic-coverletter-contact-item:nth-of-type(2){
            margin-top: 0;
        }
        .resume-template .ic-coverletter-contact-item a {
            order: 2;
            font-size: 16px;
            margin-right: 0;
            margin-left: 10px;
            vertical-align: top;
        }
        .resume-template .ic-coverletter-contact-item svg {
            width: 20px;
            height: 20px;
            order: 1;
        }
        .ic-resume-template-editor .ic-coverletter-box-top{
            margin-bottom: 20px;
            display: grid;
        }

        .ic-resume-language-edit-content.ic-resume-single-slot-content-text-edit {
            margin-bottom: 2px;
        }
        .resume-template .ic-coverletter-contact-item:first-child{
            margin-top: 0 !important;
        }


        /*===============================================================================
      =========================== Resume Template Design =========================
      ===============================================================================*/
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-coverletter-contact-item{
            width: 100%;
        }
        #template-functional .ic-resume-single-slot-title-text{
            color:{{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot-content-text-edit .ic-resume-single-slot-contact-info{
            display: block;
        }
        #template-functional.ic-resume-scrollbar-part .ic-coverletter-contact-items {
            padding: 0px !important;
            border-top: none;
            border-bottom: none;
        }
        .resume-template #template-functional.two-column-layout .ic-coverletter-contact-item:nth-of-type(2) {
            margin-top: 18px;
        }
        #template-functional.ic-resume-scrollbar-part.one-column-layout .ic-coverletter-contact-items{
            margin-bottom: 20px;
        }
        #template-functional.ic-resume-scrollbar-part .ic-coverletter-contact-item:nth-of-type(2n+1){
            padding-right: 0px;
        }
        #template-functional.ic-resume-scrollbar-part .ic-coverletter-contact-item:nth-of-type(2n+2){
            padding-left: 0px;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-coverletter-contact-item a{
            margin-left: 0;
            display: block;
        }
        .resume-template #template-functional .ic-coverletter-title-text:focus{
            border-bottom:1px solid #fff;
        }
        #template-functional .ic-resume-single-slot-header{
            border-bottom:2px solid {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        #template-functional .ic-resume-box-slot2 .ic-resume-single-slot-header{
            padding-left: 24px;
            padding-right: 0;
        }
        #template-functional .ic-resume-single-slot-content-text .ic-resume-single-slot-list-item-bullet{
            top:6px;
        }
        #template-functional .ic-resume-single-slot-list-item-bullet {
            top: 8px;
            height: 8px;
            width: 8px;
            background: transparent;
            border: 2px solid {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        #template-functional .one-column-layout .ic-resume-box-slot1 .ic-resume-single-slot-header,
        #template-functional .ic-resume-box-slot1 .ic-resume-single-slot-header{
            padding-left: 0;
            padding-right: 0;
        }
        #template-functional .ic-resume-box-slot1 .ic-resume-language-groups.ic-resume-single-slot-groups,
        #template-functional .ic-resume-box-slot1 .ic-resume-skill-groups.ic-resume-single-slot-groups{
            padding-left: 0;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot-content-text .ic-resume-single-slot-location-text,
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot-content-text .ic-resume-single-slot-location {
            width: 100%;
            float: none;
            text-align: left;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot-content-text .ic-resume-single-slot-period{
            width: 100%;
            float: none;
        }
        #template-functional ul.ic-resume-rating-stars li.star {
            padding-right: 14px;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot.icon-show .ic-resume-interest-group.ic-resume-single-slot-group,
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot.icon-show .ic-resume-cause-group.ic-resume-single-slot-group{
            width:100%;
            max-width: 100%;
            padding-right: 0 !important;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show .ic-resume-skill-group:nth-of-type(2n+1) ,
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-skill.infographics-show .ic-resume-skill-group:nth-of-type(2n+1) {
            padding-right: 0px;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+2) ,
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+2) {
            padding-left: 0px;
        }
        #template-functional.ic-resume-box-slot1 .ic-resume-single-slot.infographics-show .ic-resume-legend-row{
            padding-left: 0;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show .ic-resume-single-slot-group-header ,
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-skill.infographics-show .ic-resume-single-slot-group-header {
            padding-right: 0;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-rating {
            position: static;
            width: 100%;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-skill-group.ic-resume-single-slot-group {
            width: 100% !important;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show .ic-resume-single-slot-group-header,
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-skill.infographics-show .ic-resume-single-slot-group-header{
            margin-bottom: 4px;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 ul.ic-resume-rating-stars li.star .star-main {
            height: 28px;
            width: 28px;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 ul.ic-resume-rating-stars .inner-star {
            height: 28px;
            width: 28px;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars.hover li.star.selected.hover .inner-star,
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars li.star.selected .inner-star,
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars li.star.hover .inner-star {
            width: 24px;
            height: 24px;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 ul.ic-resume-rating-stars li.star{
            padding-right: 25px;
        }
        #template-functional #ic-resume-skill.infographics-show .ic-resume-rating,#template-functional #ic-resume-soft-skill.infographics-show .ic-resume-rating{
            width: auto;
        }
        #template-functional ul.ic-resume-rating-stars li.star .star-main {
            height: 20px;
            width: 20px;
        }
        #template-functional ul.ic-resume-rating-stars .inner-star {
            height: 20px;
            width: 20px;
        }
        #template-functional .ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars.hover li.star.selected.hover .inner-star,
        #template-functional .ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars li.star.selected .inner-star,
        #template-functional .ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars li.star.hover .inner-star{
            width: 16px;
            height: 16px;
        }
        #template-functional .ic-resume-box-slot1 #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1) {
            clear: none;
        }
        #template-functional .ic-resume-box-slot1 #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1) {
            clear: none;
        }
        #template-functional .ic-resume-box-slot1 #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1) {
            clear: both;
        }
        #template-functional .ic-resume-box-slot1 #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(4n+1) {
            clear: both;
        }
        #template-functional #ic-resume-soft-skill.infographics-show .ic-resume-single-slot-group-header,
        #template-functional #ic-resume-skill.infographics-show .ic-resume-single-slot-group-header{
            padding-right: 156px !important;
        }
        #template-functional #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1) ,
        #template-functional #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1) ,
        #template-functional #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1) {
            padding-right:36px;
        }
        #template-functional #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+2) ,
        #template-functional #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+2) ,
        #template-functional #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+2) {
            padding-left:36px;
        }

        #template-functional #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1) .ic-resume-rating ,
        #template-functional #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1) .ic-resume-rating ,
        #template-functional #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1) .ic-resume-rating {
            right: 36px;
        }
        #template-functional .ic-resume-box-slot1 .ic-resume-single-slot.icon-show .ic-resume-interest-group.ic-resume-single-slot-group,
        #template-functional .ic-resume-box-slot1 .ic-resume-single-slot.icon-show .ic-resume-cause-group.ic-resume-single-slot-group{
            width: 25%;
            padding-right: 36px;
        }
        #template-functional .ic-resume-box-slot1 .ic-resume-single-slot.icon-show .ic-resume-interest-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 .ic-resume-single-slot.icon-show .ic-resume-cause-group.ic-resume-single-slot-group:nth-of-type(2n+1) {
            padding-right: 36px;
            clear: none;
        }
        #template-functional .ic-resume-box-slot1 .ic-resume-single-slot.icon-show .ic-resume-interest-group.ic-resume-single-slot-group:nth-of-type(4n+1),
        #template-functional .ic-resume-box-slot1 .ic-resume-single-slot.icon-show .ic-resume-cause-group.ic-resume-single-slot-group:nth-of-type(4n+1) {
            padding-right: 36px;
            clear: both;
        }
        #template-functional .ic-resume-box-slot1 .ic-resume-single-slot.icon-show .ic-resume-interest-group.ic-resume-single-slot-group:nth-of-type(4n),
        #template-functional .ic-resume-box-slot1 .ic-resume-single-slot.icon-show .ic-resume-cause-group.ic-resume-single-slot-group:nth-of-type(4n) {
            padding-right: 0px;
        }
        .resume-template #template-functional div#ic-coverletter-full-name{
            color: #fff;
        }
        .resume-template #template-functional .ic-coverletter-title-text{
            color:#fff;
        }
        .resume-template #template-functional .ic-coverletter-title-text[contenteditable=true]:empty:before {
            color: #fff;
        }
        .resume-template #template-functional .ic-resume-box-name-holder{
            padding:18px 24px 22px 24px;
            background: #1E87FF;
            width: 76.4%!important;
            float: right;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot-contact-label{
            display: none;
        }
        #template-functional .ic-coverletter-title-sumury-text {
            color:#fff;
        }
        #template-functional .ic-coverletter-title-sumury-text:focus {
            border-bottom: 1px solid #fff;
        }
        #template-functional .ic-resume-top-header{
            padding-right: 0;
        }
        #template-functional #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group,
        #template-functional #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group{
            width: 50%;
            min-height: 23px !important;
            margin-bottom: 24px;
        }
        #template-functional .ic-resume-box-slot2 .ic-resume-single-slot .ic-resume-single-slot-groups{
            padding-left:0px;
            padding-right: 0px;
        }
        #template-functional .ic-resume-box-slot1 .ic-resume-single-slot .ic-resume-single-slot-groups{
            padding-left: 0px;
            padding-right: 0px;
        }
        #template-functional .ic-resume-box-slot2 .ic-resume-language-groups.ic-resume-single-slot-groups,
        #template-functional .ic-resume-box-slot2 .ic-resume-skill-groups.ic-resume-single-slot-groups {
            padding-left: 24px;
            padding-right: 0;
        }
        #template-functional .ic-resume-box-slot1 {
            width: 26.90% !important;
        }
        #template-functional.ic-resume-scrollbar-part.one-column-layout .ic-resume-box-slot1{
            width: 100% !important;
        }
        .ic-resume-scrollbar-part.one-column-layout .ic-resume-box-slot1{
            width: 100% !important;
            float: none;
        }
        #template-functional .ic-resume-box-slot2 {
            width: 73.1%!important;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot {
            /*width: 240px;*/
            margin-left: 50px;
            margin-right: 30px;
        }
        #template-functional .ic-resume-single-slot-content-text .ic-resume-single-slot-location{
            width: 50%;
        }
        #template-functional .ic-resume-single-slot-group{
            margin-left: 24px;
        }
        #template-functional .ic-resume-box-slot2 .ic-resume-interest-groups.ic-resume-single-slot-groups ,
        #template-functional .ic-resume-box-slot2 .ic-resume-cause-groups.ic-resume-single-slot-groups {
            margin-left: 24px;
        }

        #template-functional.ic-resume-scrollbar-part.one-column-layout .ic-resume-box-slot1{
            padding-right: 50px;
            padding-left: 50px;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group{
            width:100%;
            padding-right: 0;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+2){
            padding-left: 0;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-language.infographics-show .ic-resume-single-slot-group-header{
            padding-right: 0 !important;
        }
        #template-functional.ic-resume-scrollbar-part.one-column-layout .ic-resume-top-header {
            margin-bottom: 10px;
        }
        #template-functional.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot.icon-show .ic-resume-interest-group.ic-resume-single-slot-group:nth-last-of-type(2),
        #template-functional.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot.icon-show .ic-resume-cause-group.ic-resume-single-slot-group:nth-last-of-type(2){
            margin-bottom: 12px !important;
        }

        #template-functional.two-column-layout .ic-resume-box-slot1 #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group{
            margin-bottom: 18px !important;
        }
        #template-functional.two-column-layout .ic-resume-box-slot1 #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:last-child{
            margin-bottom: 0px !important;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group,
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group {
            width: 100% !important;
            margin-bottom: 24px;
            padding: 0 4px !important;
        }
        #template-functional #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-single-slot-group-header,
        #template-functional #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-single-slot-group-header{
            width: 100%;
        }
        #template-functional .ic-resume-box-slot2 #ic-resume-language.infographics-show .ic-resume-language-group.ic-resume-single-slot-group{
            width: 50%;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot2 #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group,
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot2 #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group {
            width: 25% !important;
        }
        #template-functional.two-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(3n+1),
        #template-functional.two-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(3n+1){
            clear: none;
        }
        #template-functional.two-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(4n+1),
        #template-functional.two-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(4n+1){
            clear: both;
        }
        #template-functional.two-column-layout .ic-resume-box-slot2 #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(1),
        #template-functional.two-column-layout .ic-resume-box-slot2 #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(2),
        #template-functional.two-column-layout .ic-resume-box-slot2 #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(3),
        #template-functional.two-column-layout .ic-resume-box-slot2 #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(4),
        #template-functional.two-column-layout .ic-resume-box-slot2 #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(5),
        #template-functional.two-column-layout .ic-resume-box-slot2 #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(1),
        #template-functional.two-column-layout .ic-resume-box-slot2 #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(2),
        #template-functional.two-column-layout .ic-resume-box-slot2 #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(3),
        #template-functional.two-column-layout .ic-resume-box-slot2 #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(4),
        #template-functional.two-column-layout .ic-resume-box-slot2 #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-language-group.ic-resume-single-slot-group:nth-last-of-type(5){
            margin-bottom: 0 !important;
        }
        #template-functional #ic-resume-skill.infographics-show .ic-resume-rating,
        #template-functional #ic-resume-soft-skill.infographics-show .ic-resume-rating {
            width: 156px !important;
        }
        #template-functional.one-column-layout .vue-slider-rail,
        #template-functional.one-column-layout .ic-resume-single-slot .ic-resume-legend{
            width: 50% !important;
        }
        #template-functional.one-column-layout #ic-resume-skill.infographics-show .ic-resume-rating,
        #template-functional.one-column-layout #ic-resume-soft-skill.infographics-show .ic-resume-rating {
            width: calc(50% - 18px) !important;
        }
        #template-functional.one-column-layout #ic-resume-soft-skill.infographics-show .ic-resume-single-slot-group-header,
        #template-functional.one-column-layout #ic-resume-skill.infographics-show .ic-resume-single-slot-group-header{
            padding-right: 50% !important;
        }

        #template-functional.two-column-layout .ic-resume-box-slot1 #ic-resume-skill.infographics-show .ic-resume-rating,
        #template-functional.two-column-layout .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show .ic-resume-rating{
            width: auto;
        }
        #template-functional.two-column-layout .ic-resume-box-slot2 .ic-resume-single-slot .ic-resume-progress-bar-legend:nth-of-type(2n+2) {
            display: block;
        }
        #template-functional.two-column-layout .ic-resume-single-slot .ic-resume-progress-bar-legend:nth-of-type(2n+1){
            padding-right: 36px;
        }
        #template-functional.two-column-layout .ic-resume-single-slot .ic-resume-progress-bar-legend:nth-of-type(2n+2){
            padding-left: 36px;
        }
        #template-functional.two-column-layout .ic-resume-single-slot .ic-resume-progress-bar-legend {
            width: 50%;
            float: left;
        }
        #template-functional .ic-resume-box-slot2 .ic-resume-legend-row {
            padding-left: 24px;
            padding-right: 0;
        }
        #template-functional .vue-slider-rail,
        #template-functional .ic-resume-single-slot .ic-resume-legend {
            float: right;
            width: 156px!important;
        }
        #template-functional.two-column-layout .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show .ic-resume-single-slot-group-header,
        #template-functional.two-column-layout .ic-resume-box-slot1 #ic-resume-skill.infographics-show .ic-resume-single-slot-group-header{
            padding-right: 0 !important;
        }
        #template-functional.two-column-layout .ic-resume-box-slot1 #ic-resume-skill.infographics-show .ic-resume-rating,
        #template-functional.two-column-layout .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show .ic-resume-rating{
            width: 100% !important;
        }
        #template-functional.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot .ic-resume-progress-bar-legend {
            width: 100%;
            float: none;
            padding-right: 0;
        }
        #template-functional.two-column-layout .ic-resume-box-slot1 .ic-resume-legend-row {
            padding-left: 0;
            padding-right: 0;
        }
        #template-functional.two-column-layout .ic-resume-box-slot1  .vue-slider-rail,
        #template-functional.two-column-layout .ic-resume-box-slot1  .ic-resume-single-slot .ic-resume-legend {
            float: right;
            width: 100%!important;
        }
        #template-functional.two-column-layout .ic-resume-box-slot1 .ic-resume-skill-group.ic-resume-single-slot-group:nth-last-of-type(2){
            margin-bottom: 14px !important;
        }
        #template-functional.two-column-layout .ic-resume-box-slot1 #ic-resume-skill.infographics-show .ic-resume-skill-group:nth-last-of-type(2),
        #template-functional.two-column-layout .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show .ic-resume-skill-group:nth-last-of-type(2){
            margin-bottom: 24px !important;
        }
        #template-functional.two-column-layout .ic-resume-box-slot2 #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-last-of-type(2),
        #template-functional.two-column-layout .ic-resume-box-slot2 #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-last-of-type(2){
            margin-bottom: 0 !important;
        }
        #template-functional.one-column-layout .ic-resume-box-slot1 .ic-resume-legend-row{
            padding-right: 0;
            padding-left: 0;
        }
        .resume-template #template-functional.ic-resume-scrollbar-part,
        .resume-template .ic-resume-scrollbar-part.template-functional{
            display: none;
        }
        .resume-template #template-functional.ic-resume-scrollbar-part.template-functional{
            display: block;
        }

        .resume-template #template-functional.one-column-layout .ic-coverletter-contact-item:nth-of-type(2) {
            margin-top: 0;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show .ic-resume-single-slot-content-text,
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 #ic-resume-skill.infographics-show .ic-resume-single-slot-content-text{
            width:100%;
        }

        /*----------------------------------------------------------------------------------
      ----------------------------------Template Simple ----------------------------------
      ------------------------------------------------------------------------------------*/
        #template-simple.ic-resume-scrollbar-part .ic-coverletter-contact-items {
            border-top: none;
            border-bottom: none;
            background: #1E87FF;
        }
        #template-simple.ic-resume-scrollbar-part .ic-coverletter-contact-item a{
            color:#fff;
        }
        #template-simple.ic-resume-scrollbar-part .ic-coverletter-contact-item svg polygon,
        #template-simple.ic-resume-scrollbar-part .ic-coverletter-contact-item svg path {
            fill: #fff;
        }
        #template-simple .ic-resume-box-slot1 .ic-resume-single-slot-header .ic-resume-menu-slot {
            left: 50px;
        }
        #template-simple .ic-resume-box-slot2 .ic-resume-single-slot-header .ic-resume-menu-slot {
            left: 36px;
        }
        #template-simple .ic-resume-box-name-holder #ic-cv-title{
            padding-bottom: 0;
        }
        #template-simple .ic-resume-single-slot-content-text .ic-resume-single-slot-list-item-bullet {
            top: 7px;
        }
        #template-simple .ic-resume-single-slot-list-item-bullet {
            top: 8px;
            height: 8px;
            width: 8px;
            border: 2px solid #1E87FF;
            border-radius: 100%;
            background: transparent;
        }
        #template-simple .ic-resume-list-style .ic-resume-single-slot-group {
            position: relative;
        }
        #template-simple .ic-resume-box-slot1 .ic-resume-list-style .ic-resume-single-slot-group:after {
            position: absolute;
            content: "";
            top: 14px;
            left: -50px;
            width: 24px;
            height: 2px;
            background-color: #1E87FF;
        }
        #template-simple .ic-resume-box-slot2 .ic-resume-list-style .ic-resume-single-slot-group:after {
            position: absolute;
            content: "";
            top: 14px;
            left: -36px;
            width: 24px;
            height: 2px;
            background-color: #1E87FF;
        }
        .resume-template #template-simple .ic-coverletter-title-text[contenteditable=true]:empty:before,
        .resume-template #template-simple .ic-coverletter-title-text{
            color:#1E87FF;
        }

        /*----------------------------------------------------------------------------------
      ----------------------------------Template Creative ----------------------------------
      ------------------------------------------------------------------------------------*/
        .resume-template #template-creative .ic-coverletter-box-top {
            background: #9E2B6F;
        }
        #template-creative .ic-resume-photo-img {
            border: 6px solid #1E87FF;
        }
        #template-creative .ic-resume-photo {
            float: right;
        }

        #template-creative .ic-resume-box-name-holder{
            margin-left: 0;
        }
        #template-creative.ic-resume-scrollbar-part .ic-coverletter-contact-items {
            border-top: none;
            border-bottom: none;
            background: rgba(0,0,0,.5);
        }
        .resume-template #template-creative .ic-coverletter-title-sumury-text,
        .resume-template #template-creative .ic-coverletter-title-text,
        .resume-template #template-creative .ic-coverletter-title-text[contenteditable=true]:empty:before,
        .resume-template #template-creative .ic-resume-box-name-holder div#ic-coverletter-full-name,
        .resume-template #template-creative.ic-resume-scrollbar-part .ic-coverletter-contact-item a{
            color:#fff;
        }
        #template-creative.ic-resume-scrollbar-part .ic-coverletter-contact-item svg polygon,
        #template-creative.ic-resume-scrollbar-part .ic-coverletter-contact-item svg path {
            fill: #fff;
        }
        .resume-template #template-creative .ic-resume-top-header{
            margin-bottom: 34px;
        }
        .resume-template #template-creative .ic-resume-single-slot-header-holder {
            display: inline-block;
            margin-bottom: 6px;
            border-bottom: 4px solid #1E87FF;
        }
        #template-creative .ic-resume-single-slot-content-text .ic-resume-single-slot-list-item-bullet {
            top: 7px;
        }
        #template-creative .ic-resume-single-slot-list-item-bullet {
            top: 8px;
            height: 8px;
            width: 8px;
            border: 2px solid #1E87FF;
            border-radius: 100%;
            background:#1E87FF;
        }
        #template-creative .ic-resume-photo-shape,
        #template-creative .ic-resume-photo-control{
            left:auto;
            right: 0;
        }
        #template-creative .ic-resume-box-slot1 .ic-resume-single-slot-header .ic-resume-menu-slot {
            left: 50px;
        }
        #template-creative .ic-resume-box-slot2 .ic-resume-single-slot-header .ic-resume-menu-slot {
            left: 36px;
        }
        /*----------------------------------------------------------------------------------
      ----------------------------------Template Basic ----------------------------------
      ------------------------------------------------------------------------------------*/
        #template-basic .ic-resume-single-slot-location-text,
        #template-basic .ic-resume-single-slot-list-header-text,
        #template-basic .ic-resume-single-slot-contact-label,
        #template-basic .ic-resume-single-slot-period-class{
            color:#909090;
        }
        #template-basic .ic-resume-box-name-holder #ic-cv-title{
            padding-bottom: 0;
        }
        .resume-template #template-basic #ic-coverletter-full-name{
            color:#1E87FF;
        }
        #template-basic .ic-resume-box-slot1 .ic-resume-single-slot-header .ic-resume-menu-slot {
            left: 50px;
        }
        #template-basic .ic-resume-box-slot2 .ic-resume-single-slot-header .ic-resume-menu-slot {
            left: 36px;
        }
        /*----------------------------------------------------------------------------------
      ----------------------------------Template Professional ----------------------------------
      ------------------------------------------------------------------------------------*/

        .resume-template #template-professional .ic-coverletter-contact-item:nth-of-type(2) {
            margin-top: 18px;
        }
        .resume-template #template-professional .ic-coverletter-title-text[contenteditable=true]:empty:before,
        .resume-template #template-professional .ic-coverletter-title-text{
            color:#1E87FF;
        }
        .resume-template #template-professional.ic-resume-scrollbar-part,
        .resume-template .ic-resume-scrollbar-part.template-professional{
            display: none;
        }
        .resume-template #template-professional.ic-resume-scrollbar-part.template-professional{
            display: block;
        }
        #template-professional.ic-resume-scrollbar-part .ic-coverletter-contact-item:nth-of-type(2n+1){
            padding-right: 0px;
        }
        #template-professional.ic-resume-scrollbar-part .ic-coverletter-contact-item:nth-of-type(2n+2){
            padding-left: 0px;
        }
        .resume-template #template-professional .ic-resume-box-name-holder {
            width: 38.5%;
            margin-left: 0;
            padding-left: 28px;
        }
        .resume-template #template-professional.ic-resume-scrollbar-part .ic-coverletter-contact-items {
            width: 38.5%;
            float: right;
            padding: 0 !important;
            padding-left: 46px !important;
            border-top: none;
            border-bottom: none;
        }
        .resume-template #template-professional.ic-resume-scrollbar-part .ic-coverletter-contact-item {
            width: 100%;
            justify-content: flex-end;
        }
        .resume-template #template-professional.ic-resume-scrollbar-part .ic-coverletter-contact-item a {
            order: 1;
            margin-right: 10px;
            margin-left: 0;
        }
        .resume-template #template-professional .ic-coverletter-box-top {
            border-bottom: 2px solid #1E87FF;
        }
        .resume-template #template-professional .ic-resume-photo-img {
            border: 6px solid #1E87FF;
        }
        .resume-template #template-professional .ic-resume-photo {
            position: absolute;
            left: 42.5%;
        }
        .resume-template #template-professional .ic-resume-box-name-holder::before {
            content: ""!important;
            position: absolute;
            background: #9E2B6F;
            width: 48px;
            left: -50px;
            top: 6px;
            height: 74px;
        }
        .resume-template #template-professional .ic-coverletter-contact-item svg polygon,
        .resume-template #template-professional .ic-coverletter-contact-item svg path {
            fill: {{ $document->template_data['template_data']['themes']['color'] ?? '#9E2B6F' }};
        }
        #template-professional .ic-resume-box-slot1 .ic-resume-single-slot{
            padding-left: 28px;
        }
        #template-professional .ic-resume-box-name-holder #ic-cv-title {
            padding-bottom: 0;
        }
        #template-professional .ic-resume-box-slot1 .ic-resume-list-style .ic-resume-single-slot-group {
            position: relative;
        }
        #template-professional .ic-resume-box-slot1 .ic-resume-list-style .ic-resume-single-slot-group:after {
            position: absolute;
            content: "";
            width: 48px;
            left: -78px;
            top: 6px;
            height: 74px;
            background-color: #1E87FF;
        }
        #template-professional .ic-resume-single-slot-content-text .ic-resume-single-slot-list-item-bullet {
            top: 7px;
        }
        #template-professional .ic-resume-single-slot-list-item-bullet {
            top: 8px;
            height: 8px;
            width: 8px;
            border: 2px solid #1E87FF;
            border-radius: 100%;
            background:#1E87FF;
        }
        #template-professional .ic-resume-box-slot1 .ic-resume-single-slot-header .ic-resume-menu-slot {
            left: 50px;
        }
        #template-professional .ic-resume-box-slot2 .ic-resume-single-slot-header .ic-resume-menu-slot {
            left: 36px;
        }
        /*----------------------------------------------------------------------------------
      ----------------------------------Template College ----------------------------------
      ------------------------------------------------------------------------------------*/
        #template-college .ic-resume-box-slot1 .ic-resume-single-slot-header .ic-resume-menu-slot {
            left: 50px;
        }
        #template-college .ic-resume-box-slot2 .ic-resume-single-slot-header .ic-resume-menu-slot {
            left: 36px;
        }
        .resume-template #template-college #ic-coverletter-full-name{
            color:#1E87FF;
        }
        .resume-template #template-college.ic-resume-scrollbar-part,
        .resume-template .ic-resume-scrollbar-part.template-professional{
            display: none;
        }
        .resume-template #template-college.ic-resume-scrollbar-part.template-professional{
            display: block;
        }
        #template-college.ic-resume-scrollbar-part .ic-coverletter-contact-item:nth-of-type(2n+1){
            padding-right: 0px;
        }
        #template-college.ic-resume-scrollbar-part .ic-coverletter-contact-item:nth-of-type(2n+2){
            padding-left: 0px;
        }
        .resume-template #template-college .ic-coverletter-contact-item:nth-of-type(2) {
            margin-top: 18px;
        }
        .resume-template #template-college .ic-resume-box-name-holder {
            width: 38.5%;
            margin-left: 0;
        }
        .resume-template #template-college.ic-resume-scrollbar-part .ic-coverletter-contact-items {
            width: 38.5%;
            float: right;
            padding: 0 !important;
            padding-left: 46px !important;
            border-top: none;
            border-bottom: none;
        }
        .resume-template #template-college.ic-resume-scrollbar-part .ic-coverletter-contact-item {
            width: 100%;
            justify-content: flex-end;
        }
        .resume-template #template-college.ic-resume-scrollbar-part .ic-coverletter-contact-item a {
            order: 1;
            margin-right: 10px;
            margin-left: 0;
        }
        .resume-template #template-college .ic-resume-photo {
            border: 1px solid #d3d3d3;
            border-radius: 100%;
            position: absolute;
            left: 42.5%;
        }
        #template-college .ic-resume-box-name-holder #ic-cv-title {
            padding-bottom: 0;
        }
        #template-college .ic-resume-single-slot-header-holder {
            padding-bottom: 4px;
            display: inline-block;
            border-top: 2px solid #202020;
            border-bottom: 2px solid #202020;
            padding-top: 6px;
        }
        .resume-template #template-college.ic-resume-scrollbar-part.one-column-layout .ic-resume-single-slot-header-holder {
            display: block;
            text-align: center;
        }
        .resume-template #template-college.ic-resume-scrollbar-part.one-column-layout .ic-resume-single-slot {
            margin-bottom: 48px;
        }
        #template-college .ic-resume-single-slot-header {
            margin-bottom: 16px;
        }
        /*----------------------------------------------------------------------------------
      ----------------------------------Template Executive ----------------------------------
      ------------------------------------------------------------------------------------*/
        #template-executive.ic-resume-scrollbar-part .ic-coverletter-contact-items {
            margin: 0;
            width: auto;
            display: inline-block;
            padding-right: 50px;
            padding-left: 50px;
            padding-top: 20px;
            padding-bottom: 20px;
            border-top: none;
            border-bottom: none;
            background: #F1F1F1;
        }
        #template-executive .ic-resume-single-slot-location-text,
        #template-executive .ic-resume-single-slot-list-header-text,
        #template-executive .ic-resume-single-slot-contact-label,
        #template-executive .ic-resume-single-slot-period-class{
            color:#909090;
        }
        #template-executive .ic-resume-box-name-holder #ic-cv-title {
            padding-bottom: 0;
        }
        #template-executive .ic-resume-box-slot1 .ic-resume-single-slot-header .ic-resume-menu-slot {
            left: 50px;
        }
        #template-executive .ic-resume-box-slot2 .ic-resume-single-slot-header .ic-resume-menu-slot {
            left: 36px;
        }
        .ic-resume-single-slot-header-title.editable-div[contenteditable=true]:empty:before {
            content: attr(placeholder);
            font-weight: 700;
            color: #1E87FF;
            font-style: normal;
        }
        .ic-resume-single-slot-header-title.editable-div[contenteditable=true]:empty:focus:before {
            content: attr(placeholder);
            font-weight: 700;
            display: block;
            font-style: italic;
            color: #202020;
        }
        .ic-resume-single-slot-list-sub-content{
            position: relative;
        }
        .ic-resume-single-slot[data-course-list="course-list-2"] .ic-resume-education-courses-list-item:nth-of-type(2n + 1){
            clear:both;
        }
        .ic-resume-single-slot[data-course-list="course-list-3"] .ic-resume-education-courses-list-item:nth-of-type(2n + 1){
            clear:none;
        }
        .ic-resume-single-slot[data-course-list="course-list-3"] .ic-resume-education-courses-list-item:nth-of-type(3n + 1){
            clear:both;
        }



        /*================= Coverletter Font Size ===============================*/
        .ic-small-font-size #ic-coverletter-creation-date,
        .ic-small-font-size .ic-coverletter-company-details-text,
        .ic-small-font-size .ic-coverletter-title-text{
            font-size:22px;
            min-height: 22px;
            line-height: 24px;
        }
        .ic-small-font-size .ic-coverletter-field-label{
            font-size:14px;
            min-height: 14px;
            line-height: 16px;
        }
        .ic-small-font-size #ic-cover-letter-content-text{
            font-size: 16px;
            line-height: 20px;
        }
        .ic-small-font-size .ic-coverletter-title-signature-text,
        .ic-small-font-size .ic-coverletter-title-signature-label,
        .ic-small-font-size #ic-cover-letter-content-text p{
            font-size: 16px;
            min-height: 16px;
            line-height: 20px;
        }



        .ic-small-font-size .ic-resume-box-name-holder div#ic-coverletter-full-name {
            font-size: 46px;
        }
        .ic-small-font-size .ic-resume-single-slot-place-text,
        .ic-small-font-size .ic-resume-single-slot-title-text,
        .ic-small-font-size .ic-resume-box-name-holder .ic-coverletter-title-text {
            font-size: 22px !important;
            min-height: 22px;
            line-height: 24px !important;
        }
        .ic-small-font-size .ic-resume-single-slot-skill-text,.ic-small-font-size .ic-resume-single-slot-date-text,
        .ic-small-font-size .ic-resume-interest-icontitle-text, .ic-small-font-size .ic-resume-cause-icontitle-text,
        .ic-small-font-size .ic-resume-single-slot-group-name-text,.ic-small-font-size .ic-resume-single-slot-extra-text,.ic-small-font-size .ic-resume-single-slot-author-list-text,.ic-small-font-size .ic-resume-single-slot-titlex,
        .ic-small-font-size .ic-coverletter-title-sumury-text {
            font-size: 16px !important;
            min-height: 16px;
            line-height: 20px !important;
        }
        .ic-small-font-size .ic-resume-single-slot-contact-info-text, .ic-small-font-size .ic-resume-single-slot-contact-person-text,
        .ic-small-font-size .ic-resume-single-slot-contact-label,.ic-small-font-size .ic-resume-single-slot-contact-text.editable-div,
        .ic-small-font-size .ic-resume-single-slot-list-text,.ic-small-font-size .ic-resume-single-slot-author-text, .ic-small-font-size .ic-resume-single-slot-type-text,
        .ic-small-font-size .ic-resume-single-slot-list-header-text,
        .ic-small-font-size .ic-resume-single-slot-desc-text,
        .ic-small-font-size .ic-resume-single-slot-location-text,
        .ic-small-font-size .ic-resume-language-label-holder,
        .ic-small-font-size .ic-resume-single-slot-period-class {
            font-size: 14px!important;
            min-height: 14px;
            line-height: 16px!important;
        }
        .ic-small-font-size .ic-resume-single-slot-name-text {
            font-size: 18px!important;
            min-height: 18px;
            line-height: 20px!important;
        }
        .ic-small-font-size .ic-resume-single-slot-content-text .ic-resume-single-slot-list-item-bullet{
            top:3px !important;
        }
        .ic-small-font-size .ic-resume-single-slot-content-text .ic-resume-single-slot-contact-separator{
            line-height: 16px;
        }
        /*======= Coverletter large Font Size =========*/
        .ic-large-font-size .ic-resume-box-name-holder div#ic-coverletter-full-name {
            font-size: 46px;
        }
        .ic-large-font-size #ic-coverletter-creation-date,
        .ic-large-font-size .ic-coverletter-company-details-text,
        .ic-large-font-size .ic-coverletter-title-text{
            font-size: 26px;
            min-height: 26px;
            line-height: 28px;
        }
        .ic-large-font-size .ic-coverletter-field-label{
            font-size:18px;
            min-height: 18px;
            line-height: 20px;
        }
        .ic-large-font-size .ic-coverletter-contact-item{
            font-size: 18px;
            line-height: 22px;
        }
        .ic-large-font-size #ic-cover-letter-content-text{
            font-size: 20px;
            line-height: 24px;
        }
        .ic-large-font-size .ic-coverletter-title-signature-text,
        .ic-large-font-size .ic-coverletter-title-signature-label,
        .ic-large-font-size #ic-cover-letter-content-text p{
            font-size: 20px;
            min-height: 20px;
            line-height: 24px;
        }


        .ic-large-font-size .ic-resume-single-slot-place-text,
        .ic-large-font-size .ic-resume-single-slot-title-text,
        .ic-large-font-size .ic-resume-box-name-holder .ic-coverletter-title-text {
            font-size: 26px !important;
            min-height: 26px;
            line-height: 28px !important;
        }
        .ic-large-font-size .ic-resume-single-slot-skill-text,.ic-large-font-size .ic-resume-single-slot-date-text,
        .ic-large-font-size .ic-resume-interest-icontitle-text, .ic-large-font-size .ic-resume-cause-icontitle-text,
        .ic-large-font-size .ic-resume-single-slot-group-name-text,.ic-large-font-size .ic-resume-single-slot-extra-text,.ic-large-font-size .ic-resume-single-slot-author-list-text,.ic-large-font-size .ic-resume-single-slot-titlex,
        .ic-large-font-size .ic-coverletter-title-sumury-text {
            font-size: 20px !important;
            min-height: 20px;
            line-height: 24px !important;
        }
        .ic-large-font-size .ic-resume-single-slot-contact-info-text, .ic-large-font-size .ic-resume-single-slot-contact-person-text,
        .ic-large-font-size .ic-resume-single-slot-contact-label,.ic-large-font-size .ic-resume-single-slot-contact-text.editable-div,
        .ic-large-font-size .ic-resume-single-slot-list-text,.ic-large-font-size .ic-resume-single-slot-author-text, .ic-large-font-size .ic-resume-single-slot-type-text,
        .ic-large-font-size .ic-resume-single-slot-list-header-text,
        .ic-large-font-size .ic-resume-single-slot-desc-text,
        .ic-large-font-size .ic-resume-single-slot-location-text,
        .ic-large-font-size .ic-resume-language-label-holder,
        .ic-large-font-size .ic-resume-single-slot-period-class {
            font-size: 18px!important;
            min-height: 18px;
            line-height: 20px!important;
        }
        .ic-large-font-size .ic-resume-single-slot-name-text {
            font-size: 20px!important;
            min-height: 20px;
            line-height: 22px!important;
        }
        .ic-large-font-size .ic-coverletter-contact-item a{
            font-size: 18px !important;
            line-height: 22px !important;
        }
        .ic-large-font-size .ic-resume-single-slot-content-text .ic-resume-single-slot-list-item-bullet{
            top:8px;
        }
        .ic-large-font-size .ic-resume-single-slot-content-text .ic-resume-single-slot-contact-separator{
            line-height: 20px;
        }

        /*================= Editor Background ===============================*/
        .ic-coverletter-page-wrapper .ic-bg-skin-top svg,
        .ic-coverletter-page-wrapper .ic-bg-skin-bottom svg{
            display: none;
        }
        .ic-coverletter-page-wrapper[d-bg="bg-1"] #theme-bg-1,
        .ic-coverletter-page-wrapper[d-bg="bg-2"] #theme-bg-2,
        .ic-coverletter-page-wrapper[d-bg="bg-3"] #theme-bg-3,
        .ic-coverletter-page-wrapper[d-bg="bg-4"] #theme-bg-4,
        .ic-coverletter-page-wrapper[d-bg="bg-5"] #theme-bg-5,
        .ic-coverletter-page-wrapper[d-bg="bg-6"] #theme-bg-6,
        .ic-coverletter-page-wrapper[d-bg="bg-7"] #theme-bg-7,
        .ic-coverletter-page-wrapper[d-bg="bg-8"] #theme-bg-8,
        .ic-coverletter-page-wrapper[d-bg="bg-9"] #theme-bg-9,
        .ic-coverletter-page-wrapper[d-bg="bg-10"] #theme-bg-10,
        .ic-coverletter-page-wrapper[d-bg="bg-11"] #theme-bg-11,
        .ic-coverletter-page-wrapper[d-bg="bg-12"] #theme-bg-12,
        .ic-coverletter-page-wrapper[d-bg="bg-13"] #theme-bg-13,
        .ic-coverletter-page-wrapper[d-bg="bg-14"] #theme-bg-14,
        .ic-coverletter-page-wrapper[d-bg="bg-15"] #theme-bg-15,
        .ic-coverletter-page-wrapper[d-bg="bg-16"] #theme-bg-16,
        .ic-coverletter-page-wrapper[d-bg="bg-17"] #theme-bg-17,
        .ic-coverletter-page-wrapper[d-bg="bg-18"] #theme-bg-18,
        .ic-coverletter-page-wrapper[d-bg="bg-19"] #theme-bg-19,
        .ic-coverletter-page-wrapper[d-bg="bg-20"] #theme-bg-20{
            display: block;
        }
        /*================= Resume Layout Controll ===============================*/
        .ic-coverletter-page-inner[layout-width="48"] .ic-resume-box-slot1 {
            width: 48% !important;
        }
        .ic-coverletter-page-inner[layout-width="49"] .ic-resume-box-slot1 {
            width: 49% !important;
        }
        .ic-coverletter-page-inner[layout-width="51"] .ic-resume-box-slot1 {
            width: 51% !important;
        }
        .ic-coverletter-page-inner[layout-width="52"] .ic-resume-box-slot1 {
            width: 52% !important;
        }
        .ic-coverletter-page-inner[layout-width="53"] .ic-resume-box-slot1 {
            width: 53% !important;
        }
        .ic-coverletter-page-inner[layout-width="54"] .ic-resume-box-slot1 {
            width: 54% !important;
        }
        .ic-coverletter-page-inner[layout-width="55"] .ic-resume-box-slot1 {
            width: 55% !important;
        }
        .ic-coverletter-page-inner[layout-width="56"] .ic-resume-box-slot1 {
            width: 56% !important;
        }
        .ic-coverletter-page-inner[layout-width="57"] .ic-resume-box-slot1 {
            width: 57% !important;
        }
        .ic-coverletter-page-inner[layout-width="58"] .ic-resume-box-slot1 {
            width: 58% !important;
        }
        .ic-coverletter-page-inner[layout-width="59"] .ic-resume-box-slot1 {
            width: 59% !important;
        }
        .ic-coverletter-page-inner[layout-width="60"] .ic-resume-box-slot1 {
            width: 60% !important;
        }
        .ic-coverletter-page-inner[layout-width="61"] .ic-resume-box-slot1 {
            width: 61% !important;
        }
        /*================== Slot Two ======================*/
        .ic-coverletter-page-inner[layout-width="48"] .ic-resume-box-slot2 {
            width: 52% !important;
        }
        .ic-coverletter-page-inner[layout-width="49"] .ic-resume-box-slot2 {
            width: 51% !important;
        }
        .ic-coverletter-page-inner[layout-width="51"] .ic-resume-box-slot2 {
            width: 49% !important;
        }
        .ic-coverletter-page-inner[layout-width="52"] .ic-resume-box-slot2 {
            width: 48% !important;
        }
        .ic-coverletter-page-inner[layout-width="53"] .ic-resume-box-slot2 {
            width: 47% !important;
        }
        .ic-coverletter-page-inner[layout-width="54"] .ic-resume-box-slot2 {
            width: 46% !important;
        }
        .ic-coverletter-page-inner[layout-width="55"] .ic-resume-box-slot2 {
            width: 45% !important;
        }
        .ic-coverletter-page-inner[layout-width="56"] .ic-resume-box-slot2 {
            width: 44% !important;
        }
        .ic-coverletter-page-inner[layout-width="57"] .ic-resume-box-slot2 {
            width: 43% !important;
        }
        .ic-coverletter-page-inner[layout-width="58"] .ic-resume-box-slot2 {
            width: 42% !important;
        }
        .ic-coverletter-page-inner[layout-width="59"] .ic-resume-box-slot2 {
            width: 41% !important;
        }
        .ic-coverletter-page-inner[layout-width="60"] .ic-resume-box-slot2 {
            width: 40% !important;
        }
        .ic-coverletter-page-inner[layout-width="61"] .ic-resume-box-slot2 {
            width: 39% !important;
        }




        /*skill range slider css */
        .two-column-layout .vue-slider {
            width: 115px !important;
        }
        .two-column-layout .ic-resume-box-slot1 .vue-slider {
            width: 100% !important;
        }
        #template-functional.two-column-layout .ic-resume-box-slot1 .ic-resume-single-slot .ic-resume-progress-bar-legend:nth-of-type(2n+1),
        #template-functional.two-column-layout .ic-resume-box-slot1 #ic-resume-language .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional.two-column-layout .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional.two-column-layout .ic-resume-box-slot1 #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1) {
            padding-right: 0;
        }
        #template-functional.one-column-layout .vue-slider-rail{
            width:100% !important;
        }
        .one-column-layout .vue-slider {
            width: 100% !important;
        }
        #template-functional.one-column-layout #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1) .ic-resume-rating,
        #template-functional.one-column-layout #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1) .ic-resume-rating {
            right: 36px !important;
        }
        #template-functional.one-column-layout #ic-resume-soft-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional.one-column-layout #ic-resume-skill.infographics-show .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1) {
            padding-right: 36px !important;
        }







        .ic-resume-single-slot.two-col-active .ic-resume-single-slot-group .ic-resume-single-slot-location {
            width: auto !important;
        }
        .ic-resume-publication-name-text.ic-resume-single-slot-name-text{
            margin-bottom: 5px !important;
        }
        .ic-resume-single-slot-content-text-edit .ic-resume-single-slot-list-wrapper{
            margin-bottom: 8px;
        }
        .ic-resume-project-group{
            margin-bottom: 14px;
        }
        .ic-resume-award-group,
        .ic-resume-reference-group,
        .ic-resume-conference-group,
        .ic-resume-certificate-group,
        .ic-resume-organization-group,
        .ic-resume-achievement-group{
            margin-bottom: 10px;
        }
        #template-functional.two-column-layout .ic-resume-box-slot1 #ic-resume-skill.infographics-show .ic-resume-skill-group:nth-last-of-type(2), #template-functional.two-column-layout .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show .ic-resume-skill-group:nth-last-of-type(2),
        .ic-resume-skill-group {
            margin-bottom: 8px !important;
        }
        .two-column-layout .ic-resume-box-slot1 .vue-slider-rail{
            height: 12px !important;
        }
        .ic-resume-reference-contact-holder.ic-resume-single-slot-contact-holder {
            display: inline-block;
        }
        .one-column-layout .two-col-active .ic-resume-publication-group:nth-last-of-type(1),
        .one-column-layout .two-col-active .ic-resume-publication-group:nth-last-of-type(2){
            margin-bottom: 0;
        }

        .one-column-layout .two-col-active .ic-resume-single-slot-group:nth-last-of-type(1),
        .one-column-layout .two-col-active .ic-resume-single-slot-group:nth-last-of-type(2){
            margin-bottom: 0;
        }


    /*    For Custom style */
        .ic-resume-single-slot-header-title.editable-div[contenteditable=true]:empty:before {
            color: {{ $document->template_data['template_data']['themes']['color'] ?? '#0056b3' }};
        }
        .ic-resume-layout-header-icon{
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#0056b3' }};
        }
        .ic-coverletter-contact-item svg path{
            fill : {{ $document->template_data['template_data']['themes']['color'] ?? '#0056b3' }};
        }
        .resume-template #template-functional .ic-resume-box-name-holder{
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#0056b3' }};
        }
        .ic-resume-single-slot-header-title{
            color: {{ $document->template_data['template_data']['themes']['color'] ?? '#0056b3' }};
        }
        #template-functional .ic-resume-single-slot-header{
            border-bottom: 2px solid {{ $document->template_data['template_data']['themes']['color'] ?? '#0056b3' }};
        }
        #template-functional .ic-resume-single-slot-list-item-bullet{
            border: 2px solid {{ $document->template_data['template_data']['themes']['color'] ?? '#0056b3' }};
        }
        .ic-resume-single-slot-period-class{
            color: {{ $document->template_data['template_data']['themes']['color'] ?? '#0056b3' }};
        }
        #template-functional .ic-resume-single-slot-title-text{
            color: {{ $document->template_data['template_data']['themes']['color'] ?? '#0056b3' }};
        }
        .ic-resume-single-slot-period-btn-inner{
            background-color: {{ $document->template_data['template_data']['themes']['color'] ?? '#0056b3' }};
        }
        .ic-resume-single-slot-period-edit.present-active .ic-resume-single-slot-period-btn{
            border: 1px solid {{ $document->template_data['template_data']['themes']['color'] ?? '#0056b3' }};
        }
        .ic-resume-single-slot-location-text{
            color: {{ $document->template_data['template_data']['themes']['color'] ?? '#0056b3' }};
        }
        .ic-resume-single-slot-list-header-text{
            color: {{ $document->template_data['template_data']['themes']['color'] ?? '#0056b3' }};
        }


    </style>

    {{--    Style For Infographics  --}}
    <style>
        #template-functional ul.ic-resume-rating-stars li.star .star-main{
            position: relative;
            overflow: hidden;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot1 ul.ic-resume-rating-stars li.star .star-main svg {
            height: 28px;
            width: 28px;
            margin-top: 0;
        }
        #template-functional ul.ic-resume-rating-stars li.star .star-main svg {
            height: 20px;
            width: 20px;
            margin-top: 0;
            position:absolute;
            top:0;
            left:0;
        }
        .ic-resume-rating-stars.hover li.star.selected.hover svg.ic-resume-start-1,
        .ic-resume-rating-stars.hover li.star.selected svg.ic-resume-start-2,
        .ic-resume-rating-stars li.star.hover svg.ic-resume-start-1,
        .ic-resume-rating-stars li.star svg.ic-resume-start-2 {
            top:110% !important;
        }
        .ic-resume-rating-stars li.star.selected svg.ic-resume-start-2{
            top:0 !important;
        }
        .ic-resume-rating-stars.hover li.star.selected.hover svg.ic-resume-start-2,
        .ic-resume-rating-stars.hover li.star.selected svg.ic-resume-start-1,
        .ic-resume-rating-stars li.star.selected svg.ic-resume-start-2,
        .ic-resume-rating-stars li.star.hover svg.ic-resume-start-2 {
            top:0;
        }
        .ic-resume-rating-stars li.star svg {
            height: 24px;
            width: 24px;
            margin-top: -2px;
        }
        .ic-resume-rating-stars li.star svg path {
            fill: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-resume-single-slot[data-style=infographic-style-5] ul.ic-resume-rating-stars li.star .star-main svg,
        .ic-resume-single-slot[data-style=infographic-style-4] ul.ic-resume-rating-stars li.star .star-main svg,
        .ic-resume-single-slot[data-style=infographic-style-3] ul.ic-resume-rating-stars li.star .star-main svg,
        .ic-resume-single-slot[data-style=infographic-style-2] ul.ic-resume-rating-stars li.star .star-main svg,
        .ic-resume-single-slot[data-style=infographic-style-1] ul.ic-resume-rating-stars li.star .star-main svg,
        .ic-resume-single-slot[data-style=infographic-style-23] ul.ic-resume-rating-stars li.star .inner-star,
        .ic-resume-single-slot[data-style=infographic-style-22] ul.ic-resume-rating-stars li.star .inner-star,
        .ic-resume-single-slot[data-style=infographic-style-21] ul.ic-resume-rating-stars li.star .inner-star {
            display: none !important;
        }
        .ic-resume-single-slot[data-style=infographic-style-21] .ic-resume-rating-slider-main,
        .ic-resume-single-slot[data-style=infographic-style-22] .ic-resume-rating-slider-main,
        .ic-resume-single-slot[data-style=infographic-style-23] .ic-resume-rating-slider-main,
        .ic-resume-single-slot[data-style=infographic-style-21] .ic-resume-legend-row,
        .ic-resume-single-slot[data-style=infographic-style-22] .ic-resume-legend-row,
        .ic-resume-single-slot[data-style=infographic-style-23] .ic-resume-legend-row {
            display: none;
        }
        .ic-resume-single-slot[data-style=infographic-style-24] ul.ic-resume-rating-stars li.star svg.ic-resume-start-2,
        .ic-resume-single-slot[data-style=infographic-style-23] ul.ic-resume-rating-stars li.star svg.ic-resume-start-1,
        .ic-resume-single-slot[data-style=infographic-style-22] ul.ic-resume-rating-stars li.star svg.ic-resume-start-1 {
            display: none !important;
        }
        .ic-resume-single-slot[data-style=infographic-style-22] .ic-resume-rating-stars.hover li.star.hover svg.ic-resume-start-2,
        .ic-resume-single-slot[data-style=infographic-style-22] .ic-resume-rating-stars li.star.selected svg.ic-resume-start-2 {
            opacity: 1;
        }
        .ic-resume-single-slot[data-style=infographic-style-24] ul.ic-resume-rating-stars li.star svg.ic-resume-start-1,
        .ic-resume-single-slot[data-style=infographic-style-23] ul.ic-resume-rating-stars li.star svg.ic-resume-start-2,
        .ic-resume-single-slot[data-style=infographic-style-22] ul.ic-resume-rating-stars li.star svg.ic-resume-start-2 {
            top:0 !important;
        }
        .ic-resume-single-slot[data-style=infographic-style-22] .ic-resume-rating-stars.hover li.star.selected svg.ic-resume-start-2,
        .ic-resume-single-slot[data-style=infographic-style-22] .ic-resume-rating-stars li.star svg.ic-resume-start-2 {
            opacity: .5;
        }
        .ic-resume-single-slot[data-style=infographic-style-23] .ic-resume-rating-stars.hover li.star.hover svg.ic-resume-start-2,
        .ic-resume-single-slot[data-style=infographic-style-23] .ic-resume-rating-stars li.star.selected svg.ic-resume-start-2 {
            -webkit-transform: scale(1);
            transform: scale(1);
        }
        .ic-resume-single-slot[data-style=infographic-style-23] .ic-resume-rating-stars.hover li.star.selected svg.ic-resume-start-2,
        .ic-resume-single-slot[data-style=infographic-style-23] .ic-resume-rating-stars li.star svg.ic-resume-start-2 {
            -webkit-transform: scale(.5);
            transform: scale(.5);
        }
        .ic-resume-single-slot[data-style=infographic-style-23] .ic-resume-rating-stars.hover li.star.selected svg.ic-resume-start-2 path,
        .ic-resume-single-slot[data-style=infographic-style-23] .ic-resume-rating-stars li.star svg.ic-resume-start-2 path {
            fill: #202020;
        }
        .ic-resume-single-slot[data-style=infographic-style-23] .ic-resume-rating-stars.hover li.star.hover svg.ic-resume-start-2 path,
        .ic-resume-single-slot[data-style=infographic-style-23] .ic-resume-rating-stars li.star.selected svg.ic-resume-start-2 path {
            fill: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-resume-single-slot[data-style="infographic-style-13"] .ic-resume-legend-row,
        .ic-resume-single-slot[data-style="infographic-style-14"] .ic-resume-legend-row,
        .ic-resume-single-slot[data-style="infographic-style-15"] .ic-resume-legend-row,
        .ic-resume-single-slot[data-style="infographic-style-16"] .ic-resume-legend-row,
        .ic-resume-single-slot[data-style="infographic-style-17"] .ic-resume-legend-row,
        .ic-resume-single-slot[data-style="infographic-style-18"] .ic-resume-legend-row,
        .ic-resume-single-slot[data-style="infographic-style-19"] .ic-resume-legend-row,
        .ic-resume-single-slot[data-style="infographic-style-20"] .ic-resume-legend-row {
            display: none;
        }
        .ic-resume-round-slider {
            width: 80px;
            height: 80px;
            border-radius: 50%;
            border: 2px solid {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            position: relative;
        }
        .ic-resume-round-slider .progress-left{
            left: 0;
            width: 50%;
            height: 100%;
            overflow: hidden;
            position: absolute;
            top: 0;
            z-index: 1;
        }
        .ic-resume-round-slider .progress-bar{
            width: 100%;
            height: 100%;
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            position: absolute;
            top: 0;
        }
        .ic-resume-round-slider .progress-left .progress-bar{
            left: 100%;
            border-top-right-radius: 40px;
            border-bottom-right-radius: 40px;
            border-left: 0;
            -webkit-transform-origin: center left;
            transform-origin: center left;
            -webkit-transform: rotate(140deg);
            transform: rotate(140deg);
        }
        .ic-resume-round-slider .progress-right{
            right: 0;
            width: 50%;
            height: 100%;
            overflow: hidden;
            position: absolute;
            top: 0;
            z-index: 1;
        }
        .ic-resume-round-slider .progress-right .progress-bar{
            left: -100%;
            border-top-left-radius: 40px;
            border-bottom-left-radius: 40px;
            border-right: 0;
            -webkit-transform-origin: center right;
            transform-origin: center right;
            -webkit-transform: rotate(180deg);
            transform: rotate(180deg);
        }
        .ic-resume-round-slider-inner {
            height: 60px;
            width: 60px;
            background: #fff;
            border-radius: 100%;
            top: 50%;
            position: absolute;
            left: 50%;
            font-size:16px;
            font-weight: 600;
            -webkit-transform: translate(-50%,-50%);
            transform: translate(-50%,-50%);
            border: 2px solid {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            line-height: 60px;
            text-align: center;
            z-index: 9;
        }
        .ic-resume-single-slot[data-style="infographic-style-13"] .ic-resume-rating,
        .ic-resume-single-slot[data-style="infographic-style-14"] .ic-resume-rating,
        .ic-resume-single-slot[data-style="infographic-style-15"] .ic-resume-rating,
        .ic-resume-single-slot[data-style="infographic-style-16"] .ic-resume-rating,
        .ic-resume-single-slot[data-style="infographic-style-17"] .ic-resume-rating,
        .ic-resume-single-slot[data-style="infographic-style-18"] .ic-resume-rating,
        .ic-resume-single-slot[data-style="infographic-style-19"] .ic-resume-rating,
        .ic-resume-single-slot[data-style="infographic-style-20"] .ic-resume-rating {
            display: none !important;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-19"] .ic-resume-round-slider,
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-round-slider,
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-17"] .ic-resume-round-slider,
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-16"] .ic-resume-round-slider,
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-15"] .ic-resume-round-slider {
            display: block;
        }
        .ic-resume-round-slider {
            display: none;
            margin: 0 auto 10px;
        }
        .ic-resume-round-slider-inner span {
            font-weight: 600;
            font-size: 16px;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-16"] .ic-resume-round-slider{
            border:none;
            background: #cdcdcd;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-16"] .ic-resume-round-slider-inner {
            border:none;
            padding: 10px;
            line-height: 42px;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-16"] .ic-resume-round-slider-inner .progress-value{
            height: 100%;
            width: 100%;
            background: #cdcdcd;
            display: inline-block;
            border-radius: 100%;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-17"] .ic-resume-round-slider {
            width: 74px;
            height: 74px;
            border: 3px solid #fff;
            margin-top: 3px;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-17"] .ic-resume-round-slider-inner {
            background: transparent;
            border:none;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-17"] .ic-resume-round-slider:after {
            content: "";
            position: absolute;
            top: -6px;
            left: -6px;
            height: 80px;
            width: 80px;
            border-radius: 100%;
            border: 3px solid {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-round-slider {
            border: none;
            background: #cdcdcd;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-round-slider-inner {
            border: none;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-round-hanlde {
            position: absolute;
            height: 100%;
            left: 50%;
            width: 1px;
            z-index: 10;
            -webkit-transform: rotate(320deg);
            transform: rotate(320deg);
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-round-arrow{
            height:13px;
            width:13px;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-round-arrow:after {
            content: "";
            position: absolute;
            border-right: 13px solid {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            height: 13px;
            width: 13px;
            border-bottom: 13px solid transparent;
            -webkit-transform: rotate(45deg) translate(-8px, 5px);
            transform:rotate(45deg) translate(-8px, 5px);
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-round-line-1,
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-round-line-2{
            position: relative;
            height: 80px;
            width: 80px;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-round-slider:after {
            content: "";
            position: absolute;
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            height: 10px;
            width: 4px;
            -webkit-transform: rotate(49deg);
            transform: rotate(49deg);
            bottom: 2px;
            left: 2px;
            border-radius: 30px;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-round-slider:before {
            content: "";
            position: absolute;
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            height: 10px;
            width: 4px;
            -webkit-transform: rotate(59deg);
            transform: rotate(59deg);
            bottom: 8px;
            left: -3px;
            opacity: 0.9;
            border-radius: 30px;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-round-line-1:after {
            content: "";
            position: absolute;
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            height: 10px;
            width: 4px;
            -webkit-transform: rotate(65deg);
            transform: rotate(65deg);
            bottom: 15px;
            left: -7px;
            opacity: .7;
            border-radius: 30px;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-round-line-1:before {
            content: "";
            position: absolute;
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            height: 10px;
            width: 4px;
            -webkit-transform: rotate(75deg);
            transform: rotate(75deg);
            bottom: 22px;
            left: -9px;
            opacity: .5;
            border-radius: 30px;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-round-line-2:after {
            content: "";
            position: absolute;
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            height: 10px;
            width: 4px;
            -webkit-transform: rotate(84deg);
            transform: rotate(84deg);
            bottom: 29px;
            left: -10px;
            opacity: .4;
            border-radius: 30px;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-round-line-2:before {
            content: "";
            position: absolute;
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            height: 10px;
            width: 4px;
            -webkit-transform: rotate(91deg);
            transform: rotate(91deg);
            bottom: 37px;
            left: -10px;
            opacity: .2;
            border-radius: 30px;
        }
        .ic-resume-language-piecart-shape{
            display: none;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-20"] .ic-resume-language-piecart-shape,
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-20"] .ic-resume-language-rate-holder{
            display:block;
        }
        .ic-resume-language-piecart-shape {
            height: 58px;
            width: 50px;
            margin: 0 auto;
            position: relative;
        }
        .ic-resume-language-piecart-bottom {
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            height: 4px;
            width: 100%;
            position: absolute;
            bottom: 0;
            border-radius: 10px;
        }
        .ic-resume-language-piecart-line-1 {
            height: 18px;
            width: 6px;
            background: #d1d1d1;
            border-top-right-radius: 2px;
            border-top-left-radius: 2px;
            position: absolute;
            bottom: 6px;
            left:2px;
        }
        .ic-resume-language-piecart-line-2 {
            height: 26px;
            width: 6px;
            background: #d1d1d1;
            border-top-right-radius: 2px;
            border-top-left-radius: 2px;
            position: absolute;
            bottom: 6px;
            left: 12px;
        }
        .ic-resume-language-piecart-line-3 {
            height: 34px;
            width: 6px;
            background: #d1d1d1;
            border-top-right-radius: 2px;
            border-top-left-radius: 2px;
            position: absolute;
            bottom: 6px;
            left: 22px;
        }
        .ic-resume-language-piecart-line-4 {
            height: 42px;
            width: 6px;
            background: #d1d1d1;
            border-top-right-radius: 2px;
            border-top-left-radius: 2px;
            position: absolute;
            bottom: 6px;
            left: 32px;
        }
        .ic-resume-language-piecart-line-5 {
            height: 50px;
            width: 6px;
            background: #d1d1d1;
            border-top-right-radius: 2px;
            border-top-left-radius: 2px;
            position: absolute;
            bottom: 6px;
            left: 42px;
        }
        .ic-resume-language-rate-holder[data-rate="1"] .ic-resume-language-piecart-line-1{
            background-color:{{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-resume-language-rate-holder[data-rate="2"] .ic-resume-language-piecart-line-1,
        .ic-resume-language-rate-holder[data-rate="2"] .ic-resume-language-piecart-line-2{
            background-color:{{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-resume-language-rate-holder[data-rate="3"] .ic-resume-language-piecart-line-1,
        .ic-resume-language-rate-holder[data-rate="3"] .ic-resume-language-piecart-line-2,
        .ic-resume-language-rate-holder[data-rate="3"] .ic-resume-language-piecart-line-3{
            background-color:{{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-resume-language-rate-holder[data-rate="4"] .ic-resume-language-piecart-line-1,
        .ic-resume-language-rate-holder[data-rate="4"] .ic-resume-language-piecart-line-2,
        .ic-resume-language-rate-holder[data-rate="4"] .ic-resume-language-piecart-line-3,
        .ic-resume-language-rate-holder[data-rate="4"] .ic-resume-language-piecart-line-4{
            background-color:{{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-resume-language-rate-holder[data-rate="5"] .ic-resume-language-piecart-line-1,
        .ic-resume-language-rate-holder[data-rate="5"] .ic-resume-language-piecart-line-2,
        .ic-resume-language-rate-holder[data-rate="5"] .ic-resume-language-piecart-line-3,
        .ic-resume-language-rate-holder[data-rate="5"] .ic-resume-language-piecart-line-4,
        .ic-resume-language-rate-holder[data-rate="5"] .ic-resume-language-piecart-line-5{
            background-color:{{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
        }
        .ic-resume-single-slot[data-style="infographic-style-13"] .ic-resume-language-rate-shape {
            height: 80px;
            width: 80px;
            background: #d1d1d1;
            margin: 0 auto;
            position: relative;
        }
        .ic-resume-single-slot[data-style="infographic-style-13"] .ic-resume-language-rate-inner {
            display: block;
            position: absolute;
            bottom: 0;
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            height: 80%;
            width: 100%;
        }
        .ic-resume-single-slot[data-style="infographic-style-13"] .ic-resume-language-rate-holder[data-rate="1"] .ic-resume-language-rate-inner {
            height: 20%;
        }
        .ic-resume-single-slot[data-style="infographic-style-13"] .ic-resume-language-rate-holder[data-rate="2"] .ic-resume-language-rate-inner {
            height: 40%;
        }
        .ic-resume-single-slot[data-style="infographic-style-13"] .ic-resume-language-rate-holder[data-rate="3"] .ic-resume-language-rate-inner {
            height: 58%;
        }
        .ic-resume-single-slot[data-style="infographic-style-13"] .ic-resume-language-rate-holder[data-rate="4"] .ic-resume-language-rate-inner {
            height: 80%;
        }
        .ic-resume-single-slot[data-style="infographic-style-13"] .ic-resume-language-rate-holder[data-rate="5"] .ic-resume-language-rate-inner {
            height: 100%;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-13"] .ic-resume-language-rate-holder,
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-14"] .ic-resume-language-rate-holder {
            display: block;
        }
        .ic-resume-single-slot[data-style="infographic-style-14"] .ic-resume-language-rate-shape {
            height: 80px;
            width: 80px;
            margin: 0 auto;
            border: 2px solid #202020;
            border-radius: 100%;
            position: relative;
        }
        .ic-resume-single-slot[data-style="infographic-style-14"] .ic-resume-language-rate-inner {
            display: block;
            position: absolute;
            bottom: 0;
            background: {{ $document->template_data['template_data']['themes']['color'] ?? '#1E87FF' }};
            height: 100%;
            width: 100%;
            border-radius: 100%;
            border: 20px solid #fff;
        }
        .ic-resume-single-slot[data-style="infographic-style-14"] .ic-resume-language-rate-holder[data-rate="1"] .ic-resume-language-rate-inner {
            border: 20px solid #fff;
        }
        .ic-resume-single-slot[data-style="infographic-style-14"] .ic-resume-language-rate-holder[data-rate="2"] .ic-resume-language-rate-inner {
            border: 14px solid #fff;
        }
        .ic-resume-single-slot[data-style="infographic-style-14"] .ic-resume-language-rate-holder[data-rate="3"] .ic-resume-language-rate-inner {
            border: 12px solid #fff;
        }
        .ic-resume-single-slot[data-style="infographic-style-14"] .ic-resume-language-rate-holder[data-rate="4"] .ic-resume-language-rate-inner {
            border: 6px solid #fff;
        }
        .ic-resume-single-slot[data-style="infographic-style-14"] .ic-resume-language-rate-holder[data-rate="5"] .ic-resume-language-rate-inner {
            border: 2px solid #fff;
        }
        #template-functional.two-column-layout .ic-resume-box-slot2 .ic-resume-single-slot.infographics-show[data-style="infographic-style-15"] .ic-resume-single-slot-group:nth-of-type(4n+1),
        #template-functional.two-column-layout .ic-resume-box-slot2 .ic-resume-single-slot.infographics-show[data-style="infographic-style-16"] .ic-resume-single-slot-group:nth-of-type(4n+1),
        #template-functional.two-column-layout .ic-resume-box-slot2 .ic-resume-single-slot.infographics-show[data-style="infographic-style-17"] .ic-resume-single-slot-group:nth-of-type(4n+1),
        #template-functional.two-column-layout .ic-resume-box-slot2 .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-single-slot-group:nth-of-type(4n+1),
        #template-functional.two-column-layout .ic-resume-box-slot2 .ic-resume-single-slot.infographics-show[data-style="infographic-style-19"] .ic-resume-single-slot-group:nth-of-type(4n+1),
        #template-functional.two-column-layout .ic-resume-box-slot2 .ic-resume-single-slot.infographics-show[data-style="infographic-style-20"] .ic-resume-single-slot-group:nth-of-type(4n+1) {
            clear: both;
        }
        #template-functional.two-column-layout .ic-resume-box-slot2 .ic-resume-single-slot.infographics-show[data-style="infographic-style-15"] .ic-resume-single-slot-group,
        #template-functional.two-column-layout .ic-resume-box-slot2 .ic-resume-single-slot.infographics-show[data-style="infographic-style-16"] .ic-resume-single-slot-group,
        #template-functional.two-column-layout .ic-resume-box-slot2 .ic-resume-single-slot.infographics-show[data-style="infographic-style-17"] .ic-resume-single-slot-group,
        #template-functional.two-column-layout .ic-resume-box-slot2 .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-single-slot-group,
        #template-functional.two-column-layout .ic-resume-box-slot2 .ic-resume-single-slot.infographics-show[data-style="infographic-style-19"] .ic-resume-single-slot-group,
        #template-functional.two-column-layout .ic-resume-box-slot2 .ic-resume-single-slot.infographics-show[data-style="infographic-style-20"] .ic-resume-single-slot-group {
            width: 25% !important;
            clear: none;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-15"] .ic-resume-single-slot-group:nth-of-type(2n+2),
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-16"] .ic-resume-single-slot-group:nth-of-type(2n+2),
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-17"] .ic-resume-single-slot-group:nth-of-type(2n+2),
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-single-slot-group:nth-of-type(2n+2),
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-19"] .ic-resume-single-slot-group:nth-of-type(2n+2),
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-20"] .ic-resume-single-slot-group:nth-of-type(2n+2),
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-15"] .ic-resume-single-slot-group:nth-of-type(2n+1),
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-16"] .ic-resume-single-slot-group:nth-of-type(2n+1),
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-17"] .ic-resume-single-slot-group:nth-of-type(2n+1),
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-single-slot-group:nth-of-type(2n+1),
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-19"] .ic-resume-single-slot-group:nth-of-type(2n+1),
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-20"] .ic-resume-single-slot-group:nth-of-type(2n+1) {
            padding: 0 4px !important;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-15"] .ic-resume-single-slot-group-header,
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-16"] .ic-resume-single-slot-group-header,
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-17"] .ic-resume-single-slot-group-header,
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-single-slot-group-header,
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-19"] .ic-resume-single-slot-group-header,
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-20"] .ic-resume-single-slot-group-header {
            width: 100% !important;
            padding-right: 0 !important;
            float: none !important;
            text-align: center;
        }
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-15"] .ic-resume-single-slot-titlex-text,
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-16"] .ic-resume-single-slot-titlex-text,
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-17"] .ic-resume-single-slot-titlex-text,
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-single-slot-titlex-text,
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-19"] .ic-resume-single-slot-titlex-text,
        .ic-resume-single-slot.infographics-show[data-style="infographic-style-20"] .ic-resume-single-slot-titlex-text {
            float: none;
            width: 100% !important;
        }
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot2 .ic-resume-single-slot.infographics-show[data-style="infographic-style-14"] .ic-resume-single-slot-group,
        #template-functional.ic-resume-scrollbar-part.two-column-layout .ic-resume-box-slot2 .ic-resume-single-slot.infographics-show[data-style="infographic-style-13"] .ic-resume-single-slot-group {
            width: 25% !important;
            clear:none;
        }
        #ic-resume-skill.infographics-show[data-style="infographic-style-13"] .ic-resume-single-slot-group-header,
        #ic-resume-skill.infographics-show[data-style="infographic-style-14"] .ic-resume-single-slot-group-header,
        #ic-resume-skill.infographics-show[data-style="infographic-style-15"] .ic-resume-single-slot-group-header,
        #ic-resume-skill.infographics-show[data-style="infographic-style-16"] .ic-resume-single-slot-group-header,
        #ic-resume-skill.infographics-show[data-style="infographic-style-17"] .ic-resume-single-slot-group-header,
        #ic-resume-skill.infographics-show[data-style="infographic-style-18"] .ic-resume-single-slot-group-header,
        #ic-resume-skill.infographics-show[data-style="infographic-style-19"] .ic-resume-single-slot-group-header,
        #ic-resume-skill.infographics-show[data-style="infographic-style-20"] .ic-resume-single-slot-group-header,
        #ic-resume-soft-skill.infographics-show[data-style="infographic-style-13"] .ic-resume-single-slot-group-header,
        #ic-resume-soft-skill.infographics-show[data-style="infographic-style-14"] .ic-resume-single-slot-group-header,
        #ic-resume-soft-skill.infographics-show[data-style="infographic-style-15"] .ic-resume-single-slot-group-header,
        #ic-resume-soft-skill.infographics-show[data-style="infographic-style-16"] .ic-resume-single-slot-group-header,
        #ic-resume-soft-skill.infographics-show[data-style="infographic-style-17"] .ic-resume-single-slot-group-header,
        #ic-resume-soft-skill.infographics-show[data-style="infographic-style-18"] .ic-resume-single-slot-group-header,
        #ic-resume-soft-skill.infographics-show[data-style="infographic-style-19"] .ic-resume-single-slot-group-header,
        #ic-resume-soft-skill.infographics-show[data-style="infographic-style-20"] .ic-resume-single-slot-group-header{
            width: 100% !important;
            padding-right: 0 !important;
            float: none !important;
            text-align: center;
        }
        #template-functional #ic-resume-skill.infographics-show[data-style="infographic-style-13"] .ic-resume-single-slot-group-header,
        #template-functional #ic-resume-skill.infographics-show[data-style="infographic-style-14"] .ic-resume-single-slot-group-header,
        #template-functional #ic-resume-skill.infographics-show[data-style="infographic-style-15"] .ic-resume-single-slot-group-header,
        #template-functional #ic-resume-skill.infographics-show[data-style="infographic-style-16"] .ic-resume-single-slot-group-header,
        #template-functional #ic-resume-skill.infographics-show[data-style="infographic-style-17"] .ic-resume-single-slot-group-header,
        #template-functional #ic-resume-skill.infographics-show[data-style="infographic-style-18"] .ic-resume-single-slot-group-header,
        #template-functional #ic-resume-skill.infographics-show[data-style="infographic-style-19"] .ic-resume-single-slot-group-header,
        #template-functional #ic-resume-skill.infographics-show[data-style="infographic-style-20"] .ic-resume-single-slot-group-header,
        #template-functional #ic-resume-soft-skill.infographics-show[data-style="infographic-style-13"] .ic-resume-single-slot-group-header,
        #template-functional #ic-resume-soft-skill.infographics-show[data-style="infographic-style-14"] .ic-resume-single-slot-group-header,
        #template-functional #ic-resume-soft-skill.infographics-show[data-style="infographic-style-15"] .ic-resume-single-slot-group-header,
        #template-functional #ic-resume-soft-skill.infographics-show[data-style="infographic-style-16"] .ic-resume-single-slot-group-header,
        #template-functional #ic-resume-soft-skill.infographics-show[data-style="infographic-style-17"] .ic-resume-single-slot-group-header,
        #template-functional #ic-resume-soft-skill.infographics-show[data-style="infographic-style-18"] .ic-resume-single-slot-group-header,
        #template-functional #ic-resume-soft-skill.infographics-show[data-style="infographic-style-19"] .ic-resume-single-slot-group-header,
        #template-functional #ic-resume-soft-skill.infographics-show[data-style="infographic-style-20"] .ic-resume-single-slot-group-header{
            padding-right: 0 !important;
        }
        #ic-resume-skill.infographics-show[data-style="infographic-style-14"] .ic-resume-skill-group.ic-resume-single-slot-group,
        #ic-resume-skill.infographics-show[data-style="infographic-style-13"] .ic-resume-skill-group.ic-resume-single-slot-group,
        #ic-resume-soft-skill.infographics-show[data-style="infographic-style-14"] .ic-resume-skill-group.ic-resume-single-slot-group,
        #ic-resume-soft-skill.infographics-show[data-style="infographic-style-13"] .ic-resume-skill-group.ic-resume-single-slot-group {
            padding: 0 4px !important;
        }
        .one-column-layout .ic-resume-single-slot.infographics-show[data-style="infographic-style-13"] .ic-resume-single-slot-group,
        .one-column-layout .ic-resume-single-slot.infographics-show[data-style="infographic-style-14"] .ic-resume-single-slot-group,
        .one-column-layout .ic-resume-single-slot.infographics-show[data-style="infographic-style-15"] .ic-resume-single-slot-group,
        .one-column-layout .ic-resume-single-slot.infographics-show[data-style="infographic-style-16"] .ic-resume-single-slot-group,
        .one-column-layout .ic-resume-single-slot.infographics-show[data-style="infographic-style-17"] .ic-resume-single-slot-group,
        .one-column-layout .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-single-slot-group,
        .one-column-layout .ic-resume-single-slot.infographics-show[data-style="infographic-style-19"] .ic-resume-single-slot-group,
        .one-column-layout .ic-resume-single-slot.infographics-show[data-style="infographic-style-20"] .ic-resume-single-slot-group {
            width: 20% !important;
        }
        #template-functional .ic-resume-box-slot1 #ic-resume-language.infographics-show[data-style="infographic-style-15"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-language.infographics-show[data-style="infographic-style-16"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-language.infographics-show[data-style="infographic-style-17"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-language.infographics-show[data-style="infographic-style-18"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-language.infographics-show[data-style="infographic-style-19"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-language.infographics-show[data-style="infographic-style-20"] .ic-resume-language-group.ic-resume-single-slot-group:nth-of-type(2n+1){
            clear:none;
        }
        #template-functional.one-column-layout #ic-resume-soft-skill.infographics-show[data-style="infographic-style-13"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional.one-column-layout #ic-resume-skill.infographics-show[data-style="infographic-style-13"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional.one-column-layout #ic-resume-soft-skill.infographics-show[data-style="infographic-style-14"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional.one-column-layout #ic-resume-skill.infographics-show[data-style="infographic-style-14"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional.one-column-layout #ic-resume-soft-skill.infographics-show[data-style="infographic-style-15"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional.one-column-layout #ic-resume-skill.infographics-show[data-style="infographic-style-15"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional.one-column-layout #ic-resume-soft-skill.infographics-show[data-style="infographic-style-16"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional.one-column-layout #ic-resume-skill.infographics-show[data-style="infographic-style-16"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional.one-column-layout #ic-resume-soft-skill.infographics-show[data-style="infographic-style-17"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional.one-column-layout #ic-resume-skill.infographics-show[data-style="infographic-style-17"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional.one-column-layout #ic-resume-soft-skill.infographics-show[data-style="infographic-style-18"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional.one-column-layout #ic-resume-skill.infographics-show[data-style="infographic-style-18"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional.one-column-layout #ic-resume-soft-skill.infographics-show[data-style="infographic-style-20"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional.one-column-layout #ic-resume-skill.infographics-show[data-style="infographic-style-20"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1){
            padding-right: 4px !important;
        }
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-single-slot-group:nth-of-type(2n+2),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-single-slot-group:nth-of-type(2n+2),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-15"] .ic-resume-single-slot-group:nth-of-type(2n+2),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-16"] .ic-resume-single-slot-group:nth-of-type(2n+2),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-17"] .ic-resume-single-slot-group:nth-of-type(2n+2),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-18"] .ic-resume-single-slot-group:nth-of-type(2n+2),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-19"] .ic-resume-single-slot-group:nth-of-type(2n+2),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-20"] .ic-resume-single-slot-group:nth-of-type(2n+2),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-13"] .ic-resume-single-slot-group:nth-of-type(2n+1),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-14"] .ic-resume-single-slot-group:nth-of-type(2n+1),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-15"] .ic-resume-single-slot-group:nth-of-type(2n+1),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-16"] .ic-resume-single-slot-group:nth-of-type(2n+1),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-17"] .ic-resume-single-slot-group:nth-of-type(2n+1),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-18"] .ic-resume-single-slot-group:nth-of-type(2n+1),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-19"] .ic-resume-single-slot-group:nth-of-type(2n+1),
        .one-column-layout #ic-resume-language.infographics-show[data-style="infographic-style-20"] .ic-resume-single-slot-group:nth-of-type(2n+1) {
            padding: 0 4px !important;
        }
        .one-column-layout .ic-resume-single-slot.infographics-show[data-style="infographic-style-15"] .ic-resume-single-slot-group:nth-of-type(5n+1),
        .one-column-layout .ic-resume-single-slot.infographics-show[data-style="infographic-style-16"] .ic-resume-single-slot-group:nth-of-type(5n+1),
        .one-column-layout .ic-resume-single-slot.infographics-show[data-style="infographic-style-17"] .ic-resume-single-slot-group:nth-of-type(5n+1),
        .one-column-layout .ic-resume-single-slot.infographics-show[data-style="infographic-style-18"] .ic-resume-single-slot-group:nth-of-type(5n+1),
        .one-column-layout .ic-resume-single-slot.infographics-show[data-style="infographic-style-20"] .ic-resume-single-slot-group:nth-of-type(5n+1) {
            clear: both;
        }
        #template-functional.one-column-layout #ic-resume-language.infographics-show .ic-resume-rating{
            width: calc(50% - 0px) !important;
        }
        #template-functional .ic-resume-box-slot1 #ic-resume-skill.infographics-show[data-style="infographic-style-13"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-skill.infographics-show[data-style="infographic-style-14"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-skill.infographics-show[data-style="infographic-style-15"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-skill.infographics-show[data-style="infographic-style-16"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-skill.infographics-show[data-style="infographic-style-17"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-skill.infographics-show[data-style="infographic-style-18"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-skill.infographics-show[data-style="infographic-style-19"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-skill.infographics-show[data-style="infographic-style-20"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show[data-style="infographic-style-13"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show[data-style="infographic-style-14"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show[data-style="infographic-style-15"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show[data-style="infographic-style-16"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show[data-style="infographic-style-17"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show[data-style="infographic-style-18"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show[data-style="infographic-style-19"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1),
        #template-functional .ic-resume-box-slot1 #ic-resume-soft-skill.infographics-show[data-style="infographic-style-20"] .ic-resume-skill-group.ic-resume-single-slot-group:nth-of-type(2n+1){
            clear:none;
        }
        .one-column-layout .ic-resume-single-slot.infographics-show ul.ic-resume-rating-stars{
            float:right;
        }
        #ic-resume-language.infographics-show[data-style="infographic-style-6"] .ic-resume-rating, #ic-resume-language.infographics-show[data-style="infographic-style-7"] .ic-resume-rating, #ic-resume-language.infographics-show[data-style="infographic-style-8"] .ic-resume-rating, #ic-resume-language.infographics-show[data-style="infographic-style-9"] .ic-resume-rating, #ic-resume-language.infographics-show[data-style="infographic-style-10"] .ic-resume-rating, #ic-resume-language.infographics-show[data-style="infographic-style-11"] .ic-resume-rating, #ic-resume-language.infographics-show[data-style="infographic-style-12"] .ic-resume-rating{
            width:156px !important;
        }
    </style>
</head>
<body class="ic-resume-template-editor">
<div class="ic-coverletter-edit-section resume-template">
    @include('api.pdf.resume.layout.' .$document->document_layout)
</div>
</body>
</html>
