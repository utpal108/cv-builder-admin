@component('mail::message')
Hello {{ $user->name }},

Welcome back to CVBility, we are delighted to see you again!


Click the button below within the next 6 hours to log into your account:

@component('mail::button', ['url' => config('settings.web_url').'magic/login/' .$token])
Log Into {{ config('app.name') }}
@endcomponent

Or copy this link:

[{{config('settings.web_url').'magic/login/' .$token}}]({{config('settings.web_url').'magic/login/' .$token}})

Thanks,<br>
{{ config('app.name') }}
@endcomponent
