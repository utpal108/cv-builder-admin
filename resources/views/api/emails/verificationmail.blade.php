@component('mail::message')
Hello {{ $user->name }},

We’re very excited to have you on board!

Copy the following code in the sign-up form

@component('mail::button', ['url' => '#'])
{{ $user->verification_code}}
@endcomponent

Please do not forward this email to others to prevent anybody else from accessing your account.

Thanks,<br>
{{ config('app.name') }}
@endcomponent