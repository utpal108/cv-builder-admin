@extends('admin.layouts.master')
@section('title')
EDIT USERS
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Administration > <a href="{{ route('users.index') }}" class="black-text">Users</a></h2>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Edit User</h2>
            </div>
            <div class="body">
                <form method="post" action="{{ route('users.update', ['id'=>$user->id]) }}"
                    enctype="multipart/form-data" class="ng-untouched ng-pristine ng-valid">
                    @csrf
                    @method('put')

                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            <input type="text" name="first_name" value="{{ $user->first_name }}" required
                                id="first_name" class="form-control">
                            <label class="form-label active">Last Name *</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            <input type="text" name="last_name" value="{{ $user->last_name }}" required id="last_name"
                                class="form-control">
                            <label class="form-label active">Last Name *</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            <input type="email" name="email" value="{{ $user->email }}" required id="email"
                                class="form-control">
                            <label class="form-label active">Email *</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            <input type="text" name="username" value="{{ $user->username }}" required id="username"
                                class="form-control">
                            <label class="form-label active">Username *</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            <input type="password" name="password" id="password" class="form-control">
                            <label class="form-label active">Password</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            <input type="password" name="password_confirmation" id="password_confirmation"
                                class="form-control">
                            <label class="form-label active">Password Confirmation</label>
                        </div>
                    </div>

                    @if(auth()->user()->user_type == 'admin')
                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            {!! Form::select('status', ['active'=>'Active', 'pending'=>'Pending'], $user->status,
                            ['class'=>'form-control', 'id'=>'status']) !!}
                            <label class="form-label active">Status *</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            {!! Form::select('user_type', ['admin'=>'Admin', 'user'=>'User'], $user->user_type ,
                            ['class'=>'form-control', 'id'=>'user_type']) !!}
                            <label class="form-label active">User Type *</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            {!! Form::select('role', $roles, count($user->roles)?$user->roles[0]->id:'',
                            ['class'=>'form-control', 'id'=>'role']) !!}
                            <label class="form-label active">User Role *</label>
                        </div>
                    </div>
                    @endif

                    <div class="form-group form-float">
                        <input type="file" name="profile_image" class="form-control">
                        <label class="form-label active">Profile Image</label>
                        <small>Leave it blank if you don't want to change picture</small>
                    </div>
                    <div>
                        <img style="height: 200px;width: auto;" src="{{ adminimage($user->profile_image) }}" alt="">
                    </div>

                    <br>

                    <button class="btn btn-primary m-t-15 waves-effect" type="submit">
                        <i class="material-icons">update</i>
                        <span>UPDATE</span>
                    </button>
                    <a class="btn btn-warning m-t-15 waves-effect" href="{{ route('users.index') }}">
                        <i class="material-icons">keyboard_backspace</i>
                        <span>BACK</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('csss')

@endpush
@push('scripts')
<script src="{{ asset('vendor') }}/laravel-filemanager/js/stand-alone-button.js"></script>
<script>
    $(document).ready(function () {
            $('#lfm').filemanager('image');
        });
</script>
@endpush