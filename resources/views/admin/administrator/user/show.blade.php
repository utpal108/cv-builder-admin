@extends('admin.layouts.master')
@section('title')
SHOW USERS
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Administration > <a href="{{ route('users.index') }}" class="black-text">Users</a></h2>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Show Users</h2>

            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                        <thead>
                            <tr>
                                <th>Key</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Name</td>
                                <td>{{ $user->name }}</td>
                            </tr>

                            <tr>
                                <td>Email</td>
                                <td>{{ $user->email }}</td>
                            </tr>

                            <tr>
                                <td>User Type</td>
                                <td>{{ ucfirst($user->user_type) }}</td>
                            </tr>

                            <tr>
                                <td>Status</td>
                                <td>{{ ucfirst($user->status) }}</td>
                            </tr>

                            <tr>
                                <td>Role</td>
                                <td>{{ count($user->roles)?$user->roles[0]->name:'' }}</td>
                            </tr>
                            <tr>
                                <td>Profile Image</td>
                                <td><img src="{{ adminimage($user->profile_image) }}" alt="{{ $user->name }}"
                                        style="height: 200px; width: auto;"></td>
                            </tr>


                            <tr>
                                <td></td>
                                <td>
                                    <a class="btn btn-warning m-t-15 waves-effect pull-right"
                                        href="{{ route('users.index') }}">
                                        <i class="material-icons">keyboard_backspace</i>
                                        <span>BACK</span>
                                    </a>
                                </td>
                            </tr>

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('csss')

@endpush
@push('scripts')
@endpush