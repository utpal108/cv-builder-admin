@extends('admin.layouts.master')
@section('title')
ADD USERS
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Administration > <a href="{{ route('users.index') }}" class="black-text">Users</a></h2>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Add User</h2>
            </div>
            <div class="body">
                <form method="post" action="{{ route('users.store') }}" enctype="multipart/form-data"
                    class="ng-untouched ng-pristine ng-valid">
                    @csrf

                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            <input type="text" name="first_name" value="{{ old('first_name') }}" required
                                id="first_name" class="form-control">
                            <label class="form-label active">First Name *</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            <input type="text" name="last_name" value="{{ old('last_name') }}" required id="last_name"
                                class="form-control">
                            <label class="form-label active">Last Name *</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            <input type="email" name="email" value="{{ old('email') }}" required id="email"
                                class="form-control">
                            <label class="form-label active">Email *</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            <input type="text" name="username" value="{{ old('username') }}" required id="username"
                                class="form-control">
                            <label class="form-label active">Username *</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            <input type="password" name="password" required id="password" class="form-control">
                            <label class="form-label active">Password *</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            <input type="password" name="password_confirmation" required id="password_confirmation"
                                class="form-control">
                            <label class="form-label active">Password Confirmation *</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            {!! Form::select('status', ['active'=>'Active', 'pending'=>'Pending'], old('status'),
                            ['class'=>'form-control', 'id'=>'status']) !!}
                            <label class="form-label active">Status *</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            {!! Form::select('user_type', ['admin'=>'Admin', 'user'=>'User'], old('user_type'),
                            ['class'=>'form-control', 'id'=>'user_type']) !!}
                            <label class="form-label active">User Type *</label>
                        </div>
                    </div>

                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            {!! Form::select('role', $roles, old('role'), ['class'=>'form-control', 'id'=>'role']) !!}
                            <label class="form-label active">User Role *</label>
                        </div>
                    </div>

                    <div class="form-group form-float ">
                        <div class="form-line input-field">
                            <input type="file" class="form-control" id="profile_image" name="profile_image" required>
                            <label class="form-label active">Profile Image *</label>
                        </div>
                    </div>


                    <br>

                    <button class="btn btn-primary m-t-15 waves-effect" type="submit">
                        <i class="material-icons">save</i>
                        <span>SAVE</span>
                    </button>
                    <a class="btn btn-warning m-t-15 waves-effect" href="{{ route('users.index') }}">
                        <i class="material-icons">keyboard_backspace</i>
                        <span>BACK</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')

@endpush
@push('js')
<script>
    // $(document).ready(function(){
    //     $('#profile_image_preview').on('click', function(){
    //         $('#profile_image').trigger('click');
    //     });

    //     $('#profile_image').on('change', function(){
    //         console.log($(this));
    //         if ($(this).files && $(this).files[0]) {            
    //             var FR= new FileReader();            
    //             FR.addEventListener("load", function(e) {
    //                 $('#profile_image_preview').attr('src', e.target.result)
    //             });             
    //             FR.readAsDataURL( $(this).files[0] );                
    //         }  
    //     });
    // });
</script>
@endpush