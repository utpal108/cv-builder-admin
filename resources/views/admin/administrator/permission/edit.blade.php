@extends('admin.layouts.master')
@section('title')
EDIT PERMISSION
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Administrator > <a href="{{ route('permissions.index') }}" class="black-text">Permissions</a></h2>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Edit Permission</h2>
            </div>
            <div class="body">
                <form novalidate="" method="post" action="{{ route('permissions.update', ['id'=>$permission->id]) }}"
                    class="ng-untouched ng-pristine ng-valid">
                    @csrf
                    @method('put')

                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            @php $permissions->prepend('Select Parent Id', ''); @endphp
                            {!! Form::select('parent_id', $permissions, $permission->parent_id,
                            ['class'=>'form-control', 'id'=>'parent_id']) !!}
                            <label class="form-label active">Parent Permission</label>
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            <input type="text" name="name" value="{{ $permission->name }}" id="name"
                                class="form-control">
                            <label class="form-label active">Permission Name</label>
                        </div>
                    </div>
                    <br>

                    <button class="btn btn-primary m-t-15 waves-effect" type="submit">
                        <i class="material-icons">update</i>
                        <span>UPDATE</span>
                    </button>
                    <a class="btn btn-warning m-t-15 waves-effect" href="{{ route('permissions.index') }}">
                        <i class="material-icons">keyboard_backspace</i>
                        <span>BACK</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('csss')

@endpush
@push('scripts')

@endpush