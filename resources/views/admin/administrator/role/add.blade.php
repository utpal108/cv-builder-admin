@extends('admin.layouts.master')
@section('title')
ADD ROLES
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Administrator > <a href="{{ route('roles.index') }}" class="black-text">Roles</a></h2>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Add Role</h2>
            </div>
            <div class="body">
                <form novalidate="" method="post" action="{{ route('roles.store') }}"
                    class="ng-untouched ng-pristine ng-valid">
                    @csrf

                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            <input type="text" name="name" value="{{ old('name') }}" required id="name"
                                class="form-control">
                            <label class="form-label active">Role Name</label>
                        </div>
                    </div>
                    <br>
                    <div class="m-b-20">
                        <p>All Permissions</p>
                        @foreach($permissions as $permission)
                        <p>
                            <label>
                                <input type="checkbox" name="permissions[]" data-pclass="pclass-{{$permission->id}}"
                                    value="{{ $permission->name }}">
                                <span>{{ $permission->name }}</span>
                            </label>
                        </p>
                        @if(count($permission->children) > 0)
                        <div class="checkbox-div m-l-35 m-b-10 pclass-{{$permission->id}}" style="display: none;">
                            @foreach($permission->children as $child)
                            <label class="m-r-10">
                                <input value="{{ $child->name }}" data-pclass="pclass-{{$child->id}}" type="checkbox"
                                    name="permissions[]">
                                <span>{{ $child->name }}</span>
                            </label>
                            @endforeach
                        </div>
                        @endif
                        @endforeach
                    </div>

                    <button class="btn btn-primary m-t-15 waves-effect" type="submit">
                        <i class="material-icons">save</i>
                        <span>SAVE</span>
                    </button>
                    <a class="btn btn-warning m-t-15 waves-effect" href="{{ route('roles.index') }}">
                        <i class="material-icons">keyboard_backspace</i>
                        <span>BACK</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')

@endpush
@push('js')
<script>
    $(document).ready(function () {
            $('[name="permissions[]"]').on('change', function () {
                let classname = $(this).data('pclass');
                if ($('.'+classname).length > 0)
                {
                    if (this.checked)
                    {
                        $('.'+classname).show('show');
                    }else {
                        $('.'+classname).hide('show');
                    }
                }
            });
        });
</script>
@endpush