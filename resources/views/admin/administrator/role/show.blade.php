@extends('admin.layouts.master')
@section('title')
SHOW ROLES
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Administrator > <a href="{{ route('roles.index') }}" class="black-text">Roles</a></h2>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Show Role</h2>

            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="display table table-hover table-checkable order-column m-t-20 width-per-100">
                        <thead>
                            <tr>
                                <th>Key</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Role Name</td>
                                <td>{{ $role->name }}</td>
                            </tr>
                            <tr>
                                <td>Role Permissions</td>
                                <td>
                                    <ul class="list-group">
                                        @foreach($role->permissions as $permission)
                                        <li class="list-group-item">{{ $permission->name }}</li>
                                        @endforeach
                                    </ul>
                                </td>
                            </tr>

                            <tr>
                                <td></td>
                                <td>
                                    <a class="btn btn-warning m-t-15 waves-effect pull-right"
                                        href="{{ route('roles.index') }}">
                                        <i class="material-icons">keyboard_backspace</i>
                                        <span>BACK</span>
                                    </a>
                                </td>
                            </tr>

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('csss')

@endpush
@push('scripts')
@endpush