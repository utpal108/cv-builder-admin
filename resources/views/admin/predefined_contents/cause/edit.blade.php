@extends('admin.layouts.master')
@section('title')
    EDIT CAUSE PREDEFINED CONTENT
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Administration > <a href="{{ action('Admin\PredefinedCauseController@index') }}" class="black-text">Predefined Causes</a></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Edit Predefined Causes</h2>
                </div>
                <div class="body">
                    <form method="post" action="{{ action('Admin\PredefinedCauseController@update',['id'=>$predefined_cause->id]) }}"
                          enctype="multipart/form-data" class="ng-untouched ng-pristine ng-valid">
                        @csrf
                        @method('put')

                        <div class="form-group form-float">
                            <div class="form-line input-field">
                                <input type="text" name="title" value="{{ $predefined_cause->title }}" required class="form-control">
                                <label class="form-label active">Title *</label>
                            </div>
                        </div>

                        <br>

                        <button class="btn btn-primary m-t-15 waves-effect" type="submit">
                            <i class="material-icons">update</i>
                            <span>UPDATE</span>
                        </button>
                        <a class="btn btn-warning m-t-15 waves-effect" href="{{ action('Admin\PredefinedCauseController@index') }}">
                            <i class="material-icons">keyboard_backspace</i>
                            <span>BACK</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('csss')

@endpush
@push('scripts')
    <script src="{{ asset('vendor') }}/laravel-filemanager/js/stand-alone-button.js"></script>
    <script>
        $(document).ready(function () {
            $('#lfm').filemanager('image');
        });
    </script>
@endpush
