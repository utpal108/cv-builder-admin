@extends('admin.layouts.master')
@section('title')
    ADD PREDEFINED TEACHING
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Administration > <a href="{{ action('Admin\PredefinedTeachingController@index') }}" class="black-text">Predefined Teaching</a></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Add Predefined Contents</h2>
                </div>
                <div class="body">
                    <form method="post" action="{{ action('Admin\PredefinedTeachingController@store') }}" enctype="multipart/form-data"
                          class="ng-untouched ng-pristine ng-valid">
                        @csrf

                        <div class="form-group form-float ">
                            <div class="form-line input-field">
                                <input type="file" class="form-control" name="predefined_teachings" required>
                                <label class="form-label active">Excel File *</label>
                            </div>
                        </div>

                        <br>

                        <button class="btn btn-primary m-t-15 waves-effect" type="submit">
                            <i class="material-icons">save</i>
                            <span>SAVE</span>
                        </button>
                        <a class="btn btn-warning m-t-15 waves-effect" href="{{ action('Admin\PredefinedTeachingController@index') }}">
                            <i class="material-icons">keyboard_backspace</i>
                            <span>BACK</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')

@endpush
@push('js')
    <script>

    </script>
@endpush
