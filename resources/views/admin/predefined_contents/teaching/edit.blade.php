@extends('admin.layouts.master')
@section('title')
    EDIT TEACHING PREDEFINED CONTENT
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Administration > <a href="{{ action('Admin\PredefinedTeachingController@index') }}" class="black-text">Predefined Teaching</a></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Edit Predefined Teaching</h2>
                </div>
                <div class="body">
                    <form method="post" action="{{ action('Admin\PredefinedTeachingController@update',['id'=>$predefined_teaching->id]) }}"
                          enctype="multipart/form-data" class="ng-untouched ng-pristine ng-valid">
                        @csrf
                        @method('put')

                        <div class="form-group form-float">
                            <div class="form-line input-field">
                                <input type="text" name="experience_title" value="{{ $predefined_teaching->experience_title }}" required class="form-control">
                                <label class="form-label active">Experience Title *</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line input-field">
                                <input type="text" name="institution" value="{{ $predefined_teaching->institution }}" required class="form-control">
                                <label class="form-label active">Institution *</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line input-field">
                                <textarea name="address" required class="form-control">{{ $predefined_teaching->address }}</textarea>
                                <label class="form-label">Address *</label>
                            </div>
                        </div>

                        <br>

                        <button class="btn btn-primary m-t-15 waves-effect" type="submit">
                            <i class="material-icons">update</i>
                            <span>UPDATE</span>
                        </button>
                        <a class="btn btn-warning m-t-15 waves-effect" href="{{ action('Admin\PredefinedTeachingController@index') }}">
                            <i class="material-icons">keyboard_backspace</i>
                            <span>BACK</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('csss')

@endpush
@push('scripts')
    <script src="{{ asset('vendor') }}/laravel-filemanager/js/stand-alone-button.js"></script>
    <script>
        $(document).ready(function () {
            $('#lfm').filemanager('image');
        });
    </script>
@endpush
