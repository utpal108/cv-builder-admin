@extends('admin.layouts.master')
@section('title')
    EDIT SOFT SKILL PREDEFINED CONTENT
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Administration > <a href="{{ action('Admin\PredefinedSoftSkillContentController@index') }}" class="black-text">Predefined Soft Skills</a></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Edit Predefined Soft Skills</h2>
                </div>
                <div class="body">
                    <form method="post" action="{{ action('Admin\PredefinedSoftSkillContentController@update',['id'=>$predefined_soft_skill->id]) }}"
                          enctype="multipart/form-data" class="ng-untouched ng-pristine ng-valid">
                        @csrf
                        @method('put')

                        <div class="form-group form-float">
                            <div class="form-line input-field">
                                <input type="text" name="category" value="{{ $predefined_soft_skill->category }}" required class="form-control">
                                <label class="form-label active">Category *</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line input-field">
                                <textarea class="form-control" name="phrase" required>{{ $predefined_soft_skill->phrase }}</textarea>
                                <label class="form-label active">Phrase *</label>
                            </div>
                        </div>

                        <br>

                        <button class="btn btn-primary m-t-15 waves-effect" type="submit">
                            <i class="material-icons">update</i>
                            <span>UPDATE</span>
                        </button>
                        <a class="btn btn-warning m-t-15 waves-effect" href="{{ action('Admin\PredefinedSoftSkillContentController@index') }}">
                            <i class="material-icons">keyboard_backspace</i>
                            <span>BACK</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('csss')

@endpush
