@extends('admin.layouts.master')
@section('title')
    ADD PREDEFINED SKILLS
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Administration > <a href="{{ action('Admin\PredefinedSkillContentController@index') }}" class="black-text">Predefined Skills</a></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Add Predefined Contents</h2>
                </div>
                <div class="body">
                    <form method="post" action="{{ action('Admin\PredefinedSkillContentController@store') }}" enctype="multipart/form-data"
                          class="ng-untouched ng-pristine ng-valid">
                        @csrf

                        <div class="form-group form-float ">
                            <div class="form-line input-field">
                                <input type="file" class="form-control" id="profile_image" name="predefined_skills" required>
                                <label class="form-label active">Excel File *</label>
                            </div>
                        </div>

                        <br>

                        <button class="btn btn-primary m-t-15 waves-effect" type="submit">
                            <i class="material-icons">save</i>
                            <span>SAVE</span>
                        </button>
                        <a class="btn btn-warning m-t-15 waves-effect" href="{{ action('Admin\PredefinedSkillContentController@index') }}">
                            <i class="material-icons">keyboard_backspace</i>
                            <span>BACK</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')

@endpush
@push('js')
    <script>
        // $(document).ready(function(){
        //     $('#profile_image_preview').on('click', function(){
        //         $('#profile_image').trigger('click');
        //     });

        //     $('#profile_image').on('change', function(){
        //         console.log($(this));
        //         if ($(this).files && $(this).files[0]) {
        //             var FR= new FileReader();
        //             FR.addEventListener("load", function(e) {
        //                 $('#profile_image_preview').attr('src', e.target.result)
        //             });
        //             FR.readAsDataURL( $(this).files[0] );
        //         }
        //     });
        // });
    </script>
@endpush
