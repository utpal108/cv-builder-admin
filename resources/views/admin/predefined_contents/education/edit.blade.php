@extends('admin.layouts.master')
@section('title')
    EDIT EDUCATION PREDEFINED CONTENT
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Administration > <a href="{{ action('Admin\PredefinedEducationContentController@index') }}" class="black-text">Predefined Study Programs</a></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Edit Predefined Study Program</h2>
                </div>
                <div class="body">
                    <form method="post" action="{{ action('Admin\PredefinedEducationContentController@update',['id'=>$predefined_education->id]) }}"
                          enctype="multipart/form-data" class="ng-untouched ng-pristine ng-valid">
                        @csrf
                        @method('put')

                        <div class="form-group form-float">
                            <div class="form-line input-field">
                                <input type="text" name="type" value="{{ $predefined_education->type }}" required class="form-control">
                                <label class="form-label active">Type *</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line input-field">
                                <input type="text" name="category" value="{{ $predefined_education->category }}" required class="form-control">
                                <label class="form-label active">Category *</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line input-field">
                                <input type="text" name="phrase" value="{{ $predefined_education->phrase }}" required class="form-control">
                                <label class="form-label active">Phrase *</label>
                            </div>
                        </div>

                        <br>

                        <button class="btn btn-primary m-t-15 waves-effect" type="submit">
                            <i class="material-icons">update</i>
                            <span>UPDATE</span>
                        </button>
                        <a class="btn btn-warning m-t-15 waves-effect" href="{{ action('Admin\PredefinedEducationContentController@index') }}">
                            <i class="material-icons">keyboard_backspace</i>
                            <span>BACK</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('csss')

@endpush
@push('scripts')
    <script src="{{ asset('vendor') }}/laravel-filemanager/js/stand-alone-button.js"></script>
    <script>
        $(document).ready(function () {
            $('#lfm').filemanager('image');
        });
    </script>
@endpush
