@extends('admin.layouts.master')
@section('title')
    EDIT COVER OPTIMIZER
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Administration > <a href="{{ action('Admin\CoverOptimizerController@index') }}" class="black-text">Cover Optimizers</a></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Edit Cover Optimizer Message</h2>
                </div>
                <div class="body">
                    <form method="post" action="{{ action('Admin\CoverOptimizerController@update',$cover_optimizer->id) }}" class="ng-untouched ng-pristine ng-valid">
                        @csrf
                        @method('put')

                        <div class="form-group form-float">
                            <div class="form-line input-field">
                                <select class="form-control" name="type" disabled>
                                    <option value="revision" {{ ($cover_optimizer->type == 'revision') ? 'selected' : '' }}>Revision</option>
                                    <option value="suggestion" {{ ($cover_optimizer->type == 'suggestion') ? 'selected' : '' }}>Suggestion</option>
                                </select>
                                <label class="form-label active">Type *</label>
                            </div>
                        </div>

                        <div class="form-group form-float ">
                            <div class="form-line input-field">
                                <input type="text" name="section_name" class="form-control" value="{{ $cover_optimizer->section_name }}" disabled>
                                <label class="form-label active">Section Name *</label>
                            </div>
                        </div>

                        <div class="form-group form-float ">
                            <div class="form-line input-field">
                                <input type="text" name="element_name" class="form-control" value="{{ $cover_optimizer->element_name }}" readonly>
                                <label class="form-label active">Element Name *</label>
                            </div>
                        </div>

                        <div class="form-group form-float ">
                            <div class="form-line input-field">
                                <input type="text" name="title" class="form-control" value="{{ $cover_optimizer->title }}">
                                <label class="form-label active">Title *</label>
                            </div>
                        </div>

                        <div class="form-group form-float ">
                            <div class="form-line input-field">
                                <textarea name="details" rows="8" class="form-control" required>{{ $cover_optimizer->details }}</textarea>
                                <label class="form-label active">Details *</label>
                            </div>
                        </div>

                        <br>

                        <button class="btn btn-primary m-t-15 waves-effect" type="submit">
                            <i class="material-icons">update</i>
                            <span>UPDATE</span>
                        </button>
                        <a class="btn btn-warning m-t-15 waves-effect" href="{{ action('Admin\CoverOptimizerController@index') }}">
                            <i class="material-icons">keyboard_backspace</i>
                            <span>BACK</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('csss')

@endpush
@push('scripts')
    <script src="{{ asset('vendor') }}/laravel-filemanager/js/stand-alone-button.js"></script>
    <script>
        $(document).ready(function () {
            $('#lfm').filemanager('image');
        });
    </script>
@endpush
