@extends('admin.layouts.master')
@section('title')
ALL TIPS
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Administration > <a href="{{ action('Admin\TipsController@index') }}" class="black-text">Tips</a></h2>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    <strong>All</strong> Tips
                    <a href="{{ action('Admin\TipsController@create') }}" class="btn btn-outline-primary pull-right">+ Add New Tips</a>
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr class="text-center">
                            <th>#</th>
                            <th>TIPS FOR</th>
                            <th>TIPS CONTENT</th>
                            <th>ACTION</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($tips as $sl=>$tip)
                                <tr>
                                    <th scope="row" class="text-center">{{ $sl+1 }}</th>
                                    <td class="text-center">{{ $tip->tips_for }}</td>
                                    <td class="text-center">{{ Str::words($tip->tips_content, '10','(...)') }}</td>
                                    <td class="text-center">
                                        <a class="btn btn-primary btn-circle waves-effect waves-circle waves-float" href="{{ action('Admin\TipsController@edit',$tip->id) }}" title="Edit"><i class="material-icons">edit</i></a>
                                        <form action="{{ action('Admin\TipsController@destroy',$tip->id) }}" id="delete-form-1" method="post" style="display: inline-block">
                                            @csrf
                                            @method('delete')
                                            <button class="btn bg-red btn-circle waves-effect waves-circle waves-float m-l-5" onclick="return makeDeleteRequest(event, 1)" type="submit" title="Delete"><i class="material-icons">delete</i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
@endpush
@push('js')

<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="{{ asset('vendor') }}/datatables/buttons.server-side.js"></script>
@endpush
