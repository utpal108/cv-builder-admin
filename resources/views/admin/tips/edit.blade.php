@extends('admin.layouts.master')
@section('title')
EDIT TIPS
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Administration > <a href="{{ action('Admin\TipsController@index') }}" class="black-text">Tips</a></h2>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Edit Tips</h2>
            </div>
            <div class="body">
                <form method="post" action="{{ action('Admin\TipsController@update',$tip->id) }}" class="ng-untouched ng-pristine ng-valid">
                    @csrf
                    @method('put')

                    <div class="form-group form-float">
                        <div class="form-line input-field">
                            <select class="form-control" name="tips_for">
                                <option value="cover" {{ ($tip->tips_for == 'cover') ? 'selected' : '' }}>Cover</option>
                                <option value="resume" {{ ($tip->tips_for == 'resume') ? 'selected' : '' }}>Resume</option>
                                <option value="both" {{ ($tip->tips_for == 'both') ? 'selected' : '' }}>Both</option>
                            </select>
                            <label class="form-label active">Tips For *</label>
                        </div>
                    </div>

                    <div class="form-group form-float ">
                        <div class="form-line input-field">
                            <textarea name="tips_content" rows="8" class="form-control" required>{{ $tip->tips_content }}</textarea>
                            <label class="form-label active">Tips Content *</label>
                        </div>
                    </div>

                    <br>

                    <button class="btn btn-primary m-t-15 waves-effect" type="submit">
                        <i class="material-icons">update</i>
                        <span>UPDATE</span>
                    </button>
                    <a class="btn btn-warning m-t-15 waves-effect" href="{{ action('Admin\TipsController@index') }}">
                        <i class="material-icons">keyboard_backspace</i>
                        <span>BACK</span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('csss')

@endpush
@push('scripts')
<script src="{{ asset('vendor') }}/laravel-filemanager/js/stand-alone-button.js"></script>
<script>
    $(document).ready(function () {
            $('#lfm').filemanager('image');
        });
</script>
@endpush
