@extends('admin.layouts.master')
@section('title')
    EDIT PREDEFINED TIP
@endsection
@push('css')
{{--    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.2.0/jquery.tinymce.min.js"></script>--}}
{{--<script>tinymce.init({selector:'textarea'});</script>--}}
<script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replaceClass='editor';
</script>
@endpush
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Administration > <a href="{{ action('Admin\PredefinedTipsController@index') }}" class="black-text">Predefined Tips</a></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Edit Predefined Tips</h2>
                </div>
                <div class="body">
                    <form method="post" action="{{ action('Admin\PredefinedTipsController@update',$predefined_tip->id) }}" class="ng-untouched ng-pristine ng-valid">
                        @csrf
                        @method('put')

                        <div class="form-group form-float ">
                            <div class="form-line input-field">
                                <input type="text" name="section_name" class="form-control" value="{{ $predefined_tip->section_name }}" disabled>
                                <label class="form-label">Section Name *</label>
                            </div>
                        </div>

                        <div class="form-group form-float ">
                            <div class="form-line input-field">
                                <textarea name="tips" rows="8" class="form-control editor" required>{{ $predefined_tip->tips }}</textarea>
                                <label class="form-label">Tips *</label>
                            </div>
                        </div>

                        <div class="form-group form-float ">
                            <div class="form-line input-field">
                                <input type="text" name="video_url" class="form-control" value="{{ $predefined_tip->video_url }}" required>
                                <label class="form-label">Video URL *</label>
                            </div>
                        </div>

                        <br>

                        <button class="btn btn-primary m-t-15 waves-effect" type="submit">
                            <i class="material-icons">update</i>
                            <span>UPDATE</span>
                        </button>
                        <a class="btn btn-warning m-t-15 waves-effect" href="{{ action('Admin\PredefinedTipsController@index') }}">
                            <i class="material-icons">keyboard_backspace</i>
                            <span>BACK</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('csss')

@endpush
@push('scripts')

@endpush
