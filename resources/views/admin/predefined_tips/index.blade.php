@extends('admin.layouts.master')
@section('title')
    ALL PREDEFINED SECTION TIPS
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Administration > <a href="{{ action('Admin\PredefinedTipsController@index') }}" class="black-text">Predefined Tips</a></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <strong>All</strong> Predefined Tips
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr class="text-center">
                                    <th>#</th>
                                    <th>SECTION NAME</th>
                                    <th>TIPS</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($predefined_tips as $sl=>$predefined_tip)
                                    <tr>
                                        <th scope="row" class="text-center">{{ $sl+1 }}</th>
                                        <td class="text-center">{{ ucfirst($predefined_tip->section_name) }}</td>
                                        <td class="text-center">{!! Str::words($predefined_tip->tips, '10','(...)') !!}</td>
                                        <td class="text-center">
                                            <a class="btn btn-primary btn-circle waves-effect waves-circle waves-float" href="{{ action('Admin\PredefinedTipsController@edit',$predefined_tip->id) }}" title="Edit"><i class="material-icons">edit</i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
@endpush
@push('js')

    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('vendor') }}/datatables/buttons.server-side.js"></script>
@endpush
