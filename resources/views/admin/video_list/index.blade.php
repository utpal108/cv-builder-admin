@extends('admin.layouts.master')
@section('title')
    ALL Video List
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Administration > <a href="{{ action('Admin\VideoListController@index') }}" class="black-text">Videos</a></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <strong>All</strong> Videos
                        <a href="{{ action('Admin\VideoListController@create') }}" class="btn btn-outline-primary pull-right">+ Add New Video</a>
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr class="text-center">
                                <th>#</th>
                                <th>VIDEO FOR</th>
                                <th>VIDEO TITLE</th>
                                <th>ACTION</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($video_lists as $sl=>$video_list)
                                    <tr>
                                        <th scope="row" class="text-center">{{ $sl+1 }}</th>
                                        <td class="text-center">{{ $video_list->video_for }}</td>
                                        <td class="text-center">{{ Str::words($video_list->video_title, '10','(...)') }}</td>
                                        <td class="text-center">
                                            <a class="btn btn-primary btn-circle waves-effect waves-circle waves-float" href="{{ action('Admin\VideoListController@edit',$video_list->id) }}" title="Edit"><i class="material-icons">edit</i></a>
                                            <form action="{{ action('Admin\VideoListController@destroy',$video_list->id) }}" id="delete-form-1" method="post" style="display: inline-block">
                                                @csrf
                                                @method('delete')
                                                <button class="btn bg-red btn-circle waves-effect waves-circle waves-float m-l-5" onclick="return makeDeleteRequest(event, 1)" type="submit" title="Delete"><i class="material-icons">delete</i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
@endpush
@push('js')

    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('vendor') }}/datatables/buttons.server-side.js"></script>
@endpush
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
