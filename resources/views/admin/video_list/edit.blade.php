@extends('admin.layouts.master')
@section('title')
    EDIT VIDEO
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Administration > <a href="{{ action('Admin\VideoListController@index') }}" class="black-text">Videos</a></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Edit Video</h2>
                </div>
                <div class="body">
                    <form method="post" action="{{ action('Admin\VideoListController@update',$video_list->id) }}" enctype="multipart/form-data" class="ng-untouched ng-pristine ng-valid">
                        @csrf
                        @method('put')

                        <div class="form-group form-float">
                            <div class="form-line input-field">
                                <select class="form-control" name="video_for">
                                    <option value="cover" {{ ($video_list->video_for == 'cover') ? 'selected' : '' }}>Cover</option>
                                    <option value="resume" {{ ($video_list->video_for == 'resume') ? 'selected' : '' }}>Resume</option>
                                    <option value="both" {{ ($video_list->video_for == 'both') ? 'selected' : '' }}>Both</option>
                                </select>
                                <label class="form-label active">Video For *</label>
                            </div>
                        </div>
                        <div class="form-group form-float ">
                            <div class="form-line input-field">
                                <input type="text" class="form-control" name="video_title" value="{{ $video_list->video_title }}" required>
                                <label class="form-label active">Video Title *</label>
                            </div>
                        </div>
                        <div class="form-group form-float ">
                            <div class="form-line input-field">
                                <input type="file" class="form-control" name="tips_video">
                                <label class="form-label active">Upload Video</label>
                            </div>
                        </div>
                        <div class="form-group form-float ">
                            <div class="form-line input-field">
                                <input type="file" class="form-control" name="bg_image">
                                <label class="form-label active">Background Image</label>
                            </div>
                        </div>

                        <br>

                        <button class="btn btn-primary m-t-15 waves-effect" type="submit">
                            <i class="material-icons">update</i>
                            <span>UPDATE</span>
                        </button>
                        <a class="btn btn-warning m-t-15 waves-effect" href="{{ action('Admin\VideoListController@index') }}">
                            <i class="material-icons">keyboard_backspace</i>
                            <span>BACK</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('csss')

@endpush
@push('scripts')
    <script src="{{ asset('vendor') }}/laravel-filemanager/js/stand-alone-button.js"></script>
    <script>
        $(document).ready(function () {
            $('#lfm').filemanager('image');
        });
    </script>
@endpush
