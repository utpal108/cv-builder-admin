<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li>
                <div class="sidebar-profile clearfix">
                    <div class="profile-img">

                        <img src="{{ adminimage(auth()->user()->profile_image) }}" alt="{{ auth()->user()->name }}">
                    </div>
                    <div class="profile-info">
                        <h3>{{ auth()->user()->name }}</h3>
                        <p>Welcome !</p>
                    </div>
                </div>
            </li>
            <li>
                <a href="{{ route('home') }}">
                    <i class="menu-icon ti-home"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            {{-- For Predefined Content --}}
            <li>
                <a href="#" onClick="return false;" class="menu-toggle">
                    <i class="menu-icon ti-user"></i>
                    <span>Predefined Contents</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{ action('Admin\CoverSampleController@index') }}">Cover Letter</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\PredefinedSkillContentController@index') }}">Skill</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\PredefinedSoftSkillContentController@index') }}">Soft Skill</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\PredefinedEducationContentController@index') }}">Study Programs</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\PredefinedAchievementController@index') }}">Achievement</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\PredefinedAwardController@index') }}">Award</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\PredefinedCauseController@index') }}">Cause</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\PredefinedCertificateController@index') }}">Certificate</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\PredefinedConferenceController@index') }}">Conference</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\PredefinedInterestController@index') }}">Interest</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\PredefinedLanguageController@index') }}">Language</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\PredefinedOrganizationController@index') }}">Organization</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\PredefinedProjectController@index') }}">Project</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\PredefinedPublicationController@index') }}">Publication</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\PredefinedTeachingController@index') }}">Teaching</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\PredefinedTechnicalSkillController@index') }}">Technical Skill</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\PredefinedVolunteerController@index') }}">Volunteer</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\PredefinedWorkContentController@index') }}">Work</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\PredefinedSummaryContentController@index') }}">Summary</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#" onClick="return false;" class="menu-toggle">
                    <i class="menu-icon ti-user"></i>
                    <span>Predefined Tips</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{ action('Admin\PredefinedTipsController@index') }}">All Tips</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#" onClick="return false;" class="menu-toggle">
                    <i class="menu-icon ti-user"></i>
                    <span>Administration</span>
                </a>
                <ul class="ml-menu">
                    <li>
                        <a href="{{ route('users.index') }}">Users</a>
                    </li>
                    <li>
                        <a href="{{ route('roles.index') }}">Roles</a>
                    </li>
                    <li>
                        <a href="{{ route('permissions.index') }}">Permissions</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\TipsController@index') }}">Tips</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\VideoListController@index') }}">Video Lists</a>
                    </li>
                    <li>
                        <a href="{{ action('Admin\CoverOptimizerController@index') }}">Cover Letter Optimizers</a>
                    </li>
                </ul>
            </li>


            <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="menu-icon ti-power-off"></i>
                    <span>Logout</span>
                </a>
            </li>

        </ul>
    </div>
    <!-- #Menu -->
</aside>
<!-- #END# Left Sidebar -->
