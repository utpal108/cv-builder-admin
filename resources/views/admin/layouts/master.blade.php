<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>{{ config('app.name') }} || @yield('title')</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset('ic_admin')}}/images/favicon.ico" type="image/x-icon">

    <link href="{{asset('ic_admin')}}/js/bundles/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css"
        rel="stylesheet" />

    <!-- Multi Select Css -->
    <link href="{{asset('ic_admin')}}/js/bundles/multiselect/css/multi-select.css" rel="stylesheet">

    <!-- Plugins Core Css -->
    <link href="{{asset('ic_admin')}}/css/app.min.css" rel="stylesheet">

    <!-- Form Related Css -->
    <link href="{{asset('ic_admin')}}/css/form.min.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="{{asset('ic_admin')}}/css/style.css" rel="stylesheet">

    <!-- You can choose a theme from css/styles instead of get all themes -->
    <link href="{{asset('ic_admin')}}/css/theme/all-themes.css" rel="stylesheet">

    <link href="{{asset('ic_admin')}}/js/bundles/rangeSlider/css/ion.rangeSlider.css" rel="stylesheet">
    <link href="{{asset('ic_admin')}}/js/bundles/rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">

    <!-- Light Gallery Css -->
    <link href="{{asset('ic_admin')}}/js/bundles/lightgallery/dist/css/lightgallery.min.css" rel="stylesheet">
    @stack('css')
    <link href="{{asset('css')}}/custom.css" rel="stylesheet">

</head>

<body class="light submenu-closed menu_dark theme-black logo-black">
    <div id="app">
        <input type="hidden" name="urlshort" id="urlshort" value="/">
        <div id="main-component">
            @include('admin.layouts.header')
            <div>
                @include('admin.layouts.sidebar')
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                @include('flash::message')
                @yield('content')
            </div>
        </section>
    </div>

    <script src="{{asset('ic_admin')}}/js/app.min.js"></script>
    <script src="{{asset('ic_admin')}}/js/table.min.js"></script>
    <script src="{{asset('ic_admin')}}/js/chart.min.js"></script>
    <script src="{{asset('ic_admin')}}/js/admin.js"></script>
    <script src="{{asset('ic_admin')}}/js/bundles/export-tables/pdfmake.min.js"></script>
    <script src="{{asset('ic_admin')}}/js/bundles/datamaps/d3.min.js"></script>
    <script src="{{asset('ic_admin')}}/js/bundles/echart/echarts.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    @stack('js')
</body>

</html>