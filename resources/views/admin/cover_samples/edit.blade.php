@extends('admin.layouts.master')
@section('title')
    EDIT CODER SAMPLE
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Administration > <a href="{{ action('Admin\CoverSampleController@index') }}" class="black-text">Cover Sample</a></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Edit Cover Sample</h2>
                </div>
                <div class="body">
                    <form method="post" action="{{ action('Admin\CoverSampleController@update',['id'=>$cover_sample->id]) }}"
                          enctype="multipart/form-data" class="ng-untouched ng-pristine ng-valid">
                        @csrf
                        @method('put')

                        <div class="form-group form-float">
                            <div class="form-line input-field">
                                <select name="type" class="form-control">
                                    <option value="general" {{ ($cover_sample->type === 'general') ? 'selected' : ''  }}>General</option>
                                    <option value="special case" {{ ($cover_sample->type === 'special case') ? 'selected' : ''  }}>Special Case</option>
                                    <option value="post interview" {{ ($cover_sample->type === 'post interview') ? 'selected' : ''  }}>Post Interview</option>
                                </select>
                                <label class="form-label active">Type *</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line input-field">
                                <input type="text" name="job_title" value="{{ $cover_sample->job_title }}" required id="title" class="form-control">
                                <label class="form-label active">Job Title *</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line input-field">
                                <input type="text" name="cover_title" value="{{ $cover_sample->cover_title }}" required id="title" class="form-control">
                                <label class="form-label active">Cover Title *</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line input-field">
                                <textarea name="cover_details" class="form-control" rows="4" required>{!! $cover_sample->cover_details !!}</textarea>
                                <label class="form-label active">Details *</label>
                            </div>
                        </div>

                        <br>

                        <button class="btn btn-primary m-t-15 waves-effect" type="submit">
                            <i class="material-icons">update</i>
                            <span>UPDATE</span>
                        </button>
                        <a class="btn btn-warning m-t-15 waves-effect" href="{{ action('Admin\CoverSampleController@index') }}">
                            <i class="material-icons">keyboard_backspace</i>
                            <span>BACK</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('csss')

@endpush
@push('scripts')
    <script src="{{ asset('vendor') }}/laravel-filemanager/js/stand-alone-button.js"></script>
    <script>
        $(document).ready(function () {
            $('#lfm').filemanager('image');
        });
    </script>
@endpush
